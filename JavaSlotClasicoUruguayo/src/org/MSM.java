package org;

import javax.swing.JFrame;

import org.microuy.javaslot.comm.CommLayer;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.constante.SistemaFrontend;
import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.persistencia.util.DBCreator;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.modulo.SalidaEstandarSluy;
import org.microuy.javaslot.util.MaquinaPoolManager;
import org.microuy.javaslot.webapp.server.SlotsWebServer;
import org.newdawn.slick.CanvasGameContainer;

public class MSM extends JFrame {

	public CommLayer commLayer;
	private SlotsWebServer webServer;
	private CanvasGameContainer container;
	
	public MSM() {
		
		super();
		try {
			
			//creamos la salida estandar 
			SalidaEstandarSluy salidaEstandarSluy = null;
			if(Sistema.SALIDA_ESTANDAR_UI) {
				salidaEstandarSluy = new SalidaEstandarSluy();	
			}

			//inicializamos la base de datos
			DBCreator.verificarYCrearDB();
			
			//creamos una maquina con una configuracion
			Maquina maquina = MaquinaPoolManager.obtenerInstanciaMaquinaJuego(Sistema.configuracionMaquina);
			
			//iniciamos el servidor web si es necesario
			if(Sistema.WEBSERVER_HABILITADO) {
				webServer = new SlotsWebServer();
			}
			
			//obtengo un canal de comunicacion con el hardware
			commLayer = CommLayer.getDefaultInstance();

			//si la interfaz visual esta habilitada 
			//iniciamos el sistema grafico
			if(Sistema.JUEGO_HABILITADO) {
			
				container = new CanvasGameContainer(new Principal(this, maquina, salidaEstandarSluy), false);
				add(container);
				
				if(SistemaFrontend.FULLSCREEN) {
					setUndecorated(true);	
				}
				else {
					setUndecorated(false);
				}
				
				setVisible(true);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		try {

			if(args != null && args.length > 0) {
				
				//recorremos la lista de argumentos
				for(String argumentoActual : args) {
					if(argumentoActual.toLowerCase().startsWith("com") || argumentoActual.toLowerCase().startsWith("/dev/tty")) {
						System.setProperty("slot_port_name", argumentoActual);	
					}
					else if(argumentoActual.toLowerCase().startsWith("--fullscreen")) {
						SistemaFrontend.FULLSCREEN = true;
					}
					else if(argumentoActual.toLowerCase().startsWith("--windowed")) {
						SistemaFrontend.FULLSCREEN = false;
					}
				}
			}

			new MSM();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}