package org.microuy.javaslot.textoAnimado.efectos;

import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

public class TextoItineranteEffect {

	private int x;
	private int y;
	
	private UnicodeFont font;
	public String texto;
	private int anchoTexto;
	private int altoTexto;
	private int posicionTextoX;
	private int posicionTextoY;
	public int modificadorX;
	public int modificadorY;
	private Timer timer;
	private long tiempo;
	protected boolean corriendo;
	private boolean mantenerRenderizado;
	private Color colorFondo;

	public TextoItineranteEffect(UnicodeFont font, String texto, int screenWidth, int screenHeight, Color colorFondo) {

		//configuramos x,y como el
		//centro de la pantalla
		this.x = screenWidth/2;
		this.y = screenHeight/2;
		this.font = font;
		this.texto = texto;
		this.colorFondo = colorFondo;
		
		anchoTexto = font.getWidth(texto);
		altoTexto = font.getHeight(texto);
		posicionTextoX = x - (anchoTexto / 2);
		posicionTextoY = y - (altoTexto / 2);
		
	}
	
	public TextoItineranteEffect(UnicodeFont font, String texto, int x, int y, int tiempo) {
		
		this.font = font;
		this.texto = texto;
		this.x = x;
		this.y = y;
		posicionTextoX = x;
		posicionTextoY = y;
		this.tiempo = tiempo;
		
		if(tiempo != 0) {
			timer = new Timer(tiempo);
		}
	}
	
	public void render(Graphics g) {
		
		if(corriendo || mantenerRenderizado) {
			
			//renderizamos el fondo 
			//si es necesario
			if(colorFondo != null) {
				Color colorPrevio = g.getColor();
				g.setColor(colorFondo);
				g.fillRoundRect(posicionTextoX + modificadorX - 10, posicionTextoY + modificadorX, anchoTexto + 20, altoTexto + 10, 25);
				g.setColor(colorPrevio);
			}
			
			//renderizamos el texto
			font.drawString(posicionTextoX + modificadorX, posicionTextoY + modificadorY, texto);
			
		}
	}
	
	public void update(long elapsedTime) {
		
		//si se ha cumplido el tiempo
		//finalizamos la animacion
		if(tiempo != 0) {
			if(timer.action(elapsedTime)) {
				corriendo = false;
			}
		}
	}
	
	/**
	 * Inicia la aplicacion
	 */
	public void iniciar() {
		corriendo = true;
	}
	
	public void reiniciar() {
		timer = new Timer((int)tiempo);
		corriendo = true;
	}
	
	public void reiniciarCentrado(long tiempo, String texto) {
		
		cambiarTexto(true, texto);
		this.tiempo = tiempo;
		timer = new Timer((int)tiempo);
		corriendo = true;
		modificadorX = 0;
	}
	
	public void finalizar() {
		corriendo = false;
	}
	
	public void setMantenerRenderizado(boolean mantenerRenderizado) {
		this.mantenerRenderizado = mantenerRenderizado;
	}
	
	public void cambiarTexto(boolean centrado, String texto) {
		
		if(centrado) {
			anchoTexto = font.getWidth(texto);
			altoTexto = font.getHeight(texto);
			posicionTextoX = x - (anchoTexto / 2);
			posicionTextoY = y - (altoTexto / 2);
		}
		
		this.texto = texto;
	}
}