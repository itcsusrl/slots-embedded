package org.microuy.javaslot.sonido;

import java.util.HashMap;
import java.util.Iterator;

import org.microuy.javaslot.constante.SistemaFrontend;
import org.microuy.javaslot.expepcion.SystemException;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

public class ReproductorSonido {

	public static final int JUGADA_GANADORA_INTERMITENCIA = 1;
	public static final int RODILLO_DETIENE = 2;
	public static final int RODILLO_GIRANDO = 3;
	public static final int DINERO_GANADO = 4;
	public static final int PITIDO = 5;
	public static final int BONUS = 6;
	public static final int SCATTER = 7;
	public static final int CLICK = 8;
	public static final int YEAH = 9;
	public static final int ENTRADA_MONEDA = 10;
	
	public static final int MUSICA_PRESENTACION = 1;
	public static final int MUSICA_GAME_OVER = 2;
	public static final int MUSICA_MINIJUEGO = 3;
	
	private static HashMap<Integer, Sound> sonidos = new HashMap<Integer, Sound>();
	private static HashMap<Integer, Music> musica = new HashMap<Integer, Music>();
	
	public static void playSound(int id) {
		
		if(SistemaFrontend.AUDIO_FX_ENABLED) {
			Sound sonido = sonidos.get(id);
			if(sonido != null) {
				sonido.play();
			}
		}
	}

	public synchronized static void playMusic(int id, int fadein, boolean cancela, float volumen) {

		if(SistemaFrontend.AUDIO_FX_ENABLED) {
			
			Music tema = musica.get(id);
			if(tema != null) {
			
				if(tema.playing()) {
					//si el tema que esta reproduciendose
					//es el mismo que nos piden poner, 
					//dejamos sonando el que esta y listo
				}
				else {
					
					//si se desea cancelar el 
					//tema actual se hace
					if(cancela) {
						
						//buscamos si hay algun tema
						//sonando para hacer un swap
						Iterator<Music> temasIt = musica.values().iterator();
						while(temasIt.hasNext()) {
							Music temaRecorrido = temasIt.next();
							if(temaRecorrido.playing()) {
								temaRecorrido.fade(4000, 0, true);
							}
						}
					}
					
					//hacemos un fadein del  
					//tema que nos pidieron
					//tema.fade(fadein, 1, false); --> TODO INVESTIGAR COMO FUNCIONA, QUEDARIA BUENO UN FADEIN
					tema.play();
					tema.setVolume(volumen);
				}
			}
		}
	}
	
	public synchronized static void playMusic(int id, int fadein, boolean cancela) {
		playMusic(id, fadein, cancela, 1f);
	}
	
	public static void loopSound(int id) {
		
		if(SistemaFrontend.AUDIO_FX_ENABLED) {
			Sound sonido = sonidos.get(id);
			if(sonido != null) {
				sonido.loop();
			}
		}
	}

	public static void loopMusic(int id) {
		
		if(SistemaFrontend.AUDIO_FX_ENABLED) {
			Music tema = musica.get(id);
			if(tema != null) {
				tema.loop();
			}
		}
	}
	
	public static void loopMusic(int id, float volume) {
		
		if(SistemaFrontend.AUDIO_FX_ENABLED) {
			Music tema = musica.get(id);
			if(tema != null) {
				tema.loop();
				tema.setVolume(volume);
			}
		}
	}
	
	public static void stopSound(int id) {

		if(SistemaFrontend.AUDIO_FX_ENABLED) {
			Sound sonido = sonidos.get(id);
			if(sonido != null) {
				sonido.stop();
			}
		}
	}
	
	public static void stopMusic(int id, boolean fadeout) {

		if(SistemaFrontend.AUDIO_FX_ENABLED) {
			Music tema = musica.get(id);
			if(tema != null) {
				if(tema.playing()) {
					if(fadeout) {
						tema.fade(3000, 0, true);
					}
					else {
						tema.stop();
					}
				}
			}
		}
	}
	
	public static void stopAllMusic(boolean fadeout) {
		
		if(SistemaFrontend.AUDIO_FX_ENABLED) {
			Iterator<Music> musicaIt = musica.values().iterator();
			while(musicaIt.hasNext()) {
				Music music = musicaIt.next();
				if(fadeout) {
					music.fade(3000, 0, true);	
				}
				else {
					music.stop();
				}
			}
		}
	}
	
	public static void cargarSonidos(int[] sonidosId) {
		
		if(SistemaFrontend.AUDIO_FX_ENABLED) {
			
			//recorremos la lista de 
			//id's de sonidos recibidos
			for(int idActual : sonidosId) {
				
				//si el sonido no existe
				//lo cargamos en el hash
				if(sonidos.get(idActual) == null) {
					try {
						
						//lo cargamos en el hash
						sonidos.put(idActual, new Sound(obtenerPathSonidoPorId(idActual)));
						
					} catch (SlickException e) {
						e.printStackTrace();
						throw new SystemException(e.getMessage());
					}
				}
			}
		}
	}
	
	public static void descargarSonidos(int[] sonidosId) {
		
		if(SistemaFrontend.AUDIO_FX_ENABLED) {

			//recorremos la lista de 
			//id's de sonidos recibidos
			for(int idActual : sonidosId) {
				
				//si el sonido no existe
				//lo descargamos del hash
				if(sonidos.get(idActual) != null) {
					sonidos.remove(idActual);
				}
			}	
		}
	}
	
	public static String obtenerPathSonidoPorId(int id) {
	
		String path = null;
		
		switch(id) {
		case JUGADA_GANADORA_INTERMITENCIA:
			path = "resources/sounds/jugada_ganadora_intermitencia.wav";
			break;
		case RODILLO_DETIENE:
			path = "resources/sounds/rodillo_detiene.wav";			
			break;
		case RODILLO_GIRANDO:
			path = "resources/sounds/rodillos_girando.wav";
			break;
		case DINERO_GANADO:
			path = "resources/sounds/dinero_ganado.wav";
			break;
		case PITIDO:
			path = "resources/sounds/pitido.wav";
			break;
		case BONUS:
			path = "resources/sounds/bonus.wav";
			break;
		case CLICK:
			path = "resources/sounds/click.wav";
			break;
		case SCATTER:
			path = "resources/sounds/scatter.wav";
			break;
		case YEAH:
			path = "resources/sounds/yeah.wav";
			break;
		case ENTRADA_MONEDA:
			path = "resources/sounds/entrada_moneda.wav";
			break;
		}
		
		return path;
	}
	
	
	public static void cargarMusica(int[] musicaId) {
		
		if(SistemaFrontend.AUDIO_FX_ENABLED) {
			
			//recorremos la lista de 
			//id's de sonidos recibidos
			for(int idActual : musicaId) {
				
				//si el sonido no existe
				//lo cargamos en el hash
				if(musica.get(idActual) == null) {
					try {
						
						//lo cargamos en el hash
						musica.put(idActual, new Music(obtenerPathMusicaPorId(idActual)));
						
					} catch (SlickException e) {
						e.printStackTrace();
						throw new SystemException(e.getMessage());
					}
				}
			}
		}
	}
	
	public static void descargarMusica(int[] musicaId) {
		
		if(SistemaFrontend.AUDIO_FX_ENABLED) {

			//recorremos la lista de 
			//id's de sonidos recibidos
			for(int idActual : musicaId) {
				
				//si el sonido no existe
				//lo descargamos del hash
				if(musica.get(idActual) != null) {
					musica.remove(idActual);
				}
			}	
		}
	}
	
	public static String obtenerPathMusicaPorId(int id) {
	
		String path = null;
		
		switch(id) {
		case MUSICA_PRESENTACION:
			path = "resources/musica/Cielo_de_un_solo_color_Mix_Down.wav";
			break;
		case MUSICA_GAME_OVER:
			path = "resources/musica/Mix_Down_game_over.wav";
			break;
		case MUSICA_MINIJUEGO:
			path = "resources/musica/musica_minijuego.wav";
			break;
		}
		
		return path;
	}
	
	/**
	 * En caso de que este sonando musica
	 * o algun sonido, se detiene inmediatamente
	 */
	public static void detenerAudio() {
		
		Iterator<Sound> sonidosIt = sonidos.values().iterator();
		while(sonidosIt.hasNext()) {
			Sound sound = sonidosIt.next();
			sound.stop();
		}

		Iterator<Music> musicaIt = musica.values().iterator();
		while(musicaIt.hasNext()) {
			Music music = musicaIt.next();
			music.stop();
		}
	}
}