package org.microuy.javaslot.presentacion.event;

public interface RodillosListener {

	/**
	 * El conjunto de rodillos en si generan este tipo 
	 * de eventos durante sus cambios de estado.
	 * 
	 * @param rodillosEvent
	 */
	public void procesarEventoRodillos(RodillosEvent rodillosEvent);
	
}
