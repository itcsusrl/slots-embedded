package org.microuy.javaslot.presentacion.event;

public class RodillosEvent {

	public static byte TIPO_EVENTO_PARADO = 0;
	public static byte TIPO_EVENTO_GIRANDO = 1;
	public static byte TIPO_EVENTO_PARANDO = 2;
	
	private byte tipoEvento;

	public byte getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(byte tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
}