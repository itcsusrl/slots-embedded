package org.microuy.javaslot.presentacion.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.microuy.javaslot.expepcion.SystemException;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class FiguraUtil {

	private static Map<Integer, String> rutaImagenes = new HashMap<Integer, String>();
	private static Map<Integer, SlotsImage> imagenes = new HashMap<Integer, SlotsImage>();
	private static Map<Integer, SlotsAnimation> animaciones = new HashMap<Integer, SlotsAnimation>();

	/**
	 * Cargamos todos los recursos graficos
	 * relacionados a las figuras de la 
	 * maquina. Entre recursos consideramos
	 * las imagenes estaticas y las animaciones
	 * con todos sus respectivos cuadros que
	 * las conforman.
	 */
	static {
		
		try {

			//scatter
			rutaImagenes.put(0, "resources/figuras/figuras-08.png");
			//bonus
			rutaImagenes.put(99, "resources/figuras/figuras-14.png");
			//wild
			rutaImagenes.put(100, "resources/figuras/figuras-13.png");
			//jackpot
			rutaImagenes.put(500, "resources/figuras/figuras-10.png");
			//bonus menor mayor
			rutaImagenes.put(800, "resources/figuras/figuras-02.png");

			rutaImagenes.put(1, "resources/figuras/figuras-01.png");
			rutaImagenes.put(3, "resources/figuras/figuras-03.png");
			rutaImagenes.put(4, "resources/figuras/figuras-04.png");
			rutaImagenes.put(5, "resources/figuras/figuras-05.png");
			rutaImagenes.put(6, "resources/figuras/figuras-06.png");
			rutaImagenes.put(7, "resources/figuras/figuras-07.png");
			rutaImagenes.put(9, "resources/figuras/figuras-09.png");
			rutaImagenes.put(11, "resources/figuras/figuras-11.png");
			rutaImagenes.put(12, "resources/figuras/figuras-12.png");
			
			/**
			 * Cargamos imagenes
			 */
			Iterator<Integer> ids = rutaImagenes.keySet().iterator();
			while(ids.hasNext()) {
				
				//obtenemos el id de
				//figura actual
				Integer idFiguraActual = ids.next();
				
				//obtenemos el string correspondiente
				//a la ruta de la figura actual
				String rutaFiguraActual = rutaImagenes.get(idFiguraActual);
				
				/**
				 * Cargamos la imagen actual
				 */
				imagenes.put(idFiguraActual, new SlotsImage(idFiguraActual, rutaFiguraActual));
				
			}
			
			/**
			 * Cargamos animaciones
			 */
			animaciones.put(0, new SlotsAnimation(0, "resources/figuras/figuras-08.png", new Image[]{new Image("resources/figuras/figuras-08.png")}, 1));
			animaciones.put(99, new SlotsAnimation(99, "resources/figuras/figuras-14.png", new Image[]{new Image("resources/figuras/figuras-14.png")}, 1));
			animaciones.put(100, new SlotsAnimation(100, "resources/figuras/figuras-13.png", new Image[]{new Image("resources/figuras/figuras-13.png")}, 1));
			animaciones.put(500, new SlotsAnimation(500, "resources/figuras/figuras-10.png", new Image[]{new Image("resources/figuras/figuras-10.png")}, 1));
			animaciones.put(800, new SlotsAnimation(800, "resources/figuras/figuras-02.png", new Image[]{new Image("resources/figuras/figuras-02.png")}, 1));

			animaciones.put(1, new SlotsAnimation(1, "resources/figuras/figuras-01.png", new Image[]{new Image("resources/figuras/figuras-01.png")}, 1));
			animaciones.put(3, new SlotsAnimation(3, "resources/figuras/figuras-03.png", new Image[]{new Image("resources/figuras/figuras-03.png")}, 1));
			animaciones.put(4, new SlotsAnimation(4, "resources/figuras/figuras-04.png", new Image[]{new Image("resources/figuras/figuras-04.png")}, 1));
			animaciones.put(5, new SlotsAnimation(5, "resources/figuras/figuras-05.png", new Image[]{new Image("resources/figuras/figuras-05.png")}, 1));
			animaciones.put(6, new SlotsAnimation(6, "resources/figuras/figuras-06.png", new Image[]{new Image("resources/figuras/figuras-06.png")}, 1));
			animaciones.put(7, new SlotsAnimation(7, "resources/figuras/figuras-07.png", new Image[]{new Image("resources/figuras/figuras-07.png")}, 1));
			animaciones.put(9, new SlotsAnimation(9, "resources/figuras/figuras-09.png", new Image[]{new Image("resources/figuras/figuras-09.png")}, 1));
			animaciones.put(11, new SlotsAnimation(11, "resources/figuras/figuras-11.png", new Image[]{new Image("resources/figuras/figuras-11.png")}, 1));
			animaciones.put(12, new SlotsAnimation(12, "resources/figuras/figuras-12.png", new Image[]{new Image("resources/figuras/figuras-12.png")}, 1));
			
		} catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}
	}

	public static SlotsAnimation obtenerAnimacionFiguraPorId(int id) {
		return animaciones.get(id);
	}

	public static SlotsImage obtenerImagenFiguraPorId(int id) {
		return imagenes.get(id);
	}
	
	public static String obtenerRutaImagenFiguraPorId(int id) {
		return rutaImagenes.get(id);
	}
}