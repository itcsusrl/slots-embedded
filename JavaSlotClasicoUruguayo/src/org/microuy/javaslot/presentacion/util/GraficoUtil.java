package org.microuy.javaslot.presentacion.util;

import org.lwjgl.opengl.GLContext;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.opengl.pbuffer.FBOGraphics;
import org.newdawn.slick.opengl.pbuffer.PBufferGraphics;

public class GraficoUtil {

	/**
	 * Devuelve la instancia de un contexto 
	 * @return
	 * @throws SlickException 
	 */
	public static Graphics obtenerContextoGraficoSoportado(Image image) throws SlickException {
		
		Graphics graphics = null;
		
		//chequeamos si se soporta FBO
		boolean soportaFBO = GLContext.getCapabilities().GL_EXT_framebuffer_object;
		boolean soportaPBuffer = GLContext.getCapabilities().GL_EXT_pixel_buffer_object;
		
		if(soportaFBO) {
			graphics = new FBOGraphics(image);
		}
		else if(soportaPBuffer) {
			graphics = new PBufferGraphics(image);
		}
		else {
			graphics = image.getGraphics();
		}
		
		return graphics;
	}
}