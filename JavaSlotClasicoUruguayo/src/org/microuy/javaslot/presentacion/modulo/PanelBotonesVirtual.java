package org.microuy.javaslot.presentacion.modulo;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.constante.SistemaFrontend;
import org.microuy.javaslot.expepcion.SystemException;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.dominio.EstadoBoton;
import org.microuy.javaslot.presentacion.pantalla.Juego;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class PanelBotonesVirtual extends ModuloGenerico {

	public static final byte ESTADO_PANEL_VISIBLE = 0;
	public static final byte ESTADO_PANEL_APARECIENDO = 1;
	public static final byte ESTADO_PANEL_ESCONDIENDO = 2;
	public static final byte ESTADO_PANEL_INVISIBLE = 3;

	private final int TIEMPO_ANIMACION_NORMAL = 1000;
	private final int TIEMPO_ANIMACION_RAPIDO = 500;

	// estado de la botonera
	public byte estado;

	// timer usado por la animacion
	// de ocultar y mostrar botonera
	private Timer timerAnimacion;

	// guarda cual es la altura
	// final del panel configurada
	private int alturaFinal;

	// lock para los botones
	private List<BotonVirtual> botonesVirtuales;

	// modifica la altura de lo que
	// se dibuja en esta clase y en
	// el modulo boton virtual
	private int modificadorAltura;

	// color de fondo de la botonera
	private Color colorFondo;

	// marco azul
	private Image marcoImage;

	public PanelBotonesVirtual(Principal principal, int x, int y, int width, int height) {

		// a la posicion 'y' se le suma
		// la altura para que por
		// defecto sea invisible
		super(null, x, y + height, width, height);

		this.principal = principal;
		
		// estado inicial de la
		// botonera es invisible
		estado = ESTADO_PANEL_INVISIBLE;

		// seteamos un valor importante
		// para el proceso de animacion
		alturaFinal = height;

		// para que aparezca desaparecido
		// es necesario cambiar el modificar
		// de altura por la altura total
		modificadorAltura = alturaFinal;

		// creo la lista de botones virtuales
		// con los que se dibuja en pantalla
		botonesVirtuales = new ArrayList<BotonVirtual>();

		// inicializo la lista
		botonesVirtuales.add(new BotonVirtual(new EstadoBoton(Boton.BOTON1, "",
				false), x + 10, y + 5 + modificadorAltura, 150, height - 10));
		botonesVirtuales.add(new BotonVirtual(new EstadoBoton(Boton.BOTON2, "",
				false), x + 180, y + 5 + modificadorAltura, 150, height - 10));
		botonesVirtuales.add(new BotonVirtual(new EstadoBoton(Boton.BOTON3, "",
				false), x + 350, y + 5 + modificadorAltura, 150, height - 10));
		botonesVirtuales.add(new BotonVirtual(new EstadoBoton(Boton.BOTON4, "",
				false), x + 520, y + 5 + modificadorAltura, 150, height - 10));
		botonesVirtuales.add(new BotonVirtual(new EstadoBoton(Boton.BOTON5, "",
				false), x + 690, y + 5 + modificadorAltura, 150, height - 10));
		botonesVirtuales.add(new BotonVirtual(new EstadoBoton(Boton.BOTON6, "",
				false), x + 860, y + 5 + modificadorAltura, 150, height - 10));

		// tiempo de ejecucion de la animacion
		timerAnimacion = new Timer(0);

		// creamos el color de fondo
		colorFondo = new Color(0, 0, 0, 150);

	}

	public void render(Graphics g) {

		if (estado != ESTADO_PANEL_INVISIBLE) {

			Color colorPrevio = null;

			if (Principal.modoConfiguracion) {

				// obtenemos el color anterior
				// y configuramos el nuevo
				colorPrevio = g.getColor();
				
				// pintamos el fondo del
				// color especificado
				if (colorFondo != null) {

					// seteamos el color de fondo
					g.setColor(colorFondo);

					// y rellenamos un rectangulo
					g.fillRect(x, y + modificadorAltura, width, height);

				}

				// color del recuadro
				g.setColor(Color.red);

				// dibujamos un rectangulo marcando
				// los limites de la botonera virtual
				g.drawRect(x, y + modificadorAltura, width, height);

			} 
			else {
				// marco azul de fondo
				marcoImage.draw(0, SistemaFrontend.SCREEN_HEIGTH - marcoImage.getHeight());
			}

			//evaluamos si la maquina se encuentra
			//en modo demo para definir si se muestra
			//la botonera o no
			boolean demo = false;
			if(principal.getCurrentStateID() == Pantalla.PANTALLA_AYUDA_DESCRIPTIVA || 
			   principal.getCurrentStateID() == Pantalla.PANTALLA_AYUDA_PREMIOS_NORMALES ||
			   principal.getCurrentStateID() == Pantalla.PANTALLA_JUEGO ||
			   principal.getCurrentStateID() == Pantalla.PANTALLA_FICHA_PERSONAJE) {
				
				//seteamos flag
				demo = principal.demo;
			}
			
			//si no es demo, dibujamos
			//los botones normalmente
			if(!demo) {
				// recorremos la lista de botones
				// virtuales para dibujarlos
				for (BotonVirtual botonVirtual : botonesVirtuales) {
					botonVirtual.setModificadorAltura(modificadorAltura);
					botonVirtual.renderizar(g);
				}
			}

			if (Principal.modoConfiguracion) {
				// volvemos al color anterior
				g.setColor(colorPrevio);
			}
		}
	}

	public void update(long elapsedTime) {

		// recorremos la lista de botones
		// virtuales para ejecutar la logica
		for (BotonVirtual botonVirtual : botonesVirtuales) {
			botonVirtual.actualizar(elapsedTime);
		}

		// si nose encontramos apareciendo
		// o desapareciendo la animacion
		if (estado == ESTADO_PANEL_APARECIENDO
				|| estado == ESTADO_PANEL_ESCONDIENDO) {

			// chequeamos si el timer ya ha
			// comenzado a contar el tiempo
			if (!timerAnimacion.isActive()) {
				// empezamos a contar
				timerAnimacion.setActive(true);
			}

			// termino el tiempo del timer
			if (timerAnimacion.action(elapsedTime)) {
				if (estado == ESTADO_PANEL_APARECIENDO) {
					modificadorAltura = -alturaFinal;
					estado = ESTADO_PANEL_VISIBLE;
				} else if (estado == ESTADO_PANEL_ESCONDIENDO) {
					modificadorAltura = alturaFinal;
					estado = ESTADO_PANEL_INVISIBLE;
				}
			} else {

				// vamos ejecutando la animacion
				int valorActual = (int) ((timerAnimacion.getCurrentTick() * alturaFinal) / timerAnimacion
						.getDelay());

				if (estado == ESTADO_PANEL_APARECIENDO) {
					modificadorAltura = -valorActual;
				} else if (estado == ESTADO_PANEL_ESCONDIENDO) {
					modificadorAltura = -alturaFinal + valorActual;
				}
			}
		}
	}

	public void inicializarRecursos() {
		try {
			marcoImage = new Image("resources/marco-15.png");
		} catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}
	}

	/**
	 * Muestra la botonera usando una animacion de desplazamiento
	 */
	public void mostrar(int velocidad) {

		if (estado == ESTADO_PANEL_INVISIBLE
				|| estado == ESTADO_PANEL_ESCONDIENDO) {

			// reseteamos el timer de
			// la animacion
			timerAnimacion.setActive(false);
			timerAnimacion.setDelay(velocidad);
			timerAnimacion.setCurrentTick(0);

			// el panel debe de empezar
			// a aparecer en la pantalla
			estado = ESTADO_PANEL_APARECIENDO;

			for (BotonVirtual botonVirtual : botonesVirtuales) {
				botonVirtual.posicionAnimacion = 0;
				botonVirtual.ultimoRecorridoPx = 0;
			}
		}
	}

	public void mostrar() {
		mostrar(TIEMPO_ANIMACION_NORMAL);
	}

	public void mostrarRapido() {
		mostrar(TIEMPO_ANIMACION_RAPIDO);
	}

	/**
	 * Oculta la botonera usando una animacion de desplazamiento
	 */
	public void ocultar(int velocidad) {

		if (estado == ESTADO_PANEL_VISIBLE
				|| estado == ESTADO_PANEL_APARECIENDO) {

			// reseteamos el timer de
			// la animacion
			timerAnimacion.setActive(false);
			timerAnimacion.setDelay(velocidad);
			timerAnimacion.setCurrentTick(0);

			// el panel debe de empezar
			// a aparecer en la pantalla
			estado = ESTADO_PANEL_ESCONDIENDO;

		}
	}

	public void ocultar() {
		ocultar(TIEMPO_ANIMACION_NORMAL);
	}

	public void ocultarRapido() {
		ocultar(TIEMPO_ANIMACION_RAPIDO);
	}

	public void configurarEstadoBotones(List<EstadoBoton> estadoBotones) {

		// recorremos la lista de
		// botones virtuales
		for (BotonVirtual botonVirtual : botonesVirtuales) {

			// flag
			boolean encontrado = false;

			// recorremos la lista de botones recibidos
			for (EstadoBoton estadoBoton : estadoBotones) {

				// si estamos en el mismo boton
				// tanto en el estado como en el
				// boton virtual
				if (estadoBoton.getBotonId() == botonVirtual.getEstadoBoton().getBotonId()) {

					// seteamos los campos
					// correspondientes al
					// boton virtual
					botonVirtual.setActivado(estadoBoton.isActivado());
					botonVirtual.setTexto(estadoBoton.getTexto());

					// seteamos flag
					encontrado = true;
				}
			}

			if (!encontrado) {
				botonVirtual.setActivado(false);
			}
		}
	}

	public List<BotonVirtual> getBotonesVirtuales() {
		return botonesVirtuales;
	}

	public void setBotonesVirtuales(List<BotonVirtual> botonesVirtuales) {
		this.botonesVirtuales = botonesVirtuales;
	}
}