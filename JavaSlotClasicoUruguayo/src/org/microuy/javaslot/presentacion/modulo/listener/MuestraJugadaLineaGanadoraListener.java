package org.microuy.javaslot.presentacion.modulo.listener;

import org.microuy.javaslot.presentacion.modulo.listener.vo.MuestraJugadaLineaGanadoraVO;

public interface MuestraJugadaLineaGanadoraListener {

	public void mostrandoJugadaGanadora(long tiempo, MuestraJugadaLineaGanadoraVO muestraJugadaGanadoraVO);
	
}
