package org.microuy.javaslot.presentacion.modulo.listener;

public interface EscritorioListener {

	public void entraEscritorio();
	public void saleEscritorio();
	
}
