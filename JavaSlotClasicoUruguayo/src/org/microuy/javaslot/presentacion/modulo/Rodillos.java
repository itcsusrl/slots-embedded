package org.microuy.javaslot.presentacion.modulo;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.FiguraTipo;
import org.microuy.javaslot.constante.JugadaTipo;
import org.microuy.javaslot.constante.SistemaFrontend;
import org.microuy.javaslot.dominio.Figura;
import org.microuy.javaslot.dominio.JugadaLineaGanadora;
import org.microuy.javaslot.expepcion.SystemException;
import org.microuy.javaslot.logica.configuracion.Configuracion;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.event.RodillosEvent;
import org.microuy.javaslot.presentacion.event.RodillosListener;
import org.microuy.javaslot.presentacion.modulo.listener.EscritorioListener;
import org.microuy.javaslot.presentacion.pantalla.Juego;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.presentacion.util.FiguraUtil;
import org.microuy.javaslot.presentacion.util.SlotsAnimation;
import org.microuy.javaslot.presentacion.util.SlotsImage;
import org.microuy.javaslot.sonido.ReproductorSonido;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Rodillos extends ModuloGenerico implements EscritorioListener {

	public static final byte VELOCIDAD_PARADA_NORMAL = 0;
	public static final byte VELOCIDAD_PARADA_RAPIDO = 1;

	public static final byte ESTADO_DETENIDO = 0;
	public static final byte ESTADO_PRE_GIRO = 1;
	public static final byte ESTADO_GIRANDO = 2;
	public static final byte ESTADO_DETENIENDO = 3;
	public static final byte ESTADO_ANIMANDO = 4;

	// instancias de objetos de tipo Rodillo
	// que conforman la coleccion de rodillos
	private List<Rodillo> rodilloLista;
	
	//flag que indica si los rodillos
	//estan mostrando animaciones
	private boolean mostrarAnimacionesConRecuadro;

	// una vez que se inicia el giro se setea esta
	// variable, y al final el giro debe de tener
	// los mismos valores que la variable 'indicesRodillos'
	private int[] combinacion;
	
	/**
	 * Una tirada ficticia es aquella que hace
	 * que no se respete la apuesta como tal, 
	 * sino que se modifique la misma para cumplir
	 * ciertos requisitos (de pago en este caso), 
	 * para que el jugador no se exceda en las
	 * ganancias.
	 */
	private int[][] tiradaFicticia;

	// el indice actual de cada rodillo
	// el conjunto conforma la jugada
	private byte[] indicesRodillos;

	// estado de los rodillos
	public byte estado = ESTADO_DETENIDO;
	private byte velocidadParada = VELOCIDAD_PARADA_NORMAL;
	private Timer timerVibracion;
	private Timer timerGirando;
	private Timer timerDeteniendo;
	
	//lista de listeners de los rodillos
	private List<RodillosListener> rodillosListeners;
	
	//evento generado durante los cambios
	//de estados de los rodillos
	private RodillosEvent rodillosEvent;
	
	//referencia al estado juego
	private Juego juego;
	
	//separacion de rodillos en pixeles
	public static int SEPARACION_RODILLOS_PX = 60;
	
	//flag que indica si el efecto
	//de vibracion de los rodillos 
	//de cuando se gana el jackpot
	//debe de ejecutarse
	public boolean vibrarRodillos;
	
	//cantidad de rodillos
	private int cantRodillos;
	
	//ancho de cada rodillo
	private int anchoRodillo;

	//fondo
	private Image redImage;

	//marco azul superior
	private Image marcoAzul;

	public Rodillos(PantallaGenerica pantallaGenerica, int x, int y, int width,int height) {
		super(pantallaGenerica, x, y, width, height);
	}

	public void inicializarRecursos() {

		//referencia al estado juego
		juego = ((Juego)principal.getState(Pantalla.PANTALLA_JUEGO));
		
		//obtengo la configuracion de la maquina
		Configuracion configuracion = principal.maquina.configuracion;
		
		//instancio la lista de rodillos
		rodilloLista = new ArrayList<Rodillo>();
		
		//instancio la lista de listeners de rodillos
		rodillosListeners = new ArrayList<RodillosListener>();
		
		//instancio el evento usado por los listeners
		rodillosEvent = new RodillosEvent();
		
		//obtengo la cantidad de rodillos
		cantRodillos = configuracion.getRodillos().size();
		
		//calculamos el ancho 
		//de cada rodillo
		anchoRodillo = calcularAnchoRodillo();
		
		for (int i = 0; i < cantRodillos; i++) {

			//obtengo el rodillo de dominio actual
			List<Figura> figuras = configuracion.getRodillos().get(i).getFiguras();

			List<SlotsImage> imagenesRodillo = new ArrayList<SlotsImage>();
			List<SlotsAnimation> animacionesRodillo = new ArrayList<SlotsAnimation>();
			
			int j = 0;
			for (Figura figuraActual : figuras) {

				//obtengo el Image correspondiente
				SlotsImage figuraImage = FiguraUtil.obtenerImagenFiguraPorId(figuraActual.getId());
				SlotsAnimation figuraAnimation = FiguraUtil.obtenerAnimacionFiguraPorId(figuraActual.getId());
				
				//lo agrego a la coleccion
				// de figuras del rodillo
				imagenesRodillo.add(figuraImage);
				animacionesRodillo.add(figuraAnimation);
				
				//incremento el contador
				j++;
			}
			
			//calculamos la posicion en 
			//'x' del rodillo actual
			//int posicionRodilloX = x + (i * anchoRodillo);
			int posicionRodilloX = calcularPosicionRodilloX(i);
			
			//instanciamos el 
			//modulo del rodillo
			Rodillo rodillo = new Rodillo(i, this, imagenesRodillo, animacionesRodillo, pantallaGenerica, posicionRodilloX, getY(), anchoRodillo, getHeight());
			
			//y lo agregamos a la lista 
			rodilloLista.add(rodillo);
			
		}

		try {
			redImage = new Image("resources/game_BG_net.png");
		} catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}

		try {
			marcoAzul = new Image("resources/marco-14.png");
		} catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}

		//timer de vibracion
		timerVibracion = new Timer(30);
		
		//cargamos los sonidos correspondientes
		ReproductorSonido.cargarSonidos(new int[]{ReproductorSonido.RODILLO_DETIENE, ReproductorSonido.RODILLO_GIRANDO});
		
	}
	
	private int calcularPosicionRodilloX(int i) {
		return (x + (i * anchoRodillo) + (getWidth()/2) - (principal.getWidth()/2) + 20);
	}
	
	public static int calcularAnchoRodillo() {
		return 172;
	}
	
	public static int calcularAnchoRodillos(int cantRodillos) {
		
		return ((calcularAnchoRodillo() + SEPARACION_RODILLOS_PX) * cantRodillos);
	}
	
	public void agregarRodillosListener(RodillosListener rodillosListener) {
		rodillosListeners.add(rodillosListener);
	}

	public void render(Graphics g) {

		g.setClip(x, y, width, height);
		
		//renderizo cada rodillo por separado
		for (Rodillo rodillo : rodilloLista) {
			rodillo.renderizar(g);
		}

		//reseteamos el clip
		g.clearClip();		

		if(!SistemaFrontend.MOSTRAR_DEBUG_RODILLO) {
			//red
			redImage.draw(x,0);
		}
		
		//el marco
		marcoAzul.draw(0,57);

	}
	
	public void update(long elapsedTime) {

		// si el estado de los rodillos es
		// pre giro significa que estos deben
		// de ser configurados para comenzar
		// a girar
		if (estado == ESTADO_PRE_GIRO) {

			//genero el evento para indicar que
			//los rodillos estan girando actualmente
			generarEventoRodillos(RodillosEvent.TIPO_EVENTO_GIRANDO);

			// recorro la lista de objetos rodillo
			// para inicializar su giro
			for(int i=0; i<rodilloLista.size(); i++) {

				//obtengo el rodillo actual
				Rodillo rodillo = rodilloLista.get(i);
				
				// si el estado del rodillo es detenido
				// tenemos que comenzar el giro
				if (rodillo.getEstado() == Rodillo.ESTADO_DETENIDO) {
					
					//contenedor de tirada ficticia (puede ser nula)
					int[] tiradaFicticiaRodillo = tiradaFicticia != null ? tiradaFicticia[i] : null;
					
					//giramos el rodillo
					rodillo.girar(combinacion[i], tiradaFicticiaRodillo);
				}
			}

			// los rodillos pueden comenzar
			estado = ESTADO_GIRANDO;

		}
		else if (estado == ESTADO_GIRANDO) {

			//si los rodillos giraron el tiempo 
			//necesario comenzamos a parar los
			//rodillos
			if(timerGirando.action(elapsedTime)) {
				estado = ESTADO_DETENIENDO;
			}
			else if(juego.ultimaTiradaGratuita) {
				
//				//si la ultima tirada fue gratuita
//				//hacemos una detencion inmediata
//				//asi no tarda tanto la tirada
				detenerRodillos(VELOCIDAD_PARADA_NORMAL, 200);
				
			}
		}
		else if(estado == ESTADO_DETENIENDO) {
			
			if(chequearGanadorJackpot()) {
				vibrarRodillos = true;
			}
			
			//comenzamos a parar los rodillos 
			//de a uno y con un delay configurado
			//por un timer
			if(timerDeteniendo.action(elapsedTime)) {
				for(Rodillo rodillo : rodilloLista) {
					if(rodillo.getEstado() == Rodillo.ESTADO_GIRANDO) {
						rodillo.detener();
						break;
					}
				}
			}
		}
		
		/**
		 * Si el flag de vibracion de 
		 * rodillos esta seteado significa
		 * que debemos de mover los rodillos
		 * aleatoriamente algunos pixeles en
		 * los dos ejes para simular un efecto
		 * de vibracion
		 */
		logicaVibracion(elapsedTime);

		// ejecuto la logica de cada
		// rodillo independiente
		for (int i = 0; i < cantRodillos; i++) {
			
			Rodillo rodillo = rodilloLista.get(i);
			//calculamos la posicion en 
			//'x' del rodillo actual
			int posicionRodilloX = calcularPosicionRodilloX(i);
			
			rodillo.setX(posicionRodilloX);
			rodillo.actualizar(elapsedTime);
		}
	}

	/**
	 * Vibra los rodillos en los 
	 * ejes X e Y
	 */
	private void logicaVibracion(long elapsedTime) {

		//si el flag esta seteado
		if(vibrarRodillos) {
			
			//timer para recalcular la
			//posicion en x e y
			if(timerVibracion.action(elapsedTime)) {
				
				int vibracionX = (int)(Math.random() * (float)15);
				int vibracionY = (int)(Math.random() * (float)15);

				for(Rodillo rodillo : rodilloLista) {
					rodillo.vibracionX = vibracionX;
					rodillo.vibracionY = vibracionY;
				}
			}
		}
		else {
			for(Rodillo rodillo : rodilloLista) {
				rodillo.vibracionX = 0;
				rodillo.vibracionY = 0;
			}
		}
	}
	
	private boolean chequearGanadorJackpot() {
		
		boolean ganador = false;

		//chequeamos que exista al menos una
		//jugada ganadora para verificar
		if(juego.jugadasLineasGanadoras != null && juego.jugadasLineasGanadoras.size() > 0) {
			
			//recorremos la lista de
			//jugadas ganadoras
			for(JugadaLineaGanadora jugadaLineaGanadora : juego.jugadasLineasGanadoras) {
				//chequeo de jugada jackpot
				if(jugadaLineaGanadora.getTipo() == JugadaTipo.JACKPOT) {
					//cambiamos el flag
					ganador = true;
					break;
				}
			}
		}
		
		return ganador;
	}
	
	/**
	 * Gira los rodillos hasta formar la combinacion
	 * indicada por los indices de rodillo recibidos
	 * por parametro
	 * 
	 * @param combinacion
	 * @param rodilloGiradorId
	 */
	public void girarRodillos(int[] combinacion, int[][] tiradaFicticia) {

		//reproduzco el sonido de rodillos girando
		detenerSonidosPremiacion();
		ReproductorSonido.loopSound(ReproductorSonido.RODILLO_GIRANDO);
		
		//configuro la combinacion a tirar
		this.combinacion = combinacion;
		
		//configuramos la tirada ficticia 
		//(que puede llegar a ser nula) en
		//caso de que no se haya cancelado
		//la jugada
		this.tiradaFicticia = tiradaFicticia;
		
		//seteo la velocidad de parada normal
		this.velocidadParada = VELOCIDAD_PARADA_NORMAL;
		
		//oculto si hubo algun tipo de animacion
		//y recuadro en alguna jugada ganadora previa
		ocultarAnimacionesConRecuadro();

		//instancio el timer de giro y detencion
		timerGirando = new Timer(700);
		timerDeteniendo = new Timer(200);

		//habla por si mismo
		estado = ESTADO_PRE_GIRO;

		//re instancio la lista de indices de cada rodillo
		indicesRodillos = new byte[rodilloLista.size()];

		//reseteo a su valor nulo cada indice de cada rodillo
		for (int i = 0; i < indicesRodillos.length; i++) {
			indicesRodillos[i] = -1;
		}
	}
	
	private void detenerSonidosPremiacion() {
		
		ReproductorSonido.stopSound(ReproductorSonido.JUGADA_GANADORA_INTERMITENCIA);
		ReproductorSonido.stopSound(ReproductorSonido.PITIDO);
		ReproductorSonido.stopSound(ReproductorSonido.DINERO_GANADO);
		ReproductorSonido.stopSound(ReproductorSonido.SCATTER);
		ReproductorSonido.stopSound(ReproductorSonido.BONUS);
		ReproductorSonido.stopSound(ReproductorSonido.RODILLO_GIRANDO);
	}

	/**
	 * Detiene los rodillos con un delay
	 * de parada entre rodillos definido
	 * en el parametro 
	 * 
	 * @param delayRodillos
	 */
	public void detenerRodillos(byte velocidadParada, int delayRodillos) {
		
		if(estado == ESTADO_GIRANDO) {
			this.velocidadParada = velocidadParada;
			timerDeteniendo = new Timer(delayRodillos);
			estado = ESTADO_DETENIENDO;
		}
	}
	
	public void detenerRodillosInmediatamente() {
		for(Rodillo rodillo : rodilloLista) {
			rodillo.detenerInmeditamente();
		}
		estado = ESTADO_DETENIDO;
	}

	/**
	 * Funcion callback usada por la clase Rodillo para indicar que ya se ha
	 * detenido, y que debe de ser reevaluado el estado logico de esta clase.
	 * 
	 * Fundamental que este metodo sea sincronizado para no entrar en un estado
	 * logico invalido debido principalmente al flag condicional 'girando'.
	 * 
	 * @param rodillo
	 * @param posicion
	 */
	public synchronized void finalizaGiroRodilloCallback(int rodillo, byte posicion) {

		// seteo el indice
		indicesRodillos[rodillo] = posicion;
		
		//flag usado para chequear 
		//que todos los rodillos se
		//hayan detenido
		boolean todosParados = true;
		
		// si todos los id' son distinto a -1
		// significa que los rodillos ya dejaron
		// de girar en su totalidad
		for (int i = 0; i < indicesRodillos.length; i++) {
			if (indicesRodillos[i] == -1) {
				todosParados = false;
				break;
			}
		}
		
		// cambio el estado a 'detenido' en base
		// al estado de los valores -1 en el
		// array indicesRodillos
		if (todosParados) {

			//detemos el sonido de los rodillos girando
			ReproductorSonido.stopSound(ReproductorSonido.RODILLO_GIRANDO);
			
			//configuro el nuevo estado
			estado = ESTADO_DETENIDO;

			//genero el evento para indicar que
			//los rodillos estan girando actualmente
			generarEventoRodillos(RodillosEvent.TIPO_EVENTO_PARADO);

		}

		//reproducimos sonido del rodillo deteniendose
		ReproductorSonido.playSound(ReproductorSonido.RODILLO_DETIENE);

	}
	
	/**
	 * Usado para mostrar jugadas ganadoras de tipo
	 * scatter o bonus, que no tienen una linea definida
	 * 
	 * @param tirada
	 */
	public void mostrarAnimacionesConRecuadro(int[][] tirada, int figuraId) {
		
		//cambiamos el estado
		estado = ESTADO_ANIMANDO;
		
		//seteo flag de clase
		mostrarAnimacionesConRecuadro = true;
		
		//los valores de visibilidad
		//de las figuras que componen
		//el rodillo
		boolean[] valoresRodillo = new boolean[tirada.length+1];
		
		//recorremos cada rodillo (5 recorridas)
		for(int rodilloIndex=0; rodilloIndex<tirada[0].length; rodilloIndex++) {
			
			//siempre falso porque es la
			//figura previa a la primera
			valoresRodillo[0] = false;
			
			//recorremos cada figura del rodillo (3 recorridas)
			for(int piezaRodilloIndex=1; piezaRodilloIndex<valoresRodillo.length; piezaRodilloIndex++) {
				valoresRodillo[piezaRodilloIndex] = (tirada[piezaRodilloIndex-1][rodilloIndex] == figuraId);
			}

			/**
			 * Obtenemos el color de cada tipo de jugada,
			 * bonus o scatter
			 */
			Color colorJugada = null;
			Configuracion configuracion = principal.maquina.configuracion;

			Figura figuraBonus = configuracion.obtenerFiguraBonus();
			Figura figuraScatter = configuracion.obtenerFiguraScatter();
			Figura figuraBonusMenorMayor = configuracion.obtenerFiguraBonusMenorMayor();

			if(figuraBonus != null && figuraBonus.getId() == figuraId) {
				colorJugada = SistemaFrontend.obtenerColorPorFiguraTipo(FiguraTipo.BONUS);
			}
			else if(figuraScatter != null && figuraScatter.getId() == figuraId) {
				colorJugada = SistemaFrontend.obtenerColorPorFiguraTipo(FiguraTipo.SCATTER);
			}
			else if(figuraBonusMenorMayor != null && figuraBonusMenorMayor.getId() == figuraId) {
				colorJugada = SistemaFrontend.obtenerColorPorFiguraTipo(FiguraTipo.BONUS_MENOR_MAYOR);
			}
			
			//mostramos las animaciones con recuadro
			rodilloLista.get(rodilloIndex).setMostrarAnimacionesConRecuadro(valoresRodillo, colorJugada);
			
		}
	}
	
	/**
	 * Usado para mostrar jugadas ganadoras normales
	 * 
	 * @param linea
	 */
	public void mostrarAnimacionesConRecuadro(JugadaLineaGanadora jugadaLineaGanadora) {
		
		//cambiamos el estado
		estado = ESTADO_ANIMANDO;
		
		//obtengo la forma de la 
		//linea para dibujar correctamente
		//el recuadro y la animacion
		int[][] formaLinea = jugadaLineaGanadora.getLinea().getForma();
		boolean[] valoresRodillo = new boolean[formaLinea.length+1];
		
		//debemos obtener la cantidad de 
		//figuras que componen la jugada
		//ganadora
		int cantFigurasJugadaGanadora = jugadaLineaGanadora.getJugadaGanadora().figuras.size();
		
		//recorremos cada rodillo (5 recorridas)
		for(int rodilloIndex=0; rodilloIndex<formaLinea[0].length; rodilloIndex++) {
			
			//siempre falso porque es la
			//figura previa a la primera
			valoresRodillo[0] = false;
			
			//recorremos cada figura del rodillo (3 recorridas)
			for(int piezaRodilloIndex=1; piezaRodilloIndex<valoresRodillo.length; piezaRodilloIndex++) {
				valoresRodillo[piezaRodilloIndex] = (formaLinea[piezaRodilloIndex-1][rodilloIndex] == 1);
			}
			
			if(cantFigurasJugadaGanadora > 0) {
				
				//obtenemos el color de la
				//linea ganadora
				Color colorLinea = SistemaFrontend.obtenerColorPorIdLinea(jugadaLineaGanadora.getLinea().getId());
				
				//indicamos los recuadros del
				//rodillo que deben de dibujarse
				//debido a la jugada ganadora
				rodilloLista.get(rodilloIndex).setMostrarAnimacionesConRecuadro(valoresRodillo, colorLinea);
				
			}
			else {
				
				//ya pasamos la cantidad de figuras
				//ganadoras, debemos limpiar los 
				//rodillos que no usamos
				rodilloLista.get(rodilloIndex).setMostrarAnimacionesConRecuadro(null, null);
			}

			//como nos movimos de rodillo
			//hacia la derecha, disminuimos
			//en uno la figura ganadora actual
			cantFigurasJugadaGanadora--;
			
		}
	}
	
	public void ocultarAnimacionesConRecuadro() {
		
		//cuando se termina la animacion 
		//se pasa al estado detenido
		estado = ESTADO_DETENIDO;
		
		mostrarAnimacionesConRecuadro = false;
		for(Rodillo rodillo : rodilloLista) {
			rodillo.setMostrarAnimacionesConRecuadro(null, null);
		}
	}

	public List<Rodillo> getRodilloLista() {
		return rodilloLista;
	}

	public void setRodilloLista(List<Rodillo> rodilloLista) {
		this.rodilloLista = rodilloLista;
	}

	public byte getEstado() {
		return estado;
	}
	
	public void generarEventoRodillos(byte tipoEvento) {
		
		//configuro el evento del rodillo para
		//indicar un tipo de evento nuevo
		rodillosEvent.setTipoEvento(tipoEvento);
		
		//recorro la lista de listeners para
		//generar y ejecutar el evento 
		for(RodillosListener rodillosListener : rodillosListeners) {
			rodillosListener.procesarEventoRodillos(rodillosEvent);
		}
	}

	public byte getVelocidadParada() {
		return velocidadParada;
	}

	public void setVelocidadParada(byte velocidadParada) {
		this.velocidadParada = velocidadParada;
	}

	public boolean isMostrarAnimaciones() {
		return mostrarAnimacionesConRecuadro;
	}

	public void setMostrarAnimaciones(boolean mostrarAnimaciones) {
		this.mostrarAnimacionesConRecuadro = mostrarAnimaciones;
	}
	
	public void setX(int x) {
		
		this.x = x;
		for(Rodillo rodillo : rodilloLista) {
			rodillo.setX(x);
		}
	}

	/**
	 * Entramos nuevamente al juego de slots
	 */
	public void entraEscritorio() {
		
		//realizamos la entrada en
		//pantalla del juego principal
		juego.entraEnPantalla();
		
		//mostramos el panel de botones
		principal.panelBotonesVirtual.mostrarRapido();
		
		//si existian tiradas gratuitas
		//las ejecutamos al volver al 
		//escritorio
		if(principal.logicaMaquina.cantidadTiradasGratis > 0) {
			juego.evaluacionTiradaGratuita();
		}
		
		//configuramos el estado de los
		//botones para los slots nuevamente
		juego.configurarEstadoBotones();

		/**
		 * Si venimos de otro escritorio 
		 * chequeamos que el monto nuevo
		 * del jugador no sea cero
		 */
		if(juego.maquina.montoJugador <= 0) {
			juego.finalizarPartida();
		}
	}

	public void saleEscritorio() {
		
	}
}