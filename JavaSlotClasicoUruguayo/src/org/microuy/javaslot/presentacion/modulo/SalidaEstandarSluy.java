package org.microuy.javaslot.presentacion.modulo;

import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.presentacion.Principal;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

public class SalidaEstandarSluy extends ModuloGenerico {

	private Color colorFondo = new Color(100, 100, 100, 200);
	
	private String[] lineas;

	//stream desde el que recibimos las
	//salidas de system.out y .err
    private PrintStream printStream = new PrintStream(new FilteredStream(new ByteArrayOutputStream()));
    
    //salida estandar se agrega a
    //esta variable, luego la misma
    //es descompuesta por \\n
    private StringBuffer salida = new StringBuffer();
    
    //indica si la ui ya esa pronta
    //para desplegar en pantalla
    private boolean uiLista;
    
    //fuente de salida estandar
    private UnicodeFont fuente;
    
	public SalidaEstandarSluy() {
		
		super(null, 0, 0, 100, 100);
        lineas = new String[0];
		System.setOut(printStream); // catches System.out messages
        System.setErr(printStream); // catches error messages		

	}

	public void render(Graphics g) {
		
		if(uiLista) {
			
			//calculamos el alto de una linea
			int altoLinea = (int)((float)fuente.getHeight(lineas[0]) * 1.10f);
			
			//calculamos el alto total del texto
			int altoTotal = lineas.length * altoLinea;
			
			g.setColor(colorFondo);
			g.fillRect(0, 0, width, height);
			
			int y = 0;
			for(int i=0; i<lineas.length; i++) {
				
				//obtenemos la linea
				String valor = lineas[i];
				
				//la imprimimos
				fuente.drawString(10, height + y - altoTotal - 80, valor);
				
				//incrementamos el alto de la fuente
				//y un porcentaje extra dado
				y += altoLinea;
			}
		}
	}

	public void update(long elapsedTime) {
		
	}

	public void inicializarRecursos() {

	}

	class FilteredStream extends FilterOutputStream {
		public FilteredStream(OutputStream aStream) {
			super(aStream);
		}

		public void write(byte b[]) throws IOException {
			String aString = new String(b);
			salida.append(aString);
			lineas = salida.toString().split("\\n");
			escribirArchivo(aString);
		}

		public void write(byte b[], int off, int len) throws IOException {
			String aString = new String(b, off, len);
			salida.append(aString);
			lineas = salida.toString().split("\\n");
			escribirArchivo(aString);
		}

		/**
		 * Escribe a un archivo la salida estandar
		 * @param string
		 */
		private void escribirArchivo(String string) {

			if (Sistema.SALIDA_ESTANDAR_UI_FILE != null) {
				try {
					FileWriter aWriter = new FileWriter(Sistema.SALIDA_ESTANDAR_UI_FILE, true);
					aWriter.write(string);
					aWriter.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void setPrincipal(Principal principal) {
		this.principal = principal;
		fuente = Principal.salidaEstandarSluyFont;
		setWidth(principal.getWidth());
		setHeight(principal.getHeight());
	}
	
	public void cambiarUiLista() {
		uiLista = !uiLista;
	}
}