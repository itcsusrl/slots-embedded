package org.microuy.javaslot.presentacion.modulo;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.JugadaTipo;
import org.microuy.javaslot.dominio.JugadaLineaGanadora;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.modulo.listener.MuestraJugadaLineaGanadoraListener;
import org.microuy.javaslot.presentacion.modulo.listener.vo.MuestraJugadaLineaGanadoraVO;
import org.microuy.javaslot.presentacion.pantalla.Juego;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.sonido.ReproductorSonido;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Graphics;

public class MuestraJugadasGanadoras extends ModuloGenerico {

	//Contiene la ultima lista con jugadas ganadoras. Es una
	//copia de la variable homonima de la clase LogicaMaquina
	private List<JugadaLineaGanadora> jugadasLineasGanadoras;
	
	//array usado para configurar que lineas mostrar
	private short[] lineas;
	private List<JugadaLineaGanadora> jugadaLineaGanadoraList;
	
	/**
	 * Contiene la jugada ganadora de tipo bonus
	 */
	private JugadaLineaGanadora jugadaLineaGanadoraBonus;
	
	/**
	 * Contiene una copia de la tirada
	 * actual de la maquina 
	 */
	private int[][] tiradaActual;
	
	/**
	 * Usado para no incrementar el credito
	 * del jugador tras cada iteracion de 
	 * la animacion 
	 */
	private boolean inicializado;
	
	/**
	 * Contiene la jugada ganadora de tipo scatter
	 */
	private JugadaLineaGanadora jugadaLineaGanadoraScatter;

	/**
	 * Contiene la jugada ganadora de tipo JACKPOT
	 */
	private JugadaLineaGanadora jugadaLineaGanadoraJackpot;
	
	/**
	 * Contiene la jugada ganadora de tipo bonus 'Menor o Mayor'
	 */
	public JugadaLineaGanadora jugadaLineaGanadoraBonusMenorMayor;
	
	//lista de vo's de jugadas ganadora
	private List<MuestraJugadaLineaGanadoraVO> muestraJugadaLineaGanadoraVOs;
	
	//lista de listeners de visualizadores de jugadas ganadoras
	private List<MuestraJugadaLineaGanadoraListener> muestraJugadaLineaGanadoraListeners;
	
	//configura las varibles usadas 
	//por el siguiente paso	
	private final byte PASO_1_CONFIGURA_MUESTRA_SCATTER_INTERMITENTE = 1;
	
	//muestra la jugada scatter en caso
	//de que exista
	private final byte PASO_2_MUESTRA_SCATTER_INTERMITENTE = 2;

	//configura las varibles usadas 
	//por el siguiente paso	
	private final byte PASO_3_CONFIGURA_MUESTRA_LINEAS_INTERMITENTES = 3;
	
	//muestra todas las lineas a la vez 
	//con un delay con intermitencia
	private final byte PASO_4_MUESTRA_LINEAS_INTERMITENTES = 4;
	
	//configura las varibles usadas 
	//por el siguiente paso	
	private final byte PASO_5_CONFIGURA_MUESTRA_ANIMACIONES_CON_RECUADRO = 5;

	//muestra las animaciones de cada figura
	private final byte PASO_6_MUESTRA_ANIMACIONES_CON_RECUADRO = 6;

	//configura las varibles usadas 
	//por el siguiente paso	
	private final byte PASO_7_CONFIGURA_MUESTRA_BONUS_INTERMITENTE = 7;
	
	//muestra la jugada bonus en caso
	//de que exista
	private final byte PASO_8_MUESTRA_BONUS_INTERMITENTE = 8;

	//configura las varibles usadas 
	//por el siguiente paso	
	private final byte PASO_9_CONFIGURA_VIBRACION_JACKPOT = 9;
	
	//muestra la jugada jackpot 
	//en caso de que exista
	private final byte PASO_10_VIBRACION_JACKPOT = 10;
	
	//configura la intermitencia de la linea
	//en donde salio ganador el jackpot
	private final byte PASO_11_CONFIGURA_MUESTRA_LINEAS_INTERMITENTES_JACKPOT = 11;

	//muestra la linea intermitente
	//correspondiente a donde salio
	//el jackpot
	private final byte PASO_12_MUESTRA_LINEAS_INTERMITENTES_JACKPOT = 12;

	//configura la animacion de la figura
	//de jackpot con un recuadro indicando
	//la linea ganadora
	private final byte PASO_13_CONFIGURA_MUESTRA_ANIMACIONES_CON_RECUADRO_JACKPOT = 13;
	
	//muestra efectivamente la animacion
	//configurada en el paso anterior
	private final byte PASO_14_MUESTRA_ANIMACIONES_CON_RECUADRO_JACKPOT = 14;
	
	private byte paso;
	private Timer timer;
	private Timer timerHelper;
	
	//indica el id de linea que debe 
	//de mostrar un recuadro y la 
	//animacion para la linea ganadora
	private short animacionConRecuadroActual;
	
	//indica el ciclo en el que se
	//encuentra el muestreo de jugadas
	//ganadoras
	private int cicloMuestreoJugadasGanadoras;
	
	//referencia a la pantalla de juego
	private Juego juego;
	
	public MuestraJugadasGanadoras(PantallaGenerica pantallaGenerica, int x, int y, int width, int height) {
		
		super(pantallaGenerica, x, y, width, height);
		
		//instancio la lista de jugada de lineas ganadoras
		jugadasLineasGanadoras = new ArrayList<JugadaLineaGanadora>();
		muestraJugadaLineaGanadoraVOs = new ArrayList<MuestraJugadaLineaGanadoraVO>();
		muestraJugadaLineaGanadoraListeners = new ArrayList<MuestraJugadaLineaGanadoraListener>();
		
		//configuramos la referencia a
		//la pantalla de juego
		juego = (Juego)pantallaGenerica;
		
	}

	public void inicializarRecursos() {
		
		ReproductorSonido.cargarSonidos(new int[]{ReproductorSonido.JUGADA_GANADORA_INTERMITENCIA});
		jugadaLineaGanadoraList = new ArrayList<JugadaLineaGanadora>();

	}

	public void render(Graphics g) {
		
	}

	public void update(long elapsedTime) {
		
		//si nos encontramos premiando
		//o finalizando una premacion
		Juego juego = (Juego)principal.getState(Pantalla.PANTALLA_JUEGO);
		
		if(juego.estado == Juego.ESTADO_JUEGO_PREMIANDO || juego.estado == Juego.ESTADO_JUEGO_PREMIACION_FINALIZADA) {
			
			if(timer == null) {
				timer = new Timer(0);
				timerHelper = new Timer(0);
			}
			
			/**
			 * Este condicional permite que no vuelva
			 * a sumar al credito del jugador tras varias
			 * repeticiones de la animacion
			 */
			if(!inicializado) {
				
				//limpiamos la lista de vo's de jugadas lineas ganadoras
				muestraJugadaLineaGanadoraVOs.clear();

				//cargamos la lista nuevamente
				for(JugadaLineaGanadora jugadaLineaGanadora : jugadaLineaGanadoraList) {
					
					//creamos el vo
					MuestraJugadaLineaGanadoraVO muestraJugadaLineaGanadoraVO = new MuestraJugadaLineaGanadoraVO(jugadaLineaGanadora);
					
					//lo agregamos a la lista
					muestraJugadaLineaGanadoraVOs.add(muestraJugadaLineaGanadoraVO);
				}
				
				//chequeamos que exista un jugada 
				//bonus para crear el vo correspondiente
				if(jugadaLineaGanadoraBonus != null) {
					
					//creamos el vo
					MuestraJugadaLineaGanadoraVO muestraJugadaLineaGanadoraVO = new MuestraJugadaLineaGanadoraVO(jugadaLineaGanadoraBonus);
					
					//lo agregamos a la lista
					muestraJugadaLineaGanadoraVOs.add(muestraJugadaLineaGanadoraVO);
					
				}
				
				//chequeamos que exista un jugada 
				//scatter para crear el vo correspondiente
				if(jugadaLineaGanadoraScatter != null) {
					
					//creamos el vo
					MuestraJugadaLineaGanadoraVO muestraJugadaLineaGanadoraVO = new MuestraJugadaLineaGanadoraVO(jugadaLineaGanadoraScatter);
					
					//lo agregamos a la lista
					muestraJugadaLineaGanadoraVOs.add(muestraJugadaLineaGanadoraVO);
				}
				
				if(jugadaLineaGanadoraJackpot != null) {
					
					//creamos el vo
					MuestraJugadaLineaGanadoraVO muestraJugadaLineaGanadoraVO = new MuestraJugadaLineaGanadoraVO(jugadaLineaGanadoraJackpot);
					
					//lo agregamos a la lista
					muestraJugadaLineaGanadoraVOs.add(muestraJugadaLineaGanadoraVO);
				}
				
				if(jugadaLineaGanadoraBonusMenorMayor != null) {
					
					//creamos el vo
					MuestraJugadaLineaGanadoraVO muestraJugadaLineaGanadoraVO = new MuestraJugadaLineaGanadoraVO(jugadaLineaGanadoraBonusMenorMayor);
					
					//lo agregamos a la lista
					muestraJugadaLineaGanadoraVOs.add(muestraJugadaLineaGanadoraVO);
				}
				
				//ya se inicializaron las jugadas ganadoras
				inicializado = true;
			}
			
			/**
			 * Comenzamos la logica para configurar
			 * las variables que muestran dependiendo
			 * el paso en que nos encontramos, que 
			 * jugada ganadora se debe de mostrar
			 */
			if(paso == PASO_1_CONFIGURA_MUESTRA_SCATTER_INTERMITENTE) {

				//si existe scatter configuramos las variables 
				//necesarias para el siguiente paso a ejecutar
				if(jugadaLineaGanadoraScatter != null || jugadaLineaGanadoraBonusMenorMayor != null) {
					
					//timer de repeticion
					timer.setDelay(500);
					//delay para que arranque antes 
					//el proximo paso (4)
					timer.setCurrentTick(0);
					
					//tiempo global del paso 4
					timerHelper.setDelay(2300);
					timerHelper.setCurrentTick(0);
					
					//escondemos lineas y animaciones
					juego.muestraLineas.mostrarLineas(null, MuestraLineas.TIPO_ENCENDIDO_APAGADO, 0);
					juego.rodillos.ocultarAnimacionesConRecuadro();
					
					//obtengo el vo actual
					MuestraJugadaLineaGanadoraVO muestraJugadaLineaGanadoraVO = obtenerVOPorJugadaLineaGanadora(jugadaLineaGanadoraScatter != null ? jugadaLineaGanadoraScatter : jugadaLineaGanadoraBonusMenorMayor);

					//si es la primer repeticion aviso a 
					//los listeners para que hagan algo
					if(muestraJugadaLineaGanadoraVO.getRepeticion() == 0) {

						//incrementamos las visualizaciones
						muestraJugadaLineaGanadoraVO.incrementarRepeticion();
						
						//llamamos a los listeners
						for(MuestraJugadaLineaGanadoraListener muestraJugadaLineaGanadoraListener : muestraJugadaLineaGanadoraListeners) {
							muestraJugadaLineaGanadoraListener.mostrandoJugadaGanadora(timerHelper.getDelay()-300, muestraJugadaLineaGanadoraVO);
						}
					}
					
					//mostramos el scatter
					paso = PASO_2_MUESTRA_SCATTER_INTERMITENTE;
					
				}
				else {
					
					//si no existe una jugada scatter 
					//pasamos al paso de configuracion
					//de variables de normales
					paso = PASO_3_CONFIGURA_MUESTRA_LINEAS_INTERMITENTES;
					
				}
				
				//indicamos que la primer vuelta de
				//la premiacion ha finalizado
				if(cicloMuestreoJugadasGanadoras == 1) {
					juego.finalizaPremiacion();
				}
				
				//incremento el numero de pasada
				//por el ciclo entero de pasos
				cicloMuestreoJugadasGanadoras++;

			}
			else if(paso == PASO_2_MUESTRA_SCATTER_INTERMITENTE) {
		
				//si cumplimos el tiempo de la animacion
				if(timer.action(elapsedTime)) {
					
					if(juego.rodillos.isMostrarAnimaciones()) {
						//ocultamos la animacion
						juego.rodillos.ocultarAnimacionesConRecuadro();
					}
					else {
						
						if(jugadaLineaGanadoraScatter != null) {
							
							//mostramos la animacion
							juego.rodillos.mostrarAnimacionesConRecuadro(tiradaActual, jugadaLineaGanadoraScatter.getFiguras().get(0).getId());
						}
						else if(jugadaLineaGanadoraBonusMenorMayor != null) {
							
							//mostramos la animacion
							juego.rodillos.mostrarAnimacionesConRecuadro(tiradaActual, jugadaLineaGanadoraBonusMenorMayor.getFiguras().get(0).getId());
						}
					}
				}
				
				//si se cumplio el tiemo global
				//del muestreo de la jugada bonus
				if(timerHelper.action(elapsedTime)) {
					
					//cambiamos de escritorio
					if(jugadaLineaGanadoraBonusMenorMayor != null) {

						//dejamos de mostrar la 
						//premiacion asi no entramos
						//mas veces al escritorio
						ocultarJugadasLineasGanadoras();

						//lanzamos la ejecucion del minijuego
						juego.cambiarEscritorio();
					}
					else {
						paso = PASO_3_CONFIGURA_MUESTRA_LINEAS_INTERMITENTES;	
					}
				}
			}
			else if(paso == PASO_3_CONFIGURA_MUESTRA_LINEAS_INTERMITENTES) {

				//chequeamos que hayan lineas 
				//ganadoras a mostrar
				if(jugadaLineaGanadoraList.size() > 0) {
					
					//configuro el modulo MuestraLineas 
					//para que empiece a renderizar
					juego.muestraLineas.mostrarLineas(lineas, MuestraLineas.TIPO_ENCENDIDO_APAGADO, 500);
					
					//ocultamos alguna animacion
					//se si esta ejecutando
					juego.rodillos.ocultarAnimacionesConRecuadro();

					//timer de lineas intermitentes
					timer.setDelay(500);
					timer.setCurrentTick(0);
					timerHelper.setDelay(1500);
					timerHelper.setCurrentTick(0);
					
					//pasamos al siguiente paso
					paso = PASO_4_MUESTRA_LINEAS_INTERMITENTES;

				}
				else {
					
					//si no existen lineas ganadoras
					//saltamos los pasos 6,7 y 8 porque
					//son inutiles al no tener nada para
					//mostrar
					paso = PASO_7_CONFIGURA_MUESTRA_BONUS_INTERMITENTE;
					
				}
			}
			else if(paso == PASO_4_MUESTRA_LINEAS_INTERMITENTES) {
				
				//si el tiempo se cumplio
				if(timer.action(elapsedTime)) {
					
					//cambio el estado para que se haga lo opuesto a lo que
					//se hace actualmente con respecto a si se pintan las
					//lineas o no
					MuestraLineas muestraLineas = juego.muestraLineas;
					muestraLineas.setPintaLineas(!muestraLineas.isPintaLineas());
					
					if(muestraLineas.isPintaLineas()) {
						
						//sonido de la intermitencia cuando se
						//despliega una jugada ganadora
						ReproductorSonido.playSound(ReproductorSonido.JUGADA_GANADORA_INTERMITENCIA);
						
					}
				}
				
				//si se cumplio el tiempo para este
				//paso, pasamos al siguiente
				if(timerHelper.action(elapsedTime)) {
					paso = PASO_5_CONFIGURA_MUESTRA_ANIMACIONES_CON_RECUADRO;
				}
			}
			else if(paso == PASO_5_CONFIGURA_MUESTRA_ANIMACIONES_CON_RECUADRO) {
				
				//timer de linea
				timer.setDelay(2500);
				
				//delay para que arranque antes 
				//el proximo paso (8)
				timer.setCurrentTick(timer.getDelay());

				//ocultamos animaciones previas
				juego.rodillos.ocultarAnimacionesConRecuadro();
				
				//reconfiguramos algunas variables
				animacionConRecuadroActual = 0;
				
				//pasamos al siguiente paso
				paso = PASO_6_MUESTRA_ANIMACIONES_CON_RECUADRO;
				
			}
			else if(paso == PASO_6_MUESTRA_ANIMACIONES_CON_RECUADRO) {
			
				//si cumplimos el tiempo de la animacion
				if(timer.action(elapsedTime)) {
					
					//y existen jugadas ganadoras en la lista
					if(jugadaLineaGanadoraList.size() > 0) {
						
						//si el contador de la animacion actual se 
						//paso de la cantidad de jugadas ganadoras,
						//volvemos nuevamente al principio
						if(animacionConRecuadroActual > jugadaLineaGanadoraList.size()-1) {
							
							//para no pasarnos de la cantidad
							//de elementos de la lista de 
							//jugadas ganadoras
							animacionConRecuadroActual = 0;
							
							//ya dimos dos vueltas, volvemos
							//a mostrar el bonus y scatter
							paso = PASO_7_CONFIGURA_MUESTRA_BONUS_INTERMITENTE;
							
						}

						//muestro la animacion y la linea o la oculto
						if(juego.rodillos.isMostrarAnimaciones()) {
							juego.rodillos.ocultarAnimacionesConRecuadro();	
						}
						else {

							//mostramos la animacion
							juego.rodillos.mostrarAnimacionesConRecuadro(jugadaLineaGanadoraList.get(animacionConRecuadroActual));
		
							//obtengo el vo actual
							MuestraJugadaLineaGanadoraVO muestraJugadaLineaGanadoraVO = obtenerVOPorJugadaLineaGanadora(jugadaLineaGanadoraList.get(animacionConRecuadroActual));

							//si es la primer repeticion aviso a 
							//los listeners para que hagan algo
							if(muestraJugadaLineaGanadoraVO.getRepeticion() == 0) {
								
								
								//incrementamos las visualizaciones
								muestraJugadaLineaGanadoraVO.incrementarRepeticion();
								
								//llamamos a los listeners
								for(MuestraJugadaLineaGanadoraListener muestraJugadaLineaGanadoraListener : muestraJugadaLineaGanadoraListeners) {
									muestraJugadaLineaGanadoraListener.mostrandoJugadaGanadora(timer.getDelay(), muestraJugadaLineaGanadoraVO);
								}
							}
							
							//muestra la linea actual
							if(jugadaLineaGanadoraList.size() > 0) {
								short[] tmp = new short[]{(short)jugadaLineaGanadoraList.get(animacionConRecuadroActual).getLinea().getId()};
								juego.muestraLineas.mostrarLineas(tmp, MuestraLineas.TIPO_ENCENDIDO_APAGADO, 0);
							}
						}
											
						//incrementamos la linea a mostrar
						animacionConRecuadroActual++;
						
					}
				}
			}
			else if(paso == PASO_7_CONFIGURA_MUESTRA_BONUS_INTERMITENTE) {
				
				//si existe scatter configuramos las variables 
				//necesarias para el siguiente paso a ejecutar
				if(jugadaLineaGanadoraBonus != null) {

					//timer de repeticion
					timer.setDelay(500);
					//delay para que arranque antes 
					//el proximo paso (4)
					timer.setCurrentTick(0);
					
					//tiempo global del paso 4
					timerHelper.setDelay(3500);
					timerHelper.setCurrentTick(0);
					
					//escondemos lineas y animaciones
					juego.muestraLineas.mostrarLineas(null, MuestraLineas.TIPO_ENCENDIDO_APAGADO, 0);
					juego.rodillos.ocultarAnimacionesConRecuadro();
					
					//obtengo el vo actual
					MuestraJugadaLineaGanadoraVO muestraJugadaLineaGanadoraVO = obtenerVOPorJugadaLineaGanadora(jugadaLineaGanadoraBonus);

					//si es la primer repeticion aviso a 
					//los listeners para que hagan algo
					if(muestraJugadaLineaGanadoraVO.getRepeticion() == 0) {

						//incrementamos las visualizaciones
						muestraJugadaLineaGanadoraVO.incrementarRepeticion();
						
						//llamamos a los listeners
						for(MuestraJugadaLineaGanadoraListener muestraJugadaLineaGanadoraListener : muestraJugadaLineaGanadoraListeners) {
							muestraJugadaLineaGanadoraListener.mostrandoJugadaGanadora(timerHelper.getDelay()-300, muestraJugadaLineaGanadoraVO);
						}
					}
					
					//mostramos el scatter
					paso = PASO_8_MUESTRA_BONUS_INTERMITENTE;
					
				}
				else {
					
					//si no existe una jugada scatter 
					//pasamos al siguiente paso
					paso = PASO_9_CONFIGURA_VIBRACION_JACKPOT;
					
				}
			}
			else if(paso == PASO_8_MUESTRA_BONUS_INTERMITENTE) {
				
				//si cumplimos el tiempo de la animacion
				if(timer.action(elapsedTime)) {
					
					if(juego.rodillos.isMostrarAnimaciones()) {
						//ocultamos la animacion
						juego.rodillos.ocultarAnimacionesConRecuadro();
					}
					else {
						//mostramos la animacion
						juego.rodillos.mostrarAnimacionesConRecuadro(tiradaActual, jugadaLineaGanadoraBonus.getFiguras().get(0).getId());
					}
				}
				
				//si se cumplio el tiemo global
				//del muestreo de la jugada bonus
				if(timerHelper.action(elapsedTime)) {
					paso = PASO_9_CONFIGURA_VIBRACION_JACKPOT;
				}
			}
			else if(paso == PASO_9_CONFIGURA_VIBRACION_JACKPOT) {
				
				if(jugadaLineaGanadoraJackpot != null) {
					
					//timer para indicar la 
					//duracion de la vibracion
					timer.setDelay(2500);
					
					//delay para que arranque antes 
					//el proximo paso (4)
					timer.setCurrentTick(0);

					//siguiente paso
					paso = PASO_10_VIBRACION_JACKPOT;
					
				}
				else {
					paso = PASO_1_CONFIGURA_MUESTRA_SCATTER_INTERMITENTE;
				}
			}
			else if(paso == PASO_10_VIBRACION_JACKPOT) {

				if(timer.action(elapsedTime)) {
				 	juego.rodillos.vibrarRodillos = false;
					paso = PASO_11_CONFIGURA_MUESTRA_LINEAS_INTERMITENTES_JACKPOT;
				}
			}
			else if(paso == PASO_11_CONFIGURA_MUESTRA_LINEAS_INTERMITENTES_JACKPOT) {
				
				//configuro el modulo MuestraLineas 
				//para que empiece a renderizar
				juego.muestraLineas.mostrarLineas(lineas, MuestraLineas.TIPO_ENCENDIDO_APAGADO, 500);
				
				//ocultamos alguna animacion
				//se si esta ejecutando
				juego.rodillos.ocultarAnimacionesConRecuadro();

				//timer de lineas intermitentes
				timer.setDelay(500);
				timer.setCurrentTick(0);
				timerHelper.setDelay(1500);
				timerHelper.setCurrentTick(0);
				
				//pasamos al siguiente paso
				paso = PASO_12_MUESTRA_LINEAS_INTERMITENTES_JACKPOT;
				
			}
			else if(paso == PASO_12_MUESTRA_LINEAS_INTERMITENTES_JACKPOT) {
				
				//si el tiempo se cumplio
				if(timer.action(elapsedTime)) {
					
					//cambio el estado para que se haga lo opuesto a lo que
					//se hace actualmente con respecto a si se pintan las
					//lineas o no
					MuestraLineas muestraLineas = juego.muestraLineas;
					muestraLineas.setPintaLineas(!muestraLineas.isPintaLineas());
					
					if(muestraLineas.isPintaLineas()) {
						
						//sonido de la intermitencia cuando se
						//despliega una jugada ganadora
						ReproductorSonido.playSound(ReproductorSonido.JUGADA_GANADORA_INTERMITENCIA);
						
					}
				}
				
				//si se cumplio el tiempo para este
				//paso, pasamos al siguiente
				if(timerHelper.action(elapsedTime)) {
					paso = PASO_13_CONFIGURA_MUESTRA_ANIMACIONES_CON_RECUADRO_JACKPOT;
				}
			}
			else if(paso == PASO_13_CONFIGURA_MUESTRA_ANIMACIONES_CON_RECUADRO_JACKPOT) {
				
				//timer de linea
				timer.setDelay(2500);
				
				//delay para que arranque antes 
				//el proximo paso (8)
				timer.setCurrentTick(timer.getDelay());

				//ocultamos animaciones previas
				juego.rodillos.ocultarAnimacionesConRecuadro();
				
				//reconfiguramos algunas variables
				animacionConRecuadroActual = 0;
				
				//indicamos que la primer vuelta de
				//la premiacion ha finalizado
				if(cicloMuestreoJugadasGanadoras == 2) {
					juego.finalizaPremiacion();
				}
				
				//incremento el numero de pasada
				//por el ciclo entero de pasos
				cicloMuestreoJugadasGanadoras++;
				
				//pasamos al siguiente paso
				paso = PASO_14_MUESTRA_ANIMACIONES_CON_RECUADRO_JACKPOT;

			}
			else if(paso == PASO_14_MUESTRA_ANIMACIONES_CON_RECUADRO_JACKPOT) {
				
				//si cumplimos el tiempo de la animacion
				if(timer.action(elapsedTime)) {
					
					//si el contador de la animacion actual se 
					//paso de la cantidad de jugadas ganadoras,
					//volvemos nuevamente al principio
					if(animacionConRecuadroActual == 1) {
						
						//para no pasarnos de la cantidad
						//de elementos de la lista de 
						//jugadas ganadoras
						animacionConRecuadroActual = 0;
						
						//ya dimos dos vueltas, volvemos
						//a mostrar las lineas intermitentes
						paso = PASO_11_CONFIGURA_MUESTRA_LINEAS_INTERMITENTES_JACKPOT;
						
					}

					//muestro la animacion y la linea o la oculto
					if(juego.rodillos.isMostrarAnimaciones()) {
						juego.rodillos.ocultarAnimacionesConRecuadro();	
					}
					else {

						//mostramos la animacion
						juego.rodillos.mostrarAnimacionesConRecuadro(jugadaLineaGanadoraJackpot);

						//obtengo el vo actual
						MuestraJugadaLineaGanadoraVO muestraJugadaLineaGanadoraVO = obtenerVOPorJugadaLineaGanadora(jugadaLineaGanadoraJackpot);

						//si es la primer repeticion aviso a 
						//los listeners para que hagan algo
						if(muestraJugadaLineaGanadoraVO.getRepeticion() == 0) {
							
							
							//incrementamos las visualizaciones
							muestraJugadaLineaGanadoraVO.incrementarRepeticion();
							
							//llamamos a los listeners
							for(MuestraJugadaLineaGanadoraListener muestraJugadaLineaGanadoraListener : muestraJugadaLineaGanadoraListeners) {
								muestraJugadaLineaGanadoraListener.mostrandoJugadaGanadora(timer.getDelay(), muestraJugadaLineaGanadoraVO);
							}
						}
						
						short[] tmp = new short[]{(short)jugadaLineaGanadoraJackpot.getLinea().getId()};
						juego.muestraLineas.mostrarLineas(tmp, MuestraLineas.TIPO_ENCENDIDO_APAGADO, 0);

					}
										
					//incrementamos la linea a mostrar
					animacionConRecuadroActual++;

				}
			}
		}
	}
	
	private MuestraJugadaLineaGanadoraVO obtenerVOPorJugadaLineaGanadora(JugadaLineaGanadora jugadaLineaGanadora) {
		
		for(MuestraJugadaLineaGanadoraVO muestraJugadaLineaGanadoraVO : muestraJugadaLineaGanadoraVOs) {
			
			if(muestraJugadaLineaGanadoraVO.getJugadaLineaGanadora() == jugadaLineaGanadora) {
				
				return muestraJugadaLineaGanadoraVO;
			}
		}
		
		return null;
	}

	/**
	 * Configura las variables necesarias para 
	 * comenzar el proceso grafico que muestra
	 * las lineas que tuvieron premio durante
	 * la ultima apuesta
	 */
	public void mostrarJugadasGanadoras() {
		
		//reinicializamos el ciclo de 
		//jugadas ganadoras mostradas
		cicloMuestreoJugadasGanadoras = 0;
		
		//usado para no seguir sumando credito al
		//jugador en cada repeticion de la animacion
		//completa de la clase
		inicializado = false;
		
		//obtenemos la instancia de 
		//la pantalla de juego
		Juego juego = ((Juego)principal.getState(Pantalla.PANTALLA_JUEGO));
		
		/**
		 * Obtenemos la lista de jugadas ganadoras normales
		 */
		List<JugadaLineaGanadora> var = juego.jugadasLineasGanadoras;
		
		//inicializamos el array de lineas
		lineas = new short[var.size()];
		
		//limpio la lista de jugadas de lineas ganadoras
		jugadasLineasGanadoras.clear();
		jugadasLineasGanadoras.addAll(var);
		
		jugadaLineaGanadoraList.clear();
		jugadaLineaGanadoraBonus = null;
		jugadaLineaGanadoraScatter = null;
		jugadaLineaGanadoraJackpot = null;
		jugadaLineaGanadoraBonusMenorMayor = null;
		
		//hacemos  una  copia de la 
		//tirada actual de la maquina
		int[][] tiradaActualValores = juego.tiradaActualValores;

		//si el array tirada de esta
		//clase es nulo, lo instanciamos
		if(tiradaActual == null) {
			tiradaActual = new int[tiradaActualValores.length][tiradaActualValores[0].length];
		}
		
		for(int i=0; i<tiradaActualValores.length; i++) {
			for(int j=0; j<tiradaActualValores[0].length; j++) {
				tiradaActual[i][j] = tiradaActualValores[i][j];
			}
		}
		
		//recorro la lista de jugadas lineas ganadoras
		//para configurar el contenido del array 'lineas'
		for(int i=0; i<jugadasLineasGanadoras.size(); i++) {
			
			//obtengo la linea actual
			JugadaLineaGanadora jugadaLineaGanadoraActual = jugadasLineasGanadoras.get(i);
			
			//si la jugada es normal
			if(jugadaLineaGanadoraActual.getTipo() == JugadaTipo.NORMAL || jugadaLineaGanadoraActual.getTipo() == JugadaTipo.WILD) {
				//NORMAL
				lineas[i] = (short)jugadaLineaGanadoraActual.getLinea().getId();
				jugadaLineaGanadoraList.add(jugadaLineaGanadoraActual);
			}
			else if(jugadaLineaGanadoraActual.getTipo() == JugadaTipo.BONUS) {
				//BONUS
				jugadaLineaGanadoraBonus = jugadaLineaGanadoraActual;
			}
			else if(jugadaLineaGanadoraActual.getTipo() == JugadaTipo.SCATTER) {
				//SCATTER
				jugadaLineaGanadoraScatter = jugadaLineaGanadoraActual;
			}
			else if(jugadaLineaGanadoraActual.getTipo() == JugadaTipo.JACKPOT) {
				//JACKPOT
				lineas[i] = (short)jugadaLineaGanadoraActual.getLinea().getId();
				jugadaLineaGanadoraJackpot = jugadaLineaGanadoraActual;
			}
			else if(jugadaLineaGanadoraActual.getTipo() == JugadaTipo.BONUS_MENOR_MAYOR) {
				//BONUS MENOR O MAYOR
				jugadaLineaGanadoraBonusMenorMayor = jugadaLineaGanadoraActual;
			}
		}

		//reseteamos el id de linea que
		//muestra la animacion y el 
		//recuadro de la linea ganadora 
		animacionConRecuadroActual = 0;
		
		//configuro el primer paso
		paso = PASO_1_CONFIGURA_MUESTRA_SCATTER_INTERMITENTE;
		
		//comienzo el timer
		timer = new Timer(500);
		timerHelper = new Timer(2000);
		
	}

	/**
	 * Oculta las lineas ganadoras
	 */
	public void ocultarJugadasLineasGanadoras() {

		//el paso cero no existe por lo tanto 
		//no renderiza nada en la pantalla
		paso = 0;
		
		//limpio la lista de jugadas de lineas ganadoras
		jugadasLineasGanadoras.clear();
		jugadaLineaGanadoraList.clear();
		jugadaLineaGanadoraBonus = null;
		jugadaLineaGanadoraScatter = null;
		jugadaLineaGanadoraBonusMenorMayor = null;
		jugadaLineaGanadoraJackpot = null;
		juego.rodillos.vibrarRodillos = false;
		
	}
	
	public void agregarMuestraJugadaLineaGanadoraVOListener(MuestraJugadaLineaGanadoraListener muestraJugadaLineaGanadoraListener) {
		muestraJugadaLineaGanadoraListeners.add(muestraJugadaLineaGanadoraListener);
	}

	public int getCicloMuestreoJugadasGanadoras() {
		return cicloMuestreoJugadasGanadoras;
	}

	public void setCicloMuestreoJugadasGanadoras(int cicloMuestreoJugadasGanadoras) {
		this.cicloMuestreoJugadasGanadoras = cicloMuestreoJugadasGanadoras;
	}
}