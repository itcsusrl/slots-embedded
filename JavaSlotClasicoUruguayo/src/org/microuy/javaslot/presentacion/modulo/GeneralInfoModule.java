package org.microuy.javaslot.presentacion.modulo;

import org.microuy.javaslot.expepcion.SystemException;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class GeneralInfoModule extends ModuloGenerico {
	
	//separador en caso de que
	//se repita el texto
	private Image separadorImage;
	
	//split del string por si
	//define color en parte
	private String[] textoSplitColor;
	private String[] textoSplitEspacio;
	
	private int repeticionesNecesarias;
	private int anchoTextoYSeparador;
	private int anchoSeparador;
	private int mitadAlturaSeparador;
	private int anchoTexto;
	private int altoTexto;
	private int mitadAltoTexto;
	private String textoBlanco;
	private String textoColor;
	
	private int anchoTextoAnterior;
	private int mitadAltura;
	
	//el tiempo que tarda en moverse
	//una pieza (texto sin repetir)
	//medido en milisegundos
	public int TIEMPO_MOVIMIENTO_PIEZA_TEXTO_MS = 5000;
	private int tiempoMovimientoPiezaTextoActual;
	
	private int movimientoXPixels;
	
	//texto a mostrar
	public String texto;
	
	//timer de movimiento de texto
	private Timer movimientoTextoTimer;
	
	public GeneralInfoModule(Principal principal, int x, int y, int width, int height) {
		
		super(null, x, y, width, height);
		this.principal = principal;
		mitadAltura = height / 2;
	}

	public void render(Graphics g) {
		
		//configuramos el nuevo
		g.setColor(Color.black);
		//dibujamos el rectanglo
		g.fillRect(0, 0, principal.getWidth(), 60);
		
		if(texto != null) {
			
			//dibujamos todo a lo largo
			//los textos y los separadores
			for(int i=0; i<repeticionesNecesarias; i++) {
				
				int iteracionAnchoTextoYSeparador = i*anchoTextoYSeparador;
				
				//dibujamos el separador
				separadorImage.draw(iteracionAnchoTextoYSeparador - movimientoXPixels, mitadAltura - mitadAlturaSeparador + 8);
				
				//dibujamos el texto
				Principal.bigWhiteOutlineGameFont.drawString(iteracionAnchoTextoYSeparador + anchoSeparador - movimientoXPixels, mitadAltura - mitadAltoTexto - 5, textoBlanco);
				
				//dibujamos lo posterior
				//al color, si un color fue
				//elegido
				if(textoColor != null) {
					Principal.bigYellowOutlineGameFont.drawString(iteracionAnchoTextoYSeparador + anchoSeparador + anchoTextoAnterior - movimientoXPixels, mitadAltura - mitadAltoTexto - 5, textoColor);
				}
			}
		}
	}
	
	public void mostrarTexto(String texto) {
		
		//split del string por si
		//define color en parte
		this.texto = texto;
		
		//calculos varios que se
		//hacen solo una vez
		textoSplitColor = texto.split("@");
		textoSplitEspacio = texto.split(" ");
		
		//calculo el ancho de 
		//una repeticion de texto
		anchoTextoYSeparador = separadorImage.getWidth() + 15;
		anchoSeparador = separadorImage.getWidth() + 15;
		mitadAlturaSeparador = separadorImage.getHeight() / 2;
		anchoTexto = 0;
		altoTexto = separadorImage.getHeight();
		mitadAltoTexto = separadorImage.getHeight() / 2;
		for(int i=0; i<textoSplitEspacio.length; i++) {
			anchoTextoYSeparador += Principal.smallWhiteOutlineGameFont.getWidth(textoSplitEspacio[i] + " ");
			
			if(i<textoSplitEspacio.length-1) {
				anchoTexto += Principal.smallWhiteOutlineGameFont.getWidth(textoSplitEspacio[i] + " ");	
			}
			else {
				anchoTexto += Principal.smallWhiteOutlineGameFont.getWidth(textoSplitEspacio[i]);
			}
		}
		
		//calculo cuantas iteraciones se 
		//precisan para cubrir todo el ancho
		//de la pantalla 
		repeticionesNecesarias = (getWidth() / anchoTextoYSeparador) + anchoTextoYSeparador;
		
		textoBlanco = textoSplitColor[0];
		textoColor = (textoSplitColor.length > 1 ? textoSplitColor[1] : null);

		//calculamos el ancho 
		//del texto blanco
		anchoTextoAnterior = Principal.bigWhiteOutlineGameFont.getWidth(textoBlanco);
		
	}

	public void update(long elapsedTime) {
	
		//tiempo de logica de movimiento
		//de texto cumplido, ejecuta accion
		if(movimientoTextoTimer.action(elapsedTime)) {
			
			//restamos el tiempo
			tiempoMovimientoPiezaTextoActual += elapsedTime;
			
			//calculamos el movimiento en pixeles
			//necesarios para hacer la animacion en
			//base al tiempo transcurrido
			movimientoXPixels = tiempoMovimientoPiezaTextoActual * anchoTextoYSeparador / TIEMPO_MOVIMIENTO_PIEZA_TEXTO_MS;
			
			if(tiempoMovimientoPiezaTextoActual >= TIEMPO_MOVIMIENTO_PIEZA_TEXTO_MS) {
				//comenzamos nuevamente la animacion
				tiempoMovimientoPiezaTextoActual = 0;
				movimientoXPixels = anchoTextoYSeparador;
			}
		}
	}

	public void inicializarRecursos() {
		
		try {
			separadorImage = new Image("resources/star.png");
		} catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}
		
		//creamos el timer usado
		//para el movimiento de texto
		movimientoTextoTimer = new Timer(10);
		
	}
}