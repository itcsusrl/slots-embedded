package org.microuy.javaslot.presentacion.modulo;

import org.microuy.javaslot.expepcion.SystemException;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.dominio.EstadoBoton;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class BotonVirtual extends ModuloGenerico {

	private Image botonVirtualImage;
	
	private EstadoBoton estadoBoton;
	
	//variables usadas por la
	//animacion del boton
	private boolean animacionApareciendo;
	public int posicionAnimacion;
	private Timer timerAnimacion = new Timer(400);
	public int ultimoRecorridoPx;
	
	//modifica la altura de lo que 
	//se dibuja en esta clase 
	private int modificadorAltura;
	
	//ancho del texto
	private int anchoTexto;
	private int altoTexto;
	
	private String[] textoTokens;
	private int[] altoTokens;
	private int[] anchoTokens;
	
	public BotonVirtual(EstadoBoton estadoBoton, int x, int y, int width, int height) {
		
		super(null, x, y, width, height);
		this.estadoBoton = estadoBoton;
	}

	public void render(Graphics g) {
		
		//color previo
		Color colorPrevio = g.getColor();
		
		if(Principal.modoConfiguracion) {

			//dibujamos un rectangulo
			g.fillRect(x, y + modificadorAltura + posicionAnimacion, width, height);
		}
		else {
			//dibujamos la imagen
			botonVirtualImage.draw(x, y + modificadorAltura + posicionAnimacion);	
		}
		
		//color del texto
		g.setColor(Color.white);
		
		if(Principal.modoConfiguracion) {
			//dibujamos el texto
			Principal.smallWhiteOutlineGameFont.drawString(x + 10, y + 5 + modificadorAltura + posicionAnimacion, estadoBoton.getTexto());
		}
		else {

			if(textoTokens != null) {
				
				if(textoTokens.length > 1) {
					for(int i=0; i<textoTokens.length; i++) {
						//dibujamos el texto
						Principal.botonVirtualFont.drawString(x + (width / 2) - (anchoTokens[i] / 2), y + (i*altoTokens[i]) + 5 + modificadorAltura + posicionAnimacion, textoTokens[i]);
					}
				}
				else {
					//dibujamos el texto
					Principal.botonBiggerVirtualFont.drawString(x + (width / 2) - (anchoTokens[0] / 2), y + (height / 2) - (altoTokens[0] / 2) + 5 + modificadorAltura + posicionAnimacion, textoTokens[0]);			
				}
			}
		}
		
		//volvemos al color previo
		g.setColor(colorPrevio);
		
	}

	public void update(long elapsedTime) {
		
		//calculo el recorrido a hacer
		//en px desde el principio hasta
		//el final de la animacion
		int recorridoTotalPx = getHeight() - (getHeight()/2);
		
		if(animacionApareciendo) {
			if(posicionAnimacion > 0) {
				
				//si el tiempo se cumplio 
				//forzamos a la posicion
				if(timerAnimacion.action(elapsedTime)) {
					posicionAnimacion = 0;
					ultimoRecorridoPx = 0;
				}
				else {

					//calculamos la posicion
					//de la animacion
					int posicionAnimacionTmp = (int)((timerAnimacion.getCurrentTick() * recorridoTotalPx) / timerAnimacion.getDelay());
					
					//calculamos la diferencia de
					//movimiento entre el ultimo
					//movimiento y el actual
					int diferenciaMovimientoPx = posicionAnimacionTmp - ultimoRecorridoPx;
					
					//movemos el boton 
					//hacia arriba
					posicionAnimacion -= diferenciaMovimientoPx;	
					
					//guardamos el ultimo recorrido
					ultimoRecorridoPx = posicionAnimacionTmp; 

				}
			}
		}
		else {
			if(posicionAnimacion < (getHeight()/2)) {
				
				//si el tiempo se cumplio 
				//forzamos a la posicion
				if(timerAnimacion.action(elapsedTime)) {
					posicionAnimacion = getHeight()/2;
					ultimoRecorridoPx = getHeight()/2;
				}
				else {
					
					//calculamos la posicion
					//de la animacion
					int posicionAnimacionTmp = (int)((timerAnimacion.getCurrentTick() * recorridoTotalPx) / timerAnimacion.getDelay());
					
					//calculamos la diferencia de
					//movimiento entre el ultimo
					//movimiento y el actual
					int diferenciaMovimientoPx = posicionAnimacionTmp - ultimoRecorridoPx;
					
					//movemos el boton 
					//hacia abajo
					posicionAnimacion += diferenciaMovimientoPx;
					
					//guardamos el ultimo recorrido
					ultimoRecorridoPx = posicionAnimacionTmp; 
				}
			}
		}
		
		//nunca dejar que se pase
		if(posicionAnimacion < 0) {
			posicionAnimacion = 0;
		}
	}

	public void inicializarRecursos() {
		
		try {
			botonVirtualImage = new Image("resources/boton_virtual.png");
		} catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}
	}
	
	public void setActivado(boolean estado) {
		
		//cambiamos la animacion
		this.animacionApareciendo = estado;
		this.ultimoRecorridoPx = 0;
		
		//cambiamos el estado 
		//en el objeto EstadoBoton
		getEstadoBoton().setActivado(estado);
		
	}
	
	public void setTexto(String texto) {
		
		//confiugramos el texto
		estadoBoton.setTexto(texto);
		
		//calculamos el  ancho del texto
		anchoTexto = Principal.botonVirtualFont.getWidth(estadoBoton.getTexto());
		altoTexto = Principal.botonVirtualFont.getHeight(estadoBoton.getTexto());
		
		//creamos tokens de la
		//descripcion por espacios
		textoTokens = estadoBoton.getTexto().split("\n");
		anchoTokens = new int[textoTokens.length];
		altoTokens = new int[textoTokens.length];
		
		//calculamos el ancho
		//de cada token
		for(int i=0; i<textoTokens.length; i++) {
			anchoTokens[i] = textoTokens.length == 1 ? Principal.botonBiggerVirtualFont.getWidth(textoTokens[i]) : Principal.botonVirtualFont.getWidth(textoTokens[i]);
			altoTokens[i] = textoTokens.length == 1 ? Principal.botonBiggerVirtualFont.getHeight(textoTokens[i]) : Principal.botonVirtualFont.getHeight(textoTokens[i]);
		}

	}
	
	public EstadoBoton getEstadoBoton() {
		return estadoBoton;
	}

	public void setEstadoBoton(EstadoBoton estadoBoton) {
		this.estadoBoton = estadoBoton;
	}

	public int getModificadorAltura() {
		return modificadorAltura;
	}

	public void setModificadorAltura(int modificadorAltura) {
		this.modificadorAltura = modificadorAltura;
	}
}