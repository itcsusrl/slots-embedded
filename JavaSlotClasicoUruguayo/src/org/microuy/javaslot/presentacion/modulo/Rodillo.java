package org.microuy.javaslot.presentacion.modulo;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.SistemaFrontend;
import org.microuy.javaslot.expepcion.SystemException;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.presentacion.util.FiguraUtil;
import org.microuy.javaslot.presentacion.util.SlotsAnimation;
import org.microuy.javaslot.presentacion.util.SlotsImage;
import org.microuy.javaslot.sonido.ReproductorSonido;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Rodillo extends ModuloGenerico {

	private int id;
	private Rodillos rodillos;
	
	/**
	 * Coleccion de figuras del rodillo
	 */
	private final List<SlotsImage> figuras;
	
	/**
	 * Coleccion de animaciones del rodillo
	 */
	private List<SlotsAnimation> animaciones;
	
	/**
	 * Brillo sobre la figura
	 * cuando hay premio
	 */
	private Animation shineAnimation;
	
	/**
	 * Coleccion de figuras ficticias del rodillo.
	 * Usadas cuando una tirada ficticia debe de
	 * ser mostrada
	 */
	private List<SlotsImage> figurasFicticias;

	/**
	 * Coleccion de animaciones ficticias del rodillo.
	 * Usadas cuando una tirada ficticia debe de
	 * ser mostrada
	 */
	private List<SlotsAnimation> animacionesFicticias;
	
	private int figurasSize;
	private int figuraHeight;
	private int figuraWidth;
	private Timer timerRodillo;
	
	//representa el indice de la figura que se
	//encuentra en primer lugar. El primer lugar
	//se considera aquella que su posicion sea 
	//entre cero y figuraHeight
	private int indiceActual;
	private int offsetActual;
	
	public static final byte ESTADO_DETENIDO = 0;
	public static final byte ESTADO_GIRANDO = 1;
	public static final byte ESTADO_PRE_DETENCION = 2;
	public static final byte ESTADO_DETENIENDO = 3;
	
	//valor de 1 a 10 que indica la velocidad 
	//en la que gira el rodillo, mientras mas
	//alto el numero mas rapido gira
	public final static byte VELOCIDAD_RODILLO = 30;
	
	//estado del rodillo actual
	private byte estado = ESTADO_DETENIDO;
	
	//indice hasta donde debe de llegar el rodillo
	private int indiceFinal;
	
	//pixel hasta donde debe de llegar el rodillo
	private int pixelFinal;
	
	//indica cuantos pixels
	//se van recorriendo para llegar al
	//pixel deseado final (pixelFinal)
	private int recorridoHastaPixelFinal;
	private int velocidadMaxPx = 50;
	private int velocidadMinPx = 10;
	
	/**
	 * Puede ocurrir que la jugada hubiese sido
	 * cancelada para no exceder los limites
	 * de pago de la maquina , y sea necesario  
	 * modificar las figuras del rodillo. Esta
	 * coleccion se encarga de guardar las figuras
	 * de reemplazo para el rodillo 
	 */
	private int[] tiradaFicticia;
	
	/**
	 * Flag usado por la logica relacionada
	 * a la tirada ficticia
	 */
	private boolean esTiradaFicticia = false;
	
	//flag que indica si se muestran
	//animaciones o imagenes fijas
	private boolean[] posicionAnimacionConRecuadro;
	private Color colorRecuadro;
	
	//efecto en las figuras 
	//ganadoras del rodillo
	//public FiguraGanadoraEffect figuraGanadoraEffect;
	
	public int vibracionX;
	public int vibracionY;
	
	public Rodillo(int id, Rodillos rodillos, List<SlotsImage> figuras, List<SlotsAnimation> animaciones, PantallaGenerica pantallaGenerica, int x, int y, int width, int height) {
		
		super(pantallaGenerica, x, y, width, height);
		
		//seteo el id recibido
		this.id = id;
		
		//seteo la instancia de clase Rodillos
		//a esta instancia de Rodillo
		this.rodillos = rodillos;
		
		//seteo la coleccion de imagenes
		this.figuras = figuras;
		this.animaciones = animaciones;
		
		figuraHeight = figuras.get(0).getHeight() + 10;
		figuraWidth = figuras.get(0).getWidth();
		
		//cantidad total de figuras en el rodillo
		figurasSize = figuras.size();
		
		//inicia en un indice aleatorio
		indiceActual = (int)((float)Math.random() * (float)figurasSize);
		
		//inicializa el timer propio del rodillo
		timerRodillo = new Timer(VELOCIDAD_RODILLO);
		
		//inicializamos array que dibuja 
		//animaciones con recuadro
		posicionAnimacionConRecuadro = new boolean[principal.maquina.lineas.get(0).getForma().length+1];
		
		//instanciamos la clase que renderiza
		//el efecto de cuando un jackpot sale
		//figuraGanadoraEffect = new FiguraGanadoraEffect();
		
	}

	public void inicializarRecursos() {
		
		try {
			
			Image shine1 = new Image("resources/shine/shine-01.png");
			Image shine2 = new Image("resources/shine/shine-02.png");
			Image shine3 = new Image("resources/shine/shine-03.png");
			Image shine4 = new Image("resources/shine/shine-04.png");
			Image shine5 = new Image("resources/shine/shine-05.png");
			Image shine6 = new Image("resources/shine/shine-06.png");
			Image shine7 = new Image("resources/shine/shine-07.png");
			Image shine8 = new Image("resources/shine/shine-08.png");
			Image shine9 = new Image("resources/shine/shine-09.png");
			Image shine10 = new Image("resources/shine/shine-10.png");
			Image shine11 = new Image("resources/shine/shine-11.png");
			Image shine12 = new Image("resources/shine/shine-12.png");
			Image shine13 = new Image("resources/shine/shine-12.png");
			
			shineAnimation = new Animation(new Image[]{
													   shine1,
													   shine2,
													   shine3,
													   shine4,
													   shine5,
													   shine6,
													   shine7,
													   shine8,
													   shine9,
													   shine10,
													   shine11,
													   shine12,													   
													   shine13,
													   shine12,
													   shine11,
													   shine10,
													   shine9,
													   shine8,
													   shine7,
													   shine6,
													   shine5,
													   shine4,
													   shine3,
													   shine2
													   }, 
													   60
										   			   );
		} catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}
	}
	
	public void render(Graphics g) {
		
		//chequeo general
		if(estado == ESTADO_DETENIDO && offsetActual > 0) {
			System.err.println("********** ERROR: Rodillo detenido con offsetActual > 0 (" + offsetActual + ")");
		}
		
		//seleccionamos las figuras y 
		//animaciones que deseamos dibujar
		List<SlotsImage> imagenesRenderizar = esTiradaFicticia ? figurasFicticias : figuras;
		List<SlotsAnimation> animacionesRenderizar = esTiradaFicticia ? animacionesFicticias : animaciones;
		
		/**
		 * No dibujamos solamente las tres figuras visibles
		 * del rodillo, sino que dibujamos ademas una cuarta
		 * que se ve cuando el rodillo gira y queda la misma
		 * parcialmente visible
		 */
		for(int i=indiceActual, j=0; i<indiceActual+4; i++, j++) {
			
			/**
			 * Calculo sencillo para chequear que 
			 * figura o animacion debe de ser dibujada 
			 */
			int tmp = i-1;
			if(tmp < 0) {
				tmp = figurasSize-1;
			}
			else if(tmp > (figurasSize-1)) {
				tmp = (i % (figurasSize))-1;
			}
			
			/**
			 * Calculamos la posicion en la coordenada 'y'
			 * donde se van a dibujar las animaciones, las
			 * figuras estaticas, o los efectos de figura
			 */
			int yCalculado = y - figuraHeight + (j*figuraHeight) + offsetActual + vibracionY;
			int xCalculado = x + vibracionX;
			
			/**
			 * Siempre y cuando la posicion del
			 * arreglo sea verdadero, se dibuja
			 * en vez de la imagen estatica, la
			 * animacion
			 */
			if(posicionAnimacionConRecuadro[j]) {

				//obtenemos la animacion
				SlotsAnimation animation = animacionesRenderizar.get(tmp);
				
				if(animation != null) {
					//dibujamos la animacion
					animation.draw(xCalculado, yCalculado);
				}
				
				//configuramos la imagen
				//usada por el efecto
				//figuraGanadoraEffect.setImageName(animation.rutaImagenPrincipal);

				//configuramos la posicion del
				//efecto para cuando se use
				//figuraGanadoraEffect.setCoordinates(xCalculado + figuraWidth/2, yCalculado + figuraHeight/2);
				
				//guardo el color anterior antes
				//de pintar de un nuevo color
				Color colorPrevio = g.getColor();
				
				/**
				 * Tendriamos que reemplazar el
				 * cuadrado horrible este por 
				 * un cuadrado solo con bordes
				 * y vacio en el medio
				 */
				g.setColor(colorRecuadro);
//				g.fillRect(xCalculado, yCalculado, figuraWidth, figuraHeight);
//				shineAnimation.draw(xCalculado, yCalculado, colorRecuadro);
				shineAnimation.draw(xCalculado, yCalculado);
				
				//vuelvo al viejo color
				g.setColor(colorPrevio);

			}
			else {
				
				//dibujamos la imagen estatica
				SlotsImage imagenActual = imagenesRenderizar.get(tmp);
				
				if(imagenActual != null) {
					g.drawImage(imagenActual, xCalculado, yCalculado, null);	
				}
				else {
					System.err.println("Error Imagen: " + tmp);
				}
			}
			
			/**
			 * DEBUG
			 */
			if(SistemaFrontend.MOSTRAR_DEBUG_RODILLO) {

				//Id de figura
				int figuraId = rodillos.principal.maquina.rodillos.get(getId()).getFiguras().get(tmp).getId();
				Principal.bigWhiteOutlineGameFont.drawString(x, y-figuraHeight+(j*figuraHeight)+offsetActual, " F:"+figuraId);

				//Indice de figura
				int indiceFigura = tmp+1;
				String indiceFiguraStr = "I:" + indiceFigura;
				int anchoIndiceFiguraStr = Principal.bigWhiteOutlineGameFont.getWidth(indiceFiguraStr);
				Principal.bigWhiteOutlineGameFont.drawString(x + figuraWidth - anchoIndiceFiguraStr, y-figuraHeight+(j*figuraHeight)+offsetActual, indiceFiguraStr);
			
				//tirada ficticia o real
				String tipoTirada = principal.logicaMaquina.tiradaFicticia != null ? " FT" : " RL";
				int altoTipoTirada = Principal.bigWhiteOutlineGameFont.getHeight(tipoTirada);
				Principal.bigWhiteOutlineGameFont.drawString(x, y-figuraHeight+(j*figuraHeight)+offsetActual-altoTipoTirada, tipoTirada);
				
			}
		}
	}
	
	public void update(long elapsedTime) {
		
		//si esta girando o deteniendose ejecuto la 
		//logica del rodillo con timer incluido
		if(estado == ESTADO_GIRANDO || estado == ESTADO_DETENIENDO || estado == ESTADO_PRE_DETENCION) {
			
			//un timer que se ejecuta cada 10 ms.
			if(timerRodillo.action(elapsedTime)) {
				
				//si el estado actual es girando o previo a detenerse significa
				//que la velocidad actual debe de ser constante (velocidadMaxPx)
				if(estado == ESTADO_GIRANDO || estado == ESTADO_PRE_DETENCION) {
					
					//incremento el offset actual
					offsetActual += velocidadMaxPx;
				}
				else if(estado == ESTADO_DETENIENDO) {
					
					//configuro la velocidad actual (recorrido en pixeles)
					//en base al pixel final a llegar desde el que estamos
					//actualmente posicionados
					float speed = (float)Math.cos(Math.PI / 2f * (float)recorridoHastaPixelFinal / (float)pixelFinal);
					speed *= velocidadMaxPx - velocidadMinPx;
					speed += velocidadMinPx;
					
					//variable temporal para calcular posteriormente
					//cuantos pixeles fueron recorridos desde el
					//movimiento anterior
					int recorridoHastaPixelFinalAnterior = recorridoHastaPixelFinal;
					
					//calculo el avance en pixeles que 
					//hubo durante esta iteracion 
					recorridoHastaPixelFinal += Math.round(speed);
					recorridoHastaPixelFinal = Math.min(recorridoHastaPixelFinal, pixelFinal); //nos aseguramos de que no nos pasamos	
					
					//calculo el movimiento del offset actual
					offsetActual += recorridoHastaPixelFinal - recorridoHastaPixelFinalAnterior;
					
					//si el estado actual es deteniendose debo chequear 
					//si el recorrido de pixeles para llegar al final 
					//fue completado
					if(recorridoHastaPixelFinal >= pixelFinal && estado != ESTADO_DETENIDO) {

						//fix 04012012
						recorridoHastaPixelFinal = pixelFinal;
						
						//vuelvo a dejar el rodillo detenido 
						//por ende sin actividad logica 
						estado = ESTADO_DETENIDO;
						
						//llamo a la funcion callback de Rodillos
						rodillos.finalizaGiroRodilloCallback(id, (byte)indiceActual);
					}
				}

				//una vez que se hicieron los cambios 
				//de posicion pertinentes en el rodillo
				//reconfiguro las variables de indice
				//y posicion
				if(offsetActual >= figuraHeight) {
					
					//decremento el indice actual en 1
					//porque la rueda iria para atras en
					//su movimiento mas normal de arriba
					//hacia abajo
					indiceActual--;
					
					//al set el offset actual mayor o igual
					//a la altura de la figura, debemos de 
					//sumar la diferencia al offset del proximo
					//movimiento (proxima figura)
					offsetActual = offsetActual - figuraHeight;
					
					//si el indice esta por debajo de uno 
					//significa que debemos comenzar nuevamente
					//en la ultima figura 'figurasSize'
					if(indiceActual < 1) {
						indiceActual = figurasSize;
					}
					
					//si el estado es previo a detenerse debemos
					//configurar las variables para que comience
					//la detenencion del rodillo progresivamente
					if(estado == ESTADO_PRE_DETENCION) {
						
						//el offset actual debe setearse a cero para
						//que no queden corridos los graficos de la
						//figura actual en el rodillo
						offsetActual = 0;
						
						//configura el tiempo de la parada 
						//para que sea normal o rapida
						/** IMPORTANTE, LOS CAMBIOS DE INDICE NECESARIOS NO
						 *  PUEDEN EXCEDER LA CANTIDAD DE FIGURAS DEL RODILLO **/
						int cambiosIndiceVelocidad = rodillos.getVelocidadParada() == Rodillos.VELOCIDAD_PARADA_NORMAL ? (figurasSize > 15 ? 15 : figurasSize-1) : (figurasSize > 5 ? 5 : figurasSize-1);
						
						//si nos pasamos de la cantidad total de figuras
						if((indiceFinal+cambiosIndiceVelocidad) > figurasSize) {
							indiceActual = ((indiceFinal+cambiosIndiceVelocidad) % figurasSize)-1;
						}
						else {
							indiceActual = indiceFinal+cambiosIndiceVelocidad-1;
						}
						
						//si el indice es cero significa
						//que es la ultima figura del rodillo
						if(indiceActual == 0) {
							indiceActual = figurasSize;
						}
						
						//calculo cual es el pixel final a ser
						//recorrido para llegar a los cambios 
						//necesarios
						pixelFinal = cambiosIndiceVelocidad * figuraHeight;
						
						//seteo como cero la cantidad de pixeles 
						//recorridos para llegar a igualar pixelFinal
						recorridoHastaPixelFinal = 0;
						
						//durante la predetencion hacemos los
						//cambios necesarios en caso de que 
						//haya una tirada ficticia pronta para
						//ejecutarse
						logicaTiradaFicticia();
						
						//luego de configurar las variables necesarias
						//para la detenecion, cambio el estado a DETENIENDO
						//para la ejecucion de esa funcion
						estado = ESTADO_DETENIENDO;

					}
				}
			}
		}
		else if(estado == ESTADO_DETENIDO) {
		
			/**
			 * Si el rodillo se encuentra detenido
			 */
			//figuraGanadoraEffect.update((int)elapsedTime);
			
		}
	}
	
	/**
	 * Realiza la logica necesaria para 
	 * satisfacer los valores de la tirada
	 * ficticia (en caso de que hubiese)
	 */
	public void logicaTiradaFicticia() {
		
		if(tiradaFicticia != null) {
			
			//creamos la lista de imagenes 
			//y animaciones ficticias
			if(figurasFicticias == null) {
				figurasFicticias = new ArrayList<SlotsImage>();
				animacionesFicticias = new ArrayList<SlotsAnimation>();
			}
			
			//recreamos las listas nuevamente
			figurasFicticias.clear();
			figurasFicticias.addAll(figuras);
			animacionesFicticias.clear();
			animacionesFicticias.addAll(animaciones);
			
			//modificamos los valores necesarios
			for(int i=0, posicionInsercion=indiceFinal; i<tiradaFicticia.length; i++, posicionInsercion++) {
				
				//si nos excedimos del tamano 
				//del rodillo volvemos al inicio
				if(posicionInsercion > figurasSize) {
					posicionInsercion = 1;
				}
				
				//reemplazamos la imagen y la  
				//animacion original por las nuevas
				figurasFicticias.set(posicionInsercion-1, FiguraUtil.obtenerImagenFiguraPorId(tiradaFicticia[i]));
				animacionesFicticias.set(posicionInsercion-1, FiguraUtil.obtenerAnimacionFiguraPorId(tiradaFicticia[i]));
			}
			
			//seteamos el flag necesario para
			//usar animaciones e imagenes ficticias
			esTiradaFicticia = true;
			
		}
		else {
			
			//cancelamos el uso de imagenes
			//y animaciones ficticias
			esTiradaFicticia = false;
		}
	}
	
	/**
	 * Comienza la logica de giro 
	 * de los rodillos.
	 * 
	 * @param indiceFinal
	 * @param tiradaFicticia
	 */
	public void girar(int indiceFinal, int [] tiradaFicticia) {
		
		this.indiceFinal = indiceFinal;
		this.tiradaFicticia = tiradaFicticia;
		
		//una vez que seteo todas las 
		//variables necesarias para girar
		//comienzo el giro
		estado = ESTADO_GIRANDO;
		
	}
	
	public void detener() {
		estado = ESTADO_PRE_DETENCION;
	}
	
	public void detenerInmeditamente() {
		
		if(estado == ESTADO_GIRANDO || estado == ESTADO_DETENIENDO || estado == ESTADO_PRE_DETENCION) {
			estado = ESTADO_DETENIDO;
			offsetActual = 0;
			ReproductorSonido.stopSound(ReproductorSonido.RODILLO_GIRANDO);
		}
	}
	

	public byte getEstado() {
		return estado;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setMostrarAnimacionesConRecuadro(boolean[] posicion, Color color) {
		
		if(posicion != null) {

			//dibujamos el efecto
			//this.figuraGanadoraEffect.renderizar = true;

			for(int i=0; i<posicionAnimacionConRecuadro.length; i++) {
				posicionAnimacionConRecuadro[i] = posicion[i];
			}
			
			shineAnimation.restart();
		}
		else {

			//dejamos de dibujar el efecto
			//this.figuraGanadoraEffect.renderizar = false;

			for(int i=0; i<posicionAnimacionConRecuadro.length; i++) {
				posicionAnimacionConRecuadro[i] = false;
			}
		}
		
		this.colorRecuadro = color;
	}
}