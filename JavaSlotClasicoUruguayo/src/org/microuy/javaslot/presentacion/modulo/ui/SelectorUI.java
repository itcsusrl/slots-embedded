package org.microuy.javaslot.presentacion.modulo.ui;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.dominio.util.Pair;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.modulo.ModuloGenerico;
import org.microuy.javaslot.presentacion.modulo.ui.listener.SelectorUIListener;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.sonido.ReproductorSonido;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;

public class SelectorUI extends ModuloGenerico implements EnfocableIf {

	private List<Pair> contenido;
	public boolean enfocado;
	private Color colorFuente = Color.white;
	
	private Color colorFlechaApretadaIzq;
	private Color colorFlechaApretadaDer;
	
	public int elementoActualIndice;
	public Object elementoActualClave;
	public String elementoActualValor;
	
	private int anchoMinimoNecesario;
	
	private Image flechaIzquierda;
	private Image flechaDerecha;
	
	private static UnicodeFont fuente = Principal.bigWhiteOutlineGameFont;
	
	private Timer cambioColorTimer;
	
	/**
	 * Codigo obtenido de ConfiguracionDAO
	 * para identificar unicamente cada
	 * registro de configuracion
	 */
	public String codigo;
	
	/**
	 * Lista con listeners del selector
	 */
	private List<SelectorUIListener> listeners;	
	
	public SelectorUI(PantallaGenerica pantallaGenerica, int x, int y, String codigo, boolean enfocado) {
		super(pantallaGenerica, x, y, 0, fuente.getHeight("A")+4);
		this.enfocado = enfocado;
		this.codigo = codigo;
		cambioColorTimer = new Timer(0);
		listeners = new ArrayList<SelectorUIListener>();
	}
	
	public void agregarSelectorUIListener(SelectorUIListener listener) {
		listeners.add(listener);
	}
	
	public void agregarElemento(Object clave, String valor) {
		
		//si es el primer elemento que
		//agregamos dejamos ese valor
		//seleccionado
		if(elementoActualValor == null) {
			elementoActualValor = valor;
			elementoActualClave = clave;
			elementoActualIndice = 0;
			reevaluarAnchoTotalValor(valor);
		}
		
		Pair pair = new Pair();
		pair.clave = clave;
		pair.valor = valor;
		contenido.add(pair);
		
		
	}
	
	public void borrarElementos() {
		contenido.clear();
		elementoActualValor = null;
		reevaluarAnchoTotalValor(" ");
	}
	
	private void reevaluarAnchoTotalValor(String valor) {
		
		//reevaluamos el ancho del componenete   
		//en base al nuevo valor recibido
		int anchoNuevoElemento = fuente.getWidth(valor) + 3;
		width = anchoNuevoElemento + 4 + anchoMinimoNecesario;

	}
	
	/**
	 * Se mueve al proximo elemento
	 * de la lista
	 */
	public void proximoElemento() {
		
		//obtenemos la cantidad 
		//de elementos
		int cantidadElementos = contenido.size();
		
		elementoActualIndice++;
		if(elementoActualIndice > cantidadElementos-1) {
			elementoActualIndice = 0;
		}
		
		elementoActualValor = contenido.get(elementoActualIndice).valor.toString();
		elementoActualClave = contenido.get(elementoActualIndice).clave;
		
		//reevaluamos el ancho del componente
		//en base al valor seleccionado
		reevaluarAnchoTotalValor(elementoActualValor);
		
		//logica de presion de tecla 
		presionaFlechaDerecha();
		
		//avisamos a los listeners del
		//cambio de valor
		cambioValorListeners();
		
	}

	
	/**
	 * Se mueve al elemento previo
	 * de la lista
	 */
	public void previoElemento() {

		elementoActualIndice--;
		if(elementoActualIndice < 0) {
			elementoActualIndice = contenido.size()-1;
		}
		
		elementoActualValor = contenido.get(elementoActualIndice).valor.toString();
		elementoActualClave = contenido.get(elementoActualIndice).clave;
		
		//reevaluamos el ancho del componente
		//en base al valor seleccionado
		reevaluarAnchoTotalValor(elementoActualValor);

		//logica de presion de tecla
		presionaFlechaIzquierda();
		
		//avisamos a los listeners del
		//cambio de valor
		cambioValorListeners();
		
	}
	
	private void cambioValorListeners() {
		for(SelectorUIListener listener : listeners) {
			listener.cambiaValorSelectorUI(this);
		}
	}

	public void render(Graphics g) {
		
		//obtenemos el color 
		//de fondo anterior
		Color colorOriginal = g.getColor();
		
		if(enfocado) {
			if(colorFlechaApretadaIzq != null) {
				//dibujamos las flechas con filtro de color
				flechaIzquierda.draw(x, y + (height/2) - (flechaIzquierda.getHeight()/2), 1, colorFlechaApretadaIzq);
			}
			else {
				//dibujamos las flechas
				flechaIzquierda.draw(x, y + (height/2) - (flechaIzquierda.getHeight()/2), 1);
			}
		}
		
		//configuramos el color para 
		//dibujar el texto del color
		//indicado
		g.setColor(colorFuente);
		
		if(elementoActualValor != null) {
			//dibujamos el elemento actual
			fuente.drawString( x + flechaIzquierda.getWidth() + 6, y+2, elementoActualValor);
		}

		if(enfocado) {
			if(colorFlechaApretadaDer != null) {
				//dibujamos las flechas
				flechaDerecha.draw(x + width - flechaDerecha.getWidth(),  y + (height/2) - (flechaDerecha.getHeight()/2), colorFlechaApretadaDer);
			}
			else {
				//dibujamos las flechas
				flechaDerecha.draw(x + width - flechaDerecha.getWidth(),  y + (height/2) - (flechaDerecha.getHeight()/2));
			}
		}
		
		//volvemos al color original
		g.setColor(colorOriginal);
		
	}

	public void update(long elapsedTime) {

		if(cambioColorTimer.action(elapsedTime)) {
			cambioColorTimer.setActive(false);
			colorFlechaApretadaIzq = null;
			colorFlechaApretadaDer = null;
		}
	}

	public void inicializarRecursos() {

		contenido = new ArrayList<Pair>();
		
		try {
			flechaDerecha = new Image("resources/flecha_derecha.gif");
			flechaIzquierda = new Image("resources/flecha_izquierda.gif");
		} catch (SlickException e) {
			e.printStackTrace();
		}
		
		if(height < flechaIzquierda.getHeight()) {
			height = flechaIzquierda.getHeight();
		}
		
		//cargamos los sonidos necesarios
		ReproductorSonido.cargarSonidos(new int[]{ReproductorSonido.CLICK});
		
		//calculamos el ancho minimo necesario
		anchoMinimoNecesario = flechaDerecha.getWidth() + flechaIzquierda.getWidth() + 10;
		width = anchoMinimoNecesario;
		
	}

	public void presionaFlechaIzquierda() {
	
		cambioColorTimer.setActive(true);
		ReproductorSonido.playSound(ReproductorSonido.CLICK);
		cambioColorTimer.setDelay(150);
		cambioColorTimer.setCurrentTick(0);
		colorFlechaApretadaIzq = Color.orange;
	}

	public void presionaFlechaDerecha() {
		
		cambioColorTimer.setActive(true);
		ReproductorSonido.playSound(ReproductorSonido.CLICK);
		cambioColorTimer.setDelay(150);
		cambioColorTimer.setCurrentTick(0);
		colorFlechaApretadaDer = Color.orange;
	}
	
	public boolean isEnfocado() {
		return enfocado;
	}

	public void setEnfocado(boolean enfocado) {
		this.enfocado = enfocado;
	}
	
	public void mostrarClavePorDefecto(Object clave) {
		
		for(int i=0; i<contenido.size(); i++) {
			Pair pair = contenido.get(i);
			if(pair.clave.toString().equals(clave.toString())) {
				elementoActualClave = pair.clave;
				elementoActualIndice = i;
				elementoActualValor = pair.valor.toString();
				
				reevaluarAnchoTotalValor(elementoActualValor);
				break;
			}
		}
	}
}