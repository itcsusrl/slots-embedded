package org.microuy.javaslot.presentacion.constante;

public class Pantalla {

	//id's pantallas
	public final static int PANTALLA_MENU_PRESENTACION = 0;
	public final static int PANTALLA_TABLA_PAGOS = 1;
	public final static int PANTALLA_JUEGO = 2;
	public final static int PANTALLA_MENU_CONFIGURACION_BASICA = 4;
	public final static int PANTALLA_MENU_PRINCIPAL = 5;
	public final static int PANTALLA_MENU_JACKPOT = 6;
	public final static int PANTALLA_MENU_CONFIGURACION_AVANZADA = 7;
	public final static int PANTALLA_MENU_RESUMEN_COBRANZA = 8;
	public final static int PANTALLA_MENU_RECUPERACION_DINERO = 9;
	public final static int PANTALLA_MENU_MENOR_MAYOR = 10;
	public final static int PANTALLA_AYUDA_PREMIOS_NORMALES = 11;
	public final static int PANTALLA_AYUDA_DESCRIPTIVA = 12;
	public final static int PANTALLA_FICHA_PERSONAJE = 13;
	public final static int PANTALLA_MENU_PAGO_EXCEDIDO = 14;

	public final static int PANTALLA_TEST = 1000;

	//estado pantallas
	public final static int ESTADO_NO_INICIADA = 0;
	public final static int ESTADO_INICIADA = 1;

	//tiempo de espera de la pantalla de inicio
	public final static int TIEMPO_ESPERA_PANTALLA_INICIO = 3000;
	
}
