package org.microuy.javaslot.presentacion.constante;

public class Cuadro {

	//cuadros
	public final static byte NACIONAL = 0;
	public final static byte PENAROL = 1;

	public static String obtenerDescripcionCuadro(byte cuadro) {
		
		String desc = null;
		
		switch(cuadro) {
		case NACIONAL:
			desc = "Nacional";
			break;
		case PENAROL:
			desc = "Pe�arol";
			break;
		}
		
		return desc;
	}
}