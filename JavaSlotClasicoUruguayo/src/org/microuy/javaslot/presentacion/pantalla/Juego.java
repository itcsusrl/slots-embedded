package org.microuy.javaslot.presentacion.pantalla;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.comm.EjecutorComando;
import org.microuy.javaslot.comm.listener.ComandoEvent;
import org.microuy.javaslot.comm.listener.ComandoListener;
import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.constante.Comando;
import org.microuy.javaslot.constante.EstadoMaquina;
import org.microuy.javaslot.constante.JugadaTipo;
import org.microuy.javaslot.constante.ModoExpulsionMoneda;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.JugadaGanadoraBonus;
import org.microuy.javaslot.dominio.JugadaGanadoraJackpot;
import org.microuy.javaslot.dominio.JugadaLineaGanadora;
import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.expepcion.SystemException;
import org.microuy.javaslot.license.LicenseValidator;
import org.microuy.javaslot.logica.DistribuidorPremios;
import org.microuy.javaslot.logica.LogicaMaquina;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.dominio.EstadoBoton;
import org.microuy.javaslot.presentacion.event.RodillosEvent;
import org.microuy.javaslot.presentacion.event.RodillosListener;
import org.microuy.javaslot.presentacion.modulo.Escritorio;
import org.microuy.javaslot.presentacion.modulo.ManejadorEscritorio;
import org.microuy.javaslot.presentacion.modulo.MinijuegoMenorMayor;
import org.microuy.javaslot.presentacion.modulo.MuestraJugadasGanadoras;
import org.microuy.javaslot.presentacion.modulo.MuestraLineas;
import org.microuy.javaslot.presentacion.modulo.PanelBotonesVirtual;
import org.microuy.javaslot.presentacion.modulo.Rodillos;
import org.microuy.javaslot.presentacion.modulo.listener.MuestraJugadaLineaGanadoraListener;
import org.microuy.javaslot.presentacion.modulo.listener.vo.MuestraJugadaLineaGanadoraVO;
import org.microuy.javaslot.sonido.ReproductorSonido;
import org.microuy.javaslot.textoAnimado.efectos.TextoItineranteEffect;
import org.microuy.javaslot.util.MathUtil;
import org.microuy.javaslot.util.SlotsRandomGenerator;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class Juego extends PantallaGenerica implements RodillosListener, MuestraJugadaLineaGanadoraListener, ComandoListener {

	
	/**
	 * Define los posibles estados de
	 * esta pantalla
	 */
	public static final byte ESTADO_JUEGO_DETENIDO = 0;
	public static final byte ESTADO_JUEGO_GIRANDO = 1;
	public static final byte ESTADO_JUEGO_PREMIANDO = 2;
	public static final byte ESTADO_JUEGO_PREMIACION_FINALIZADA = 3;
	
	/**
	 * Define el estado en el que se encuentra
	 * esta pantalla particularmente
	 */
	public byte estado = ESTADO_JUEGO_DETENIDO;
	
	/**
	 * Variables varias necesarias
	 */
	public MuestraLineas muestraLineas;
	public MuestraJugadasGanadoras muestraJugadasGanadoras;
	public Rodillos rodillos;
	public MinijuegoMenorMayor minijuegoMenorMayor;
	
	private TextoItineranteEffect textoZoomIn;
	private TextoItineranteEffect tiradaGratuitaZoomIn;
	public TextoItineranteEffect premiacionMenorMayorZoomIn;
	
	//utilitario para formateo de decimales
	private DecimalFormat decimalFormat = new DecimalFormat("#.##");
	private DecimalFormat integerFormat = new DecimalFormat("#");
	
	//el logo del juego
	public Image logoImage;
	
	//imagen de la red
	private Image fondoImage;
	
	/**
	 * Es el monto del jugador, pero para ser
	 * usado en la logica de la capa de presentacion.
	 * Difiere con la variable 'montoJugador' de la
	 * clase 'Maquina' en que el de esta clase puede
	 * contener valores invalidos mientras se suma
	 * el monto de la variable 'montoJugadorSumarUI'
	 * a esta variable
	 * 
	 */
	private float montoJugadorUI;
	
	/**
	 * Variables relacionadas a 
	 * tiradas gratuitas
	 */
	public boolean ultimaTiradaGratuita;
	
	private int tiradasGratuitas;
	
	/**
	 * El monto total entregado a traves de
	 * tiradas gratuitas (bonus)
	 */
	private float montoEntragadoTiradasGratuitasUI;
	private float animacionMontoGratuito;
	
	//Indica cual fue el ultimo boton presionado
	private byte ultimoBotonPresionado = -1;
	
	/**
	 * Variables de modulo 'MostrarJugadasGanadoras'
	 */
	private long montoJugadaTiempoTotal;
	private float montoJugadaGanadoraActual;
	private float animacionMonto;
	
	//para acelerar el acceso
	//a variables muy usadas
	public Maquina maquina;
	public LogicaMaquina logicaMaquina;

	
	/**
	 * Cantidad de lineas apostadas
	 * en la ultima apuesta
	 */
	public short ultimaApuestaLineasApostadas;

	/**
	 * Cantidad de dinero apostado por
	 * linea en la ultima apuesta
	 */
	public float ultimaApuestaDineroApostadoPorLinea;

	
	//vo de la jugada ganadora actual
	private MuestraJugadaLineaGanadoraVO muestraJugadaGanadoraVO;
	
	/**
	 * Modulo que administra y  
	 * renderiza los escritorios
	 */
	public ManejadorEscritorio manejadorEscritorio;
	
	/**
	 * Contiene la lista con jugadas
	 * ganadoras de la ultima tirada
	 */
	public List<JugadaLineaGanadora> jugadasLineasGanadoras;
	public int[][] tiradaActualValores;
	
	/**
	 * Cuenta la cantidad de tiradas
	 * en modo demo se van haciendo
	 */
	private int tiradasDemo;
	
	/**
	 * Constructor del juego
	 * @param principal
	 */
	public Juego(Principal principal) {

		super(Pantalla.PANTALLA_JUEGO, principal);

		//inicializamos maquina y logicamaquina
		maquina = principal.maquina;
		logicaMaquina = principal.maquina.logicaMaquina;
		
		//lo agrego como listener 
		//de los comandos
		EjecutorComando.agregarComandoListener(this);
		
	}
	
	/**
	 * Inicializa los escritorios y todos los
	 * componentes que lo forman
	 */
	private void inicializarEscritorios() {
		
		//instanciamos el manejador 
		//de escritorios
		manejadorEscritorio = new ManejadorEscritorio(this);
		
		//posicion de los elementos del Escritorio 1
		int rodillosWidth = Rodillos.calcularAnchoRodillos(principal.maquina.rodillos.size());
		int rodillosHeight = 500;
		int rodillosX = (principal.getWidth() / 2) - (rodillosWidth / 2);
		int rodillosY = 200;
		
		//posicion de los elementos del Escritorio 2
		int minijuegoWidth = principal.getWidth();
		int minijuegoHeight = principal.getHeight();
		int minijuegoX = (principal.getWidth() / 2) - (minijuegoWidth / 2);
		int minijuegoY = 0;
		
		//creo la lista de rodillos que van
		//a ser dibujados en pantalla
		rodillos = new Rodillos(this, rodillosX, rodillosY, rodillosWidth, rodillosHeight);
		
		//creo el modulo que muestra las jugadas ganadoras
		muestraJugadasGanadoras = new MuestraJugadasGanadoras(this, rodillosX, rodillosY, rodillosWidth, rodillosHeight);
		muestraJugadasGanadoras.agregarMuestraJugadaLineaGanadoraVOListener(this);
		
		//creo el modulo que muestra las lineas
		muestraLineas = new MuestraLineas(this, rodillosX, rodillosY, rodillosWidth, rodillosHeight);
		
		//creo el modulo que muestra 
		//el minijuego 'menor o mayor'
		minijuegoMenorMayor = new MinijuegoMenorMayor(this, minijuegoX, minijuegoY, minijuegoWidth, minijuegoHeight);
		
		//creamos el escritorio del 
		//juego de slots (rodillos)
		Escritorio escritorio1 = new Escritorio(this, ManejadorEscritorio.ESCRITORIO_SLOTS);
		escritorio1.agregarComponente(rodillos);
		escritorio1.agregarComponente(muestraJugadasGanadoras);
		escritorio1.agregarComponente(muestraLineas);
		
		//creamos el escritorio del
		//minijuego 'menor o mayor'
		Escritorio escritorio2 = new Escritorio(this, ManejadorEscritorio.ESCRITORIO_MINIJUEGO_MENOR_MAYOR);
		escritorio2.agregarComponente(minijuegoMenorMayor);
		
		//agregamos los escritorios
		manejadorEscritorio.agregarEscritorio(escritorio1, true);
		manejadorEscritorio.agregarEscritorio(escritorio2, false);
		
		//inicializamos los recursos 
		//de cada escritorio
		manejadorEscritorio.inicializarRecursos();
		
	}
	

	/**
	 * Dibuja el juego en pantalla
	 */
	protected void renderizar(Graphics g) {
		
		//posicion en x del 
		//escritorio de slots
		int escritorioSlotsX = manejadorEscritorio.escritorios.get(ManejadorEscritorio.ESCRITORIO_SLOTS).getX();
		
		//pinto el fondo
		g.setColor(Color.white);
		
		//cielo de fondo
		fondoImage.draw();
		
		//renderizamos el manejador 
		//de escritorios, incluyendo
		//sus animaciones y todas sus
		//funcionalidades graficas
		manejadorEscritorio.renderizar(g);
		
		//solamente mostramos las jugadas
		//ganadoras si el estado del juego
		//es 'premiando'
		if(estado == ESTADO_JUEGO_PREMIANDO || estado == ESTADO_JUEGO_PREMIACION_FINALIZADA) {
			//efecto de texto con zoom 
			//usado en la premiacion
			textoZoomIn.render(g);
		}
		
		//renderizacion relacionada
		//a la tirada gratuita y al
		//minijuego menor mayor
		tiradaGratuitaZoomIn.render(g);
		premiacionMenorMayorZoomIn.render(g);
		
		if(!principal.demo) {
			
			//renderizamos la data 
			//de las apuestas y el
			//credito del jugador
			Principal.extraSmallBlueSkyOutlineGameFont.drawString(escritorioSlotsX+480, 95, "L�NEAS");
			Principal.smallBlueskyOutlineGameFont.drawString(escritorioSlotsX+554, 86, ""+principal.maquina.lineasApostadas);
			
			Principal.extraSmallBlueSkyOutlineGameFont.drawString(escritorioSlotsX+465, 125, "APUESTA");
			Principal.smallBlueskyOutlineGameFont.drawString(escritorioSlotsX+554, 116, decimalFormat.format(principal.maquina.dineroApostadoPorLinea * Sistema.FACTOR_CONVERSION_MONEDA));

			Principal.extraSmallBlueSkyOutlineGameFont.drawString(escritorioSlotsX+480, 155, "TOTAL");
			Principal.smallBlueskyOutlineGameFont.drawString(escritorioSlotsX+554, 146, decimalFormat.format(principal.maquina.dineroApostadoPorLinea * principal.maquina.lineasApostadas * Sistema.FACTOR_CONVERSION_MONEDA));

			//renderizamos los creditos
			//del jugador
			Principal.mediumLargeWhiteOutlineGameFont.drawString(escritorioSlotsX + 650, 110, "CR�DITOS");
			
			float montoJugadorUITmp = montoJugadorUI;
			if(montoJugadorUITmp < Sistema.INCREMENTO_APUESTA && montoJugadorUI != 0) {
				montoJugadorUITmp = Sistema.INCREMENTO_APUESTA;
			}
			
			Principal.extraBigShadowOutlineGameFont.drawString(escritorioSlotsX + 820, 90, integerFormat.format((montoJugadorUITmp + animacionMonto) * Sistema.FACTOR_CONVERSION_MONEDA));
		}
	}
	
	//constantes de la 
	//animacion del logo
	private final static int ROTACION_LOGO_MS = 2000;
	private final static int STANDBY_LOGO_MS = 30000;
	
	//timers usados para la
	//rotacion del logo
	private Timer rotacionLogoTickTimer = new Timer(10);
	private Timer rotacionLogoTimer = new Timer(ROTACION_LOGO_MS);
	
	//timer de tirada de demo
	private Timer tiradaDemoTimer = new Timer(2000);
	
	//estado de la animacion
	//del logo, detenida o
	//girando
	private boolean rotacionLogoEstadoDetenido;

	/**
	 * Anima el logo para que realice 
	 * varios giros sobre su propio 
	 * centro, se detenga un tiempo
	 * determinado y luego haga lo 
	 * mismo
	 */
	private void animarLogo(long elapsedTime) {
		
		if(!rotacionLogoEstadoDetenido) {

			//tick de la animacion 
			if(rotacionLogoTickTimer.action(elapsedTime)) {

				//calculamos la rotacion 
				//necesaria en base al
				//tiempo del tick
				float rotacion = (elapsedTime * 1800f) / ROTACION_LOGO_MS;
				
				//rotamos la imagen de las 
				//luces para dar una animacion
				logoImage.rotate(rotacion);
			}
		}
		
		//tiempo total cumplido para
		//dejar al logo en su posicion
		//original
		if(rotacionLogoTimer.action(elapsedTime)) {

			//seteo de la rotacion a cero
			logoImage.setRotation(0f);
			
			//cambio de estado
			rotacionLogoEstadoDetenido = !rotacionLogoEstadoDetenido;

			//si estaba detenido cambiamos el timer 
			if(rotacionLogoEstadoDetenido) {
				
				//calculamos un tiempo aleatorio para
				//adicionar a la constante STANDBY_LOGO_MS.
				//Esta aleatoriedad va a ser usada para dar
				//mayor probabilidad a los jugadores que 
				//apuestan mientras esta girando el logo.
				//Algo simple pero le da un toque 
				int tiempoStandbyAleatorio = (int)(SlotsRandomGenerator.getInstance().nextFloat() * 90) * 1000;
				
				//mientras el logo no gira
				//las posibilidades son las
				//definidas en la maquina
				DistribuidorPremios.modificadorPorcentajeEntregapremios = 0;
				
				rotacionLogoTimer.setDelay(STANDBY_LOGO_MS + tiempoStandbyAleatorio);
				rotacionLogoTimer.setCurrentTick(0);
			}
			else {
				
				//mientras el logo esta girando
				//el jugador tiene mas posibilidades
				//de ganar un premio
				DistribuidorPremios.modificadorPorcentajeEntregapremios = 10;
				
				rotacionLogoTimer.setDelay(ROTACION_LOGO_MS);
				rotacionLogoTimer.setCurrentTick(0);
			}
		}
	}
	
	/**
	 * Logica del juego
	 */
	protected void actualizar(long elapsedTime) {
		
		/**
		 * Hacemos un chequeo por si
		 * cantidad de lineas o dinero
		 * por linea estan en cero
		 */
		if(rodillos.getEstado() == Rodillos.ESTADO_DETENIDO) {
			if(maquina.lineasApostadas == 0) {
				maquina.lineasApostadas = 1;
			}
			if(maquina.dineroApostadoPorLinea == 0) {
				maquina.setDineroApostadoPorLinea(Sistema.INCREMENTO_APUESTA);
			}
		}
		
		//solo si nos encontramos premiando
		if(estado == ESTADO_JUEGO_PREMIANDO) {
			
			//efecto de texto con zoom
			textoZoomIn.update(elapsedTime);

			//realiza la logica relacionada 
			//al cambio de valores del monto
			logicaAnimacionMonto(elapsedTime);
		}
		
		//si existen tiradas gratuitas
		if(tiradasGratuitas > 0) {
			
			//si el juego esta detenido o premiando
			//ejecutamos logica de tirada automatica
			if(estado == ESTADO_JUEGO_DETENIDO || estado == ESTADO_JUEGO_PREMIACION_FINALIZADA) {
				
				//hacemos la evaluacion para 
				//hacer las tiradas gratuitas
				//correspondientes
				evaluacionTiradaGratuita();
				
			}
		}
		
		//logica relacionada a las
		//tiradas gratuitas y al
		//minijuego menor mayor
		tiradaGratuitaZoomIn.update(elapsedTime);
		premiacionMenorMayorZoomIn.update(elapsedTime);
		
		//actualizamos la logica de los
		//componentes de los escritorios
		manejadorEscritorio.actualizar(elapsedTime);
		
		//animamos el logo para
		//que gire
		animarLogo(elapsedTime);
		
		/**
		 * Logica solo relacionada a la
		 * maquina en estado de demo
		 */
		if(principal.demo) {
			
			//si los rodillos estan detenidos
			//y no se esta premiando, entonces
			//realizamos una tirada ganadora
			if(estado == ESTADO_JUEGO_DETENIDO || estado == ESTADO_JUEGO_PREMIACION_FINALIZADA) {
				
				if(tiradaDemoTimer.action(elapsedTime)) {

					//realizamos una apuesta 
					//en modo demo
					logicaMaquina.apostarSlots(true);
					
					//prepara la tirada para 
					//visualizar la apuesta 
					//que se acaba de realizar
					prepararTirada();
				}
				
				if(estado == ESTADO_JUEGO_PREMIACION_FINALIZADA) {

					//dejamos de mostrar las
					//lineas ganadoras
					detienePremiacion();
					
					//incrementamos la cantidad
					//de tiradas demo que se van
					//haciendo
					tiradasDemo++;
					
					if(tiradasDemo > 4) {
						
						//reseteamos el contador
						tiradasDemo = 0;
						
						//volvemos al menu 
						//de presentacion
						principal.cambiarPantalla(Pantalla.PANTALLA_MENU_PRESENTACION, new FadeOutTransition(), new FadeInTransition());
					}
				}
			}
		}
	}
	
	/**
	 * Logica relacionada a montos del juego, tanto
	 * del jugador como los montos de jugadas
	 * gratuitas
	 * 
	 * @param elapsedTime
	 */
	private void logicaAnimacionMonto(long elapsedTime) {
		
		/**
		 * Logica relacionada al monto del jugador
		 */
		float tmp = 0f;
		
		//chequeo para que 'tmp' no
		//divida entre cero y quede
		//NaN
		if(montoJugadaTiempoTotal > 0) {
			tmp = (float)MathUtil.roundTwoDecimals(((float)elapsedTime * montoJugadaGanadoraActual) / (float)montoJugadaTiempoTotal);	
		}
		
		if(animacionMonto <= montoJugadaGanadoraActual) {
			
			//vamos agregando al monto  
			//del jugador el monto ganador
			animacionMonto = (float)MathUtil.roundTwoDecimals(animacionMonto + tmp);

			//si la jugada es jackpot, animamos
			//el monto del pozo jackpot
			if(muestraJugadaGanadoraVO != null && muestraJugadaGanadoraVO.getJugadaLineaGanadora().getTipo() == JugadaTipo.JACKPOT) {
				
				//decrementamos el monto visible
				//en pantalla del jackpot
				principal.montoJackpotUI -= tmp;
				
				if(principal.montoJackpotUI < 0) {
					principal.montoJackpotUI = 0;
				}
			}
		}
		else {
			//detenemos el sonido de la premiacion
			ReproductorSonido.stopSound(ReproductorSonido.DINERO_GANADO);
		}
		
		//logica de animacion de monto gratuito
		if(ultimaTiradaGratuita) {
			float tmpGrt = (((float)elapsedTime * montoJugadaGanadoraActual) / (float)montoJugadaTiempoTotal);
			if((animacionMontoGratuito + tmpGrt) <= montoJugadaGanadoraActual) {
				animacionMontoGratuito += tmpGrt;
			}
		}
		
		/**
		 * Chequeo de limites
		 */
		if((montoJugadorUI + animacionMonto) >  maquina.montoJugador) {
			ReproductorSonido.stopSound(ReproductorSonido.DINERO_GANADO);
			animacionMonto = 0;
			montoJugadorUI = maquina.montoJugador;
		}
	}
	
	/**
	 * Inicializa los recursos de esta pantalla
	 */
	protected void inicializarRecursos() {

		try {
			logoImage = new Image("resources/LOGO_big-17.png").getScaledCopy(0.5f);
			fondoImage = new Image("resources/game_BG_sky.png");
		} catch (SlickException e) {
			throw new SystemException(e.getMessage());
		}
		
		//inicializamos los escritorios
		inicializarEscritorios();

		//agrego a los rodillos un nuevo listener
		rodillos.agregarRodillosListener(this);

		//creamos el efecto del texto con zoom
		textoZoomIn = new TextoItineranteEffect(Principal.extraBigShadowOutlineGameFont, "", principal.getWidth(), principal.getHeight(), new Color(24, 116, 205, 200));
		
		//lo mismo que lo anterior pero para 
		//los textos de tiradas gratuitas y
		//el minijuego menor mayor
		tiradaGratuitaZoomIn = new TextoItineranteEffect(Principal.extraBigShadowOutlineGameFont, "", principal.getWidth(), principal.getHeight(), Color.black);
		premiacionMenorMayorZoomIn = new TextoItineranteEffect(Principal.extraBigShadowOutlineGameFont, "", principal.getWidth(), principal.getHeight(), new Color(51, 172, 63, 200));
		
		//cargamos los sonidos necesarios
		ReproductorSonido.cargarSonidos(new int[]{ReproductorSonido.DINERO_GANADO, ReproductorSonido.PITIDO, ReproductorSonido.BONUS, ReproductorSonido.SCATTER});
		//y la musica tambien
		ReproductorSonido.cargarMusica(new int[]{ReproductorSonido.MUSICA_GAME_OVER});
		
	}
	
	/**
	 * Recalcula si el jugador participa
	 * en la premiacion del jackpot en base
	 * al monto apostado
	 */
	private void recalcularParticipacionJackpot() {
		
		//obtenemos los datos de la apuesta
		//para cerciorarnos de que es una
		//apuesta maxima
		short lineasApostadas = maquina.lineasApostadas;
		float dineroApostadoPorLinea = maquina.dineroApostadoPorLinea;
		float apuestaActualUsuario = lineasApostadas * dineroApostadoPorLinea;
		
		//si el usuario hizo la apuesta
		//minima para participar en el jackpot
		// se le toma un porcentaje de la 
		//apuesta para participar por el JACKPOT
		if(apuestaActualUsuario >= Sistema.JACKPOT_APERTURA_POZO || (tiradasGratuitas > 0 && apuestaActualUsuario >= Sistema.JACKPOT_APERTURA_POZO)) {
		
			//configuramos el texto del
			//jackpot a mostrar en la 
			//barra informativa
			String textoInfo = "JACKPOT @" + integerFormat.format(principal.montoJackpotUI * Sistema.FACTOR_CONVERSION_MONEDA);	

			//refrescamos la info
			principal.mostrarInfo(textoInfo);

		}
		else {
			
				
			//configuramos el texto del
			//jackpot a mostrar en la 
			//barra informativa
			String textoInfo = "JACKPOT @0";	

			//refrescamos la info
			principal.mostrarInfo(textoInfo);

		}
	}
	
	/**
	 * Cuando un boton de la maquina es 
	 * presionado, esta logica es ejecutada
	 * inmediatamente debido al uso de 
	 * interrupciones
	 */
	public void botonPresionado(byte botonId) {
		
		if(manejadorEscritorio.escritorioActivoId == ManejadorEscritorio.ESCRITORIO_SLOTS) {

			/**
			 * Boton 'Efectuar Pago'.
			 * Solo paga cuando los rodillos
			 * no estan girando. Podria llegar
			 * a quedar en un estado invalido
			 * la maquina mientras paga y
			 * gana un premio
			 */
			if(botonId == Boton.BOTON1 && rodillos.getEstado() == Rodillos.ESTADO_DETENIDO) {
				
				//enviamos comando de pago
				int monedasAPagar = (int)MathUtil.roundTwoDecimals(maquina.montoJugador / Sistema.APUESTA_ADMITIDA);

				/**
				 * Si la licencia del sistema fue
				 * validada correctamente entonces
				 * cambiamos el modo de expulsion
				 * a PAGO_JUGADOR. 
				 * Esto lo realizamos porque se puede
				 * tornar realmente confuso detectar
				 * una falla en el sistema de este tipo
				 */
				if(LicenseValidator.licenciaValida) {
					//cambiamos el modo de expulsion 
					//de moneda para que sea modo pago
					//de jugador
					EjecutorComando.cambiarModoExpulsionMoneda(ModoExpulsionMoneda.MODO_EXPULSION_MONEDA_PAGO_JUGADOR);
				}
				else {
					//ACA CAGAMOS LA LOGICA DEL
					//SISTEMA PARA TORNAR DIFICIL
					//LA DETECCION DE LA NO 
					//VALIDACION DEL SISTEMA
					EjecutorComando.cambiarModoExpulsionMoneda(ModoExpulsionMoneda.MODO_EXPULSION_MONEDA_ADMINISTRADOR);
				}
				
				//ejecutamos el comando para
				//que vaya a la placa y prenda
				//el motor
				EjecutorComando.efectuarPago(monedasAPagar);
				
				//creamos la lista contenedora 
				//del estado de los botones
				List<EstadoBoton> estadoBotones = new ArrayList<EstadoBoton>();
				estadoBotones.add(new EstadoBoton(Boton.BOTON1, "", false));
				estadoBotones.add(new EstadoBoton(Boton.BOTON2, "", false));
				estadoBotones.add(new EstadoBoton(Boton.BOTON3, "", false));
				estadoBotones.add(new EstadoBoton(Boton.BOTON4, "", false));
				estadoBotones.add(new EstadoBoton(Boton.BOTON5, "", false));
				estadoBotones.add(new EstadoBoton(Boton.BOTON6, "", false));
				principal.configurarEstadoBotones(estadoBotones);
				return;
				
			}
			
			//si existen tiradas gratuitas por 
			//ejecutar, o esta por lanzarse un
			//minijuego menor mayor no permito 
			//tocar ningun boton
			if(tiradasGratuitas == 0 && muestraJugadasGanadoras.jugadaLineaGanadoraBonusMenorMayor == null)  {
				
				//flag que indica si durante
				//la ejecucion actual de este
				//metodo fue ejecutada la apuesta
				boolean ejecutaApuesta = false;
				
				// si el jugador tiene credito para jugar
				if (principal.getEstado() == EstadoMaquina.JUGANDO) {
					
					//array contenedor de lineas
					//a apostar, posiblemente 
					//modificado por el apostador
					short[] lineas = null;
					
					//cantidad de lineas apostadas
					int cantidadLineas = maquina.lineasApostadas;
					
					//dinero apostado por linea
					float dineroPorLinea = maquina.dineroApostadoPorLinea;
					
					//monto real del jugador
					float montoJugador = maquina.montoJugador;
					
					//maximo de lineas
					int maximoLineas = maquina.lineas.size();

					/**
					 * Si el estado del juego es 'premiando'
					 * y cualquier boton fue presionado, 
					 * detenemos la premiacion
					 */
					if(estado == ESTADO_JUEGO_PREMIANDO || estado == ESTADO_JUEGO_PREMIACION_FINALIZADA) {
						//detenemos la premiacion
						detienePremiacion();
					}

					/**
					 * Apuesta Maxima
					 */
					if (botonId == Boton.BOTON2 && rodillos.getEstado() == Rodillos.ESTADO_DETENIDO) {

						//configuro la maxima cantidad 
						cantidadLineas = maximoLineas;
						dineroPorLinea = Sistema.APUESTA_MAXIMA_POR_LINEA;
						
						//recalcula la participacion
						//por el pozo acumulado
						recalcularParticipacionJackpot();

					}
					
					/**
					 * Incrementamos el monto apostado por linea
					 */
					if (botonId == Boton.BOTON3 && rodillos.getEstado() == Rodillos.ESTADO_DETENIDO) {
						
						//incrementamos el monto
						logicaMaquina.incrementarApuesta();
						
						//configuramos las variables locales
						cantidadLineas = maquina.lineasApostadas;
						dineroPorLinea = maquina.dineroApostadoPorLinea;
						
						//recalcula la participacion
						//por el pozo acumulado
						recalcularParticipacionJackpot();
						
					}
					
					/**
					 * Si se incrementa la cantidad de lineas apostadas debo de
					 * verificar que la cantidad de lineas no se pase del maximo
					 */
					if (botonId == Boton.BOTON4 && rodillos.getEstado() == Rodillos.ESTADO_DETENIDO) {
						
						//incrementamos las lineas apostadas
						logicaMaquina.incrementarLineas();

						//recreamos el array de lineas apostadas
						lineas = new short[maquina.lineasApostadas];
						for(int i=1; i<=lineas.length; i++) {lineas[i-1] = (short)i;}
						
						//mostramos las lineas
						muestraLineas.mostrarLineas(lineas, MuestraLineas.TIPO_ENCENDIDO_PRENDIDO_UNA_VEZ, 3000);
					
						//configuramos las variables locales
						cantidadLineas = maquina.lineasApostadas;
						dineroPorLinea = maquina.dineroApostadoPorLinea;
						
						//recalcula la participacion
						//por el pozo acumulado
						recalcularParticipacionJackpot();
						
					}

					/**
					 * Boton AYUDA
					 */
					if(botonId == Boton.BOTON5) {
						principal.cambiarPantalla(Pantalla.PANTALLA_AYUDA_PREMIOS_NORMALES, new FadeOutTransition(), new FadeInTransition());
						return;
					}
					
					/**
					 * Boton 'Efectuar Apuesta'
					 */
					if(botonId == Boton.BOTON6) {
						
						//configuramos las variables locales
						cantidadLineas = maquina.lineasApostadas;
						dineroPorLinea = maquina.dineroApostadoPorLinea;
					}
					
					/**
					 * Debemos hacer un chequeo de limites
					 * para asegurarnos de que no nos pasamos
					 * en la cantidad de dinero apostado
					 */
					if(MathUtil.roundTwoDecimals(cantidadLineas * dineroPorLinea) > montoJugador) {
						
						boolean montoEncontrado = false;
						
						//recorremos la las lineas
						//de adelante hacia atras
						for(int i=maximoLineas; i>0; i--) {
						
							//recorremos el monto saltando
							//entre vuelta por apuesta minima
							int cantidadIteraciones = (int)(Sistema.APUESTA_MAXIMA_POR_LINEA / Sistema.INCREMENTO_APUESTA);
							for(int j=cantidadIteraciones; j>0; j--) {

								/**
								 * Calculamos el monto actual en base
								 * a la cantidad de lineas actuales (i) 
								 * y al monto por linea actual 
								 * (j * Sistema.INCREMENTO_APUESTA)
								 */
								float montoActual = (float)MathUtil.roundTwoDecimals((((float)j) * Sistema.INCREMENTO_APUESTA) * ((float)i));
								
								//chequeamos si ya satisfacemos 
								//las necesidas del jugador
								if(montoActual <= montoJugador) {
									
									cantidadLineas = i;
									dineroPorLinea = (float)MathUtil.roundTwoDecimals(((float)j) * Sistema.INCREMENTO_APUESTA);
									
									montoEncontrado = true;
									break;
								}
							}
							
							if(montoEncontrado) {
								break;
							}
						}
						
						//fix 04012012 por las dudas que
						//no se hubiera encontrado el valor
						//adecuado
						if(!montoEncontrado) {
							cantidadLineas = 1;
							dineroPorLinea = maquina.montoJugador;
						}
					}

					/**
					 * Solo cambiamos los valores de la 
					 * apuesta del jugador cuando los
					 * rodillos se encuentran detenidos
					 */
					if(rodillos.getEstado() == Rodillos.ESTADO_DETENIDO) {
						
						// seteo las variables fundamentales
						// previo a ejecutar la apuesta
						maquina.setDineroApostadoPorLinea(dineroPorLinea);
						maquina.lineasApostadas = (short)cantidadLineas;
					}
					
					/**
					 * Si cualquier boton de apuesta fue presionado
					 */
					if ((botonId == Boton.BOTON2 || botonId == Boton.BOTON6) && estado == ESTADO_JUEGO_DETENIDO) 
					{	
						
						/**
						 * Si el ultimo boton presionado
						 * no fue igual al de apuesta, pero
						 * el boton actual es de apuesta, 
						 * debemos confirmar con el jugador
						 * que desea apostar. Lo hacemos mostrando
						 * las lineas a apostar previo a la apuesta
						 * final
						 */
						if(botonId != ultimoBotonPresionado && ultimoBotonPresionado != Boton.BOTON4) {
							
							//obtengo la cantidad de lineas apostadas
							//para mostrar en pantalla cada vez que
							//se agrega una
							lineas = new short[cantidadLineas];
							for(int i=1; i<=cantidadLineas; i++) {lineas[i-1] = (short)i;}
							
							//mostramos las lineas
							muestraLineas.mostrarLineas(lineas, MuestraLineas.TIPO_ENCENDIDO_PRENDIDO_UNA_VEZ, 3000);
							
						}
						else {
							
							/**
							 * Si un boton de apuesta fue presionado
							 * y el juego se encuentra detenido, 
							 * ejecutamos la siguiente apuesta
							 */
							if (estado == ESTADO_JUEGO_DETENIDO && (rodillos.getEstado() == Rodillos.ESTADO_DETENIDO || rodillos.getEstado() == Rodillos.ESTADO_ANIMANDO)) {

								maquina.setDineroApostadoPorLinea(dineroPorLinea);
								maquina.lineasApostadas = (short)cantidadLineas;

								//ejecutamos la apuesta
								apostar();

								//cambiamos el flag
								ejecutaApuesta = true;

							}
						}
					}

					// TODO REMOVIDO TEMPORALMENTE reconfiguro el estado de los botones
					//configurarEstadoBotones();

				}

				if(estado == ESTADO_JUEGO_GIRANDO) {
					
					//solo detenemos el giro de los
					//rodillos si la apuesta fue
					//ejecutada en una presion de 
					//boton anterior
					if(!ejecutaApuesta) {
						//si el estado de los rodillos 
						//es girando y un boton fue 
						//presionado detenemos los rodillos
						rodillos.detenerRodillos(Rodillos.VELOCIDAD_PARADA_RAPIDO, 50);
					}
				}
				else {
					
					//setea el id del boton presionado
					//NOTA: No consideramos una presion
					//de boton normal cuando los rodillos
					//se encuentran girando, por esa razon
					//este codigo se encuentra en el else
					ultimoBotonPresionado = botonId;
				}
			}
			else { //si se encuentra ejecutando
	  			   //apuestas gratuitas (bonus)
				
				//si el jugador tiene credito para jugar
				if(principal.getEstado() == EstadoMaquina.JUGANDO) {
					
					if(rodillos.getEstado() != Rodillos.ESTADO_DETENIDO) {
						rodillos.detenerRodillos(Rodillos.VELOCIDAD_PARADA_RAPIDO, 50);
					}
					else if(estado == ESTADO_JUEGO_PREMIANDO && tiradasGratuitas > 0) {
						estado = ESTADO_JUEGO_DETENIDO;
					}
					else if(muestraJugadasGanadoras.jugadaLineaGanadoraBonusMenorMayor != null) {
						cambiarEscritorio();
					}
				}
			}
		}
		else if(manejadorEscritorio.escritorioActivoId == ManejadorEscritorio.ESCRITORIO_MINIJUEGO_MENOR_MAYOR) {
			
			//ejecutamos la logica de 
			//botones del minijuego
			minijuegoMenorMayor.botonPresionado(botonId);
			
		}
	}
	
	/**
	 * Cambia entre el escritorio 
	 * del minijuego y el de slots
	 * configurando todo lo necesario
	 */
	public void cambiarEscritorio() {
		
		if(manejadorEscritorio.escritorioActivoId == ManejadorEscritorio.ESCRITORIO_SLOTS) {

			//cambiamos al escritorio del minijuego
			manejadorEscritorio.cambiarEscritorio(ManejadorEscritorio.ESCRITORIO_MINIJUEGO_MENOR_MAYOR);	
		}
		else if(manejadorEscritorio.escritorioActivoId == ManejadorEscritorio.ESCRITORIO_MINIJUEGO_MENOR_MAYOR) {
			
			//cambiamos al escritorio de slots
			manejadorEscritorio.cambiarEscritorio(ManejadorEscritorio.ESCRITORIO_SLOTS);
		}
	}
	
	private void apostar() {
		
		//necesitamos una nueva 
		//confirmacion de apuesta
		ultimoBotonPresionado = -1;
		
		//ocultamos las lineas
		muestraLineas.ocultarLineas();
		
		//reiniciamos el valor de
		//la variable monto ui
		montoJugadorUI = maquina.montoJugador;	
		principal.montoJackpotUI = logicaMaquina.distribuidorPremios.montoDistribuidoJackpot;
		
		//reiniciamos el valor de la variable 
		//del monto entregado gratuitamente
		montoEntragadoTiradasGratuitasUI = logicaMaquina.montoEntragadoTiradasGratuitas;
		
		//obtengo la cantidad de tiradas
		//gratuitas previas a la apuesta
		int cantidadTiradasGratis = logicaMaquina.cantidadTiradasGratis;
		
		//si la cantidad de tiradas gratis
		//es cero significa que debemos de
		//cobrar la apuesta
		if(cantidadTiradasGratis == 0) {
			
			//restamos el costo de la apuesta
			montoJugadorUI -= maquina.dineroApostadoPorLinea * maquina.lineasApostadas;
			montoJugadorUI = (float)MathUtil.roundTwoDecimals(montoJugadorUI);
			
			//volvemos a falso el flag
			//que indica si la ultima
			//tirada fue gratuita
			ultimaTiradaGratuita = false;
			
		}
		else {
			
			//seteamos un flag para indicar
			//que la ultima apuesta fue 
			//gratuita
			ultimaTiradaGratuita = true;
			
		}
		
		/**
		 * Apuesta
		 */
		logicaMaquina.apostarSlots(false);

		/**
		 * Deshabilitamos todos los botones
		 */
		List<EstadoBoton> estadoBotones = new ArrayList<EstadoBoton>();
		estadoBotones.add(new EstadoBoton(Boton.BOTON1, "", false));
		estadoBotones.add(new EstadoBoton(Boton.BOTON2, "", true));
		estadoBotones.add(new EstadoBoton(Boton.BOTON3, "", true));
		estadoBotones.add(new EstadoBoton(Boton.BOTON4, "", true));
		estadoBotones.add(new EstadoBoton(Boton.BOTON5, "", false));
		estadoBotones.add(new EstadoBoton(Boton.BOTON6, "", true));
		principal.configurarEstadoBotones(estadoBotones);
		
		//prepara la tirada para 
		//visualizar la apuesta 
		//que se acaba de realizar
		prepararTirada();
		
	}
	
	/**
	 * Prepara las variables necesarias
	 * para efectuar visualmente la
	 * ejecucion de la apuesta
	 */
	private void prepararTirada() {
		
		/**
		 * Guardamos en variables independientes
		 * a la logica de la maquina la cantidad
		 * de lineas y el monto apostado por linea
		 * en la apuesta actual
		 */
		ultimaApuestaLineasApostadas = logicaMaquina.maquina.lineasApostadas;
		ultimaApuestaDineroApostadoPorLinea = logicaMaquina.maquina.dineroApostadoPorLinea;

		//lista contenedora de las 
		//jugadas ganadoras
		jugadasLineasGanadoras = logicaMaquina.jugadasLineasGanadoras;
				
		tiradaActualValores = logicaMaquina.tiradaActualValores;
		
		//Obtengo la tirada actual desde la logica de la maquina.
		//Estos valores representa los indices en los que se deben
		//de posicionar los rodillos para llegar a la combinacion 
		//deseada. En el caso que este arreglo sea nulo, significa
		//que la jugada fue cancelada por alguna razon, y que debe
		//de usarse el array 'tiradaFicticia' para posicionar
		//los rodillos en posiciones especificas
		int[] tiradaActualIndices = logicaMaquina.tiradaActualIndices;
		
		//Este arreglo bidimensional es usado cuando se desea que no
		//se respeten el orden de las figuras en los rodillos debido
		//a que un premio fue cancelado. De esta manera se logra que
		//se entreguen premios especificos con un algoritmo sencillo
		//de creacion del premio
		int[][] tiradaFicticia = logicaMaquina.tiradaFicticia;
		
		//cambiamos el estado
		estado = ESTADO_JUEGO_GIRANDO;		
		
		//giro los rodillos en la ui
		rodillos.girarRodillos(tiradaActualIndices, tiradaFicticia);

		//configuramos el texto del
		//jackpot a mostrar en la 
		//barra informativa
		String textoInfo = "JACKPOT @" + integerFormat.format(principal.montoJackpotUI * Sistema.FACTOR_CONVERSION_MONEDA);	

		//refrescamos la info
		principal.mostrarInfo(textoInfo);

	}
	
	/**
	 * Detiene de una manera ordenada todo
	 * lo que refiere a animaciones y
	 * timings usados durante la premiacion
	 */
	public void detienePremiacion() {
		
		//cambiamos el estado del juego
		estado = ESTADO_JUEGO_DETENIDO;
		
		//reiniciamos las variables para
		//para toda la animacion de premiacion
		montoJugadorUI = maquina.montoJugador;
		principal.montoJackpotUI = logicaMaquina.distribuidorPremios.montoDistribuidoJackpot;
		
		//reiniciamos el valor de la variable 
		//del monto entregado gratuitamente
		montoEntragadoTiradasGratuitasUI = logicaMaquina.montoEntragadoTiradasGratuitas;

		montoJugadaGanadoraActual = 0 ;
		animacionMonto = 0;
		animacionMontoGratuito = 0;
		
		muestraJugadasGanadoras.ocultarJugadasLineasGanadoras();
		muestraLineas.mostrarLineas(new short[]{}, MuestraLineas.TIPO_ENCENDIDO_APAGADO, 500);
		textoZoomIn.finalizar();
		rodillos.ocultarAnimacionesConRecuadro();

		//si existia un sonido 
		//de premacion lo detenemos
		ReproductorSonido.stopSound(ReproductorSonido.DINERO_GANADO);
		
		//reconfiguramos el estado de los botones
		configurarEstadoBotones();
		
	}
	
	/**
	 * Llamado cuando un evento generado 
	 * por los rodillos es escuchado
	 */
	public void procesarEventoRodillos(RodillosEvent rodillosEvent) {
		
		/**
		 * Si los rodillos pararon, evaluamos la apuesta hecha
		 */
		if (rodillosEvent.getTipoEvento() == RodillosEvent.TIPO_EVENTO_PARADO) {
			
			// chequeo el estado monetario del jugador
			// para mantenerlo jugando o sacarlo
			if (!principal.demo && maquina.montoJugador == 0 && tiradasGratuitas == 0 && !maquina.logicaMaquina.bonusMenorMayorDisponible) {
				
				//ejecuta la finalizacion
				//de la partida
				finalizarPartida();
				
			} 
			else {
				
				// paramos el juego de luces
				principal.detenerJuegoLuces();
				
				// configuramos nuevamente el estado de los botones
				configurarEstadoBotones();

				//seteamos la cantidad de tiradas
				//gratuitas restantes del jugador
				tiradasGratuitas = logicaMaquina.cantidadTiradasGratis;
				
				//si existe alguna jugada ganadora
				//comenzamos el proceso para mostrar
				//las lineas que ganaron
				if(jugadasLineasGanadoras.size() > 0) {
					
					//cambiamos el estado
					estado = ESTADO_JUEGO_PREMIANDO;
					
					//mostramos las jugadas ganadoras
					muestraJugadasGanadoras.mostrarJugadasGanadoras();
					
				}
				else {
					
					//cambiamos el estado
					estado = ESTADO_JUEGO_DETENIDO;

				}
			}
		} 
		else if (rodillosEvent.getTipoEvento() == RodillosEvent.TIPO_EVENTO_GIRANDO) {

			// comenzamos el juego de luces
			principal.comenzarJuegoLuces((byte) 0);
			
			//ya empezamos la musica de finalizacion
			//cuando arrancar a girar los rodillos y
			//sabemos que el jugador no tiene mas creditos
			if(maquina.montoJugador == 0 && !principal.demo) {
				//reproducimos tema de salida
				ReproductorSonido.playMusic(ReproductorSonido.MUSICA_GAME_OVER, 5000, true);
			}
		}
	}
	
	public void finalizarPartida() {
		
		if(principal.getEstado() == EstadoMaquina.JUGANDO) {

			//volvemos a modo demo
			principal.demo = true;
			
			//cambiamos el flag de ingreso
			//de dinero asi no se inicia
			//la cuenta regresiva nuevamente
			MenuPresentacion menuPresentacion = (MenuPresentacion)principal.getState(Pantalla.PANTALLA_MENU_PRESENTACION);
			menuPresentacion.flagIngresoDinero = false;
			
			//comenzamos el juego de luces
			//debido a que termina el juego
			principal.comenzarJuegoLuces((byte) 0);
			
			if(!principal.demo) {
				//reproducimos tema de salida
				ReproductorSonido.playMusic(ReproductorSonido.MUSICA_GAME_OVER, 0, true);
			}
			
			//dejamos la maquina en espera 
			//por el proximo jugador
			principal.setEstado(EstadoMaquina.EN_ESPERA);
			
			//reiniciamos el valor de la variable 
			//del monto entregado gratuitamente
			montoEntragadoTiradasGratuitasUI = 0;
			logicaMaquina.montoEntragadoTiradasGratuitas = 0;
			
			//cambiamos el estado
			estado = ESTADO_JUEGO_DETENIDO;
			principal.cambiarPantalla(Pantalla.PANTALLA_MENU_PRESENTACION, new FadeOutTransition(), new FadeInTransition());

		}
	}
	
	/**
	 * Chequea si es necesario ejecutar
	 * la logica relacionada a las 
	 * tiradas gratuitas
	 */
	public void evaluacionTiradaGratuita() {
		
		//chequeamos por tiradas gratuitas
		if(tiradasGratuitas > 0) {
			//ejecutamos la apuesta
			apostar();
		}
	}
	
	/**
	 * Entramos a la pantalla del juego
	 */
	public void entraEnPantalla() {
		
		//mostramos el panel de 
		//botones virtuales
		principal.panelBotonesVirtual.mostrarRapido();

		//configuramos el estado de los botones
		configurarEstadoBotones();

//		//configuramos la apuesta actual 
//		//con los valores de la apuesta
//		//maxima y evaluamos si el jugador
//		//cuenta con ese monto, sino 
//		//reducimos la apuesta hasta donde 
//		//pueda pagar el jugador
//		maquina.setDineroApostadoPorLinea(Sistema.APUESTA_MAXIMA_POR_LINEA);
//		maquina.lineasApostadas = (short)maquina.lineas.size();
		logicaMaquina.reconfigurarPosiblesApuestas();
		
		//recalcula la participacion
		//por el pozo acumulado
		recalcularParticipacionJackpot();
		
		//usamos como credito la variable
		//montoJugadorUI para poder hacer
		//juegos visuales con el monto
		montoJugadorUI = maquina.montoJugador;
		principal.montoJackpotUI = logicaMaquina.distribuidorPremios.montoDistribuidoJackpot;
		
		//reiniciamos el valor de la variable 
		//del monto entregado gratuitamente
		montoEntragadoTiradasGratuitasUI = logicaMaquina.montoEntragadoTiradasGratuitas;

		//para que no aparezcan premios 
		//generados durante la demo
		if(jugadasLineasGanadoras != null) {
			jugadasLineasGanadoras.clear();
			muestraJugadasGanadoras.ocultarJugadasLineasGanadoras();
		}
		
		muestraLineas.ocultarLineas();
		muestraJugadasGanadoras.ocultarJugadasLineasGanadoras();
		rodillos.detenerRodillosInmediatamente();
		
		//cambiamos al estado correspondiente
		estado = ESTADO_JUEGO_DETENIDO;
		
	}
	
	/**
	 * Salimos de la pantalla del juego. No necesariamente
	 * significa que el juego se haya terminado.
	 */
	public void saleDePantalla() {
		
		ReproductorSonido.stopSound(ReproductorSonido.RODILLO_GIRANDO);
		
	}

	/**
	 * Configura el estado de los botones para esta pantalla basado en varios
	 * parametros, como el credito del jugador.
	 */
	public void configurarEstadoBotones() {

		//lista de estado de los botones
		List<EstadoBoton> estadoBotones = new ArrayList<EstadoBoton>(); 
		
		if(manejadorEscritorio.escritorioActivoId == ManejadorEscritorio.ESCRITORIO_SLOTS) {

			/**
			 * Solo procesamos los botones
			 * cuando el panel se encuentra
			 * visible
			 */
			if(principal.panelBotonesVirtual.estado == PanelBotonesVirtual.ESTADO_PANEL_VISIBLE ||
			   principal.panelBotonesVirtual.estado == PanelBotonesVirtual.ESTADO_PANEL_APARECIENDO) {
				
				//si el jugador tiene creditos
				if (maquina.montoJugador > 0) {

					// Habilitacion boton efectuar pago
					if (maquina.montoJugador >= Sistema.APUESTA_ADMITIDA) {
						estadoBotones.add(new EstadoBoton(Boton.BOTON1, "PAGAME!!!", true));
					}
					
					// Habilitacion boton apuesta maxima siempre
					// esta habilitado debido a que siempre se
					// puede apostar todo
					estadoBotones.add(new EstadoBoton(Boton.BOTON2, "APUESTA\nMAXIMA", true));

					/**
					 * Habilitacion boton incrementar apuesta
					 */
					estadoBotones.add(new EstadoBoton(Boton.BOTON3, "SUBIR\nAPUESTA", true));

					/**
					 * Habilitacion boton incrementar lineas
					 */
					estadoBotones.add(new EstadoBoton(Boton.BOTON4, "SUBIR\nLINEAS", true));

					/**
					 * Habilitacion boton ayuda
					 */
					estadoBotones.add(new EstadoBoton(Boton.BOTON5, "AYUDA", true));

					/**
					 * Habilitacion boton efectuar apuesta
					 */
					estadoBotones.add(new EstadoBoton(Boton.BOTON6, "APOSTAR", true));

				}
			}
		}
		else if(manejadorEscritorio.escritorioActivoId == ManejadorEscritorio.ESCRITORIO_MINIJUEGO_MENOR_MAYOR) {
			
			estadoBotones.add(new EstadoBoton(Boton.BOTON1, "VOLVER", true));
			estadoBotones.add(new EstadoBoton(Boton.BOTON2, "", false));
			estadoBotones.add(new EstadoBoton(Boton.BOTON3, "", false));
			estadoBotones.add(new EstadoBoton(Boton.BOTON4, "MENOR", true));
			estadoBotones.add(new EstadoBoton(Boton.BOTON5, "", false));
			estadoBotones.add(new EstadoBoton(Boton.BOTON6, "MAYOR", true));
		}

		// ejecuto el cambio de estado en hardware
		principal.configurarEstadoBotones(estadoBotones);

	}

	/**
	 * Llamado por el modulo 'MuestraJugadasGanadoras' cada
	 * vez que se muestra en pantalla una linea ganadora 
	 * con su respectiva animacion
	 */
	public void mostrandoJugadaGanadora(long tiempo, MuestraJugadaLineaGanadoraVO muestraJugadaGanadoraVO) {
		
		//seteamos la referencia al VO
		this.muestraJugadaGanadoraVO = muestraJugadaGanadoraVO;
		
		/**
		 * Solo ejecutamos la logica de este metodo
		 * si el estado del juego es 'premiando'
		 */
		if(estado == ESTADO_JUEGO_PREMIANDO) {
			
			String texto = null;
			
			if(muestraJugadaGanadoraVO.getJugadaLineaGanadora().getTipo() == JugadaTipo.NORMAL || muestraJugadaGanadoraVO.getJugadaLineaGanadora().getTipo() == JugadaTipo.WILD) {
				
				int multiplicador = muestraJugadaGanadoraVO.getJugadaLineaGanadora().getJugadaGanadora().multiplicador;
				
				//el tiempo de la animacion es el
				//tiempo que se muestra el cartel 
				//menos un valor especificado
				montoJugadaTiempoTotal = tiempo-500;
				
				//agregamos al monto del jugador
				//el monto ganado en la ejecucion
				//anterior de este metodo
				montoJugadorUI += montoJugadaGanadoraActual;
				
				//si la ultima tirada fue gratuita
				//debemos de sumar a la variable que
				//indica el monto total regalado el
				//monto de la ultima apuesta gratuita
				if(ultimaTiradaGratuita) {
					montoEntragadoTiradasGratuitasUI += montoJugadaGanadoraActual;
				}
				
				//el monto de la jugada ganadora que
				//hizo que se ejecutra este metodo
				montoJugadaGanadoraActual = multiplicador * ultimaApuestaDineroApostadoPorLinea;
				
				//la animacion arranca de cero
				animacionMonto = 0;
				animacionMontoGratuito = 0;
				
				//creamos el texto a 
				//mostrar en el cartel 
				texto = multiplicador + " x " + decimalFormat.format(ultimaApuestaDineroApostadoPorLinea * Sistema.FACTOR_CONVERSION_MONEDA) + " = " + decimalFormat.format((montoJugadaGanadoraActual * Sistema.FACTOR_CONVERSION_MONEDA));

				//comenzamos el sonido de la premiacion
				ReproductorSonido.stopSound(ReproductorSonido.DINERO_GANADO);
				ReproductorSonido.playSound(ReproductorSonido.DINERO_GANADO);

			}
			else if(muestraJugadaGanadoraVO.getJugadaLineaGanadora().getTipo() == JugadaTipo.BONUS) {
			
				//obtenemos el multiplicador
				//de la jugada scatter
				JugadaGanadoraBonus jugadaGanadoraBonus = (JugadaGanadoraBonus)muestraJugadaGanadoraVO.getJugadaLineaGanadora().getJugadaGanadora();
				
				//creamos el texto a 
				//mostrar en el cartel 
				texto = "La CELESTE te regala " + jugadaGanadoraBonus.getTiradasGratis() + " tiradas!!!";
			
				//comenzamos el sonido de la premiacion
				ReproductorSonido.playSound(ReproductorSonido.BONUS);

			}
			else if(muestraJugadaGanadoraVO.getJugadaLineaGanadora().getTipo() == JugadaTipo.SCATTER) {

				int multiplicador = muestraJugadaGanadoraVO.getJugadaLineaGanadora().getJugadaGanadora().multiplicador;
				
				//el tiempo de la animacion es el
				//tiempo que se muestra el cartel 
				//menos un valor especificado
				montoJugadaTiempoTotal = tiempo-500;
				
				//agregamos al monto del jugador
				//el monto ganado en la ejecucion
				//anterior de este metodo
				montoJugadorUI += montoJugadaGanadoraActual;
				
				//si la ultima tirada fue gratuita
				//debemos de sumar a la variable que
				//indica el monto total regalado el
				//monto de la ultima apuesta gratuita
				if(ultimaTiradaGratuita) {
					montoEntragadoTiradasGratuitasUI += montoJugadaGanadoraActual;
				}
				
				//el monto de la jugada ganadora que
				//hizo que se ejecutara este metodo
				montoJugadaGanadoraActual = multiplicador * ultimaApuestaDineroApostadoPorLinea;
				
				//la animacion arranca de cero
				animacionMonto = 0;
				animacionMontoGratuito = 0;

				//creamos el texto a 
				//mostrar en el cartel 
				texto = "El MAESTRO te regala " + decimalFormat.format(multiplicador * ultimaApuestaDineroApostadoPorLinea * Sistema.FACTOR_CONVERSION_MONEDA) + " creditos!!!";
			
				//comenzamos el sonido de la premiacion
				ReproductorSonido.playSound(ReproductorSonido.SCATTER);

				//comenzamos el sonido de la premiacion
				ReproductorSonido.stopSound(ReproductorSonido.DINERO_GANADO);
				ReproductorSonido.playSound(ReproductorSonido.DINERO_GANADO);

			}
			else if(muestraJugadaGanadoraVO.getJugadaLineaGanadora().getTipo() == JugadaTipo.BONUS_MENOR_MAYOR) {

				//creamos el texto a 
				//mostrar en el cartel 
				texto = " *** MINIJUEGO MENOR O MAYOR *** ";
			
				//comenzamos el sonido de la premiacion
				ReproductorSonido.playSound(ReproductorSonido.PITIDO);

			}
			else if(muestraJugadaGanadoraVO.getJugadaLineaGanadora().getTipo() == JugadaTipo.JACKPOT) {
				
				//el tiempo de la animacion es el
				//tiempo que se muestra el cartel 
				//menos un valor especificado
				montoJugadaTiempoTotal = tiempo-500;
				
				//agregamos al monto del jugador
				//el monto ganado en la ejecucion
				//anterior de este metodo
				montoJugadorUI += montoJugadaGanadoraActual;
				
				//si la ultima tirada fue gratuita
				//debemos de sumar a la variable que
				//indica el monto total regalado el
				//monto de la ultima apuesta gratuita
				if(ultimaTiradaGratuita) {
					montoEntragadoTiradasGratuitasUI += montoJugadaGanadoraActual;
				}
				
				//el monto de la jugada ganadora que
				//hizo que se ejecutara este metodo
				montoJugadaGanadoraActual = ((JugadaGanadoraJackpot)muestraJugadaGanadoraVO.getJugadaLineaGanadora().getJugadaGanadora()).monto;
				
				//la animacion arranca de cero
				animacionMonto = 0;
				animacionMontoGratuito = 0;
				
				//creamos el texto a 
				//mostrar en el cartel 
				texto = "Te ganaste el POZO de " + integerFormat.format(montoJugadaGanadoraActual) + " $ !!!";
			
				//comenzamos el sonido de la premiacion
				ReproductorSonido.playSound(ReproductorSonido.SCATTER);

				//comenzamos el sonido de la premiacion
				ReproductorSonido.stopSound(ReproductorSonido.DINERO_GANADO);
				ReproductorSonido.playSound(ReproductorSonido.DINERO_GANADO);

			}
			
			//reiniciamos la animacion
			textoZoomIn.reiniciarCentrado(tiempo, texto);

		}
	}
	
	public void finalizaPremiacion() {
		
		estado = ESTADO_JUEGO_PREMIACION_FINALIZADA;

		//reiniciamos las variables para
		//para toda la animacion de premiacion
		montoJugadorUI = maquina.montoJugador;
		principal.montoJackpotUI = logicaMaquina.distribuidorPremios.montoDistribuidoJackpot;
		
		//reiniciamos el valor de la variable 
		//del monto entregado gratuitamente
		montoEntragadoTiradasGratuitasUI = logicaMaquina.montoEntragadoTiradasGratuitas;

		montoJugadaGanadoraActual = 0 ;
		animacionMonto = 0;
		animacionMontoGratuito = 0;
		
	}

	public void procesarEventoComando(ComandoEvent comandoEvent) {
		
		if(comandoEvent.getComandoId() == Comando.COMANDO_VERIFICAR_PAGO_MONEDA) {
			
			//obtenemos el modo de expulsion de 
			//moneda que genera este comando
			byte modoExpulsionMoneda = (Byte)comandoEvent.getGenerico();
			
			if(modoExpulsionMoneda == ModoExpulsionMoneda.MODO_EXPULSION_MONEDA_PAGO_JUGADOR) {
				
				//igualamos al valor del monto
				//de jugador de la logica 
				montoJugadorUI = logicaMaquina.maquina.montoJugador;

				//evaluamos el estado del 
				//juego dado el monto actual
				if(logicaMaquina.maquina.montoJugador == 0) {
					finalizarPartida();
				}
				else {
					if(logicaMaquina.maquina.montoJugador < Sistema.APUESTA_ADMITIDA) {

						//reconfiguramos los botones
						configurarEstadoBotones();

						//mostramos el panel
						principal.panelBotonesVirtual.mostrarRapido();

						//recalcula la participacion
						//por el pozo acumulado
						recalcularParticipacionJackpot();

					}
				}
			}
		}
		else if(comandoEvent.getComandoId() == Comando.COMANDO_DINERO_INGRESADO) {
			
			//recalcula la participacion
			//por el pozo acumulado
			recalcularParticipacionJackpot();
			
			//igualamos al valor del monto
			//de jugador de la logica 
			montoJugadorUI = logicaMaquina.maquina.montoJugador;

			//reconfiguramos los botones
			configurarEstadoBotones();
			
			//paramos la musica de salida
			//en caso de que este sonando
			ReproductorSonido.stopMusic(ReproductorSonido.MUSICA_GAME_OVER, true);

		}
		else if(comandoEvent.getComandoId() == Comando.COMANDO_DINERO_INGRESADO) {
			
			//obtengo la cantidad de monedas
			//que todavia quedan por pagar
			int monedasPago = (Integer)comandoEvent.getGenerico();
			
			//igualamos el monto del jugador
			//en la UI con respecto al monto
			//faltante de pago de la maquina
			montoJugadorUI = monedasPago * Sistema.APUESTA_ADMITIDA;
			animacionMonto = 0;
			animacionMontoGratuito = 0;
				
		}
	}
}