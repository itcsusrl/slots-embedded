package org.microuy.javaslot.presentacion.pantalla;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.comm.EjecutorComando;
import org.microuy.javaslot.comm.listener.BotonListener;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.pantalla.listener.BotonSinEstadoListener;
import org.microuy.javaslot.presentacion.pantalla.listener.PantallaListener;
import org.microuy.javaslot.presentacion.util.StringUtil;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public abstract class PantallaGenerica extends BasicGameState implements BotonListener, PantallaListener {

	protected int id;
	protected boolean inicializado;
	public Principal principal;

	//contenedor de la descripcion procesada
	//para que entre horizontalmente en la 
	//pantalla
	private List<String> infoProcesadaList = new ArrayList<String>();
	
	//la fuente usada para 
	//mostrar la descripcion 
	private UnicodeFont infoFont;

	//color de fondo de la info
	private Color colorFondoInfo = new Color(0,0,0,150);

	//titulo de la pantalla, se dibuja
	//en la parte superior de la pantalla
	//y alineado al medio
	protected String titulo;
	
	//usado cuando queremos procesar
	//botones que no estan habilitados
	//directamente para el usuario 
	private BotonSinEstadoListener botonSinEstadoListener;

	public PantallaGenerica(int id, Principal principal) {
		
		this.id = id;
		this.principal = principal;
		
		//referenciamos la fuente que 
		//usada por la descripcion
		infoFont = Principal.mediumWhiteOutlineGameFont;

	}
	
	protected void addBotonSinEstadoListener(BotonSinEstadoListener botonSinEstadoListener) {
		this.botonSinEstadoListener = botonSinEstadoListener;
	}
	
	/**
	 * Metodos a implementar por superclases
	 */
	protected abstract void inicializarRecursos();
	protected abstract void renderizar(Graphics g);
	protected abstract void actualizar(long elapsedTime);
	protected abstract void botonPresionado(byte botonId);

	public int getId() {
		return id;
	}
	
    public int getID() {
        return id;
    }

	public void setId(int id) {
		this.id = id;
	}
	
	public void init(GameContainer arg0, StateBasedGame arg1) throws SlickException {
		
		inicializar();
	}
	
	private void inicializar() {
		
		inicializado = true;
		EjecutorComando.agregarBotonListener(this);
		inicializarRecursos();
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		
		if(inicializado) {
		
			//renderizado especifico de la pantalla
			renderizar(g);

			//si existe un titulo definido
			//lo renderizamos
			if(titulo != null && !titulo.trim().equals("")) {
				
				//calculamos el ancho del titulo
				int anchoTitulo = Principal.extraBigShadowOutlineGameFont.getWidth(titulo);
				
				//renderizamos el titulo
				Principal.extraBigShadowOutlineGameFont.drawString(principal.getWidth()/2 - anchoTitulo/2, 70, titulo);
			}
			
			//renderizamos info si hay
			renderizarInfo(g);
			
			//renderizo cosas generales del sistema
			principal.renderizar(gc, sbg, g);

			//tenemos que hacerlo para 
			//poder poner el logo girando
			//arriba de la franja negra
			//del jackpot
			if(this instanceof Juego) {
				
				Juego juego = (Juego)this;
				if(!principal.demo) {
					//renderizamos el logo
					g.drawImage(juego.logoImage, 20, 75);
				}
				else {
					//renderizamos el logo
					g.drawImage(juego.logoImage, 330, 55);
				}
			}
		}
	}
	
	/**
	 * Renderiza la ayuda del componente actual
	 * 
	 * @param g
	 */
	private void renderizarInfo(Graphics g) {

		//si existe descripcion
		if(infoProcesadaList.size() > 0) {
			
			//indica cual es el limite inferior
			//de la pantalla para el eje 'y'
			int limiteInferiorY = 0;
			
			//si el panel de botones virtuales no 
			//se encuentra visible, modificamos la
			//posicion en 'y' para dibujar la 
			//descripcion mas abajo
//			if(principal.panelBotonesVirtual.estado == PanelBotonesVirtual.ESTADO_PANEL_INVISIBLE) {
//				limiteInferiorY = principal.getHeight() - 20;
//			}
//			else {
				limiteInferiorY = principal.getHeight() - principal.panelBotonesVirtual.height - 20;
//			}
			
			//calculamos el alto necesario para
			//mostrar todas las lineas de texto
			int altoTotalDescripcion = infoProcesadaList.size() * infoFont.getHeight(StringUtil.ASCII_BASIC_SET);

			//obtenemos el alto de una linea
			int altoLinea = infoFont.getHeight(StringUtil.ASCII_BASIC_SET);

			//renderizamos el fondo
			g.setColor(colorFondoInfo);
			g.fillRect(0, limiteInferiorY - altoTotalDescripcion - 10, principal.getWidth(), altoTotalDescripcion + 20);
			
			//empezamos a recorrer la lista
			//de lineas de texto para imprimirlas
			//en pantalla
			for(int i=0; i<infoProcesadaList.size(); i++) {
				
				if((infoProcesadaList.size() - 1) >= i) {
					
					//obtenemos la linea actual
					String lineaActual = infoProcesadaList.get(i);
					
					//calculamos la posicion en el
					//eje 'y' donde empezar a dibujar
					int y = limiteInferiorY - altoTotalDescripcion + (i * altoLinea);
					
					if(lineaActual != null) {
						//renderizamos la descripcion
						infoFont.drawString((int)((float)principal.getWidth() * 0.05d), y, lineaActual);
					}
				}
			}
		}
	}
	
	/**
	 * Reprocesa el string de la descripcion
	 * del componente elegido actualmente. 
	 * El procesamiento es necesario debido a
	 * que el ancho del mismo debe de ser 
	 * calculado para que entre en el ancho
	 * de la pantalla
	 */
	public void reprocesarTextoDescripcion(String texto) {
		
		//limpiamos el texto anterior
		infoProcesadaList.clear();

		//obtenemos el ancho de la pantalla
		//y le quitamos un porcentaje por
		//el margen necesario
		int anchoPantalla = (int)((float)principal.getWidth() * 0.9d);
		
		//tokenizamos el texto recibido
		//usando como separador al 
		//caracter 'espacio'
		String[] textoTokens = texto.split(" ");
		
		//tmp var
		String lineaActual = "";
		
		//recorremos la lista de tokens
		for(int i=0; i<textoTokens.length; i++) {
			
			//obtenemos el token actual
			String token = textoTokens[i];
			
			//si el largo de la linea actual menos
			//el token actual es menor que el ancho
			//de pantalla, agregamos el token a la
			//linea actual
			if(infoFont.getWidth(lineaActual + token + " ") <= anchoPantalla){
				
				//agregamos a la linea 
				//actual el token
				lineaActual += token + " ";
				
				//si nos encontramos recorriendo
				//el ultimo token, agregamos a la
				//lista la linea actual
				if(i == (textoTokens.length-1)) {
					infoProcesadaList.add(lineaActual.trim());
				}
			}
			else {
				//si nos pasamos del largo, agregamos
				//la linea actual a la lista, y comenzamos
				//a crear una nueva lista
				infoProcesadaList.add(lineaActual.trim());
				lineaActual = new String(token + " ");

				//si nos encontramos recorriendo
				//el ultimo token, lo agregamos
				if(i == (textoTokens.length-1)) {
					infoProcesadaList.add(token.trim());
				}
			}
		}
	}
	
	public void leave(GameContainer arg0, StateBasedGame arg1) throws SlickException {
		
	}
	
	public void drawString(int x, int y, String str, Color color) {
		
		if(inicializado) {
			Principal.bigWhiteOutlineGameFont.drawString(x, y, str, color);	
		}	
	}

	public void update(GameContainer arg0, StateBasedGame arg1, int elapsedTime) throws SlickException {

		if(inicializado) {
			
			//logica de la pantalla
			actualizar(elapsedTime);
			
			//actualizo cosas generales del sistema
			principal.actualizar(arg0, arg1, elapsedTime);
		}
	}
	
	public void procesarBotonPresionado(byte botonId) {

		//si la pantalla fue inicializada
		//y no esta activado el modulo de
		//confirmacion de capacidad de pago
		//excedida
		if(inicializado && !principal.confirmacionCapacidadPagoExcedida.activado) {
			
			/**
			 * Solo paso el boton presionado a la
			 * pantalla si la misma es la que
			 * esta mostrandose en pantalla
			 */
			if(principal.getCurrentStateID() == getID()) {
				
				if(principal.obtenerEstadoBoton(botonId)) {
					botonPresionado(botonId);	
				}
				
				//si existe un listener de 
				//boton sin estado, le avisamos
				//sobre el evento del boton
				if(botonSinEstadoListener != null) {
					botonSinEstadoListener.botonPresionadoSinEstado(botonId);
				}
			}
		}
	}
}