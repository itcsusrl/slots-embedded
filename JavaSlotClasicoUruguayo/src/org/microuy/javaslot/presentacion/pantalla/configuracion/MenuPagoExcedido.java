package org.microuy.javaslot.presentacion.pantalla.configuracion;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.dominio.PagoExcedido;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.dominio.EstadoBoton;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.presentacion.pantalla.listener.PantallaListener;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

public class MenuPagoExcedido extends PantallaGenerica implements PantallaListener {

	//lista contenedora de los
	//ultimos pagos excedidos
	private List<PagoExcedido> pagosExcedidosList;
	private int tamanoPagosExcedidos;
	
	private Color colorFondo = Color.lightGray;
	
	//fuentes a ser usadas
	private UnicodeFont pagoExcedidoFont = Principal.smallWhiteOutlineGameFont;
	private UnicodeFont titulosFont = Principal.bigWhiteOutlineGameFont;
	
	//formateadeor de fechas
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yy HH:mm");	
	
	//formateador de monedas
	private DecimalFormat integerFormat = new DecimalFormat("#");
	
	public MenuPagoExcedido(Principal principal) {
		super(Pantalla.PANTALLA_MENU_PAGO_EXCEDIDO, principal);
	}

	public void entraEnPantalla() {

		//obtenemos la lista con los
		//ultimos resumenes de cobranza
		obtenerUltimosPagosExcedidos();
		
		//lo que dice
		configurarEstadoBotones();
		
		//muestra la botonera rapidamente
		principal.panelBotonesVirtual.mostrarRapido();
		
	}
	
	public void saleDePantalla() {
		
	}
	
	public void configurarEstadoBotones() {
		
		//creamos la lista contenedora 
		//del estado de los botones
		List<EstadoBoton> estadoBotones = new ArrayList<EstadoBoton>();
		
		estadoBotones.add(new EstadoBoton(Boton.BOTON1, "VOLVER", true));
		estadoBotones.add(new EstadoBoton(Boton.BOTON2, "", false));
		estadoBotones.add(new EstadoBoton(Boton.BOTON3, "", false));	
		estadoBotones.add(new EstadoBoton(Boton.BOTON4, "", false));	
		estadoBotones.add(new EstadoBoton(Boton.BOTON5, "", false));
		estadoBotones.add(new EstadoBoton(Boton.BOTON6, "", false));	

		//ejecutamos el cambio en el 
		//estado de los botones 
		principal.configurarEstadoBotones(estadoBotones);
	}
	
	protected void inicializarRecursos() {
		
		//definimos un titulo para 
		//esta pantalla
		titulo = "PAGO EXCEDIDO";
		
	}

	protected void renderizar(Graphics g) {
		
		//pintamos el fondo
		Color colorOriginal = g.getColor();
		g.setColor(colorFondo);
		g.fillRect(0, 0, principal.getWidth(), principal.getHeight());
		g.setColor(colorOriginal);
		
		/**
		 * Renderizamos el titulo de cada
		 * uno de los campos de pago excedido
		 */
		titulosFont.drawString(100, 200, "ID");
		titulosFont.drawString(200, 200, "FECHA");
		titulosFont.drawString(450, 200, "MONTO");
		titulosFont.drawString(650, 200, "PAGADO");
		
		//posicion en 'y' que se
		//incrementa tras cada 
		//registro renderizado
		int posicionY = 260;
		
		//recorremos la lista para 
		//renderizar cada pago excedido
		for(int i=0; i<tamanoPagosExcedidos; i++) {
			
			//obtenemos el elemento actual
			PagoExcedido pagoExcedido = pagosExcedidosList.get(i);
			
			pagoExcedidoFont.drawString(100, posicionY, String.valueOf(pagoExcedido.id));
			pagoExcedidoFont.drawString(200, posicionY, dateFormatter.format(pagoExcedido.fecha));
			pagoExcedidoFont.drawString(450, posicionY, String.valueOf("$ " + pagoExcedido.monto));
			pagoExcedidoFont.drawString(650, posicionY, pagoExcedido.pagado ? "SI" : "NO");
			
			//incrementamos 
			//posicion en 'y'
			posicionY += 40;
		}
	}

	protected void actualizar(long elapsedTime) {
	
	}

	protected void botonPresionado(byte botonId) {

		if(botonId == Boton.BOTON1) {
			principal.cambiarPantalla(Pantalla.PANTALLA_MENU_PRINCIPAL);
		}
	}
	
	private void obtenerUltimosPagosExcedidos() {
		pagosExcedidosList = principal.logicaMaquina.obtenerUltimosPagosExcedidos();
		tamanoPagosExcedidos = pagosExcedidosList.size();
	}
}