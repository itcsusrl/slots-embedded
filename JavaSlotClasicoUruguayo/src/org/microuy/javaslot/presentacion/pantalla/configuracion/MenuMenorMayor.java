package org.microuy.javaslot.presentacion.pantalla.configuracion;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.persistencia.dao.ConfiguracionDAO;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.dominio.EstadoBoton;
import org.microuy.javaslot.presentacion.modulo.ui.ButtonUI;
import org.microuy.javaslot.presentacion.modulo.ui.EnfocableIf;
import org.microuy.javaslot.presentacion.modulo.ui.SelectorUI;
import org.microuy.javaslot.presentacion.modulo.ui.TextSelectorUI;
import org.microuy.javaslot.presentacion.modulo.ui.listener.SelectorUIListener;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.presentacion.pantalla.listener.PantallaListener;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

public class MenuMenorMayor extends PantallaGenerica implements PantallaListener, SelectorUIListener {

	private Color colorFondo = Color.lightGray;
	private TextSelectorUI porcentajeExtraidoApuestaTextSelectorUI;
	private ButtonUI restablecerMontoActualButtonUI;
	
	//fuente de esta pantalla
	private UnicodeFont fuente = Principal.mediumWhiteOutlineGameFont;
	
	private DecimalFormat decimalFormat = new DecimalFormat("#.##");

	/**
	 * Lista contenedora de elementos
	 * que pueden ser enfocables
	 */
	private List<EnfocableIf> enfocables = new ArrayList<EnfocableIf>();
	
	public MenuMenorMayor(Principal principal) {
		super(Pantalla.PANTALLA_MENU_MENOR_MAYOR, principal);
	}

	public void entraEnPantalla() {

		//lo que dice
		configurarEstadoBotones();

		//muestra la botonera rapidamente
		principal.panelBotonesVirtual.mostrarRapido();
		
	}

	public void saleDePantalla() {

	}
	
	public void configurarEstadoBotones() {
		
		//creamos la lista contenedora 
		//del estado de los botones
		List<EstadoBoton> estadoBotones = new ArrayList<EstadoBoton>();
		
		
		estadoBotones.add(new EstadoBoton(Boton.BOTON1, "VOLVER", true));
		estadoBotones.add(new EstadoBoton(Boton.BOTON2, "", false));
		estadoBotones.add(new EstadoBoton(Boton.BOTON3, "SUBIR", true));
		estadoBotones.add(new EstadoBoton(Boton.BOTON4, "BAJAR", true));
		
		//los botones izquierda y derecha son 
		//solo validos para los textui, y no
		//para el boton que lleva la opcion 
		//'elegir'
		if(obtenerEnfocado() instanceof TextSelectorUI) {
			estadoBotones.add(new EstadoBoton(Boton.BOTON5, "IZQUIERDA", true));
			estadoBotones.add(new EstadoBoton(Boton.BOTON6, "DERECHA", true));
		}
		else if(obtenerEnfocado() instanceof ButtonUI) {
			estadoBotones.add(new EstadoBoton(Boton.BOTON5, "", false));
			estadoBotones.add(new EstadoBoton(Boton.BOTON6, "ELEGIR", true));
		}
		
		//ejecutamos el cambio en el 
		//estado de los botones 
		principal.configurarEstadoBotones(estadoBotones);
		
	}
	
	/**
	 * Obtiene el componente enfocado actualmente
	 * @return
	 */
	private EnfocableIf obtenerEnfocado() {
		
		EnfocableIf enfocableIf = null;
		
		for(EnfocableIf enIf : enfocables) {
			if(enIf.isEnfocado()) {
				enfocableIf = enIf;
				break;
			}
		}
		
		return enfocableIf;
	}
	
	private void presionaFlechaAbajo() {

		//buscamos el elemento
		//enfocado actualmente
		int proximoEnfocado = 0;
		for(int i=0; i<enfocables.size(); i++) {
			EnfocableIf enfocableIf = enfocables.get(i);
			if(enfocableIf.isEnfocado()) {
				proximoEnfocado = i+1;
				if(proximoEnfocado > (enfocables.size()-1)) {
					proximoEnfocado = 0;
				}
				enfocableIf.setEnfocado(false);
				break;
			}
		}
		
		//enfocamos el proximo elemento
		EnfocableIf enfocableIf = enfocables.get(proximoEnfocado);
		enfocableIf.setEnfocado(true);
		
		if(enfocableIf instanceof TextSelectorUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((TextSelectorUI)enfocableIf).descripcion);
		}
		else if(enfocableIf instanceof ButtonUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((ButtonUI)enfocableIf).descripcion);
		}
		

		//reconfiguramos el estado 
		//de los botones
		configurarEstadoBotones();
		
	}
	
	private void presionaFlechaArriba() {

		//buscamos el elemento
		//enfocado actualmente
		int anteriorEnfocado = 0;
		for(int i=0; i<enfocables.size(); i++) {
			EnfocableIf enfocableIf = enfocables.get(i);
			if(enfocableIf.isEnfocado()) {
				anteriorEnfocado = i-1;
				if(anteriorEnfocado < 0) {
					anteriorEnfocado = enfocables.size()-1;
				}
				enfocableIf.setEnfocado(false);
				break;
			}
		}
		
		//enfocamos el proximo elemento
		EnfocableIf enfocableIf = enfocables.get(anteriorEnfocado);
		enfocableIf.setEnfocado(true);
		
		if(enfocableIf instanceof TextSelectorUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((TextSelectorUI)enfocableIf).descripcion);
		}
		else if(enfocableIf instanceof ButtonUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((ButtonUI)enfocableIf).descripcion);
		}
		
		//reconfiguramos el estado 
		//de los botones
		configurarEstadoBotones();

	}

	private void presionaFlechaIzquierda() {
		
		for(EnfocableIf enfocableIf : enfocables) {
			if(enfocableIf.isEnfocado()) {
				if(enfocableIf instanceof TextSelectorUI) {
					TextSelectorUI textSelectorUI = (TextSelectorUI)enfocableIf;
					textSelectorUI.proximoElemento();
					break;
				}
			}
		}
	}

	private void presionaFlechaDerecha() {
		
		for(EnfocableIf enfocableIf : enfocables) {
			if(enfocableIf.isEnfocado()) {
				if(enfocableIf instanceof TextSelectorUI) {
					TextSelectorUI textSelectorUI = (TextSelectorUI)enfocableIf;
					textSelectorUI.previoElemento();
					break;
				}
			}
		}
	}

	protected void inicializarRecursos() {
		
		//definimos un titulo para 
		//esta pantalla
		titulo = "CONFIGURACION 'MENOR o MAYOR'";
		
		porcentajeExtraidoApuestaTextSelectorUI = new TextSelectorUI(this, 130, 190, 550,  "% Extraido Apuesta", ConfiguracionDAO.MENOR_MAYOR_PORCENTAJE_EXTRAIDO_APUESTA, true);
		porcentajeExtraidoApuestaTextSelectorUI.agregarSelectorUIListener(this);
		
		//divisor usado para el porcentaje
		//de extraccion de la apuesta
		float porcentajeExtraidoDivisor = 0.5f;
		//repeticiones del valor anterior 
		int repeticionesPorcentajeExtraido = (int)(100f / porcentajeExtraidoDivisor);
		
		//cargamos los valores del TextSelector
		for(int i=1; i<=repeticionesPorcentajeExtraido; i++) {
			float tmp = porcentajeExtraidoDivisor * (float)i;
			porcentajeExtraidoApuestaTextSelectorUI.agregarElemento(tmp, ""+tmp);
		}
		
		porcentajeExtraidoApuestaTextSelectorUI.mostrarClavePorDefecto(Sistema.MENOR_MAYOR_PORCENTAJE_EXTRAIDO_APUESTA);
		//cargamos la descripcion del  
		//componente desde la base de datos
		porcentajeExtraidoApuestaTextSelectorUI.descripcion = principal.logicaMaquina.obtenerDescripcionConfiguracion(porcentajeExtraidoApuestaTextSelectorUI.selectorUI.codigo);
		
		//creamos el boton
		restablecerMontoActualButtonUI = new ButtonUI(this, 300, 350, "Reestablecer", "Reestablece el monto acumulado del minijuego a cero.", false);
		
		//agregamos a la lista de
		//elementos enfocables
		enfocables.add(porcentajeExtraidoApuestaTextSelectorUI);
		enfocables.add(restablecerMontoActualButtonUI);
		
		//configuramos el primer texto
		reprocesarTextoDescripcion(porcentajeExtraidoApuestaTextSelectorUI.descripcion);
		
	}

	protected void renderizar(Graphics g) {
		
		//pintamos el fondo
		Color colorOriginal = g.getColor();
		g.setColor(colorFondo);
		g.fillRect(0, 0, principal.getWidth(), principal.getHeight());
		g.setColor(colorOriginal);
		
		//renderizamos cada componente
		porcentajeExtraidoApuestaTextSelectorUI.renderizar(g);
		restablecerMontoActualButtonUI.renderizar(g);
		
		//dibujamos el monto actual
		//distribuido del pozo 
		fuente.drawString(270, 300, "Monto actual: " + decimalFormat.format(principal.maquina.logicaMaquina.distribuidorPremios.montoDistribuidoMenorMayor));
		
	}

	protected void actualizar(long elapsedTime) {
	
		porcentajeExtraidoApuestaTextSelectorUI.actualizar(elapsedTime);
		restablecerMontoActualButtonUI.actualizar(elapsedTime);
		
	}

	protected void botonPresionado(byte botonId) {

		if(botonId == Boton.BOTON1) {
			principal.cambiarPantalla(Pantalla.PANTALLA_MENU_CONFIGURACION_AVANZADA);
		}
		else if(botonId == Boton.BOTON3) {
			presionaFlechaArriba();
		}
		else if(botonId == Boton.BOTON4) {
			presionaFlechaAbajo();
		}

		//logica de presionado de botones
		//izquierda y derecha solo para
		//componentes TextUI
		if(obtenerEnfocado() instanceof TextSelectorUI) {
			if(botonId == Boton.BOTON5) {
				presionaFlechaDerecha(); 
			}
			else if(botonId == Boton.BOTON6) {
				presionaFlechaIzquierda();
			}
		}
		else if(obtenerEnfocado() instanceof ButtonUI) {
			
			if(botonId == Boton.BOTON6) {

				//logica de presionado de botones
				//de tipo ButtonUI
				if(obtenerEnfocado() == restablecerMontoActualButtonUI) {

					//actualizamos el valor de configuracion
					principal.maquina.logicaMaquina.actualizarValorConfiguracion(ConfiguracionDAO.MENOR_MAYOR_MONTO_ACTUAL, "" + 0f);
				}
			}
		}
	}

	/**
	 * Llamado cuando cambia algun valor 
	 * de alguno de los combo selectores
	 * de la pantalla
	 */
	public void cambiaValorSelectorUI(SelectorUI selectorUI) {
		
		//ejecutamos el cambio 
		//en la persistencia
		principal.logicaMaquina.actualizarValorConfiguracion(selectorUI.codigo, selectorUI.elementoActualClave.toString());
		
	}
}