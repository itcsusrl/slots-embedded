package org.microuy.javaslot.presentacion.pantalla.configuracion;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.dominio.EstadoBoton;
import org.microuy.javaslot.presentacion.modulo.ui.ButtonUI;
import org.microuy.javaslot.presentacion.modulo.ui.EnfocableIf;
import org.microuy.javaslot.presentacion.pantalla.PantallaGenerica;
import org.microuy.javaslot.presentacion.pantalla.listener.PantallaListener;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class MenuPrincipal extends PantallaGenerica implements PantallaListener {

	private Color colorFondo = Color.lightGray;
	
	private ButtonUI resumenCobranzaButtonUI;
	private ButtonUI pagoExcedidoButtonUI;
	private ButtonUI configuracionAvanzadaButtonUI;
	
	/**
	 * Lista contenedora de elementos
	 * que pueden ser enfocables
	 */
	private List<EnfocableIf> enfocables = new ArrayList<EnfocableIf>();
	
	/**
	 * Indica la pantalla a la cual
	 * ir cuando el usuario sale de
	 * 
	 */
	public int pantallaAnterior = -1;
	
	public MenuPrincipal(Principal principal) {
		super(Pantalla.PANTALLA_MENU_PRINCIPAL, principal);
	}

	public void entraEnPantalla() {
		
		//lo que dice
		configurarEstadoBotones();

		//muestra la botonera rapidamente
		principal.panelBotonesVirtual.mostrarRapido();

	}

	public void saleDePantalla() {

	}
	
	public void configurarEstadoBotones() {
		
		//creamos la lista contenedora 
		//del estado de los botones
		List<EstadoBoton> estadoBotones = new ArrayList<EstadoBoton>();
		
		//volver
		estadoBotones.add(new EstadoBoton(Boton.BOTON1, "SALIR", true));
		estadoBotones.add(new EstadoBoton(Boton.BOTON2, "", false));
		estadoBotones.add(new EstadoBoton(Boton.BOTON3, "SUBIR", true));
		estadoBotones.add(new EstadoBoton(Boton.BOTON4, "BAJAR", true));
		estadoBotones.add(new EstadoBoton(Boton.BOTON5, "", false));
		estadoBotones.add(new EstadoBoton(Boton.BOTON6, "ELEGIR", true));
		
		//ejecutamos el cambio en el 
		//estado de los botones 
		principal.configurarEstadoBotones(estadoBotones);
		
	}
	
	private void presionaFlechaAbajo() {

		//buscamos el elemento
		//enfocado actualmente
		int proximoEnfocado = 0;
		for(int i=0; i<enfocables.size(); i++) {
			EnfocableIf enfocableIf = enfocables.get(i);
			if(enfocableIf.isEnfocado()) {
				proximoEnfocado = i+1;
				if(proximoEnfocado > (enfocables.size()-1)) {
					proximoEnfocado = 0;
				}
				enfocableIf.setEnfocado(false);
				break;
			}
		}
		
		//enfocamos el proximo elemento
		EnfocableIf enfocableIf = enfocables.get(proximoEnfocado);
		enfocableIf.setEnfocado(true);
		
		if(enfocableIf instanceof ButtonUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((ButtonUI)enfocableIf).descripcion);
		}
	}
	
	private void presionaFlechaArriba() {

		//buscamos el elemento
		//enfocado actualmente
		int anteriorEnfocado = 0;
		for(int i=0; i<enfocables.size(); i++) {
			EnfocableIf enfocableIf = enfocables.get(i);
			if(enfocableIf.isEnfocado()) {
				anteriorEnfocado = i-1;
				if(anteriorEnfocado < 0) {
					anteriorEnfocado = enfocables.size()-1;
				}
				enfocableIf.setEnfocado(false);
				break;
			}
		}
		
		//enfocamos el proximo elemento
		EnfocableIf enfocableIf = enfocables.get(anteriorEnfocado);
		enfocableIf.setEnfocado(true);
		
		if(enfocableIf instanceof ButtonUI) {
			//reprocesamos el texto 
			//de la descripcion
			reprocesarTextoDescripcion(((ButtonUI)enfocableIf).descripcion);
		}
	}

	protected void inicializarRecursos() {
		
		//definimos un titulo para 
		//esta pantalla
		titulo = "MENU PRINCIPAL";
		
		//creamos los componentes
		resumenCobranzaButtonUI = new ButtonUI(this, 315, 200, "RESUMEN DE COBRANZA", "Muestra los resumenes de cobranza", true);
		pagoExcedidoButtonUI = new ButtonUI(this, 400, 270, "Pago Excedido", "Muestra los ultimos pagos que excedieron la capacidad de la maquina", false);
		configuracionAvanzadaButtonUI = new ButtonUI(this, 345, 350, "Configuracion Avanzada", "Configuracion avanzada de la maquina", false);

		//agregamos a la lista de
		//elementos enfocables
		enfocables.add(resumenCobranzaButtonUI);
		enfocables.add(pagoExcedidoButtonUI);
		enfocables.add(configuracionAvanzadaButtonUI);
		
		//configuramos el primer texto
		reprocesarTextoDescripcion(resumenCobranzaButtonUI.descripcion);
		
	}

	protected void renderizar(Graphics g) {
		
		//pintamos el fondo
		Color colorOriginal = g.getColor();
		g.setColor(colorFondo);
		g.fillRect(0, 0, principal.getWidth(), principal.getHeight());
		g.setColor(colorOriginal);
		
		//renderizamos cada componente
		resumenCobranzaButtonUI.renderizar(g);
		pagoExcedidoButtonUI.renderizar(g);
		configuracionAvanzadaButtonUI.renderizar(g);
		
	}

	protected void actualizar(long elapsedTime) {
	
		resumenCobranzaButtonUI.actualizar(elapsedTime);
		pagoExcedidoButtonUI.actualizar(elapsedTime);
		configuracionAvanzadaButtonUI.actualizar(elapsedTime);
		
	}

	protected void botonPresionado(byte botonId) {

		if(botonId == Boton.BOTON1) {
			
			//escondemos la botonera
			principal.panelBotonesVirtual.ocultarRapido();
			
			if(pantallaAnterior == -1) {
				principal.cambiarPantalla(Pantalla.PANTALLA_MENU_PRESENTACION);
			}
			else {
				principal.cambiarPantalla(pantallaAnterior);	
			}
		}
		else if(botonId == Boton.BOTON3) {
			presionaFlechaArriba();
		}
		else if(botonId == Boton.BOTON4) {
			presionaFlechaAbajo();
		}
		else if(botonId == Boton.BOTON6) {
			
			//recorremos la lista de botones
			//para ver cual esta enfocado 
			for(EnfocableIf enfocableIf : enfocables) {
				
				//si el boton se encuentra
				//enfocado entramos a la 
				//pantalla
				if(enfocableIf.isEnfocado()) {
					
					if(enfocableIf == configuracionAvanzadaButtonUI) {
						principal.cambiarPantalla(Pantalla.PANTALLA_MENU_CONFIGURACION_AVANZADA);
					}
					else if(enfocableIf == pagoExcedidoButtonUI) {
						principal.cambiarPantalla(Pantalla.PANTALLA_MENU_PAGO_EXCEDIDO);
					}
					else if(enfocableIf == resumenCobranzaButtonUI) {
						principal.cambiarPantalla(Pantalla.PANTALLA_MENU_RESUMEN_COBRANZA);
					}
				}
			}
		}
	}
}