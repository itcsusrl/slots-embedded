package org.microuy.javaslot.presentacion.pantalla;

import org.microuy.javaslot.comm.listener.ComandoEvent;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.newdawn.slick.Graphics;

public class TablaPago extends PantallaGenerica {

	public TablaPago(Principal principal) {
		super(Pantalla.PANTALLA_TABLA_PAGOS, principal);
	}

	protected void renderizar(Graphics g) {
		
	}

	protected void actualizar(long elapsedTime) {
		
	}
	
	protected void inicializarRecursos() {
		
	}

	public void botonPresionado(byte botonId) {
		
	}
	
	public void entraEnPantalla() {
		
	}

	public void saleDePantalla() {
		
	}
}