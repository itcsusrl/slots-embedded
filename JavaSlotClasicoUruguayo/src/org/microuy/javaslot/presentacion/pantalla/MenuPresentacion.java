package org.microuy.javaslot.presentacion.pantalla;

import org.microuy.javaslot.constante.EstadoMaquina;
import org.microuy.javaslot.persistencia.dao.ConfiguracionDAO;
import org.microuy.javaslot.presentacion.Principal;
import org.microuy.javaslot.presentacion.constante.Pantalla;
import org.microuy.javaslot.presentacion.pantalla.listener.BotonSinEstadoListener;
import org.microuy.javaslot.sonido.ReproductorSonido;
import org.microuy.javaslot.util.SlotsRandomGenerator;
import org.microuy.javaslot.util.Timer;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class MenuPresentacion extends PantallaGenerica implements BotonSinEstadoListener {
	
	//flag que indica que
	//ha ingresado dinero a
	//la maquina
	public boolean flagIngresoDinero;
	
	//imagenes del contador
	//de segundos
	private Image[] segundosImage;
	private Image segundoActualImage;
	
	//variables de cuenta regresiva
	private Timer cuentaRegresivaTimer = new Timer(0);
	private int cuentaRegresiva;
	private static final int CUENTA_REGRESIVA = 3;
	
	//imagenes varias
	private Image efectoLuces;
	private Image logo;
	private Image sky;
	private Image background;
	private Animation tribunaAnimation;
	private Animation banderaAnimation;

	//imagenes de figuras
	private Image figuraIntro1;
	private Image figuraIntro2;
	private Image figuraIntro3;
	private Image figuraIntro4;
	private Image figuraIntro5;
	private Image figuraIntro6;
	private Image figuraIntro7;
	private Image figuraIntro8;
	private Image figuraIntro9;
	private Image figuraIntro10;
	private Image figuraIntro11;
	private Image figuraIntro12;
	
	//constantes de la 
	//animacion del logo
	private final static int ROTACION_LOGO_MS = 2000;
	private final static int STANDBY_LOGO_MS = 8000;
	
	//timers usados para la
	//rotacion del logo
	private Timer rotacionLogoTickTimer = new Timer(10);
	private Timer rotacionLogoTimer = new Timer(ROTACION_LOGO_MS);
	
	//estado de la animacion
	//del logo, detenida o
	//girando
	private boolean rotacionLogoEstadoDetenido;
	
	//timer usado para la
	//animacion del efecto
	//de luces
	private Timer efectoLucesTickTimer = new Timer(10);
	private Timer efectoLucesTimer = new Timer(2500);
	private boolean izquierdaEfectoLuces;
	private float rotacionEfectoLuces;

	//timer del cambio de fichas
//	private Timer demoTimer = new Timer(2000);
	private Timer demoTimer = new Timer(10000);
	
	/**
	 * Dibuja el fondo primario del juego y 
	 * todas las animaciones que lo componen
	 */
	private void dibujarFondoPrimario(Graphics g) {

		//cielo
		g.drawImage(sky, 0, 0);
		
		//efecto de luces
		efectoLuces.draw(principal.getWidth()/2-efectoLuces.getWidth()/2, principal.getHeight()/2-efectoLuces.getHeight()/2);
		
		//fondo con estadio
		g.drawImage(background, 0, 30);
		
		//tribuna animada
		tribunaAnimation.draw(0, 342);
		
		//bandera animada
		banderaAnimation.draw(506, 45);
		
		//logo
		logo.draw(principal.getWidth()/2 - logo.getWidth()/2, 70);

		//jugadores
		dibujarJugadores(g);
			
	}
	
	private void dibujarJugadores(Graphics g) {
		
		//andrade
		figuraIntro12.draw(380, 275);

		//tabarez
		figuraIntro8.draw(315, 255);

		//viejo 1
		figuraIntro9.draw(500, 270);

		//viejo 2
		figuraIntro11.draw(575, 290);

		//obdulio
		figuraIntro3.draw(385, 350);

		//mazurkiewicz
		figuraIntro7.draw(700, 335);

		//manicera
		figuraIntro5.draw(595, 365);

		//ghiggia
		figuraIntro2.draw(505, 400);

		//nasazzi
		figuraIntro10.draw(225, 320);

		//forlan
		figuraIntro6.draw(5, 320);

		//francescoli
		figuraIntro4.draw(110, 353);
		
		//suarez
		figuraIntro1.draw(230, 380);
		
	}	

	public MenuPresentacion(Principal principal) throws SlickException {
		
		super(Pantalla.PANTALLA_MENU_PRESENTACION,principal);
		addBotonSinEstadoListener(this);
	}

	protected void renderizar(Graphics g) {

		//dibujamos el fondo primario
		dibujarFondoPrimario(g);

		if(segundoActualImage != null) {
			segundoActualImage.draw(principal.getWidth()/2-segundoActualImage.getWidth()/2, principal.getHeight()/2-segundoActualImage.getHeight()/2);
		}
	}
	
	protected void actualizar(long elapsedTime) {

		//si el jugador todavia tiene credito
		//y estamos en esta pantalla significa
		//que tiene que volver al juego
		if(principal.logicaMaquina.maquina.montoJugador > 0f && !cuentaRegresivaTimer.isActive()) {
			reiniciamosCuentaRegresiva();
			flagIngresoDinero = true;
		}
		
		if(flagIngresoDinero) {
			
			//logica de contador regresivo
			if(cuentaRegresivaTimer.action(elapsedTime) || (cuentaRegresiva == CUENTA_REGRESIVA)) {
				
				//reiniciamos el timer
				//de la pantalla de fichas
				demoTimer.refresh();
				
				//llegamos a cero
				if(cuentaRegresiva == 0) {
					
					//reseteamos
					cuentaRegresiva = CUENTA_REGRESIVA;
					
					//comenzamos el sonido de la premiacion
					ReproductorSonido.playSound(ReproductorSonido.PITIDO);
					
					//continuamos hacia el juego
					continuarHaciaJuego(false, true);
					return;
				}

				//cargamos imagen
				segundoActualImage = segundosImage[cuentaRegresiva-1];

				//decrementamos la 
				//cuenta regresiva
				cuentaRegresiva--;
			}
		}
		
		/**
		 * Solamente entramos a alguna pantalla
		 * del modo demo si no nos encontramos 
		 * en el modulo de confirmacion de pago
		 * excedido 
		 */
		if(!principal.confirmacionCapacidadPagoExcedida.activado) {

			//si se cumplio el tiempo de cambio
			//de ficha, vamos a la pantalla adecuada
			if(demoTimer.action(elapsedTime)) {
			
				//obtenemos una pantalla de 
				//demo aleatoria a la cual ir
				int pantallaAleatoria = obtenerPantallaDemoAleatoria();
			
				//seteamos el modo demo
				principal.demo = true;
				
				if(pantallaAleatoria == Pantalla.PANTALLA_FICHA_PERSONAJE) {
					((FichaPersonaje) principal.getState(Pantalla.PANTALLA_FICHA_PERSONAJE)).entraEnPantalla();
					principal.cambiarPantalla(Pantalla.PANTALLA_FICHA_PERSONAJE, new FadeOutTransition(), new FadeInTransition());
				}
				else if(pantallaAleatoria == Pantalla.PANTALLA_JUEGO) {
					continuarHaciaJuego(true, true);
				}
				else if(pantallaAleatoria == Pantalla.PANTALLA_AYUDA_DESCRIPTIVA) {
					principal.cambiarPantalla(Pantalla.PANTALLA_AYUDA_DESCRIPTIVA, new FadeOutTransition(), new FadeInTransition());
				}
				else if(pantallaAleatoria == Pantalla.PANTALLA_AYUDA_PREMIOS_NORMALES) {
					principal.cambiarPantalla(Pantalla.PANTALLA_AYUDA_PREMIOS_NORMALES, new FadeOutTransition(), new FadeInTransition());
				}
			}
		}
		
		//logica del efecto 
		//de las luces
		animarEfectoLuces(elapsedTime);

		//logica de la rotacion 
		//del logo
		animarLogo(elapsedTime);
		
	}
	
	/**
	 * Devuelve aleatoriamente una 
	 * pantalla de demo a la cual ir 
	 * @return
	 */
	private int obtenerPantallaDemoAleatoria() {
		
		int pantallaAleatoria = -1;
		
		//posibles pantallas de 
		//demo a las cuales ir 
//		int[] pantallasDemoId = {Pantalla.PANTALLA_JUEGO, Pantalla.PANTALLA_FICHA_PERSONAJE, Pantalla.PANTALLA_AYUDA_DESCRIPTIVA, Pantalla.PANTALLA_AYUDA_PREMIOS_NORMALES};
		int[] pantallasDemoId = {Pantalla.PANTALLA_JUEGO, Pantalla.PANTALLA_FICHA_PERSONAJE}; //TODO No entramos a pantalla de ayuda en modo demo porque todavia hay algun problema
		
		//obtenemos una aleatoriamente
		pantallaAleatoria = pantallasDemoId[SlotsRandomGenerator.getInstance().nextInt(pantallasDemoId.length)];
		
		//TODO HARDCODED
//		pantallaAleatoria = Pantalla.PANTALLA_FICHA_PERSONAJE;
		
		return pantallaAleatoria;
	}

	protected void inicializarRecursos() {

		//cargamos las imagenes 
		cargarImagenes();

		//flag que indica si ha
		//ingresado dinero estando
		//en esta pantalla
		flagIngresoDinero = false;
		
		//cargamos los sonidos necesarios
		ReproductorSonido.cargarSonidos(new int[]{ReproductorSonido.PITIDO});
		ReproductorSonido.cargarMusica(new int[]{ReproductorSonido.MUSICA_PRESENTACION});

	}
	
	public void botonPresionado(byte botonId) {
		
		//si ha ingresado dinero y 
		//algun boton fue presionado
		if(flagIngresoDinero) {
			//continuamos hacia el juego
			continuarHaciaJuego(false, true);
		}
	}
	
	public void continuarHaciaJuego(boolean demo, boolean fadeout) {
		
		//en caso de que la logica
		//interprete que se debe de 
		//entrar a la pantalla de 
		//fichas
		demoTimer.refresh();
		
		//comienza el juego de luces
		principal.comenzarJuegoLuces((byte)0);
		
		//cambio el estado de la maquina
		principal.setEstado(EstadoMaquina.JUGANDO);
		
		//seteamos el modo de juego
		principal.demo = demo;
		
		//vamos al juego
		if(fadeout) {
			principal.cambiarPantalla(Pantalla.PANTALLA_JUEGO, new FadeOutTransition(), new FadeInTransition());
		}
		else {
			principal.cambiarPantalla(Pantalla.PANTALLA_JUEGO);
		}
	}
	
	/**
	 * Llamado por Principal cuando se 
	 * genera un evento de comando de
	 * ingreso de dinero
	 */
	public void ingresoDinero() {
		
		if(!flagIngresoDinero) {

			//asi no entramos a la
			//pantalla de fichas
			demoTimer.refresh();
			
			//inicia el texto itinerante
			flagIngresoDinero = true;
			
			//reiniciamos la cuenta regresiva
			reiniciamosCuentaRegresiva();

		}
	}
	
	public void entraEnPantalla() {
		
		//20% de posibilidades de
		//ejecutar la musica cuando
		//se ingresa a la pantalla
		boolean reproducirAudio = SlotsRandomGenerator.ejecutarPorcentajeEntrega(20);
		
		if(reproducirAudio) {
			//reproducimos la musica
			//de la presentacion
			ReproductorSonido.playMusic(ReproductorSonido.MUSICA_PRESENTACION, 8000, false, 0.4f);
		}
		
		//chequeamos que no haya
		//quedado la maquina excecida en
		//su capacidad de pago y se haya
		//reiniciado por alguna razon
		float montoCapacidadPagoExcedida = Float.parseFloat(principal.logicaMaquina.obtenerValorConfiguracion(ConfiguracionDAO.CAPACIDAD_PAGO_EXCEDIDA).trim());
		
		if(montoCapacidadPagoExcedida > 0) {
			
			//mostramos el modulo de 
			//capacidad de pago excedida
			principal.confirmacionCapacidadPagoExcedida.mostrarDialogo(montoCapacidadPagoExcedida);
			
		}
		else {

			//si entramos el if anterior es necesario
			//el panel de botones, sino no es necesario
			principal.panelBotonesVirtual.ocultarRapido();
		}
		
		//reiniciamos el timer
		demoTimer.refresh();
		
		//mostramos la data del jackpot
		//en la barra informativa superior
		principal.mostrarInfoJackpot();

	}
	
	/**
	 * Reinicia la cuenta regresiva y empieza
	 * a correr el timer de la cuenta
	 */
	public void reiniciamosCuentaRegresiva() {
		
		//reconfiguramos variables
		cuentaRegresiva = CUENTA_REGRESIVA;
		cuentaRegresivaTimer.setDelay(1000);
		cuentaRegresivaTimer.setActive(true);
		
	}
	
	public void saleDePantalla() {
		
		if(inicializado) {
			flagIngresoDinero = false;
			segundoActualImage = null;
		}
	}

	public void botonPresionadoSinEstado(byte botonId) {
		
		botonPresionado(botonId);
		ReproductorSonido.stopMusic(ReproductorSonido.MUSICA_PRESENTACION, true);
	}
	
	private void cargarImagenes() {
		
		try {

			//cargamos el estadio
			background = new Image("resources/stadium.png");
			
			//el cielo
			sky = new Image("resources/sky.png");
			
			//efecto de luces
			efectoLuces = new Image("resources/INTROlight-A.png");
			
			//logo
			logo = new Image("resources/LOGO_big-17.png");

			//animacion de tribuna
			tribunaAnimation = new Animation(new Image[]{
											 new Image("resources/tribuna-1.png"),
											 new Image("resources/tribuna-2.png"),
											 new Image("resources/tribuna-3.png"),
											 new Image("resources/tribuna-4.png"),
											 }, 200);
			
			//animacion de bandera
			banderaAnimation = new Animation(new Image[]{
											 new Image("resources/bandera/flag-01.png"),
											 new Image("resources/bandera/flag-02.png"),
											 new Image("resources/bandera/flag-03.png"),
											 new Image("resources/bandera/flag-04.png"),
											 new Image("resources/bandera/flag-05.png"),
											 new Image("resources/bandera/flag-06.png")
											 }, 200);
			
			//figuras
			figuraIntro1 = new Image("resources/figuras_intro/INTROfig-1.png");
			figuraIntro2 = new Image("resources/figuras_intro/INTROfig-2.png");
			figuraIntro3 = new Image("resources/figuras_intro/INTROfig-3.png");
			figuraIntro4 = new Image("resources/figuras_intro/INTROfig-4.png");
			figuraIntro5 = new Image("resources/figuras_intro/INTROfig-5.png");
			figuraIntro6 = new Image("resources/figuras_intro/INTROfig-6.png");
			figuraIntro7 = new Image("resources/figuras_intro/INTROfig-7.png");
			figuraIntro8 = new Image("resources/figuras_intro/INTROfig-8.png");
			figuraIntro9 = new Image("resources/figuras_intro/INTROfig-9.png");
			figuraIntro10 = new Image("resources/figuras_intro/INTROfig-10.png");
			figuraIntro11 = new Image("resources/figuras_intro/INTROfig-11.png");
			figuraIntro12 = new Image("resources/figuras_intro/INTROfig-12.png");
			
			//cargamos las imagenes
			//de los segundos
			segundosImage = new Image[3];
			segundosImage[0] = new Image("resources/count1.png");
			segundosImage[1] = new Image("resources/count2.png");
			segundosImage[2] = new Image("resources/count3.png");
			
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Anima el efecto de los rayos de luz
	 * para que vayan girando sobre su propio
	 * centro
	 * @param elapsedTime
	 */
	private void animarEfectoLuces(long elapsedTime) {
		
		//tick de la animacion 
		if(efectoLucesTickTimer.action(elapsedTime)) {

			//cambio en la rotacion
			rotacionEfectoLuces = izquierdaEfectoLuces ? 0.07f : -0.07f;
			
			//rotamos la imagen de las 
			//luces para dar una animacion
			efectoLuces.rotate(rotacionEfectoLuces);
			
		}
		
		//tiempo total cumplido para
		//cambiar la direccion de las
		//luces
		if(efectoLucesTimer.action(elapsedTime)) {
			izquierdaEfectoLuces = !izquierdaEfectoLuces;
		}
	}
	
	/**
	 * Anima el logo para que realice 
	 * varios giros sobre su propio 
	 * centro, se detenga un tiempo
	 * determinado y luego haga lo 
	 * mismo
	 */
	private void animarLogo(long elapsedTime) {
		
		if(!rotacionLogoEstadoDetenido) {

			//tick de la animacion 
			if(rotacionLogoTickTimer.action(elapsedTime)) {

				//calculamos la rotacion 
				//necesaria en base al
				//tiempo del tick
				float rotacion = (elapsedTime * 1800f) / ROTACION_LOGO_MS;
				
				//rotamos la imagen de las 
				//luces para dar una animacion
				logo.rotate(rotacion);
			}
		}
		
		//tiempo total cumplido para
		//dejar al logo en su posicion
		//original
		if(rotacionLogoTimer.action(elapsedTime)) {

			//seteo de la rotacion a cero
			logo.setRotation(0f);
			
			//cambio de estado
			rotacionLogoEstadoDetenido = !rotacionLogoEstadoDetenido;

			//si estaba detenido cambiamos
			//el timer 
			if(rotacionLogoEstadoDetenido) {

				//cambia las luces del panel
				//de botones si comienza con
				//el juego de luces
				if(!principal.confirmacionCapacidadPagoExcedida.activado) {
					//detenemos el juego de luces
					//principal.comenzarJuegoLuces((byte)1);
				}
					
				rotacionLogoTimer.setDelay(STANDBY_LOGO_MS);
				rotacionLogoTimer.setCurrentTick(0);
			}
			else {
				
				//cambia las luces del panel
				//de botones si comienza con
				//el juego de luces
				if(!principal.confirmacionCapacidadPagoExcedida.activado) {
					//comenzamos el juego de luces
					//principal.comenzarJuegoLuces((byte)0);
				}
				
				rotacionLogoTimer.setDelay(ROTACION_LOGO_MS);
				rotacionLogoTimer.setCurrentTick(0);
			}
		}
	}
}