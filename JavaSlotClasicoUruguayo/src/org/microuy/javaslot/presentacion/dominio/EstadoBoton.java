package org.microuy.javaslot.presentacion.dominio;

public class EstadoBoton {

	private byte botonId;
	private String texto;
	private boolean activado;
	
	public EstadoBoton(byte botonId, String texto, boolean prendido) {
		this.botonId = botonId;
		this.texto = texto;
		this.activado = prendido;
	}
	
	public byte getBotonId() {
		return botonId;
	}
	public void setBotonId(byte botonId) {
		this.botonId = botonId;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}

	public boolean isActivado() {
		return activado;
	}

	public void setActivado(boolean activado) {
		//seteamos el nuevo estado
		this.activado = activado;
	}
}