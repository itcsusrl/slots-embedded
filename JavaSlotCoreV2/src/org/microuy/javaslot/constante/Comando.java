package org.microuy.javaslot.constante;

public class Comando {

	public static final byte COMANDO_NULO = -1;
	
	/**
	 * Comando PC-PIC que estable un pago de la
	 * maquina al jugador
	 * 
	 * 1er byte - cantidad de repeticiones de 128 monedas
	 * 2o  byte - resto de monedas
	 */
	public static final byte COMANDO_EFECTUAR_PAGO = 0x64; // 0x64 == 100
	
	/**
	 * Comando PIC-PC que verifica que una moneda ha salido
	 * por la tronera de pago de la maquina
	 * 
	 * Este comando no devuelve informacion de ningun tipo, 
	 * simplemente cuando es ejecutado indica que una salida
	 * ha salido por la tronera.
	 * 
	 */
	public static final byte COMANDO_VERIFICAR_PAGO_MONEDA = 0x65;  // 0x65 == 101
	
	/**
	 * Comando PIC-PC que indica que un boton ha sido presionado
	 * 
	 * 1er byte	-	Primer bit que conforma el estado de la botoner
	 * 2er byte	-	Segundo bit que conforma el estado de la botoner
	 * 3er byte	-	Tercer bit que conforma el estado de la botoner
	 * 4o byte	-	Cuarto bit que conforma el estado de la botoner
	 * 
	 * Mas adelante se TIENE que arreglar esto para que se mande un
	 * solo byte con los 4 bits contenidos. Revisar esto en el
	 * codigo del pic y en java tambien
	 * 
	 */
	public static final byte COMANDO_BOTON_PRESIONADO = 0x66;  // 0x66 == 102
	
	/**
	 * Comando PIC-PC que indica que ha ingresado dinero a la maquina.
	 * No confundir ya que este comando no envia informacion
	 * asociada de ningun tipo. Este comando asume que una moneda 
	 * ingreso a la maquina, y comunica eso al PC.
	 * 
	 */
	public static final byte COMANDO_DINERO_INGRESADO = 0x67;  // 0x67 == 103
	
	/**
	 * Configura el estado (encendido-apagado) de los botones
	 * de la maquina. Se envia un byte con (1,0) por cada boton
	 * que se desea configurar. Se respeta una relacion de orden
	 * de bytes de llegada con el boton que se desea configurar.
	 * 1er byte->1er boton, 2o byte->2o boton, etc.
	 * 
	 */
	public static final byte COMANDO_CONFIGURAR_ESTADO_BOTONES = 0x68;  // 0x68 == 104
	

	/**
	 * Configura el juego de luces de los botones. 
	 * Si el id de juego de luces enviado es -1 se 
	 * finaliza el juego de luces y permanecen todas
	 * apagadas.
	 * 
	 */
	public static final byte COMANDO_CONFIGURAR_JUEGO_LUCES = 0x69;  // 0x69 == 105

	/**
	 * Corre el primer comando ejecutable del pic
	 * que es el de validacion. Esta validacion
	 * consta del envio de un codigo que tiene
	 * esta compilacion especifica en su codigo, 
	 * al pic para que el mismo lo compare con
	 * su propio codigo embebido en la compilacion
	 * del pic. 
	 * Si el codigo no se corresponde se borra
	 * totalmente la memoria EEPROM del chip, borrando
	 * entre todas estas cosas el bit que verifica
	 * si existio algun acceso invalido al mismo.
	 * Una vez que ese bit se apaga, el PIC durante
	 * su arranque normal valida ese bit y en caso
	 * de que no este seteado ni arranca
	 * 
	 */
	public static final byte COMANDO_VALIDAR_LICENCIA_PIC = 0x6A;  // 0x6A == 106
	
	/**
	 * El PIC luego de realizar sus chequeos de licenciamiento 
	 * devuelve un comando indicando el estado del licenciamiento
	 */
	public static final byte COMANDO_ESTADO_LICENCIAMIENTO_PIC = 0x6B;  // 0x6B == 107

	/**
	 * El PIC envia este comando cuando se le
	 * exige realizar un pago y lo excede 
	 * debido a que el credito disponible de
	 * la maquina no es suficiente para pagarlo.
	 *  
	 * 1er byte - cantidad de repeticiones de 128 monedas
	 * 2o  byte - resto de monedas
	 */
	public static final byte COMANDO_CAPACIDAD_PAGO_EXCEDIDA = 0x6C;  // 0x6C == 108

	/**
	 * El PIC envia este comando cuando el 
	 * switch de configuracion cambia de 
	 * estado   
	 * 1er byte - 1 o 0
	 */
	public static final byte COMANDO_CAMBIO_SWITCH_CONFIGURACION = 0x6D;  // 0x6D == 109

	/**
	 * El PIC envia este comando cuando el 
	 * switch de la llave del administrador 
	 * cambia de estado   
	 * 1er byte - 1 o 0
	 */
	public static final byte COMANDO_CAMBIO_SWITCH_LLAVE_ADMINISTRADOR = 0x6E;  // 0x6E == 110
	
	
	/**
	 * El PIC recibe este comando cuando el 
	 * estado del motor quiere ser cambiado
	 * 
	 * 1er byte - 1 prende / 0 apaga
	 */
	public static final byte COMANDO_CAMBIAR_ESTADO_MOTOR = 0x6F;  // 0x6F == 111
	
	/**
	 * El PIC recibe este comando cuando el 
	 * modo de expulsion de monedas quiere 
	 * ser cambiado
	 * 
	 * 1er byte - 0 MODO_EXPULSION_MONEDA_PAGO_JUGADOR / 1 MODO_EXPULSION_MONEDA_ADMINISTRADOR
	 */
	public static final byte COMANDO_CAMBIAR_MODO_EXPULSION_MONEDA = 0x70;  // 0x70 == 112
	
	/**
	 * El PIC recibe este comando cuando el 
	 * estado del motor quiere ser consultado
	 * 
	 * 1er byte - 1 prendido / 0 apagado
	 */
	public static final byte COMANDO_OBTENER_ESTADO_MOTOR = 0x71;  // 0x71 == 113

	/**
	 * Cuando el PIC recibe el comando 'COMANDO_OBTENER_ESTADO_MOTOR' 
	 * en ese momento genera un comando hacia el cliente con el codigo
	 * que indica el estado del motor
	 * 
	 * 1er byte - 1 prendido / 0 apagado
	 */
	public static final byte COMANDO_CODIGO_ESTADO_MOTOR = 0x72;  // 0x72 == 114

}