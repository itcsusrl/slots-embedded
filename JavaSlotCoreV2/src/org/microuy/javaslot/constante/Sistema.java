package org.microuy.javaslot.constante;

import org.apache.commons.lang3.RandomStringUtils;
import org.microuy.javaslot.logica.configuracion.ConfiguracionSlotsPremiosChicos;
import org.microuy.javaslot.logica.license.LogicaLicencia;

public class Sistema {

	/**
	 * Estado del servidor web al iniciar la aplicacion
	 */
	public static boolean WEBSERVER_HABILITADO = false;
	public static int WEBSERVER_PORT = 8080;
	
	/**
	 * Representa la configuracion de maquina a usarse para el juego, por ende
	 * para las apuestas reales
	 */
	public static Class configuracionMaquina = ConfiguracionSlotsPremiosChicos.class;
	
	/**
	 * Representa la configuracion usada en todo lo que represente testeos sobre
	 * la maquina. No deben de considerarse como valores generados por apuestas
	 * reales ya que no se debe de usar para eso
	 */
	public static Class configuracionTest = ConfiguracionSlotsPremiosChicos.class;
	
	/**
	 * Si el falso significa que no se va a mostrar la interfaz de usuario para
	 * jugar normalmente, o si simplemente se va usar la interfaz web junto al
	 * modulo estadistico
	 */
	public static boolean JUEGO_HABILITADO = true;
	
	/**
	 * Habilitacion del puerto de comunicacion virtual. Usado solamente durante
	 * la etapa de desarrollo para obviar la conexion de la placa
	 */
	public static boolean PUERTO_COMUNICACION_VIRTUAL = true;
	
	/**
	 * Usado junto al puerto de comunicacion virtual, para emular el
	 * funcionamiento de la botonera real
	 */
	public static boolean MOSTRAR_PANEL_BOTONES_VIRTUALES = false;
	
	/**
	 * El monto de la apuesta admitida en el juego. Esto actualmente limita el
	 * sistema a que solo pueda haber un tipo de apuesta. Lo que realmente me
	 * importa un rabano porque si manana se puede apostar otro valor es que
	 * todo anduvo bien y estamos haciendo guita!
	 */
	public static int APUESTA_ADMITIDA = 10;

	/**
	 * El incremento de la apuesta es de a esta cantidad
	 */
	public static float INCREMENTO_APUESTA = 0.2f;

	/**
	 * La cantidad maxima apostable por linea es el valor asignado aqui
	 */
	public static float APUESTA_MAXIMA_POR_LINEA = 1f;

	/**
	 * Factor multiplicador o divisor de moneda. Usado para modificar el tipo de
	 * moneda en juego y usar valores mas altos o mas bajos dependiendo de lo
	 * deseado
	 */
	public static int FACTOR_CONVERSION_MONEDA = 10;

	/**
	 * El monto apostado durante la ejecucion de una tirada gratuita generada
	 * por el bonus
	 */
	public static float MONTO_APUESTA_GRATUITA = 0.2f;

	/**
	 * Indica si el sistema hara uso o no
	 * de las jugadas ganadoras conformadas
	 * totalmente por figuras wild
	 * 
	 */
	public static byte APUESTA_WILD = 1;

	/**
	 * Las lineas apostadas durante la ejecucion de una tirada gratuita generada
	 * por el bonus
	 */
	public static short CANTIDAD_LINEAS_APUESTA_GRATUITA = 10;

	/**
	 * Define el porcentaje de pago de la maquina a el jugador. Ej.: De una
	 * apuesta de 100 pesos, 30 son devueltos al jugador, y la casa se queda con
	 * los 70 restantes. Esto no se cumple a rajatabla, sino no seria un juego
	 * muy divertido, sino que se va dando con el correr de las apuestas.
	 */
	public static int PORCENTAJE_JUGADOR = 70;

	/**
	 * El pozo puede tener un monto minimo al cual se precisa llegar antes de
	 * admitir la premiacion con el Jackpot
	 */
	public static int JACKPOT_MONTO_MINIMO = 2000;

	/**
	 * De cada apuesta que pueda participar del jackpot se retira un porcentaje
	 * para aportar al pozo
	 */
	public static float JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA = 2f;

	/**
	 * Una vez que se llega al monto minimo es necesario por parte del jugador
	 * estar dentro del porcentaje necesario para acceder luego definitivamente
	 * al JACKPOT. El rango de valores puede ser el rango de 0 a 100. Para
	 * ejemplificar, 0 significaria que el usuario no saca nunca el pozo, y 100
	 * que lo saca siempre que se llegue al monto minimo del JACKPOT
	 */
	public static float JACKPOT_PROBABILIDADES_JUGADOR = 0.1f; // 0.1 en un
																// rango de
																// 1...100
																// equivale
																// a una
																// probabilidad
																// de 1 en 1000

	/**
	 * Cuando se llega al monto indicado en este campo se realiza la apertura
	 * del pozo para empezar a participar por el tras cada tirada.
	 */
	public static float JACKPOT_APERTURA_POZO = 0f;

	/**
	 * De todas las apuestas se toma un porcentaje para el modulo 'menor o
	 * mayor'
	 */
	public static float MENOR_MAYOR_PORCENTAJE_EXTRAIDO_APUESTA = 1f;

	/**
	 * Cuantos resumenes de cobranza se obtienen desde la persistencia
	 */
	public static int RESUMENES_COBRANZA_LIMITE_OBTENCION = 100;
	
	/**
	 * Flag que indica si se desea que la salida
	 * estandar sea impresa al juego, o simplemente
	 * a la consola
	 */
	public static boolean SALIDA_ESTANDAR_UI = false;

	/**
	 * Salida estandar hacia un archivo.
	 * Si no se quiere que se imprima la
	 * salida estandar al sistema de archivos
	 * se debe de dejar el string en null
	 */
	public static String SALIDA_ESTANDAR_UI_FILE = "/home/sluy00001/Descargas/out.log";

	/**
	 * Habilita el debug de la comunicacion
	 * por el puerto serial
	 */
	public static boolean DEBUG_PUERTO_SERIAL = true;

	/**
	 * Nombre de la carpeta en el sistema de archivos donde se guarda la base de
	 * datos
	 */
	public static String dbFolder = "/home/sluy/";

	/**
	 * Nombre de la base de datos
	 */
	public static String dbName = "sluy";

	/**
	 * Protocolo usado para la conexion a la base de datos
	 */
	public static String dbProtocol = "jdbc:derby:directory:";

	/**
	 * Driver usado para la conexion a la base de datos
	 */
	public static String dbDriver = "org.apache.derby.jdbc.EmbeddedDriver";

	public void tmp1() {
		CONTENIDO_ARCHIVO_ALEATORIO1 = CONTENIDO_ARCHIVO_ALEATORIO2;
		CONTENIDO_ARCHIVO_ALEATORIO2 = CONTENIDO_ARCHIVO_ALEATORIO3;
		CONTENIDO_ARCHIVO_ALEATORIO3 = CONTENIDO_ARCHIVO_ALEATORIO4;
		CONTENIDO_ARCHIVO_ALEATORIO4 = CONTENIDO_ARCHIVO_ALEATORIO40;
		CONTENIDO_ARCHIVO_ALEATORIO5 = CONTENIDO_ARCHIVO_ALEATORIO300;
		CONTENIDO_ARCHIVO_ALEATORIO6 = CONTENIDO_ARCHIVO_ALEATORIO200;

		CONTENIDO_ARCHIVO_ALEATORIO10 = CONTENIDO_ARCHIVO_ALEATORIO400;
		CONTENIDO_ARCHIVO_ALEATORIO20 = new byte[] { 43, 12, 53, 1, 55, 77, 34,
				14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51,
				5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
				31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO30 = new byte[] { 43, 12, 53, 1, 55, 77, 34,
				14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34, 14,
				66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45, 12,
				87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO40 = new byte[] { 43, 12, 53, 1, 55, 77, 34,
				14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5,
				87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO50 = new byte[] { 43, 121, 22, 12, 110, 12,
				12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105,
				10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO60 = new byte[] { 43, 12, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9,
				55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO100 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
				51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO200 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO300 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34,
				14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45,
				12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO400 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41,
				51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO500 = new byte[] { 43, 121, 22, 12, 110, 12,
				12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105,
				10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO600 = new byte[] { 43, 12, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9,
				55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO1000 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
				51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO2000 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO3000 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34,
				14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45,
				12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO4000 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41,
				51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO5000 = new byte[] { 43, 121, 22, 12, 110,
				12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100,
				105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO6000 = new byte[] { 43, 12, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9,
				55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO10000 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
				51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO20000 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO30000 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34,
				14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45,
				12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO40000 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41,
				51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO50000 = new byte[] { 43, 121, 22, 12, 110,
				12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100,
				105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO60000 = new byte[] { 43, 12, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9,
				55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO100000 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
				51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO200000 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO300000 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34,
				14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45,
				12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO400000 = new byte[] { 43, 12, 53, 1, 55, 77,
				34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41,
				51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO500000 = new byte[] { 43, 121, 22, 12, 110,
				12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100,
				105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO600000 = new byte[] { 43, 12, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98,
				9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO1000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
				41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO2000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO3000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87,
				34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87,
				45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO4000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31,
				41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO5000000 = new byte[] { 43, 121, 22, 12, 110,
				12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100,
				105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO6000000 = new byte[] { 43, 12, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98,
				9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO10000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
				41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO20000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO30000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87,
				34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87,
				45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO40000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31,
				41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO50000000 = new byte[] { 43, 121, 22, 12,
				110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55,
				100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO60000000 = new byte[] { 43, 12, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98,
				9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO100000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
				41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO200000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO300000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87,
				34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87,
				45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO400000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31,
				41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO500000000 = new byte[] { 43, 121, 22, 12,
				110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55,
				100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO600000000 = new byte[] { 43, 12, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98,
				9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO1000000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
				41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO2000000000 = CONTENIDO_ARCHIVO_ALEATORIO600000000;
		CONTENIDO_ARCHIVO_ALEATORIO3000000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87,
				34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87,
				45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO4000000000 = new byte[] { 43, 12, 53, 1, 55,
				77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31,
				41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO5000000000 = new byte[] { 43, 121, 22, 12,
				110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55,
				100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO6000000000 = new byte[] { 43, 12, 43, 12,
				31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12,
				31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87,
				98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO10000000000 = new byte[] { 43, 12, 53, 1,
				55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
				31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO20000000000 = CONTENIDO_ARCHIVO_ALEATORIO600000000;
		CONTENIDO_ARCHIVO_ALEATORIO30000000000 = new byte[] { 43, 12, 53, 1,
				55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5,
				87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5,
				87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO40000000000 = new byte[] { 43, 12, 53, 1,
				55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12,
				31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO50000000000 = new byte[] { 43, 121, 22, 12,
				110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55,
				100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO60000000000 = new byte[] { 43, 12, 43, 12,
				31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12,
				31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87,
				98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO100000000000 = new byte[] { 43, 12, 53, 1,
				55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
				31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO200000000000 = CONTENIDO_ARCHIVO_ALEATORIO600000000;
		CONTENIDO_ARCHIVO_ALEATORIO300000000000 = new byte[] { 43, 12, 53, 1,
				55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5,
				87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5,
				87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO400000000000 = new byte[] { 43, 12, 53, 1,
				55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12,
				31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO500000000000 = new byte[] { 43, 121, 22, 12,
				110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55,
				100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO600000000000 = new byte[] { 43, 12, 43, 12,
				31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12,
				31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87,
				98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO1000000000000 = new byte[] { 43, 12, 53, 1,
				55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
				31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO2000000000000 = CONTENIDO_ARCHIVO_ALEATORIO600000000;
		CONTENIDO_ARCHIVO_ALEATORIO3000000000000 = new byte[] { 43, 12, 53, 1,
				55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5,
				87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5,
				87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO4000000000000 = new byte[] { 43, 12, 53, 1,
				55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12,
				31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO5000000000000 = new byte[] { 43, 121, 22,
				12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9,
				55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO6000000000000 = new byte[] { 43, 12, 43, 12,
				31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12,
				31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87,
				98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO10000000000000 = new byte[] { 43, 12, 53, 1,
				55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
				31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO20000000000000 = CONTENIDO_ARCHIVO_ALEATORIO600000000;
		CONTENIDO_ARCHIVO_ALEATORIO30000000000000 = new byte[] { 43, 12, 53, 1,
				55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5,
				87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5,
				87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO40000000000000 = new byte[] { 43, 12, 53, 1,
				55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12,
				31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO50000000000000 = new byte[] { 43, 121, 22,
				12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9,
				55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO60000000000000 = new byte[] { 43, 12, 43,
				12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43,
				12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12,
				87, 98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO100000000000000 = new byte[] { 43, 12, 53,
				1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO200000000000000 = new byte[] { 43, 12, 53,
				1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110,
				12, 12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO300000000000000 = new byte[] { 43, 12, 53,
				1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51,
				5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51,
				5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO400000000000000 = new byte[] { 43, 12, 53,
				1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12,
				31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO500000000000000 = new byte[] { 43, 121, 22,
				12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9,
				55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO600000000000000 = new byte[] { 43, 12, 43,
				12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43,
				12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12,
				87, 98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO1000000000000000 = new byte[] { 43, 12, 53,
				1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO2000000000000000 = new byte[] { 43, 12, 53,
				1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110,
				12, 12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO3000000000000000 = new byte[] { 43, 12, 53,
				1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51,
				5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51,
				5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO4000000000000000 = new byte[] { 43, 12, 53,
				1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12,
				31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO5000000000000000 = new byte[] { 43, 121, 22,
				12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9,
				55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO6000000000000000 = new byte[] { 43, 12, 43,
				12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43,
				12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12,
				87, 98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO10000000000000000 = new byte[] { 43, 12, 53,
				1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO20000000000000000 = new byte[] { 43, 12, 53,
				1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110,
				12, 12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO30000000000000000 = new byte[] { 43, 12, 53,
				1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51,
				5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51,
				5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO40000000000000000 = new byte[] { 43, 12, 53,
				1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12,
				31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO50000000000000000 = new byte[] { 43, 121,
				22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98,
				9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO60000000000000000 = new byte[] { 43, 12, 43,
				12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43,
				12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12,
				87, 98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO100000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10,
				4 };
		CONTENIDO_ARCHIVO_ALEATORIO200000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO300000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41,
				51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO400000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO500000000000000000 = new byte[] { 43, 121,
				22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98,
				9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO600000000000000000 = new byte[] { 43, 12,
				43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11,
				43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11,
				12, 87, 98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO1000000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10,
				4 };
		CONTENIDO_ARCHIVO_ALEATORIO2000000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO3000000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41,
				51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO4000000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO5000000000000000000 = new byte[] { 43, 121,
				22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98,
				9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO6000000000000000000 = new byte[] { 43, 12,
				43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11,
				43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11,
				12, 87, 98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO10000000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10,
				4 };
		CONTENIDO_ARCHIVO_ALEATORIO20000000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO30000000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41,
				51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO40000000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO50000000000000000000 = new byte[] { 43, 121,
				22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98,
				9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO60000000000000000000 = new byte[] { 43, 12,
				43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11,
				43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11,
				12, 87, 98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO100000000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10,
				4 };
		CONTENIDO_ARCHIVO_ALEATORIO200000000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO300000000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41,
				51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41,
				51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO400000000000000000000 = new byte[] { 43, 12,
				53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43,
				12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO500000000000000000000 = new byte[] { 43,
				121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12,
				87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO600000000000000000000 = new byte[] { 43, 12,
				43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11,
				43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11,
				12, 87, 98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO1000000000000000000000 = new byte[] { 43,
				12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12,
				12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105,
				10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO2000000000000000000000 = new byte[] { 43,
				12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12,
				12, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO3000000000000000000000 = new byte[] { 43,
				12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31,
				41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO4000000000000000000000 = new byte[] { 43,
				12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10,
				4 };
		CONTENIDO_ARCHIVO_ALEATORIO5000000000000000000000 = new byte[] { 43,
				121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12,
				87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO6000000000000000000000 = new byte[] { 43,
				12, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				11, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				11, 12, 87, 98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO10000000000000000000000 = new byte[] { 43,
				12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12,
				12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105,
				10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO20000000000000000000000 = new byte[] { 43,
				12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12,
				12, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO30000000000000000000000 = new byte[] { 43,
				12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31,
				41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO40000000000000000000000 = new byte[] { 43,
				12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10,
				4 };
		CONTENIDO_ARCHIVO_ALEATORIO50000000000000000000000 = new byte[] { 43,
				121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12,
				87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO60000000000000000000000 = new byte[] { 43,
				12, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				11, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				11, 12, 87, 98, 9, 55, 100, 105, 10, 4 };

		CONTENIDO_ARCHIVO_ALEATORIO100000000000000000000000 = new byte[] { 43,
				12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12,
				12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105,
				10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO200000000000000000000000 = new byte[] { 43,
				12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12,
				12, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO300000000000000000000000 = new byte[] { 43,
				12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31,
				41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31,
				41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO400000000000000000000000 = new byte[] { 43,
				12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12,
				43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10,
				4 };
		CONTENIDO_ARCHIVO_ALEATORIO500000000000000000000000 = new byte[] { 43,
				121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12,
				87, 98, 9, 55, 100, 105, 10, 4 };
		CONTENIDO_ARCHIVO_ALEATORIO600000000000000000000000 = new byte[] { 43,
				12, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				11, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12,
				11, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	}

	/**
	 * Esto no se cambia y simplemente es creado para marear a los
	 * decompiladores ya que con reflection no andan totalmente bien
	 */
	public static Class logicaLicencia = LogicaLicencia.class;

	/**
	 * Contenido del archivo aleatorio que no sirve para nada. Simplemente es
	 * para marear y agregar mas clases al pedo.
	 */
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO1 = { 43, 12, 53, 1, 55, 77, 34,
			14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5,
			87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO2 = { 43, 12, 53, 1, 55, 77, 34,
			14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5,
			87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO3 = { 43, 12, 53, 1, 55, 77, 34,
			14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66,
			12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98,
			9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO4 = { 43, 12, 53, 1, 55, 77, 34,
			14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87,
			45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO5 = { 43, 121, 22, 12, 110, 12,
			12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10,
			4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO6 = { 43, 12, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5,
			87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9, 55, 100,
			105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO10 = { 43, 12, 53, 1, 55, 77, 34,
			14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5,
			87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO20 = { 43, 12, 53, 1, 55, 77, 34,
			14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5,
			87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO30 = { 43, 12, 53, 1, 55, 77, 34,
			14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66,
			12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98,
			9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO40 = { 43, 12, 53, 1, 55, 77, 34,
			14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87,
			45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO50 = { 43, 121, 22, 12, 110, 12,
			12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10,
			4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO60 = { 43, 12, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5,
			87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9, 55, 100,
			105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO100 = { 43, 12, 53, 1, 55, 77, 34,
			14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5,
			87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO200 = { 43, 12, 53, 1, 55, 77, 34,
			14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5,
			87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO300 = { 43, 12, 53, 1, 55, 77, 34,
			14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66,
			12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98,
			9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO400 = { 43, 12, 53, 1, 55, 77, 34,
			14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87,
			45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO500 = { 43, 121, 22, 12, 110, 12,
			12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10,
			4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO600 = { 43, 12, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9, 55, 100,
			105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO1000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51,
			5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO2000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO3000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34, 14,
			66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87,
			98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO4000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5,
			87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO5000 = { 43, 121, 22, 12, 110, 12,
			12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10,
			4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO6000 = { 43, 12, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9, 55, 100,
			105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO10000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51,
			5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO20000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO30000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34, 14,
			66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87,
			98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO40000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5,
			87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO50000 = { 43, 121, 22, 12, 110,
			12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105,
			10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO60000 = { 43, 12, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9, 55, 100,
			105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO100000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51,
			5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO200000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO300000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34, 14,
			66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87,
			98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO400000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5,
			87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO500000 = { 43, 121, 22, 12, 110,
			12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105,
			10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO600000 = { 43, 12, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9, 55, 100,
			105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO1000000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51,
			5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO2000000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO3000000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34, 14,
			66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87,
			98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO4000000 = { 43, 12, 53, 1, 55, 77,
			34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5,
			87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO5000000 = { 43, 121, 22, 12, 110,
			12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105,
			10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO6000000 = { 43, 12, 43, 12, 31,
			41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9, 55,
			100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO10000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO20000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO30000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34,
			14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45, 12,
			87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO40000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO50000000 = { 43, 121, 22, 12, 110,
			12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105,
			10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO60000000 = { 43, 12, 43, 12, 31,
			41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9, 55,
			100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO100000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO200000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO300000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34,
			14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45, 12,
			87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO400000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO500000000 = { 43, 121, 22, 12,
			110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100,
			105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO600000000 = { 43, 12, 43, 12, 31,
			41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9, 55,
			100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO1000000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO2000000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO3000000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34,
			14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45, 12,
			87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO4000000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO5000000000 = { 43, 121, 22, 12,
			110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100,
			105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO6000000000 = { 43, 12, 43, 12, 31,
			41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9, 55,
			100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO10000000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO20000000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO30000000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87, 34,
			14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45, 12,
			87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO40000000000 = { 43, 12, 53, 1, 55,
			77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31, 41,
			51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO50000000000 = { 43, 121, 22, 12,
			110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100,
			105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO60000000000 = { 43, 12, 43, 12,
			31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31,
			41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9,
			55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO100000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO200000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO300000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87,
			34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45,
			12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO400000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO500000000000 = { 43, 121, 22, 12,
			110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100,
			105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO600000000000 = { 43, 12, 43, 12,
			31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31,
			41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9,
			55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO1000000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO2000000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO3000000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87,
			34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45,
			12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO4000000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO5000000000000 = { 43, 121, 22, 12,
			110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100,
			105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO6000000000000 = { 43, 12, 43, 12,
			31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31,
			41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9,
			55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO10000000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO20000000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO30000000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87,
			34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45,
			12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO40000000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO50000000000000 = { 43, 121, 22,
			12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55,
			100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO60000000000000 = { 43, 12, 43, 12,
			31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31,
			41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98, 9,
			55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO100000000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO200000000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO300000000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5, 87,
			34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87, 45,
			12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO400000000000000 = { 43, 12, 53, 1,
			55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO500000000000000 = { 43, 121, 22,
			12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55,
			100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO600000000000000 = { 43, 12, 43,
			12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12,
			31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98,
			9, 55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO1000000000000000 = { 43, 12, 53,
			1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO2000000000000000 = { 43, 12, 53,
			1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
			43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO3000000000000000 = { 43, 12, 53,
			1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5,
			87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87,
			45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO4000000000000000 = { 43, 12, 53,
			1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO5000000000000000 = { 43, 121, 22,
			12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55,
			100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO6000000000000000 = { 43, 12, 43,
			12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12,
			31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98,
			9, 55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO10000000000000000 = { 43, 12, 53,
			1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO20000000000000000 = { 43, 12, 53,
			1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
			43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO30000000000000000 = { 43, 12, 53,
			1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5,
			87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87,
			45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO40000000000000000 = { 43, 12, 53,
			1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO50000000000000000 = { 43, 121, 22,
			12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55,
			100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO60000000000000000 = { 43, 12, 43,
			12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12,
			31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98,
			9, 55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO100000000000000000 = { 43, 12, 53,
			1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO200000000000000000 = { 43, 12, 53,
			1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
			43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO300000000000000000 = { 43, 12, 53,
			1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51, 5,
			87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5, 87,
			45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO400000000000000000 = { 43, 12, 53,
			1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12, 31,
			41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO500000000000000000 = { 43, 121,
			22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9,
			55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO600000000000000000 = { 43, 12, 43,
			12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12,
			31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87, 98,
			9, 55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO1000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO2000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12,
			12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO3000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5,
			87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO4000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO5000000000000000000 = { 43, 121,
			22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9,
			55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO6000000000000000000 = { 43, 12,
			43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43,
			12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87,
			98, 9, 55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO10000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO20000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12,
			12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO30000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5,
			87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO40000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO50000000000000000000 = { 43, 121,
			22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9,
			55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO60000000000000000000 = { 43, 12,
			43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43,
			12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87,
			98, 9, 55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO100000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO200000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12,
			12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO300000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5,
			87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO400000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO500000000000000000000 = { 43, 121,
			22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9,
			55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO600000000000000000000 = { 43, 12,
			43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43,
			12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87,
			98, 9, 55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO1000000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO2000000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12,
			12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO3000000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41, 51,
			5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51, 5,
			87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO4000000000000000000000 = { 43, 12,
			53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43, 12,
			31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO5000000000000000000000 = { 43,
			121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87,
			98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO6000000000000000000000 = { 43, 12,
			43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43,
			12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12, 87,
			98, 9, 55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO10000000000000000000000 = { 43,
			12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
			43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO20000000000000000000000 = { 43,
			12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
			43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110,
			12, 12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO30000000000000000000000 = { 43,
			12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51,
			5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO40000000000000000000000 = { 43,
			12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO50000000000000000000000 = { 43,
			121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87,
			98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO60000000000000000000000 = { 43,
			12, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11,
			43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12,
			87, 98, 9, 55, 100, 105, 10, 4 };

	public byte[] CONTENIDO_ARCHIVO_ALEATORIO100000000000000000000000 = { 43,
			12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
			43, 12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO200000000000000000000000 = { 43,
			12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 110, 12, 12,
			43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 110,
			12, 12, 43, 12, 31, 41, 51, 5, 87, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO300000000000000000000000 = { 43,
			12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 121, 22, 12, 43, 12, 31, 41,
			51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 43, 12, 31, 41, 51,
			5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO400000000000000000000000 = { 43,
			12, 53, 1, 55, 77, 34, 14, 66, 12, 51, 12, 22, 12, 110, 12, 12, 43,
			12, 31, 41, 51, 5, 87, 45, 12, 87, 98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO500000000000000000000000 = { 43,
			121, 22, 12, 110, 12, 12, 43, 12, 31, 41, 51, 5, 87, 45, 12, 87,
			98, 9, 55, 100, 105, 10, 4 };
	public byte[] CONTENIDO_ARCHIVO_ALEATORIO600000000000000000000000 = { 43,
			12, 43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11,
			43, 12, 31, 41, 51, 5, 87, 34, 14, 66, 12, 51, 121, 22, 12, 11, 12,
			87, 98, 9, 55, 100, 105, 10, 4 };

	/**
	 * Genera un string de largo aleatorio. Este metodo usa una implementacion
	 * de la libreria lang de apache.
	 * 
	 * @return
	 */
	public static String obtenerStringAleatorio(int largo) {
		return RandomStringUtils.randomAlphanumeric(largo);
	}
}