package org.microuy.javaslot.constante;

import org.newdawn.slick.Color;

public class SistemaFrontend {

	//audio habilitado
	public static boolean AUDIO_FX_ENABLED = true;
	
	//ancho de pantalla
	public final static int SCREEN_WIDTH = 1024;
	
	//alto de pantalla
	public final static int SCREEN_HEIGTH = 768;
	
	//lo dice el nombre
	public static boolean FULLSCREEN = false;
	
	//el intervalo de ejecucion de la logica
	//y tambien del refrescado de pantalla
	public final static int INTERVALO_LOGICA = 10;

	//muestra el id de la figura
	//en los rodillos (DEBUG)
	public final static boolean MOSTRAR_DEBUG_RODILLO = false;

	/**
	 * Defino los colores para cada una de las lineas
	 */
	public final static Color COLOR_LINEA_1 = new Color(226,0,226,170);
	public final static Color COLOR_LINEA_2 = new Color(238,127,1,170);
	public final static Color COLOR_LINEA_3 = new Color(255,237,0,170);
	public final static Color COLOR_LINEA_4 = new Color(150,191,13,170);
	public final static Color COLOR_LINEA_5 = new Color(56,169,98,170);
	public final static Color COLOR_LINEA_6 = new Color(46,170,220,170);
	public final static Color COLOR_LINEA_7 = new Color(14,113,180,170);
	public final static Color COLOR_LINEA_8 = new Color(136,85,154,170);
	public final static Color COLOR_LINEA_9 = new Color(226,0,122,170);
	public final static Color COLOR_LINEA_10 = new Color(201,158,100,170);
	
	public final static Color COLOR_BONUS = new Color(0,100,255,170);
	public final static Color COLOR_SCATTER = new Color(0,255,100,170);
	public final static Color COLOR_JACKPOT = new Color(0,255,255,170);
	public final static Color COLOR_BONUS_MENOR_MAYOR = new Color(0,50,255,255);
	
	public static Color obtenerColorPorIdLinea(int lineaId) {
		
		switch(lineaId) {
		
		case 1:
			return COLOR_LINEA_1;
		case 2:
			return COLOR_LINEA_2;
		case 3: 
			return COLOR_LINEA_3;
		case 4: 
			return COLOR_LINEA_4;
		case 5: 
			return COLOR_LINEA_5;
		case 6: 
			return COLOR_LINEA_6;
		case 7: 
			return COLOR_LINEA_7;
		case 8: 
			return COLOR_LINEA_8;
		case 9: 
			return COLOR_LINEA_9;
		case 10: 
			return COLOR_LINEA_10;
		default:
			return null;
		}
	}
	
	public static Color obtenerColorPorFiguraTipo(int figuraTipo) {
		
		switch(figuraTipo) {
		
		case FiguraTipo.SCATTER:
			return COLOR_SCATTER;
		case FiguraTipo.BONUS:
			return COLOR_BONUS;
		case FiguraTipo.JACKPOT:
			return COLOR_JACKPOT;
		case FiguraTipo.BONUS_MENOR_MAYOR:
			return COLOR_BONUS_MENOR_MAYOR;
		default:
			return null;
		}
	}
}