package org.microuy.javaslot.expepcion;

public class ConfiguracionInvalidaException extends RuntimeException {

	public ConfiguracionInvalidaException(String message) {
		super(message);
	}
}
