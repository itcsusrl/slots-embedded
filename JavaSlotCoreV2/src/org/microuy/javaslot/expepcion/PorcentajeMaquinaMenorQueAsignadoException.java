package org.microuy.javaslot.expepcion;

public class PorcentajeMaquinaMenorQueAsignadoException extends SystemException {
	
	public PorcentajeMaquinaMenorQueAsignadoException(String mensaje) {
		super(mensaje);
	}
}
