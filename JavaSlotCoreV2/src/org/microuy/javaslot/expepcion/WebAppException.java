package org.microuy.javaslot.expepcion;

public class WebAppException extends RuntimeException {

	public WebAppException(String message) {
		super(message);
	}
}
