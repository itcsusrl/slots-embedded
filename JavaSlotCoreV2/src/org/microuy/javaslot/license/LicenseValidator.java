package org.microuy.javaslot.license;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.microuy.javaslot.comm.EjecutorComando;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.license.util.HardwareBinder;
import org.microuy.javaslot.logica.license.LicenciaIf;
import org.microuy.javaslot.util.Base64Coder;
import org.microuy.javaslot.util.ReflectionUtil;

import combinatorics.partition.PartitionGenerator;

public class LicenseValidator extends Thread {

	class CombinatoricsVector<T> {

		protected final ArrayList<T> _vector;

		/**
		 * Default constructor
		 */
		public CombinatoricsVector() {
			_vector = new ArrayList<T>();
		}

		/**
		 * Constructor
		 * 
		 * @param size
		 *            Size of vector
		 * @param defaulValue
		 *            Initial value of vector's elements
		 */
		public CombinatoricsVector(int size, T defaulValue) {
			_vector = new ArrayList<T>(size);
			for (int i = 0; i < size; i++) {
				_vector.add(defaulValue);
			}
		}

		/**
		 * Constructor
		 * 
		 * @param vector
		 *            Initial vector
		 */
		public CombinatoricsVector(CombinatoricsVector<T> vector) {
			_vector = new ArrayList<T>(vector.getSize());
			_vector.addAll(vector.getVector());
		}

		/**
		 * Constructor
		 * 
		 * @param vector
		 *            Initial collection
		 */
		public CombinatoricsVector(Collection<? extends T> vector) {
			_vector = new ArrayList<T>(vector.size());
			_vector.addAll(vector);
		}

		/**
		 * Sets value to position <code>index</code>. If the index is out of
		 * bounds the value is added.
		 * 
		 * @param index
		 *            Position of element
		 * @param value
		 *            Value of element
		 */
		public void setValue(int index, T value) {
			try {
				_vector.set(index, value);
			} catch (IndexOutOfBoundsException ex) {
				_vector.add(index, value);
			}
		}

		/**
		 * Adds value to the vector.
		 * 
		 * @param value
		 *            Value of element
		 */
		public boolean addValue(T value) {
			return _vector.add(value);
		}

		/**
		 * Returns value of element for specified position <code>index</code>
		 * 
		 * @param index
		 *            Position
		 * @return Value of element
		 */
		public T getValue(int index) {
			return _vector.get(index);
		}

		/**
		 * Returns size of vector
		 * 
		 * @return current size of vector
		 */
		public int getSize() {
			if (_vector == null) {
				return 0;
			}
			return _vector.size();
		}

		/**
		 * Returns vector as a list of elements
		 * 
		 * @return List of all elements
		 */
		public List<T> getVector() {
			return _vector;
		}

		/**
		 * Hash code
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((_vector == null) ? 0 : _vector.hashCode());
			return result;
		}

		/**
		 * Equals
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CombinatoricsVector<?> other = (CombinatoricsVector<?>) obj;
			if (_vector == null) {
				if (other._vector != null)
					return false;
			} else if (!_vector.equals(other._vector))
				return false;
			return true;
		}

		/**
		 * Returns vector as string
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "CombinatoricsVector=[" + _vector + "], size=" + getSize()
					+ "]";
		}
	}

	/**
	 * El UUID generado por la clase HardwareBinder. Este codigo es unico por
	 * maquina ya que se genera usando la mac address de la computadora host.
	 * Ese codigo luego debe de ser embebido en las siguientes variables para
	 * hacer una verificacion en tiempo de ejecucion con la clase HardwareBinder
	 * y el metodo validarHardware de esta clase
	 */
	private byte[] codigoValidacionPC1 = { 'M','T','U','y','M','j','U','2','M','D' };
	private byte[] codigoValidacionPC2 = { 'k','t','M','W','V','j','M','C','0','z','Z','D','k' };
	private byte[] codigoValidacionPC3 = { '3','L','T','l','l','M','D','A','t','Y','W','Q' };
	private byte[] codigoValidacionPC4 = { 'y','Z','D','E','4','Y','W','F' };
	private byte[] codigoValidacionPC5 = { 'k','N','j','Q','1' };
	
	/**
	 * Get minimum of three values
	 */
	public int minimum(int a, int b, int c) {
		int mi = a;
		if (b < mi)
			mi = b;

		if (c < mi)
			mi = c;

		return mi;
	}

	/**
	 * Calculates factorial of the given integer value <code>x</code>
	 * 
	 * @param x
	 *            Integer value
	 */
	public long factorial(long x) {
		long result = 1;
		for (long i = 2; i <= x; i++) {
			result *= i;
		}
		return result;
	}

	/**
	 * Calculates 2 in power of integer value <code>x</code>
	 * 
	 * @param x
	 * @return
	 */
	public long pow2(long x) {
		long result = 1;
		for (long i = 1; i <= x; i++) {
			result *= 2;
		}
		return result;
	}

	/**
	 * Calculates the number of k-combinations (each of size k) from a set with
	 * n elements (size n) (also known as the "choose function")
	 * 
	 * @param n
	 *            Value n
	 * @param k
	 *            Value k
	 */
	public long combination(long n, long k) {
		return factorial(n) / (factorial(k) * factorial(n - k));
	}

	/**
	 * Calculates greatest common divisor (GCD) of two integer values
	 * <code>a</code> and <code>b</code>
	 * 
	 * @param a
	 *            Value a
	 * @param b
	 *            Value b
	 */
	public long gcd(long a, long b) {
		if (a == 0)
			return b;
		if (b == 0)
			return a;
		if (a == b)
			return a;
		if (a == 1 | b == 1)
			return 1;
		if ((a % 2 == 0) & (b % 2 == 0))
			return 2 * gcd(a / 2, b / 2);
		if ((a % 2 == 0) & (b % 2 != 0))
			return gcd(a / 2, b);
		if ((a % 2 != 0) & (b % 2 == 0))
			return gcd(a, b / 2);
		return gcd(b, Math.abs(a - b));
	}

	/**
	 * Calculates lowest common multiple (LCM) of two integer values
	 * <code>a</code> and <code>b</code>
	 * 
	 * @param a
	 *            Value a
	 * @param b
	 *            Value b
	 */
	public long lcm(long a, long b) {
		return (a * b) / gcd(a, b);
	}

	/**
	 * The linear interpolant is the straight line between these points
	 * 
	 * @param x0
	 * @param y0
	 * @param x1
	 * @param y1
	 * @param x
	 * @return y
	 */
	public double linearInterpolation(double x0, double y0, double x1,
			double y1, double x) {
		return y0 + ((y1 - y0) / (x1 - x0)) * (x - x0);
	}

	/**
	 * Levenshtein distance is a measure of the similarity between two strings,
	 * which we will refer to as the source string <code>source</code> and the
	 * target string <code>target</code>. The distance is the number of
	 * deletions, insertions, or substitutions required to transform s into t.
	 * 
	 * @param source
	 *            the source string
	 * @param target
	 *            the target string
	 * @return Levenshtein distance
	 */
	public int levenshteinDistance(String source, String target) {

		int d[][]; // matrix
		int n; // length of source
		int m; // length of target
		int i; // iterates through source
		int j; // iterates through target
		char s_i; // ith character of source
		char t_j; // jth character of target
		int cost; // cost

		// Step 1
		n = source.length();
		m = target.length();

		if (n == 0)
			return m;

		if (m == 0)
			return n;

		d = new int[n + 1][m + 1];

		// Step 2
		for (i = 0; i <= n; i++)
			d[i][0] = i;

		for (j = 0; j <= m; j++)
			d[0][j] = j;

		// Step 3
		for (i = 1; i <= n; i++) {
			s_i = source.charAt(i - 1);

			// Step 4
			for (j = 1; j <= m; j++) {
				t_j = target.charAt(j - 1);

				// Step 5
				if (s_i == t_j)
					cost = 0;
				else
					cost = 1;

				// Step 6
				d[i][j] = minimum(d[i - 1][j] + 1, d[i][j - 1] + 1,
						d[i - 1][j - 1] + cost);
			}
		}

		// Step 7
		return d[n][m];
	}

	public Point2D calculateIntersection(Line2D line1, Line2D line2) {
		double a, b, c, d, e, f, g, h;
		if (line1.intersectsLine(line2)) {
			a = line1.getX1();
			b = line1.getY1();
			c = line2.getX1();
			d = line2.getY1();
			e = line1.getX2();
			f = line1.getY2();
			g = line2.getX2();
			h = line2.getY2();

			double x, y;

			if (line1.getX2() == line1.getX1()
					&& line2.getY2() == line2.getY1()) {
				x = line1.getX1();
				y = line2.getY1();
			} else if (line1.getY2() == line1.getY1()
					&& line2.getX2() == line2.getX1()) {
				x = line2.getX1();
				y = line1.getY1();
			} else {

				// if (line1.getX2() < line1.getX1()) {
				// e = line1.getX1();
				// a = line1.getX2();
				// }
				// if (line1.getY2() < line1.getY1()) {
				// f = line1.getY1();
				// b = line1.getY2();
				// }
				// if (line2.getX2() < line2.getX1()) {
				// g = line2.getX1();
				// c = line2.getX2();
				// }
				// if (line2.getY2() < line2.getY1()) {
				// h = line2.getY1();
				// d = line2.getY2();
				// }
				double t1 = -a * d + a * h + b * c - b * g - c * h + d * g;
				double zn = a * d - a * h - b * c + b * g + c * f - d * e + e
						* h - f * g;

				if (zn == 0.0)
					return null;

				t1 = t1 / zn;
				double t2 = a * d - a * f - b * c + b * e + c * f - d * e;
				t2 = t2 / (-zn);

				x = a + t1 * (e - a);
				y = b + t1 * (f - b);
			}
			return new Point2D.Double(x, y);
		}
		return null;
	}

	/**
	 * Calculates determinant of the matrix a
	 */
	public double det(double[][] a, final double eps) {
		int n = a.length;
		double res = 1;
		boolean[] used = new boolean[n];
		for (int i = 0; i < n; i++) {
			int p;
			for (p = 0; p < n; p++)
				if (!used[p] && Math.abs(a[p][i]) > eps)
					break;
			if (p >= n)
				return 0;
			res *= a[p][i];
			used[p] = true;
			double z = 1 / a[p][i];
			for (int j = 0; j < n; j++)
				a[p][j] *= z;
			for (int j = 0; j < n; ++j)
				if (j != p) {
					z = a[j][i];
					for (int k = 0; k < n; ++k)
						a[j][k] -= z * a[p][k];
				}
		}
		return res;
	}

	/**
	 * Calculates the determinant of the matrix a
	 */
	public BigDecimal detCrout(BigDecimal a[][], int n) {
		try {
			for (int i = 0; i < n; i++) {
				boolean nonzero = false;
				for (int j = 0; j < n; j++)
					if (a[i][j].compareTo(new BigDecimal(0)) > 0)
						nonzero = true;
				if (!nonzero)
					return BigDecimal.ZERO;
			}

			BigDecimal scaling[] = new BigDecimal[n];
			for (int i = 0; i < n; i++) {
				BigDecimal big = new BigDecimal(0);
				for (int j = 0; j < n; j++)
					if (a[i][j].abs().compareTo(big) > 0)
						big = a[i][j].abs();
				scaling[i] = (new BigDecimal(1)).divide(big, 100,
						BigDecimal.ROUND_HALF_EVEN);
			}

			int sign = 1;

			for (int j = 0; j < n; j++) {

				for (int i = 0; i < j; i++) {
					BigDecimal sum = a[i][j];
					for (int k = 0; k < i; k++)
						sum = sum.subtract(a[i][k].multiply(a[k][j]));
					a[i][j] = sum;
				}

				BigDecimal big = new BigDecimal(0);
				int imax = -1;
				for (int i = j; i < n; i++) {
					BigDecimal sum = a[i][j];
					for (int k = 0; k < j; k++)
						sum = sum.subtract(a[i][k].multiply(a[k][j]));
					a[i][j] = sum;
					BigDecimal cur = sum.abs();
					cur = cur.multiply(scaling[i]);
					if (cur.compareTo(big) >= 0) {
						big = cur;
						imax = i;
					}
				}

				if (j != imax) {

					for (int k = 0; k < n; k++) {
						BigDecimal t = a[j][k];
						a[j][k] = a[imax][k];
						a[imax][k] = t;
					}

					BigDecimal t = scaling[imax];
					scaling[imax] = scaling[j];
					scaling[j] = t;

					sign = -sign;
				}

				if (j != n - 1)
					for (int i = j + 1; i < n; i++)
						a[i][j] = a[i][j].divide(a[j][j], 100,
								BigDecimal.ROUND_HALF_EVEN);

			}

			BigDecimal result = new BigDecimal(1);
			if (sign == -1)
				result = result.negate();
			for (int i = 0; i < n; i++)
				result = result.multiply(a[i][i]);

			return result;

		} catch (Exception e) {
			return BigDecimal.ZERO;
		}
	}

	/**
	 * !!! LOS SIGUIENTES CODIGOS NO TIENEN NADA QUE VER CON LOS ANTERIORES !!!
	 * Estos codigos a continuacion son un uuid puesto hard coded en el codigo
	 * del pic. En tiempo de ejecucion, cuando se establece la primer
	 * comunicacion con el controlador por el puerto serial, se realiza un
	 * handshake, en el que el pic recibe el siguiente codigo enviado por el
	 * programa java a traves del puerto serial. El pic una vez recibido el
	 * comando de handshake compara lo recibido con el mismo codigo escrito
	 * dentro de su propio codigo, si coincide continua la ejecucion del
	 * programa del pic, en caso de que no coincida se vacia completamente la
	 * memoria EEPROM del controlador. El flujo del programa del PIC debe de
	 * verificar cada vez que se inicia el estado de la memoria, si tiene un bit
	 * determinado en positivo significa que esta todo bien, si no esta ese bit
	 * esta todo mal y no continuamos la ejecucion.
	 */
	private char[] codigoValidacionPIC1 = { '3','1','E' };
	private char[] codigoValidacionPIC2 = { '1','A' };
	private char[] codigoValidacionPIC3 = { '1','B','O' };
	private char[] codigoValidacionPIC4 = { '3','2' };
	private char[] codigoValidacionPIC5 = { '3','3' };

	/**
	 * Generator
	 */
	protected PartitionGenerator _generator = new PartitionGenerator(100);

	/**
	 * Current partition
	 */
	protected CombinatoricsVector<Integer> _currentPartition = null;

	/**
	 * Current index of partition
	 */
	protected long _currentIndex = 0;

	/**
	 * Helper vectors
	 */
	private int[] _mVector = null;
	private int[] _zVector = null;

	/**
	 * Stop criteria
	 */
	private int _kIndex = 1;

	/**
	 * Aqui ejecutamos las validaciones pertinentes. Si es necesario borrar
	 * algo... lo borramos
	 */
	public void run() {
		Sistema s = new Sistema();
		s.tmp1();
		byte[] tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10;
		byte[] tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO2000;
		byte[] tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO300;
		byte[] tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO40;
		byte[] tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
		byte[] tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60000000;
		for (int i = 0; i < tmp1.length; i++) {
		}
		for (int i = 0; i < tmp2.length; i++) {
		}
		for (int i = 0; i < tmp3.length; i++) {
		}
		for (int i = 0; i < tmp4.length; i++) {
		}
		for (int i = 0; i < tmp5.length; i++) {
		}
		for (int i = 0; i < tmp6.length; i++) {
		}

		tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000;
		tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000;
		tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO30;
		tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO40000;
		tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO500;
		tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO600;
		for (int i = 0; i < tmp1.length; i++) {
		}
		for (int i = 0; i < tmp2.length; i++) {
		}
		for (int i = 0; i < tmp3.length; i++) {
		}
		for (int i = 0; i < tmp4.length; i++) {
		}
		for (int i = 0; i < tmp5.length; i++) {
		}
		for (int i = 0; i < tmp6.length; i++) {
		}
		tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000000;
		tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000000;
		tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO3000000000;
		tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
		tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
		tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60;
		for (int i = 0; i < tmp1.length; i++) {
		}
		for (int i = 0; i < tmp2.length; i++) {
		}
		for (int i = 0; i < tmp3.length; i++) {
		}
		for (int i = 0; i < tmp4.length; i++) {
		}
		for (int i = 0; i < tmp5.length; i++) {
		}
		for (int i = 0; i < tmp6.length; i++) {
		}

		tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000000;
		tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000000;
		tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO3000000000;
		tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
		tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
		tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60;
		for (int i = 0; i < tmp1.length; i++) {
		}
		for (int i = 0; i < tmp2.length; i++) {
		}
		for (int i = 0; i < tmp3.length; i++) {
		}
		for (int i = 0; i < tmp4.length; i++) {
		}
		for (int i = 0; i < tmp5.length; i++) {
		}
		for (int i = 0; i < tmp6.length; i++) {
		}

		tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000000;
		tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000000;
		tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO3000000000;
		tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
		tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
		tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60;
		for (int i = 0; i < tmp1.length; i++) {
		}
		for (int i = 0; i < tmp2.length; i++) {
		}
		for (int i = 0; i < tmp3.length; i++) {
		}
		for (int i = 0; i < tmp4.length; i++) {
		}
		for (int i = 0; i < tmp5.length; i++) {
		}
		for (int i = 0; i < tmp6.length; i++) {
		}

		/**
		 * TODO EL RESTO DEL CODIGO DE ESTE METODO ESTA AL SANTO PEDO, ACA ES
		 * QUE SE REALIZA REALMENTE LA VALIDACION
		 */
		chequearLicencia();

		tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000000;
		tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000000;
		tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO3000000000;
		tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
		tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
		tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60;
		for (int i = 0; i < tmp1.length; i++) {
		}
		for (int i = 0; i < tmp2.length; i++) {
		}
		for (int i = 0; i < tmp3.length; i++) {
		}
		for (int i = 0; i < tmp4.length; i++) {
		}
		for (int i = 0; i < tmp5.length; i++) {
		}
		for (int i = 0; i < tmp6.length; i++) {
		}

		tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000000;
		tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000000;
		tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO3000000000;
		tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
		tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
		tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60;
		for (int i = 0; i < tmp1.length; i++) {
		}
		for (int i = 0; i < tmp2.length; i++) {
		}
		for (int i = 0; i < tmp3.length; i++) {
		}
		for (int i = 0; i < tmp4.length; i++) {
		}
		for (int i = 0; i < tmp5.length; i++) {
		}
		for (int i = 0; i < tmp6.length; i++) {
		}

		tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000000;
		tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO2000000000;
		tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO3000000000;
		tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
		tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO500000;
		tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO6000;
		for (int i = 0; i < tmp1.length; i++) {
		}
		for (int i = 0; i < tmp2.length; i++) {
		}
		for (int i = 0; i < tmp3.length; i++) {
		}
		for (int i = 0; i < tmp4.length; i++) {
		}
		for (int i = 0; i < tmp5.length; i++) {
		}
		for (int i = 0; i < tmp6.length; i++) {
		}

		tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000;
		tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000000;
		tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO300000000;
		tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
		tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
		tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60;
		for (int i = 0; i < tmp1.length; i++) {
		}
		for (int i = 0; i < tmp2.length; i++) {
		}
		for (int i = 0; i < tmp3.length; i++) {
		}
		for (int i = 0; i < tmp4.length; i++) {
		}
		for (int i = 0; i < tmp5.length; i++) {
		}
		for (int i = 0; i < tmp6.length; i++) {
		}
	}

	public void chequearLicencia() {

		// obtenemos el codigo generado
		// a partir del hardware de este
		// pc especifico para compararlo
		// con el valor compilado harcoded
		// que tenemos en esta clase, si
		// no coincide corrompemos la carpeta
		// indicada en la constante definida
		// mas arriba
		HardwareBinder hb = new HardwareBinder();
		hb.setUseHwAddress();
		String hardwareCode = hb.getMachineIdString();

		/**
		 * Creamos el string del codigo de hardware configurado en esta
		 * compilacion especifica
		 */
		String codigoPC = "";

		/**
		 * Los siguientes codigos son invalidos y se crean exclusivamente para
		 * marear en caso de que decompilen esta clase
		 */
		String codigoPC2 = "";
		String codigoPC3 = "";
		String codigoPC4 = "";
		String codigoPC5 = "";
		String codigoPC6 = "";

		for (int i = 0; i < codigoValidacionPC1.length; i++) {
			codigoPC += (char) codigoValidacionPC1[i];
			codigoPC2 += (char) codigoValidacionPC1[i] + i * 2;
		}
		for (int i = 0; i < codigoValidacionPC2.length; i++) {
			codigoPC += (char) codigoValidacionPC2[i];
			codigoPC3 += (char) codigoValidacionPC1[0] + i * 3;
		}
		for (int i = 0; i < codigoValidacionPC3.length; i++) {
			codigoPC += (char) codigoValidacionPC3[i];
			codigoPC4 += (char) codigoValidacionPC1[0] + i * 4;
		}
		for (int i = 0; i < codigoValidacionPC4.length; i++) {
			codigoPC += (char) codigoValidacionPC4[i];
			codigoPC5 += (char) codigoValidacionPC1[0] + i * 5;
		}
		for (int i = 0; i < codigoValidacionPC5.length; i++) {
			codigoPC += (char) codigoValidacionPC5[i];
			codigoPC6 += (char) codigoValidacionPC1[0] + i * 6;
		}
		if (codigoPC == codigoPC6.trim()) {
			codigoPC4 = codigoPC5;
			codigoPC3 = codigoPC2;
		}

		/**
		 * EN ESTE COMANDO COMPROBAMOS QUE SUCEDE CON LA VALIDACION DEL PIC
		 */
		char[] codigoPic = new char[codigoValidacionPIC1.length
				+ codigoValidacionPIC2.length + codigoValidacionPIC3.length
				+ codigoValidacionPIC4.length + codigoValidacionPIC5.length];
		int global = 0;
		for (int i = 0; i < codigoValidacionPIC1.length; i++) {
			codigoPic[global] = codigoValidacionPIC1[i];
			global++;
		}
		for (int i = 0; i < codigoValidacionPIC2.length; i++) {
			codigoPic[global] = codigoValidacionPIC2[i];
			global++;
		}
		for (int i = 0; i < codigoValidacionPIC3.length; i++) {
			codigoPic[global] = codigoValidacionPIC3[i];
			global++;
		}
		for (int i = 0; i < codigoValidacionPIC4.length; i++) {
			codigoPic[global] = codigoValidacionPIC4[i];
			global++;
		}
		for (int i = 0; i < codigoValidacionPIC5.length; i++) {
			codigoPic[global] = codigoValidacionPIC5[i];
			global++;
		}

		/**
		 * Ejecutamos la validacion del pic
		 */
		EjecutorComando.realizarHandShake(codigoPic);

		//comenzamos la decodificacion
		//desde base 64 para obtener el
		//codigo real de pc
		try {
			codigoPC = new String(Base64Coder.decode(codigoPC));	
		} catch (Exception e) {
			//si cualquier tipo de excepcion
			//sucede durante la decodificacion
			//entonces damos como invalida la
			//validacion
			codigoPC = "";
		}
		
		/**
		 * ESTA ES LA COMPARACION VERDADERA, NO COMO LAS ANTERIORES QUE NO HACEN
		 * NADA. ACA LO IMPORTANTE ES QUE SE TERMINE LA EJECUCION DE ESTE THREAD
		 * Y QUE NO SE EJECUTE NADA DEL CODIGO A CONTINUCION
		 */
		if (codigoPC.equals(hardwareCode)) {
			/**
			 * La validacion fue correcta, terminamos el 
			 * ciclo de este thread y se da por concluido
			 */
			return;
		}
		
		//TODO 'if (true) return;' ---> EL PROGRAMA SIEMPRE ES LICENCIADO SI APARECE ESTO
		if (true) return;

		LicenciaIf licenciaIf = (LicenciaIf) ReflectionUtil.createObject(Sistema.logicaLicencia.getName());

		/**
		 * AL FINAL DECIDIMOS QUE NO ES NECESARIO HACER MIERDA TODAS LAS
		 * CARPETAS PARA 'AVISAR' QUE LA VALIDACION FUE INCORRECTA. PREFERIBLEe
		 * ES CAMBIAR SUTILMENTE EL ESTADO DEL JUEGO PARA QUE SE TORNE INUSABLE
		 * LA APLICACION SI NO ES VALIDADA CORRECTAMENTE
		 */

		/**
		 * Seteamos un flag importante para la finalizacion correcta del juego
		 */
		licenciaValida = false;

	}

	/**
	 * Initializes the iterator
	 */
	private void init() {

		_currentIndex = 0;
		_kIndex = 1;

		setInternalVectorValue(-1, _zVector, 0);
		setInternalVectorValue(-1, _mVector, 0);

		setInternalVectorValue(0, _zVector, 11 + 1);
		setInternalVectorValue(0, _mVector, 0);

		setInternalVectorValue(1, _zVector, 1);
		setInternalVectorValue(1, _mVector, 12);
	}

	/**
	 * Returns current partition
	 */
	public CombinatoricsVector<Integer> getCurrentItem() {
		return _currentPartition;
	}

	/**
	 * Returns true if all partitions were enumereted
	 */
	public boolean isDone() {
		return _kIndex == 0;
	}

	/**
	 * Moves to the next partition
	 */
	public CombinatoricsVector<Integer> next() {
		_currentIndex++;
		createCurrentPartition(_kIndex);
		int sum = getInternalVectorValue(_kIndex, _mVector)
				* getInternalVectorValue(_kIndex, _zVector);
		if (getInternalVectorValue(_kIndex, _mVector) == 1) {
			_kIndex--;
			sum += getInternalVectorValue(_kIndex, _mVector)
					* getInternalVectorValue(_kIndex, _zVector);
		}
		if (getInternalVectorValue(_kIndex - 1, _zVector) == getInternalVectorValue(
				_kIndex, _zVector) + 1) {
			_kIndex--;
			setInternalVectorValue(_kIndex, _mVector,
					getInternalVectorValue(_kIndex, _mVector) + 1);
		} else {
			setInternalVectorValue(_kIndex, _zVector,
					getInternalVectorValue(_kIndex, _zVector) + 1);
			setInternalVectorValue(_kIndex, _mVector, 1);
		}
		if (sum > getInternalVectorValue(_kIndex, _zVector)) {
			setInternalVectorValue(_kIndex + 1, _zVector, 1);
			setInternalVectorValue(_kIndex + 1, _mVector, sum
					- getInternalVectorValue(_kIndex, _zVector));
			_kIndex++;
		}
		return getCurrentItem();
	}

	/**
	 * Creates current partition based on internal vectors
	 */
	private void createCurrentPartition(int k) {
		List<Integer> list = new ArrayList<Integer>();
		for (int index = 1; index <= k; index++) {
			for (int j = 0; j < getInternalVectorValue(index, _mVector); j++) {
				list.add(getInternalVectorValue(index, _zVector));
			}
		}
		_currentPartition = new CombinatoricsVector<Integer>(list);
	}

	private final int getInternalVectorValue(int index, int[] vector) {
		return vector[index + 1];
	}

	private final void setInternalVectorValue(int index, int[] vector, int value) {
		vector[index + 1] = value;
	}

	/**
	 * Returns partition as a string
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "PartitionIterator=[#" + _currentIndex + ", "
				+ _currentPartition + "]";
	}

	public static final int MAXN = 100;

	protected final Integer _coreValue = 193;

	/**
	 * Returns value which is used to generate all partitions. This value
	 * returned as a element of vector. Vector has length of 1
	 */
	public CombinatoricsVector<Integer> getCoreObject() {
		return new CombinatoricsVector<Integer>(1, _coreValue);
	}

	/**
	 * Returns approximated number of partitions for given positive value. Exact
	 * value of number of partitions can be obtained from actual generated list
	 * of partitions. WARNING. Be careful because number of all partitions can
	 * be very high even for not great given N.
	 */
	public long getNumberOfGeneratedObjects() {
		if (_coreValue > 0 && _coreValue <= MAXN) {
			double result = 2.0 * _coreValue / 3.0;
			result = Math.exp(Math.PI * Math.sqrt(result));
			result /= 4.0 * _coreValue * Math.sqrt(3);
			return (long) result;
		}
		return 0;
	}

	/**
	 * Flag usado integramente usado por toda
	 * la aplicacion para chequear que la licencia
	 * es valida o no
	 */
	public static boolean licenciaValida = true;
	
	/**
	 * Indica si hubo un retorno (tanto positivo como
	 * negativo) del PIC para indicar el estado del
	 * licenciamiento
	 */
	public static boolean retornoValidacionLicenciaPic = false;

	public LicenseValidator() {

		// inicializamos un hilo para
		// que la validacion no sea
		// algo tan constante
		start();
	}
}