package org.microuy.javaslot.license.util;

import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.license.LicenseValidator;

/**
 * En un periodo determinado aleatoriamente se
 * inicia la validacion de la licencia tanto
 * del programa java como del controlador
 * pic de la placa
 * 
 * @author Pablo Caviglia
 */
public class LicenciaRandomValidationTimeThread extends Thread {

	public LicenciaRandomValidationTimeThread() {
		start();
	}
	
	public void run() {
		
		int i = ((int)(Math.random() * (float)2) + 1) * 1000;
		
		//dormimos el thread por un tiempo
		//determinado asi la validacion se
		//realiza aleatoriamente en el tiempo
		try {
			Thread.sleep(i);
		} catch (InterruptedException e) {
		}
		
		/**
		 * Ejecutamos el hilo que se
		 * encarga de realizar las
		 * validaciones de sistema 
		 * correspondientes
		 */
		new LicenseValidator();
		
		/**
		 * Ya que estamos en un hilo, y la clase
		 * LicenseValidator es un hilo, nos quedamos
		 * esperando 10 segundos a que finalicen las
		 * rutinas de licenciamiento, si en ese tiempo
		 * no es verdadero que se licencio todo 
		 * correctamente procedemos a invalidar el
		 * software 
		 */
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
		}
		
		/**
		 * Si no hubo retorno en 10 segundos por
		 * parte del PIC indicando el estado de
		 * la validacion, asumimos que nos quieren
		 * hackear e invalidamos todo
		 */
		if(!Sistema.PUERTO_COMUNICACION_VIRTUAL) {

			//solo si el puerto de comunicacion
			//no es virtual es que chequeamos el
			//retorno del pic
			if(!LicenseValidator.retornoValidacionLicenciaPic) {
				LicenseValidator.licenciaValida = false;
			}
		}
	}
}