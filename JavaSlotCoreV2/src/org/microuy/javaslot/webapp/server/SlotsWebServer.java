package org.microuy.javaslot.webapp.server;

import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.expepcion.WebAppException;
import org.microuy.javaslot.webapp.server.servlet.EstadisticaMaquinaServlet;
import org.microuy.javaslot.webapp.server.servlet.PrincipalServlet;
import org.microuy.javaslot.webapp.server.servlet.ProbabilidadMaquinaServlet;
import org.microuy.javaslot.webapp.server.servlet.TestEstadisticoServlet;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;

public class SlotsWebServer {
	
	public SlotsWebServer() {
		
		//inicializo el puerto
		Server server = new Server(Sistema.WEBSERVER_PORT);
		Context root = new Context(server,"/",Context.SESSIONS);
		root.addServlet(new ServletHolder(new TestEstadisticoServlet()), "/test_estadistico");
		root.addServlet(new ServletHolder(new ProbabilidadMaquinaServlet()), "/probabilidad_maquina");
		root.addServlet(new ServletHolder(new EstadisticaMaquinaServlet()), "/estadistica_maquina");
		root.addServlet(new ServletHolder(new PrincipalServlet()), "/");

		try {
			server.start();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebAppException("Error inicializando servidor web");
		}
	}
}
