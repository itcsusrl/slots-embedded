package org.microuy.javaslot.webapp.server.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.dominio.MultiplicadorComparator;
import org.microuy.javaslot.dominio.probabilidad.ProbabilidadJugadaGanadora;
import org.microuy.javaslot.dominio.probabilidad.ProbabilidadJugadaGanadoraMultiplicadorComparator;
import org.microuy.javaslot.util.MaquinaPoolManager;

public class ProbabilidadMaquinaServlet extends HttpServlet {

	private Maquina maquina;

	//usado para formatear numeros decimales
	private DecimalFormat twoDForm = new DecimalFormat("#.##");

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	public void init() throws ServletException {
		
		//obtenemos la instancia de la maquina
		//del juego para obtener valores
		//estadisticos de ella
		maquina = MaquinaPoolManager.obtenerInstanciaMaquinaJuego();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse resp)
			throws ServletException, IOException {

		PrintWriter out = resp.getWriter();

		out.println("<table>");
		out.println("<tr>");
		out.println("<td>");
		out.println("<h1> Probabilidad Maquina </h1>");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>");
		out.println("Muestra informacion probabilistica relacionada a la configuracion de la maquina");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");

		out.println("<table><tr><td>&nbsp;</td></tr></table>");

		out.println("<table>");
		out.println("<tr>");
		out.println("<td>");
		out.println("Configuracion:");
		out.println("</td>");
		out.println("<td>");
		out.println("<b>" + maquina.configuracion.getClass().getSimpleName() + "</b>");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");
		

		/**
		 * Primer tabla
		 */
		out.println("<table>");
		out.println("<tr bgcolor='#2EFE2E'>");
		out.println("<td width='150px'>");
		out.println("Probabilidad Jugador");
		out.println("</td>");
		out.println("<td width='100px'>");
		out.println(twoDForm.format(maquina.probabilidad.probabilidadJugador) + "%");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr bgcolor='#2EFE2E'>");
		out.println("<td width='150px'>");
		out.println("Probabilidad Maquina");
		out.println("</td>");
		out.println("<td width='100px'>");
		out.println(twoDForm.format(maquina.probabilidad.probabilidadMaquina) + "%");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr bgcolor='#2EFE2E'>");
		out.println("<td width='150px'>");
		out.println("Cant. Combinaciones");
		out.println("</td>");
		out.println("<td width='100px'>");
		out.println(maquina.probabilidad.combinaciones);
		out.println("</td>");
		out.println("</tr>");
		
		out.println("<tr>");
		out.println("<td>");
		out.println("<br/>");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");

		//recalculamos
		float probabilidadJugador = ((float)maquina.probabilidad.maximoPagable * 100) / (float)maquina.probabilidad.maximoApostable;
		
		/**
		 * Iteramos sobre la lista de objetos con
		 * datos acerca de las probabilidades de
		 * cada jugada ganadora para mostrar estos
		 * datos en pantalla
		 */
		out.println("<table>");
		out.println("<tr bgcolor='#00FFFF'>");
		out.println("<td width='140px'>");
		out.println("Jugada");
		out.println("</td>");
		out.println("<td width='140px'>");
		out.println("Repeticiones");
		out.println("</td>");
		out.println("<td>");
		out.println("Multiplicaciones");
		out.println("</td>");
		out.println("<td>");
		out.println("Prob. Ganadoras");
		out.println("</td>");
		out.println("</tr>");

		long totalRepeticionesGanadoras = 0;
		long totalMultiplicacionesGanadoras = 0;
		
		//iteramos la lista
		List<ProbabilidadJugadaGanadora> probabilidadesJugadaGanadora = maquina.probabilidad.probabilidadJugadasGanadoras;

		//ordeno la lista de jugadas ganadoras por
		//su factor multiplicador en orden descendente
		Collections.sort(probabilidadesJugadaGanadora, new ProbabilidadJugadaGanadoraMultiplicadorComparator());
		
		for(ProbabilidadJugadaGanadora probabilidadJugadaGanadora : probabilidadesJugadaGanadora) {
			
			out.println("<tr>");
			out.println("<td>");
			out.println(probabilidadJugadaGanadora.jugadaGanadora.obtenerFormateado());
			out.println("</td>");
			out.println("<td>");
			out.println(probabilidadJugadaGanadora.repeticiones);
			out.println("</td>");
			out.println("<td align='center'>");
			out.println(probabilidadJugadaGanadora.multiplicaciones);
			out.println("</td>");
			out.println("<td align='center'>");
			out.println(twoDForm.format(probabilidadJugadaGanadora.porcentajePagoEntreGanadoras) + "%");
			out.println("</td>");
			out.println("</tr>");
			
			totalRepeticionesGanadoras += probabilidadJugadaGanadora.repeticiones;
			totalMultiplicacionesGanadoras += probabilidadJugadaGanadora.multiplicaciones;

		}
		
		out.println("<tr bgcolor='#00FFFF'>");
		out.println("<td width='140px'>");
		out.println("TOTAL");
		out.println("</td>");
		out.println("<td>");
		out.println(totalRepeticionesGanadoras);
		out.println("</td>");
		out.println("<td>");
		out.println(totalMultiplicacionesGanadoras + " (" + twoDForm.format(probabilidadJugador) + "%)" );
		out.println("</td>");
		out.println("<td align='center'>");
		out.println("100%");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");

		
		
		/**
		 * Tabla con descripcion dinamica
		 */
		out.println("<table><tr><td>&nbsp;</td></tr></table>");
		
		out.println("<table width='800px'>");
		out.println("<tr>");
		out.println("<td>");
		out.println("<h3> Descripcion dinamica </h3>");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>");
		out.println("Esta configuracion cuenta con <b>" + maquina.probabilidad.combinaciones + "</b> combinaciones diferentes.<br>");
		out.println("Las probabilidades del jugador y de la maquina mostrados en los porcentajes planteados en la tabla anterior <br>" +
					"son calculados a partir de dos datos extraidos de la maquina, el monto apostable y el monto pagable. <br>" +
					"Estas dos variables indican, por un lado cuanto es el monto de apuesta (imaginable) si se realizara una apuesta <br>" +
					"por cada combinacion posible, y por otro lado cual es el monto de pago imaginable de la maquina si se pagara el <br>" +
					"multiplicador de cada jugada ganadora que salga entre las <b>" + maquina.probabilidad.maximoApostable + "</b> combinaciones <br> posibles. <br>" +
					"El monto de apuesta imaginable es exactamente la cantidad de combinaciones <b>" + maquina.probabilidad.combinaciones + "</b> <br>" +
					"El monto pagable por la maquina es la suma de los multiplicadores de las jugadas ganadoras <b>" + maquina.probabilidad.maximoPagable + "</b> <br><br>" +
					"Haciendo una simple regla de tres llegamos a la conclusion que: <br><br>" +
					maquina.probabilidad.maximoApostable + " (Monto Apostado) -------------  100% <br>" +
					maquina.probabilidad.maximoPagable + " (Monto Recibido)  --------------- " + twoDForm.format(probabilidadJugador) + "% (Probabilidad Jugador)" );
		
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");

		out.println("<table><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr></table>");
		
		
	}
}