package org.microuy.javaslot.webapp.server.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.dominio.estadistica.EstadisticaMaquina;
import org.microuy.javaslot.util.MaquinaPoolManager;

public class TestEstadisticoServlet extends EstadisticoGenericServlet {

	private Maquina maquinaTest;
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse resp)
			throws ServletException, IOException {

		/**
		 * Lo hacemos aca y no el init
		 * asi no se inicializa junto
		 * a la maquina real, sino que
		 * cuando se vaya a usar solamente
		 */
		if(maquinaTest == null) {
			maquinaTest = MaquinaPoolManager.obtenerInstanciaMaquinaTest(Sistema.configuracionTest);
		}
		
		PrintWriter out = resp.getWriter();

		String cantidadTiradasStr = request.getParameter("cantidadTiradas");
		if (cantidadTiradasStr == null || cantidadTiradasStr.trim().equalsIgnoreCase("")) {
			cantidadTiradasStr = "1000";
		}

		String acumularStr = request.getParameter("acumular");
		
		out.println("<table>");
		out.println("<tr>");
		out.println("<td>");
		out.println("<h1> Test Estadistico </h1>");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>");
		out.println("Se realizan apuestas automaticamente para obtener valores estadisticos basados " +
					"en tiradas reales. Estas tiradas no afectan directamente a la instancia de la " +
					"maquina que se esta jugando debido a que se instancia una nueva maquina para " +
					"ejecutar los tests");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");

		out.println("<table><tr><td>&nbsp;</td></tr></table>");
		
		out.println("<form id='cantidadTiradasForm' method='post' action='test_estadistico'>");
		out.println("<table>");
		out.println("<tr>");
		out.println("<td>");
		out.println("Configuracion:");
		out.println("</td>");
		out.println("<td>");

		int probabilidadJugador = Sistema.PORCENTAJE_JUGADOR;
		if(probabilidadJugador <= 0) {
			probabilidadJugador = (int)Float.parseFloat(""+maquinaTest.probabilidad.probabilidadJugador);
		}
		
		out.println("<b>" + maquinaTest.configuracion.getClass().getSimpleName() + "</b> (" + probabilidadJugador + "% jugador)");

		out.println("</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>");
		out.println("Ingresar cantidad tiradas:");
		out.println("</td>");
		out.println("<td>");
		out.println("<input type='text' name='cantidadTiradas' value='" + cantidadTiradasStr + "' />");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>");
		out.println("Acumular");
		out.println("</td>");
		out.println("<td>");
		out.println("<input type='checkbox' name='acumular' checked='checked' />");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td colspan='2' align='center'>");
		out.println("<input type='submit' value='A P O S T A R' style='width: 100%; height: 30px' />");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");
		out.println("</form>");
		
		if(cantidadTiradasStr != null && !cantidadTiradasStr.trim().equalsIgnoreCase("")) 
		{

			int cantidadTiradas = Integer.valueOf(cantidadTiradasStr);
			boolean acumular = false;
			
			if(acumularStr != null) {
				acumular = true;
			}
			
			try {
			
				EstadisticaMaquina estadisticaMaquina = maquinaTest.estadisticaMaquina;
				
				if(!acumular) {
					//limpio los valores estadisticos anteriores
					estadisticaMaquina.resetearEstadisticas();
				}
				
				//configuro el monto del jugador 
				//con la cantidad de tiradas
				maquinaTest.montoJugador = cantidadTiradas;
				
				//configuro la apuesta maxima posible en la maquina
				maquinaTest.dineroApostadoPorLinea = 1;
				maquinaTest.lineasApostadas = (short)1;
				
				while(cantidadTiradas > 0) {
					//ejecuto una tirada
					maquinaTest.logicaMaquina.apostarSlots(false);
					cantidadTiradas--;
				}
				
				//llamamos al metodo generico
				//que escribe en la salida los
				//datos estadisticos de la maquina
				//correspondiente
				mostrarEstadisticasMaquina(out, maquinaTest);
				
				out.flush();
				out.close();

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}