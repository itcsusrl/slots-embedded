package org.microuy.javaslot.webapp.server.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PrincipalServlet  extends HttpServlet {

	public PrincipalServlet() {

	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse resp)
			throws ServletException, IOException {

		PrintWriter out = resp.getWriter();

		
		/**
		 * Probabilidad maquina
		 */
		out.println("<table width='800px'>");
		out.println("<tr>");
		out.println("<td>");
		out.println("<h1> <a href='probabilidad_maquina'> <li> Probabilidad Maquina </a> </h1>");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>");
		out.println("Muestra informacion probabilistica relacionada a la configuracion de la maquina");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");

		out.println("<table><tr><td>&nbsp;</td></tr></table>");
		
		/**
		 * Estadistica
		 */
		out.println("<table width='800px'>");
		out.println("<tr>");
		out.println("<td>");
		out.println("<h1> <a href='estadistica_maquina'> <li> Estadistica Maquina </a> </h1>");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>");
		out.println("Muestra informacion estadistica relaciona a la configuracion de la maquina");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");
		
		out.println("<table><tr><td>&nbsp;</td></tr></table>");
		
		/**
		 * Test estadistico
		 */
		out.println("<table width='800px'>");
		out.println("<tr>");
		out.println("<td>");
		out.println("<h1> <a href='test_estadistico'> <li> Test Estadistico </a> </h1>");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>");
		out.println("Se realizan apuestas automaticamente para obtener valores estadisticos basados en tiradas reales.");
		out.println("Estas tiradas no afectan directamente a la instancia de la maquina que se esta jugando debido ");
		out.println("a que se instancia una nueva maquina para ejecutar los tests");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");

		out.println("<table><tr><td>&nbsp;</td></tr></table>");
		
		
	}
}