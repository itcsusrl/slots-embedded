package org.microuy.javaslot.webapp.server.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.util.MaquinaPoolManager;

public class EstadisticaMaquinaServlet extends EstadisticoGenericServlet {

	private Maquina maquinaJuego;
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	public void init() throws ServletException {
		maquinaJuego = MaquinaPoolManager.obtenerInstanciaMaquinaJuego();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse resp)
			throws ServletException, IOException {

		PrintWriter out = resp.getWriter();

		out.println("<table>");
		out.println("<tr>");
		out.println("<td>");
		out.println("<h1> Estadistica Maquina </h1>");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>");
		out.println("Muestra informacion estadistica relaciona a la configuracion de la maquina");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");

		out.println("<table><tr><td>&nbsp;</td></tr></table>");

		out.println("<table>");
		out.println("<tr>");
		out.println("<td>");
		out.println("Configuracion:");
		out.println("</td>");
		out.println("<td>");
		
		int probabilidadJugador = Sistema.PORCENTAJE_JUGADOR;
		if(probabilidadJugador <= 0) {
			probabilidadJugador = (int)Float.parseFloat(""+maquinaJuego.probabilidad.probabilidadJugador);
		}
		
		out.println("<b>" + maquinaJuego.configuracion.getClass().getSimpleName() + "</b> (" + probabilidadJugador + "% jugador)");
		out.println("</td>");
		out.println("</tr>");
		
		out.println("</table>");
		
		//llamamos al metodo generico
		//que escribe en la salida los
		//datos estadisticos de la maquina
		//correspondiente
		mostrarEstadisticasMaquina(out, maquinaJuego);
		
		out.flush();
		out.close();

	}
}