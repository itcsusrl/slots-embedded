package org.microuy.javaslot.webapp.server.servlet;

import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServlet;

import org.microuy.javaslot.constante.JugadaTipo;
import org.microuy.javaslot.dominio.JugadaGanadora;
import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.dominio.estadistica.EstadisticaMaquina;
import org.microuy.javaslot.dominio.estadistica.GanadorTipoJugada;
import org.microuy.javaslot.dominio.probabilidad.ProbabilidadJugadaGanadora;
import org.microuy.javaslot.dominio.probabilidad.ProbabilidadJugadaGanadoraMultiplicadorComparator;
import org.microuy.javaslot.dominio.probabilidad.ProbabilidadJugadaGanadoraPorcentajePagoComparator;

public class EstadisticoGenericServlet extends HttpServlet {

	//usado para formatear numeros decimales
	protected DecimalFormat twoDForm = new DecimalFormat("#.##");
	protected DecimalFormat integerForm = new DecimalFormat("#");
	
	protected void mostrarEstadisticasMaquina(PrintWriter out, Maquina maquina) {

		//obtenemos las estadisticas de la
		//maquina con la que se esta jugando
		EstadisticaMaquina estadisticaMaquina = maquina.estadisticaMaquina;
		List<GanadorTipoJugada> ganadoresTipoJugada = estadisticaMaquina.ganadoresTipoJugada;
		
		out.println("<table>");
		out.println("<tr bgcolor='#2EFE2E'>");
		out.println("<td width='150px'>");
		out.println("Dinero Apostado");
		out.println("</td>");
		out.println("<td width='100px'>");
		out.println(integerForm.format(estadisticaMaquina.dineroApostado));
		out.println("</td>");
		out.println("<td width='90px'>");
		out.println("100%");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr bgcolor='#2EFE2E'>");
		out.println("<td width='150px'>");
		out.println("Dinero Ganado");
		out.println("</td>");
		out.println("<td width='100px'>");
		out.println(integerForm.format(estadisticaMaquina.dineroGanado));
		out.println("</td>");
		out.println("<td width='90px'>");
		
		float porcentajeGanado = (float)(estadisticaMaquina.dineroGanado * 100) / (float)estadisticaMaquina.dineroApostado;
		out.println(twoDForm.format(porcentajeGanado) + "%");
		
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>");
		out.println("<br/>");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");

		
		out.println("<table>");
		out.println("<tr bgcolor='#F78181'>");
		out.println("<td width='150px'>");
		out.println("Dinero ganado Normal");
		out.println("</td>");
		out.println("<td width='150px'>");
		out.println("Dinero ganado Scatter");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr bgcolor='#F78181'>");
		out.println("<td>");
		out.println(twoDForm.format(estadisticaMaquina.dineroGanadoNormal));
		out.println("</td>");
		out.println("<td>");
		out.println(twoDForm.format(estadisticaMaquina.dineroGanadoScatter));
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr bgcolor='#F78181'>");
		
		out.println("<td>");
		double totalDineroGanadoTipos = estadisticaMaquina.dineroGanadoNormal+estadisticaMaquina.dineroGanadoScatter;
		double porcentajeDineroGanadoNormal = (estadisticaMaquina.dineroGanadoNormal*100d)/totalDineroGanadoTipos;
		out.println(twoDForm.format(porcentajeDineroGanadoNormal)+"%");
		out.println("</td>");

		out.println("<td>");
		double porcentajeDineroGanadoScatter = (estadisticaMaquina.dineroGanadoScatter*100d)/totalDineroGanadoTipos;
		out.println(twoDForm.format(porcentajeDineroGanadoScatter)+"%");
		out.println("</td>");

		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>");
		out.println("<br/>");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");

		

		
		
		
		
		
		
		
		out.println("<table>");
		out.println("<tr bgcolor='#F78181'>");
		out.println("<td width='150px'>");
		out.println("Dinero ganado Bonus");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr bgcolor='#F78181'>");
		out.println("<td>");
		out.println(twoDForm.format(estadisticaMaquina.dineroGanadoBonus));
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");
		
		out.println("<table><tr><td>&nbsp;</td></tr></table>");
		
		
		out.println("<table>");
		out.println("<tr bgcolor='#F7FE2E'>");
		out.println("<td width='150px'>");
		out.println("Apuestas ganadas");
		out.println("</td>");
		out.println("<td width='100px'>");
		out.println(estadisticaMaquina.cantidadApuestasGanadas);
		out.println("</td>");
		out.println("<td width='90px'>");
		
		float porcentajeApuestasGanadas = (float)(estadisticaMaquina.cantidadApuestasGanadas * 100) / (float)estadisticaMaquina.cantidadApuestas;
		out.println(twoDForm.format(porcentajeApuestasGanadas) + "%");
		
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr bgcolor='#F7FE2E'>");
		out.println("<td width='150px'>");
		out.println("Apuestas perdidas");
		out.println("</td>");
		out.println("<td width='100px'>");
		out.println((estadisticaMaquina.cantidadApuestas - estadisticaMaquina.cantidadApuestasGanadas));
		out.println("</td>");
		out.println("<td width='90px'>");
		
		float porcentajeApuestasPerdidas = (float)((estadisticaMaquina.cantidadApuestas - estadisticaMaquina.cantidadApuestasGanadas) * 100) / (float)estadisticaMaquina.cantidadApuestas;
		out.println(twoDForm.format(porcentajeApuestasPerdidas) + "%");
		
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr bgcolor='#F7FE2E'>");
		out.println("<td width='150px'>");
		out.println("TOTAL");
		out.println("</td>");
		out.println("<td width='100px'>");
		out.println(estadisticaMaquina.cantidadApuestas);
		out.println("</td>");
		out.println("<td width='90px'>");
		out.println("100%");
		out.println("</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>");
		out.println("<br/>");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");

		out.println("<table>");
		out.println("<tr bgcolor='#00FFFF'>");
		out.println("<td width='80px'>");
		out.println("Jugada");
		out.println("</td>");
		out.println("<td width='80px'>");
		out.println("Repeticiones");
		out.println("</td>");
		out.println("<td width='80px'>");
		out.println("Pagado");
		out.println("</td>");
		out.println("<td width='80px'>");
		out.println("% de Pago");
		out.println("</td>");
		out.println("<td>");
		out.println("Tipo Jugada");
		out.println("</td>");
		out.println("<td>");
		out.println("Multiplicador");
		out.println("</td>");
		out.println("<td>");
		out.println("T. Gratis");
		out.println("</td>");
		out.println("</tr>");
		
		float porcentajeTotalJugadas = 0;
		for(int i=0, contador=1; i<ganadoresTipoJugada.size(); i++, contador++) {
		
			GanadorTipoJugada ganadorTipoJugada = ganadoresTipoJugada.get(i);
			
			out.println("<tr>");
			out.println("<td>");
			out.println(contador + " -> " + ganadorTipoJugada.tipoJugada);
			out.println("</td>");
			out.println("<td>");
			out.println(ganadorTipoJugada.ganadas);
			out.println("</td>");
			out.println("<td>");
			out.println(ganadorTipoJugada.ganadas * ganadorTipoJugada.multiplicador);
			out.println("</td>");

			
			out.println("<td>");
			
			out.println("</td>");

			
			out.println("<td>");
			out.println(JugadaTipo.obtenerDescripcion(ganadorTipoJugada.formatoJugada));
			out.println("</td>");
			
			out.println("<td>");
			if(ganadorTipoJugada.formatoJugada == JugadaTipo.BONUS) {
				out.println("N/A");
			}
			else {
				out.println(ganadorTipoJugada.multiplicador);
			}
			out.println("</td>");
			
			out.println("<td>");
			if(ganadorTipoJugada.formatoJugada == JugadaTipo.BONUS) {
				out.println(ganadorTipoJugada.tiradasGratuitas);	
			}
			
			out.println("</td>");
			out.println("</tr>");
		}
		
		out.println("<tr bgcolor='#00FFFF'>");
		out.println("<td width='140px'>");
		out.println("TOTAL");
		out.println("</td>");
		out.println("<td width='140px'>");
		out.println(estadisticaMaquina.cantidadApuestasGanadas);
		out.println("</td>");
		out.println("<td>");
		out.println("100%");
		out.println("</td>");
		out.println("<td>");
		out.println(twoDForm.format(porcentajeTotalJugadas) + "%");
		out.println("</td>");
		out.println("<td>");
		out.println("&nbsp;");
		out.println("</td>");
		out.println("<td>");
		out.println("&nbsp;");
		out.println("</td>");
		out.println("<td>");
		out.println("&nbsp;");
		out.println("</td>");
		out.println("<td>");
		out.println("&nbsp;");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");
		
		out.println("<table>");
		out.println("<tr>");
		out.println("<td>");
		out.println("&nbsp");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");
		
		out.println("<table>");
		out.println("<tr>");
		out.println("<td>");
		out.println("<h3>Distribuidor Premios (Jugadas tipo 'NORMAL')</h3>");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");
		
		out.println("<table>");
		out.println("<tr bgcolor='#00FFFF'>");
		out.println("<td width='80px'>");
		out.println("Jugada");
		out.println("</td>");
		out.println("<td width='80px'>");
		out.println("Monto");
		out.println("</td>");
		out.println("<td width='80px'>");
		out.println("Multiplicador");
		out.println("</td>");
		out.println("<td width='60px'>");
		out.println("% Distribucion");
		out.println("</td>");
		out.println("<td width='115px'>");
		out.println("% Probabilidades");
		out.println("</td>");
		out.println("</tr>");

		
		/**
		 * Obtenemos el mapa que identifica el
		 * monto distribuible de cada tipo 
		 * especifico de jugada
		 */
		HashMap<JugadaGanadora, Float> premiosDistribuidos = maquina.logicaMaquina.distribuidorPremios.premiosDistribuidos;
		
		//guarda el monto global que se
		//puede distribuir
		double montoTotalDistribuido = 0d;
		double porcentajeTotalProbabilidad = 0d;
		
		//obtenemos la lista de probabilidades 
		//de jugadas ganadoras
		List<ProbabilidadJugadaGanadora> probabilidadJugadaGanadoras = maquina.probabilidad.probabilidadJugadasGanadoras;
		
		//ordeno la lista de jugadas ganadoras por
		//su factor de probabilidad
		Collections.sort(probabilidadJugadaGanadoras, new ProbabilidadJugadaGanadoraPorcentajePagoComparator());
		
		int contador = 1;
		
		for(ProbabilidadJugadaGanadora probabilidadJugadaGanadora : probabilidadJugadaGanadoras) {
			
			//creamos un iterador de claves
			Iterator<JugadaGanadora> jugadasGanadorasIt = premiosDistribuidos.keySet().iterator();
		
			//y lo recorremos
			while(jugadasGanadorasIt.hasNext()) {
			
				//obtenemos el proximo elemento
				JugadaGanadora jugadaGanadora = jugadasGanadorasIt.next();
				
				if(probabilidadJugadaGanadora.jugadaGanadora == jugadaGanadora) {
					
					//y su valor (monto)
					double montoJugadaGanadora = premiosDistribuidos.get(jugadaGanadora);
					
					//incrementamos la variable helper
					montoTotalDistribuido += montoJugadaGanadora;
					
					out.println("<tr>");
					out.println("<td>");
					out.println(contador + " -> " + jugadaGanadora.obtenerFormateado());
					out.println("</td>");
					out.println("<td>");
					out.println(twoDForm.format(montoJugadaGanadora));
					out.println("</td>");
					out.println("<td>");
					out.println(probabilidadJugadaGanadora.jugadaGanadora.multiplicador);
					out.println("</td>");
					out.println("<td>");
					out.println(twoDForm.format(probabilidadJugadaGanadora.jugadaGanadora.porcentajeDistribucion));
					out.println("</td>");
					out.println("<td>");
					out.println(twoDForm.format(probabilidadJugadaGanadora.porcentajePagoEntreGanadorasNormales));
					out.println("</td>");
					
					porcentajeTotalProbabilidad += probabilidadJugadaGanadora.porcentajePagoEntreGanadorasNormales;
					break;
				}
			}
			
			out.println("</tr>");
			
			contador++;
			
		}

		out.println("<tr bgcolor='#00FFFF'>");
		out.println("<td width='140px'>");
		out.println("TOTAL");
		out.println("</td>");
		out.println("<td width='140px'>");
		out.println(twoDForm.format(montoTotalDistribuido));
		out.println("</td>");
		out.println("<td width='140px'>");
		out.println(twoDForm.format(porcentajeTotalProbabilidad));
		out.println("</td>");
		out.println("<td>");
		out.println("&nbsp;");
		out.println("</td>");
		out.println("</tr>");

		out.println("</table>");
		
	}
}