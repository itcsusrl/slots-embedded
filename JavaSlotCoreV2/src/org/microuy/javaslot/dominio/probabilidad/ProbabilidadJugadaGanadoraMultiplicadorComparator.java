package org.microuy.javaslot.dominio.probabilidad;

import java.util.Comparator;

public class ProbabilidadJugadaGanadoraMultiplicadorComparator implements Comparator<ProbabilidadJugadaGanadora> {

	public int compare(ProbabilidadJugadaGanadora arg0, ProbabilidadJugadaGanadora arg1) {
		
		long mult1 = arg0.multiplicaciones;
		long mult2 = arg1.multiplicaciones;
		
		return (int)(mult2-mult1);
	}
}