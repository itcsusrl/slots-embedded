package org.microuy.javaslot.dominio;

import java.sql.Timestamp;

public class ResumenCobranza {

	public int id;
	public float base;
	public float entrada;
	public float salida;
	public float saldo;
	public int entradaMonedasPA;
	public int entradaMonedasPC;
	public int salidaMonedasPA;
	public int salidaMonedasPC;
	public Timestamp fechaDesde;
	public Timestamp fechaHasta;
	
	public ResumenCobranza(int id, float base, float entrada, float salida, float saldo, int entradaMonedasPA, int entradaMonedasPC, int salidaMonedasPA, int salidaMonedasPC, Timestamp fechaDesde, Timestamp fechaHasta) {
		
		this.id = id;
		this.base = base;
		this.entrada = entrada;
		this.salida = salida;
		this.saldo = saldo;
		this.entradaMonedasPA = entradaMonedasPA;
		this.entradaMonedasPC = entradaMonedasPC;
		this.salidaMonedasPA = salidaMonedasPA;
		this.salidaMonedasPC = salidaMonedasPC;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
	}
}