package org.microuy.javaslot.dominio;

import java.util.ArrayList;
import java.util.List;

/**
 * Es una jugada de linea que ha sido validada
 * como ganadora de un premio.
 * 
 * @author Pablo Caviglia
 *
 */
public class JugadaLineaGanadora {

	private List<Figura> figuras = new ArrayList<Figura>();
	private Linea linea;
	private JugadaGanadora jugadaGanadora;
	private int tipo;
	
	/**
	 * Indica si el premio fue generado por
	 * el generador de premios o si la jugada
	 * fue dada por el mismo giro de los 
	 * rodillos
	 */
	private boolean generado;

	public List<Figura> getFiguras() {
		return figuras;
	}
	public void setFiguras(List<Figura> figuras) {
		this.figuras = figuras;
	}
	public Linea getLinea() {
		return linea;
	}
	public void setLinea(Linea linea) {
		this.linea = linea;
	}
	public JugadaGanadora getJugadaGanadora() {
		return jugadaGanadora;
	}
	public void setJugadaGanadora(JugadaGanadora jugadaGanadora) {
		this.jugadaGanadora = jugadaGanadora;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public boolean isGenerado() {
		return generado;
	}
	public void setGenerado(boolean generado) {
		this.generado = generado;
	}
}