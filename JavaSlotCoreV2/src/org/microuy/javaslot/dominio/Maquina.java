package org.microuy.javaslot.dominio;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.estadistica.EstadisticaMaquina;
import org.microuy.javaslot.dominio.probabilidad.ProbabilidadJugadaGanadora;
import org.microuy.javaslot.dominio.probabilidad.ProbabilidadMaquina;
import org.microuy.javaslot.expepcion.PorcentajeMaquinaMenorQueAsignadoException;
import org.microuy.javaslot.logica.LogicaMaquina;
import org.microuy.javaslot.logica.configuracion.Configuracion;
import org.microuy.javaslot.persistencia.dao.ConfiguracionDAO;
import org.microuy.javaslot.util.MathUtil;
import org.microuy.javaslot.util.ReflectionUtil;
import org.microuy.javaslot.util.SlotsRandomGenerator;

/**
 * Represenacion de una maquina tragamonedas.
 * Esta maquina tiene rodillos que contienen figuras.
 * Tambien la maquina contiene una coleccion de jugadas 
 * ganadoras.
 * La maquina tambien posee una coleccion de posibles 
 * configuraciones de lineas.
 * 
 * Todas estas variables son configuradas en las subclases de 
 * la clase abstracta 'Configuracion', en donde se guarda la configuracion
 * basica de la maquina.
 * 
 * 
 * @author Pablo Caviglia
 *
 */
public class Maquina {

	public List<Rodillo> rodillos = new ArrayList<Rodillo>();
	public List<JugadaGanadora> jugadasGanadoras = new ArrayList<JugadaGanadora>();
	public List<Linea> lineas = new ArrayList<Linea>();
	public Configuracion configuracion;
	public LogicaMaquina logicaMaquina;
	
	/**
	 * Contiene informacion estadistica de la
	 * maquina, calculada en tiempo de ejecucion
	 * tras cada apuesta ejecutada en la misma
	 */
	public EstadisticaMaquina estadisticaMaquina;
	
	/**
	 * Contiene datos calculados algoritmicamente
	 * relacionados a las probabilidades de la
	 * maquina, a nivel general, como asi 
	 * tambien a nivel de jugadas
	 */
	public ProbabilidadMaquina probabilidad;

	/**
	 * El dinero asociado con el jugador
	 * actual. Esta variable define el estado
	 * del juego. Si es mayor que cero el
	 * jugador esta jugando, si es cero no lo
	 * esta. Si es negativo es que el hijo de puta
	 * nos quedo debiendo plata (BUG!!!).
	 */
	public float montoJugador;

	/**
	 * Cantidad de lineas apostadas durante
	 * la ejecucion del metodo apostar. Este
	 * valor multiplicado por el valor de apuesta
	 * por linea resulta en la obtencion del
	 * monto total de la apuesta
	*/
	public short lineasApostadas;
	
	/**
	 * El monto apostado durante la ejecucion
	 * del metodo apostar. Este valor debe de
	 * ser multiplicado por la cantidad de lineas
	 * apostadas para obtener el valor del monto
	 * total apostado
	*/ 
	public float dineroApostadoPorLinea;

	/**
	 * Flag que indica si la instancia
	 * de la maquina es de testeo, asi
	 * no se realizan operaciones de base
	 * de datos para esta maquina especifica
	 */
	public boolean test;
	
	public Maquina(Class configuracionClass) {
		this(configuracionClass, false);
	}
	
	public Maquina(Class configuracionClass, boolean test) {
		
		this.test = test;
		
		//inicializo el generador aleatorio
		SlotsRandomGenerator.getInstance();
		
		//inicializo la configuracion
		configuracion = (Configuracion)ReflectionUtil.createObject(configuracionClass.getName());
		configuracion.inicializarConfiguracion(this);
		
		//inicializamos objetos
		probabilidad = new ProbabilidadMaquina();
		probabilidad.probabilidadJugadasGanadoras = new ArrayList<ProbabilidadJugadaGanadora>();

		//seteo los campos necesarios al slot
		this.rodillos = configuracion.getRodillos();
		this.jugadasGanadoras = configuracion.getJugadasGanadoras();
		this.lineas = configuracion.getLineas();

		//inicializamos logica maquina
		logicaMaquina = new LogicaMaquina(this);
		
		//Carga desde la persistencia los
		//valores de configuracion de la
		//maquina
		logicaMaquina.cargarValoresConfiguracion();
		
		//inicializamos la estadistica
		estadisticaMaquina = new EstadisticaMaquina(this);

//		Sistema.APUESTA_WILD = 1;
		
		//se inicializa el calculo de probabilidades
		configuracion.inicializarCalculoProbabilidades();
		
		/**
		 * Si la probabilidad del jugador calculada en 
		 * base a la configuracion de la maquina elegida
		 * es menor que la definida por el administrador
		 * para todo el juego, debemos de tirar una 
		 * excepcion indicando que se esta configurando
		 * de manera incorrecta a la maquina, debido a que
		 * nunca una configuracion puede tener una probabilidad
		 * menor que el porcentaje asignado por el administrador
		 * de la misma
		 */
		if(probabilidad.probabilidadJugador < Sistema.PORCENTAJE_JUGADOR){
			throw new PorcentajeMaquinaMenorQueAsignadoException(probabilidad.probabilidadJugador + "% es menor que " + Sistema.PORCENTAJE_JUGADOR + "%");
		}

		//inicializo los valores iniciales
		//de algunas variables importantes
		lineasApostadas = 1;
		dineroApostadoPorLinea = Sistema.INCREMENTO_APUESTA;
		
	}
	
	/**
	 * Modifica el monto actual del jugador, 
	 * dejando registro del ultimo monto
	 * en la persistencia. El valor de 
	 * persistencia es usado ante cualquier
	 * inconveniente que la maquina se 
	 * cuelgue por alguna razon o se resetee
	 */
	public synchronized void incrementarMontoJugador(float incremento) {
		
		//puede recibir un numero negativo
		montoJugador = montoJugador + incremento;

		//ajustamos el redondeo incorrecto
		//causado por manejar numeros de
		//numero flotante
		montoJugador = (float)MathUtil.roundTwoDecimals(montoJugador);

		/**
		 * Debido a que en el nuevo bot de testeo
		 * del sistema tambien se involucra el 
		 * uso de monedas, no es necesario que
		 * se guarde informacion en la base de
		 * datos
		 */
		if(!test) {
			//actualizamos el valor en 
			//la persistencia
			logicaMaquina.actualizarValorConfiguracion(ConfiguracionDAO.MONTO_JUGADOR, String.valueOf(montoJugador));
		}
	}
	
	public synchronized void actualizarMontoJugador(float nuevoMonto) {
		
		//el monto del jugador
		//ahora es el monto 
		//asignado
		montoJugador = nuevoMonto;
		
		//actualizamos el valor en 
		//la persistencia
		logicaMaquina.actualizarValorConfiguracion(ConfiguracionDAO.MONTO_JUGADOR, ""+nuevoMonto);

	}
	
	public void setDineroApostadoPorLinea(float nuevoDineroApostadoPorLinea) {
		
		//fix para acomodar redondeos
		if(nuevoDineroApostadoPorLinea < Sistema.INCREMENTO_APUESTA) {
			nuevoDineroApostadoPorLinea = Sistema.INCREMENTO_APUESTA;
		}
		
		this.dineroApostadoPorLinea = nuevoDineroApostadoPorLinea;
	}
}