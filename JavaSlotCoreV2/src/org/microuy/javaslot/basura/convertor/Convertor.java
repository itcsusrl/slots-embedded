package org.microuy.javaslot.basura.convertor;

import org.microuy.javaslot.basura.core.RrdDb;
import org.microuy.javaslot.basura.core.RrdException;

import java.io.*;

class Convertor {
	static final String SUFFIX = ".jrb";
	static final String SEPARATOR = System.getProperty("file.separator");
	static final Runtime RUNTIME = Runtime.getRuntime();

	private String rrdtoolBinary;
	private String workingDirectory;
	private String suffix;

	private int okCount, badCount;

	private Convertor(String rrdtoolBinary, String workingDirectory, String suffix) {
		this.rrdtoolBinary = rrdtoolBinary;
		this.workingDirectory = workingDirectory;
		this.suffix = suffix;
	}

	private void convert() {
		long start = System.currentTimeMillis();
		if(!workingDirectory.endsWith(SEPARATOR)) {
			workingDirectory += SEPARATOR;
		}
        File parent = new File(workingDirectory);
		if(parent.isDirectory() && parent.exists()) {
			// directory
			FileFilter filter = new FileFilter() {
				public boolean accept(File f) {
					try {
						return !f.isDirectory() && f.getCanonicalPath().endsWith(".rrd");
					} catch (IOException e) {
						return false;
					}
				}
			};
			File[] files = parent.listFiles(filter);
			for(int i = 0; i < files.length; i++) {
				convertFile(files[i]);
			}
		}
		else if(!parent.isDirectory() && parent.exists()) {
			// single file
			convertFile(parent);
		}
		else {
		}
		long secs = (System.currentTimeMillis() - start + 500L) / 1000L;
		long mins = secs / 60;
		secs %= 60;
	}

	private long convertFile(File rrdFile) {
		long start = System.currentTimeMillis();
		String xmlPath = null, destPath = null;
		try {
			String sourcePath = rrdFile.getCanonicalPath();
			xmlPath = sourcePath + ".xml";
			destPath = sourcePath + suffix;
			xmlDump(sourcePath, xmlPath);
			RrdDb rrd = new RrdDb(destPath, xmlPath);
			rrd.close();
			rrd = null;
			System.gc();
			okCount++;
			long elapsed = System.currentTimeMillis() - start;
			return elapsed;
		} catch (IOException e) {
			removeFile(destPath);
			badCount++;
			return -1;
		} catch (RrdException e) {
			removeFile(destPath);
			badCount++;
			return -2;
		}
		finally {
			removeFile(xmlPath);
		}
	}

	private static boolean removeFile(String filePath) {
		if(filePath != null) {
			return new File(filePath).delete();
		}
		return true;
	}

	private void xmlDump(String sourcePath, String xmlPath) throws IOException {
		String[] cmd = new String[] { rrdtoolBinary, "dump", sourcePath };
		Process p = RUNTIME.exec(cmd);
		OutputStream outStream = new BufferedOutputStream(new FileOutputStream(xmlPath, false));
		transportStream(p.getInputStream(), outStream);
		transportStream(p.getErrorStream(), null);
		try {
			p.waitFor();
		}
		catch(InterruptedException ie) {
			// NOP
		}
	}

	private static void transportStream(InputStream in, OutputStream out) throws IOException {
		try {
			int b;
			while((b = in.read()) != -1) {
				if(out != null) {
					out.write(b);
				}
			}
		}
		finally {
			in.close();
			if(out != null) {
				out.flush();
				out.close();
			}
		}
	}
}
