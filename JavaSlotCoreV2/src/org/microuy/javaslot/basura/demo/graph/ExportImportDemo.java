/* ============================================================
 * JRobin : Pure java implementation of RRDTool's functionality
 * ============================================================
 *
 * Project Info:  http://www.jrobin.org
 * Project Lead:  Sasa Markovic (saxon@jrobin.org);
 *
 * (C) Copyright 2003, by Sasa Markovic.
 *
 * Developers:    Sasa Markovic (saxon@jrobin.org)
 *                Arne Vandamme (cobralord@jrobin.org)
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.microuy.javaslot.basura.demo.graph;

import org.microuy.javaslot.basura.core.RrdException;
import org.microuy.javaslot.basura.core.Util;
import org.microuy.javaslot.basura.graph.*;

import java.io.File;
import java.io.IOException;
import java.util.GregorianCalendar;

/**
 * <p>This is a small demo that creates a graph based on the export XML from the ExportExportDemo.</p>
 * 
 * @author Arne Vandamme (cobralord@jrobin.org)
 */
public class ExportImportDemo
{
	private static String exportRrd1 		= ExportExportDemo.exportRrd1;
	private static String exportRrd2 		= ExportExportDemo.exportRrd2;
	private static String exportCombined 	= Util.getJRobinDemoPath( "export-combined.xml" );
	private static String graphFile 		= Util.getJRobinDemoPath( "export-graph.png" );
	private static String demoResources 	= "";

	private static void println( String str ) {
		System.out.println( str );
	}

	private static void prepare( String[] args )
	{
		demoResources = Util.getJRobinHomeDirectory() + "/res/demo/";
	}

}
