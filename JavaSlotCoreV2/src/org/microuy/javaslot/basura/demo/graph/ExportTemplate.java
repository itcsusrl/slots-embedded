/* ============================================================
 * JRobin : Pure java implementation of RRDTool's functionality
 * ============================================================
 *
 * Project Info:  http://www.jrobin.org
 * Project Lead:  Sasa Markovic (saxon@jrobin.org);
 *
 * (C) Copyright 2003, by Sasa Markovic.
 *
 * Developers:    Sasa Markovic (saxon@jrobin.org)
 *                Arne Vandamme (cobralord@jrobin.org)
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.microuy.javaslot.basura.demo.graph;

import org.microuy.javaslot.basura.core.RrdException;
import org.microuy.javaslot.basura.graph.ExportData;
import org.microuy.javaslot.basura.graph.RrdExport;
import org.microuy.javaslot.basura.graph.RrdExportDefTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.File;
import java.io.InputStreamReader;

/**
 * <p>Simple command line application that allows you to export RRD data by means
 * of a RrdGraphDefTemplate.  Pretty straightforward in use.</p>
 * 
 * @author Arne Vandamme (cobralord@jrobin.org)
 */
public class ExportTemplate
{
	private static int maxRows		= 400;			// Maximum number of rows

	private static String templateFile, dumpFile;

	private static void die( String msg )
	{
		System.err.println( msg );
		System.exit( -1 );
	}

	private static void parseArguments( String[] args )
	{
		int rpos		= args.length - 1;

		// Last argument should be the templateFile
		templateFile	= args[rpos];

		// Remaining number of parameters should be even
		if ( rpos % 2 > 0 )
			die( "Invalid number of arguments." );

		for ( int i = 0; i < rpos; i += 2 )
		{
			String arg = args[i];
			String val = args[i + 1];

			try
			{
				if ( arg.equalsIgnoreCase("-m") )
					maxRows = Integer.parseInt(val);
				else if ( arg.equalsIgnoreCase("-f") )
					dumpFile = val;
			}
			catch ( Exception e ) {
				die( "Error with option '" + arg + "': " + e.getMessage() );
			}
		}
	}

	private static String readVariable( BufferedReader in, String name ) throws IOException
	{
		System.out.print( "Variable '" + name + "' = " );

		return in.readLine();
	}


}