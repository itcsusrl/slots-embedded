/* ============================================================
 * JRobin : Pure java implementation of RRDTool's functionality
 * ============================================================
 *
 * Project Info:  http://www.jrobin.org
 * Project Lead:  Sasa Markovic (saxon@jrobin.org);
 *
 * (C) Copyright 2003, by Sasa Markovic.
 *
 * Developers:    Sasa Markovic (saxon@jrobin.org)
 *                Arne Vandamme (cobralord@jrobin.org)
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.microuy.javaslot.basura.demo.graph;

import org.microuy.javaslot.basura.core.*;
import org.microuy.javaslot.basura.graph.RrdGraph;
import org.microuy.javaslot.basura.graph.RrdGraphDef;

import javax.swing.*;
import java.io.IOException;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Date;

/**
 * <p>Swing demonstration of the minmax graph.</p>
 *
 * @author Arne Vandamme (cobralord@jrobin.org)
 */
public class SwingDemo
{
	static JFrame frame				 = null;
	static SwingDemoPanel demoPanel  = null;
	static RrdGraph graph			 = null;
	static RrdGraphDef gDef			 = null;

	static final String rrd			= "SwingDemo.rrd";
	static final long START 		= Util.getTimestamp( 2004, 1, 1 );

	static Date end 				= new Date(START);

	static void prepareRrd() throws IOException, RrdException
	{
		RrdDef rrdDef 			= new RrdDef( rrd, START - 300, 300 );
		rrdDef.addDatasource("a", "GAUGE", 600, Double.NaN, Double.NaN);
		rrdDef.addArchive("AVERAGE", 0.5, 1, 300);
		rrdDef.addArchive("MIN", 0.5, 6, 300);
		rrdDef.addArchive("MAX", 0.5, 6, 300);

		RrdDb rrdDb 			= new RrdDb( rrdDef, RrdBackendFactory.getFactory("MEMORY") );
		rrdDb.close();
	}

	static void prepareFrame() throws RrdException
	{
		gDef = new RrdGraphDef();
		gDef.setImageBorder( Color.WHITE, 0 );
		gDef.setTitle("JRobin Swing minmax demo");
		gDef.setVerticalLabel( "value" );
		gDef.setTimeAxisLabel( "time" );
		gDef.datasource("a", rrd, "a", "AVERAGE", "MEMORY" );
		gDef.datasource("b", rrd, "a", "MIN", "MEMORY" );
		gDef.datasource("c", rrd, "a", "MAX", "MEMORY");
		gDef.datasource( "avg", "a", "AVERAGE" );
		gDef.area("a", Color.decode("0xb6e4"), "real");
		gDef.line("b", Color.decode("0x22e9"), "min", 2 );
		gDef.line("c", Color.decode("0xee22"), "max", 2 );
		gDef.line("avg", Color.RED,  "Average" );
		gDef.time( "@l@lTime period: @t", "MMM dd, yyyy   HH:mm:ss", START );
		gDef.time( "to  @t@l", "HH:mm:ss", end );
		gDef.time("@lGenerated: @t@c", "HH:mm:ss" );

		// create graph finally
		graph 				= new RrdGraph(gDef);

		// Create JFrame
		frame 				= new JFrame( "JRobin Swing Demo" );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

		demoPanel			= new SwingDemoPanel( graph );
		frame.getContentPane().add( demoPanel );

		frame.pack();

		frame.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				Dimension d = frame.getContentPane().getSize();
				demoPanel.setGraphDimension(d);
			}
		});
		frame.setBounds( 10, 10, 504, 303 );
		frame.show();
	}

}
