package org.microuy.javaslot.basura.demo;

/* ============================================================
 * JRobin : Pure java implementation of RRDTool's functionality
 * ============================================================
 *
 * Project Info:  http://www.jrobin.org
 * Project Lead:  Sasa Markovic (saxon@jrobin.org);
 *
 * (C) Copyright 2003, by Sasa Markovic.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * Developers:    Sasa Markovic (saxon@jrobin.org)
 *                Arne Vandamme (cobralord@jrobin.org)
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

import org.microuy.javaslot.basura.core.*;
import org.microuy.javaslot.basura.graph.RrdGraph;
import org.microuy.javaslot.basura.graph.RrdGraphDef;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Date;
import java.awt.*;

class StressTest {
	static final String FACTORY_NAME = "NIO";

	static final String RRD_PATH = Util.getJRobinDemoPath("stress.rrd");
	static final long RRD_START = 946710000L;
	static final long RRD_STEP = 30;

	static final String RRD_DATASOURCE_NAME = "T";
	static final int RRD_DATASOURCE_COUNT = 6;

	static final long TIME_START = 1060142010L;
	static final long TIME_END = 1080013472L;

	static final String PNG_PATH = Util.getJRobinDemoPath("stress.png");
	static final int PNG_WIDTH = 400;
	static final int PNG_HEIGHT = 250;

	static void printLapTime(String message) {
		System.out.println(message + " " + Util.getLapTime());
	}

}
