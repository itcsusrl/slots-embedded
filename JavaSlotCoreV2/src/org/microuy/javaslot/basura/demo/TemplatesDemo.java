package org.microuy.javaslot.basura.demo;

/* ============================================================
 * JRobin : Pure java implementation of RRDTool's functionality
 * ============================================================
 *
 * Project Info:  http://www.jrobin.org
 * Project Lead:  Sasa Markovic (saxon@jrobin.org);
 *
 * (C) Copyright 2003, by Sasa Markovic.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * Developers:    Sasa Markovic (saxon@jrobin.org)
 *                Arne Vandamme (cobralord@jrobin.org)
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

import org.microuy.javaslot.basura.core.*;
import org.microuy.javaslot.basura.graph.RrdGraph;
import org.microuy.javaslot.basura.graph.RrdGraphDef;
import org.microuy.javaslot.basura.graph.RrdGraphDefTemplate;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

class TemplatesDemo {
	static final String RRD_TEMPLATE =
		"<rrd_def>                              " +
		"    <path>${path}</path>               " +
		"    <start>${start}</start>            " +
		"	 <step>300</step>                   " +
		"    <datasource>                       " +
		" 	     <name>sun</name>               " +
		"        <type>GAUGE</type>             " +
		"        <heartbeat>600</heartbeat>     " +
		"        <min>0</min>                   " +
		"		 <max>U</max>                   " +
		"    </datasource>                      " +
		"    <datasource>                       " +
		" 	     <name>shade</name>             " +
		"        <type>GAUGE</type>             " +
		"        <heartbeat>600</heartbeat>     " +
		"        <min>0</min>                   " +
		"		 <max>U</max>                   " +
		"    </datasource>                      " +
		"    <archive>                          " +
		"        <cf>AVERAGE</cf>               " +
		"        <xff>0.5</xff>                 " +
		"        <steps>1</steps>               " +
		"	     <rows>600</rows>               " +
		"    </archive>                         " +
		"    <archive>                          " +
		"        <cf>AVERAGE</cf>               " +
		"        <xff>0.5</xff>                 " +
		"        <steps>6</steps>               " +
		"	     <rows>700</rows>               " +
		"    </archive>                         " +
		"    <archive>                          " +
		"        <cf>AVERAGE</cf>               " +
		"        <xff>0.5</xff>                 " +
		"        <steps>24</steps>              " +
		"	     <rows>775</rows>               " +
		"    </archive>                         " +
		"    <archive>                          " +
		"        <cf>AVERAGE</cf>               " +
		"        <xff>0.5</xff>                 " +
		"        <steps>288</steps>             " +
		"	     <rows>797</rows>               " +
		"    </archive>                         " +
		"    <archive>                          " +
		"        <cf>MAX</cf>                   " +
		"        <xff>0.5</xff>                 " +
		"        <steps>1</steps>               " +
		"	     <rows>600</rows>               " +
		"    </archive>                         " +
		"    <archive>                          " +
		"        <cf>MAX</cf>                   " +
		"        <xff>0.5</xff>                 " +
		"        <steps>6</steps>               " +
		"	     <rows>700</rows>               " +
		"    </archive>                         " +
		"    <archive>                          " +
		"        <cf>MAX</cf>                   " +
		"        <xff>0.5</xff>                 " +
		"        <steps>24</steps>              " +
		"	     <rows>775</rows>               " +
		"    </archive>                         " +
		"    <archive>                          " +
		"        <cf>MAX</cf>                   " +
		"        <xff>0.5</xff>                 " +
		"        <steps>288</steps>             " +
		"	     <rows>797</rows>               " +
		"    </archive>                         " +
		"</rrd_def>                             " ;

	static final String GRAPH_TEMPLATE =
		"<rrd_graph_def>                                      " +
		"    <span>                                           " +
		"        <start>${start}</start>                      " +
		"        <end>${end}</end>                            " +
		"    </span>                                          " +
		"    <options>                                        " +
		"        <title>${title}</title>                      " +
		"        <vertical_label>temperature</vertical_label> " +
		"    </options>                                       " +
		"    <datasources>                                    " +
		"        <def>                                        " +
		"            <name>sun</name>                         " +
		"            <rrd>${rrd}</rrd>                        " +
		"            <source>sun</source>                     " +
		"            <cf>AVERAGE</cf>                         " +
		"        </def>                                       " +
		"        <def>                                        " +
		"            <name>shade</name>                       " +
		"            <rrd>${rrd}</rrd>                        " +
		"            <source>shade</source>                   " +
		"            <cf>AVERAGE</cf>                         " +
		"        </def>                                       " +
		"        <def>                                        " +
		"            <name>median</name>                      " +
		"            <rpn>sun,shade,+,2,/</rpn>               " +
		"        </def>                                       " +
		"        <def>                                        " +
		"            <name>diff</name>                        " +
		"            <rpn>sun,shade,-,ABS,-1,*</rpn>          " +
		"        </def>                                       " +
		"        <def>                                        " +
		"            <name>sine</name>                        " +
		"            <rpn>${sine}</rpn>                       " +
		"        </def>                                       " +
		"    </datasources>                                   " +
		"    <graph>                                          " +
		"        <line>                                       " +
		"            <datasource>sun</datasource>             " +
		"            <color>#00FF00</color>                   " +
		"            <legend>sun temp</legend>                " +
		"        </line>                                      " +
		"        <line>                                       " +
		"            <datasource>shade</datasource>           " +
		"            <color>#0000FF</color>                   " +
		"            <legend>shade temp</legend>              " +
		"        </line>                                      " +
		"        <line>                                       " +
		"            <datasource>median</datasource>          " +
		"            <color>#FF00FF</color>                   " +
		"            <legend>median value@L</legend>          " +
		"        </line>                                      " +
		"        <area>                                       " +
		"            <datasource>diff</datasource>            " +
		"            <color>#FFFF00</color>                   " +
		"            <legend>difference@r</legend>            " +
		"        </area>                                      " +
		"        <line>                                       " +
		"            <datasource>diff</datasource>            " +
		"            <color>#FF0000</color>                   " +
		"            <legend/>                                " +
		"        </line>                                      " +
		"        <line>                                       " +
		"            <datasource>sine</datasource>            " +
		"            <color>#00FFFF</color>                   " +
		"            <legend>sine function demo@L</legend>    " +
		"        </line>                                      " +
		"        <gprint>                                     " +
		"            <datasource>sun</datasource>             " +
		"            <cf>MAX</cf>                             " +
		"            <format>maxSun = @3@s</format>           " +
		"        </gprint>                                    " +
		"        <gprint>                                     " +
		"            <datasource>sun</datasource>             " +
		"            <cf>AVERAGE</cf>                         " +
		"            <format>avgSun = @3@S@r</format>         " +
		"        </gprint>                                    " +
		"        <gprint>                                     " +
		"            <datasource>shade</datasource>           " +
		"            <cf>MAX</cf>                             " +
		"            <format>maxShade = @3@S</format>         " +
		"        </gprint>                                    " +
		"        <gprint>                                     " +
		"            <datasource>shade</datasource>           " +
		"            <cf>AVERAGE</cf>                         " +
		"            <format>avgShade = @3@S@r</format>       " +
		"        </gprint>                                    " +
		"    </graph>                                         " +
		"</rrd_graph_def>                                     " ;

	static final long SEED = 1909752002L;

	static final Random RANDOM = new Random(SEED);
	static final String FILE = "templates_demo";

	static final long START = Util.getTimestamp(2003, 4, 1);
	static final long END = Util.getTimestamp(2003, 5, 1);

	static final int MAX_STEP = 300;

}



