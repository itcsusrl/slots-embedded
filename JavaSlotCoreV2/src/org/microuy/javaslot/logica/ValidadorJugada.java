package org.microuy.javaslot.logica;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.FiguraTipo;
import org.microuy.javaslot.constante.JugadaTipo;
import org.microuy.javaslot.dominio.Figura;
import org.microuy.javaslot.dominio.JugadaGanadora;
import org.microuy.javaslot.dominio.JugadaLinea;
import org.microuy.javaslot.dominio.JugadaLineaGanadora;
import org.microuy.javaslot.dominio.Maquina;

public class ValidadorJugada {

	private Maquina maquina;
	
	public ValidadorJugada(Maquina maquina) {
		this.maquina = maquina;
	}
	
	/**
	 * Dada una lista de posibles jugadas ganadoras y una lista de jugadas ganadoras se evalua
	 * cual de los elementos de la primer lista aplica a la segunda y se devuelve una lista
	 * con ese conjunto.
	 * 
	 * @param jugadasLineas
	 * @param jugadasGanadoras
	 * @return
	 */
	public List<JugadaLineaGanadora> obtenerJugadasLineasGanadoras(List<JugadaLinea> jugadasLineas) {
		
		List<JugadaLineaGanadora> jugadasLineasGanadoras = new ArrayList<JugadaLineaGanadora>();
		List<JugadaGanadora> jugadasGanadoras = maquina.jugadasGanadoras;
		
		/*
		 * Recorremos la lista de jugadas por linea
		 * para chequear si hay algun premio
		 */
		for(JugadaLinea jugadaLinea : jugadasLineas) {
			
			//obtengo la lista de figuras que
			//forman el juego actual del jugador
			List<Figura> figurasJugadaLinea = jugadaLinea.getFiguras();

			//recorremos la lista de posibles 
			//jugadas ganadoras para cada jugada
			for(JugadaGanadora jugadaGanadoraActual : jugadasGanadoras) {
				
				/**
				 * Si la jugada es scatter la evaluo de otra manera
				 */
				if(jugadaGanadoraActual.tipo == JugadaTipo.SCATTER && jugadaLinea.getTipo() == JugadaTipo.SCATTER) {
			
					//flag que indica si se cumple la jugada
					boolean jugadaValida = true;

					//obtengo la lista de figuras necesarias 
					//para crear una jugada ganadora
					List<Figura> figurasGanadoras = jugadaGanadoraActual.figuras;
					
					//recorro la lista de figuras ganadoras
					for(int i=0; i<figurasGanadoras.size(); i++) {
						
						//figura ganadora actual
						Figura figuraGanadoraActual = figurasGanadoras.get(i);
						
						//figura jugada actual (si es nula es porque la jugada es scatter)
						Figura figuraJugadaActual = figurasJugadaLinea.size() >= (i+1) ? figurasJugadaLinea.get(i) : null;
						
						if(figuraJugadaActual == null || figuraJugadaActual.getId() != figuraGanadoraActual.getId()) {
							jugadaValida = false;
						}
					}
					
					//si la jugada es valida creo
					//el objeto con el premio
					if(jugadaValida) {
						
						//creo la jugada de linea ganadora
						JugadaLineaGanadora jugadaLineaGanadora = new JugadaLineaGanadora();
						jugadaLineaGanadora.setTipo(JugadaTipo.SCATTER);
						jugadaLineaGanadora.setFiguras(figurasJugadaLinea);
						jugadaLineaGanadora.setJugadaGanadora(jugadaGanadoraActual);
						jugadaLineaGanadora.setLinea(jugadaLinea.getLinea());
						
						//agrego la linea de jugada ganadora a la coleccion
						jugadasLineasGanadoras.add(jugadaLineaGanadora);
						
						break;
					}
				}
				else if(jugadaGanadoraActual.tipo == JugadaTipo.NORMAL && jugadaLinea.getTipo() == JugadaTipo.NORMAL) {
					
					//flag que indica si se cumple la jugada
					boolean jugadaValida = true;
					
					//obtengo la lista de figuras necesarias 
					//para crear una jugada ganadora
					List<Figura> figurasGanadoras = jugadaGanadoraActual.figuras;

					//contadores de tipos de figura
					int cantidadFigurasNormal = 0;
					int cantidadFigurasWild = 0;

					//recorro la lista de figuras ganadoras
					for(int i=0; i<figurasGanadoras.size(); i++) {
						
						//figura ganadora actual
						Figura figuraGanadoraActual = figurasGanadoras.get(i);
						
						//figura jugada actual (si es nula es porque la jugada es scatter)
						Figura figuraJugadaActual = figurasJugadaLinea.get(i);

						if((figuraJugadaActual.getId() != figuraGanadoraActual.getId()) && figuraJugadaActual.getTipo() != FiguraTipo.WILD) {
							jugadaValida = false;
							break;
						}
						else {
							
							//si es una figura que se 
							//corresponde con la jugada
							//ganadora
							if(figuraJugadaActual.getTipo() == FiguraTipo.NORMAL) {
								
								//incrementamos el contador 
								//de figuras normales
								cantidadFigurasNormal++;
							}
							if(figuraJugadaActual.getTipo() == FiguraTipo.WILD) {
								
								//incrementamos el contador 
								//de figuras wild
								cantidadFigurasWild++;
							}
						}
					}
					
					//si hay mas figuras wild que
					//normales, entonces la jugada
					//no se valida
					if(cantidadFigurasWild >= cantidadFigurasNormal) {
						jugadaValida = false;
					}
					
					//si la jugada es valida creo
					//el objeto con el premio
					if(jugadaValida) {
						
						//creo la jugada de linea ganadora
						JugadaLineaGanadora jugadaLineaGanadora = new JugadaLineaGanadora();
						jugadaLineaGanadora.setTipo(JugadaTipo.NORMAL);
						jugadaLineaGanadora.setFiguras(figurasJugadaLinea);
						jugadaLineaGanadora.setJugadaGanadora(jugadaGanadoraActual);
						jugadaLineaGanadora.setLinea(jugadaLinea.getLinea());
						
						//agrego la linea de jugada ganadora a la coleccion
						jugadasLineasGanadoras.add(jugadaLineaGanadora);
						
						break;
					}
				}
				else if(jugadaGanadoraActual.tipo == JugadaTipo.BONUS && jugadaLinea.getTipo() == JugadaTipo.BONUS) {
					
					//flag que indica si se cumple la jugada
					boolean jugadaValida = true;

					//obtengo la lista de figuras necesarias 
					//para crear una jugada ganadora
					List<Figura> figurasGanadoras = jugadaGanadoraActual.figuras;
					
					//recorro la lista de figuras ganadoras
					for(int i=0; i<figurasGanadoras.size(); i++) {
						
						//figura ganadora actual
						Figura figuraGanadoraActual = figurasGanadoras.get(i);
						
						//figura jugada actual (si es nula es porque la jugada es bonus)
						Figura figuraJugadaActual = figurasJugadaLinea.size() >= (i+1) ? figurasJugadaLinea.get(i) : null;
						
						if(figuraJugadaActual == null || figuraJugadaActual.getId() != figuraGanadoraActual.getId()) {
							jugadaValida = false;
						}
					}
					
					//si la jugada es valida creo
					//el objeto con el premio
					if(jugadaValida) {
						
						//creo la jugada de linea ganadora
						JugadaLineaGanadora jugadaLineaGanadora = new JugadaLineaGanadora();
						jugadaLineaGanadora.setTipo(JugadaTipo.BONUS);
						jugadaLineaGanadora.setFiguras(figurasJugadaLinea);
						jugadaLineaGanadora.setJugadaGanadora(jugadaGanadoraActual);
						jugadaLineaGanadora.setLinea(jugadaLinea.getLinea());
						
						//agrego la linea de jugada ganadora a la coleccion
						jugadasLineasGanadoras.add(jugadaLineaGanadora);
						
						break;
					}
				}
				else if(jugadaGanadoraActual.tipo == JugadaTipo.WILD && jugadaLinea.getTipo() == JugadaTipo.WILD) {
					
					//creo la jugada de linea ganadora
					JugadaLineaGanadora jugadaLineaGanadora = new JugadaLineaGanadora();
					jugadaLineaGanadora.setTipo(JugadaTipo.WILD);
					jugadaLineaGanadora.setFiguras(figurasJugadaLinea);
					jugadaLineaGanadora.setJugadaGanadora(jugadaGanadoraActual);
					jugadaLineaGanadora.setLinea(jugadaLinea.getLinea());
					
					//agrego la linea de jugada ganadora a la coleccion
					jugadasLineasGanadoras.add(jugadaLineaGanadora);
						
				}
			}
		}
		
		return jugadasLineasGanadoras;
	}
}