package org.microuy.javaslot.logica;

import java.security.SecureRandom;
import java.util.List;

import org.microuy.javaslot.comm.listener.ComandoEvent;
import org.microuy.javaslot.constante.Comando;
import org.microuy.javaslot.constante.ModoExpulsionMoneda;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.dominio.Maquina;
import org.microuy.javaslot.dominio.estadistica.GanadorTipoJugada;
import org.microuy.javaslot.expepcion.JugadorVirtualException;
import org.microuy.javaslot.logica.configuracion.ConfiguracionSlotsPremiosChicos;
import org.microuy.javaslot.util.SlotsRandomGenerator;

public class JugadorVirtual {

	public Maquina maquina;
	
	public JugadorVirtual() {
		
		//configuracion de la maquina
		maquina = new Maquina(ConfiguracionSlotsPremiosChicos.class, true);
		
		//creamos un resumen de cobranza 
		//temporal para guardar algunos
		//valores estadisticos
		maquina.logicaMaquina.crearResumenCobranza();
		
		//el jugador virtual solamente es usado
		//con fines de testeo, no cambiar esto
		maquina.test = true;
		
	}
	
	/**
	 * Comienza un nuevo ciclo de apuestas
	 * en el que va a gastarse la cantidad 
	 * de dinero recibida por parametro en
	 * apuestas en apuestas
	 * @throws JugadorVirtualException 
	 */
	public void comenzarCicloApuestas(double dineroGastable) throws JugadorVirtualException {
		
		//Chequeamos que no exista un resto en la operacion.
		//Si lo hay significa que el dinero gastable recibido 
		//no es multiplo del monto de la moneda configurado
		//en la maquina
		double restoDineroGastable = dineroGastable % (double)Sistema.APUESTA_ADMITIDA;
		if(restoDineroGastable > 0) {
			throw new JugadorVirtualException("El monto ingresado para comenzar el ciclo de apuestas ($" + dineroGastable + ") no es multiplo de $" + Sistema.APUESTA_ADMITIDA);
		}
		
		//para no acceder al tamano de
		//la coleccion en las siguientes
		//iteracion, cargamos de antemano
		//este valor que no cambia nunca
		int cantidadLineasSistema = maquina.configuracion.getLineas().size();
		
		//mientras tengamos dinero
		while(dineroGastable > 0) {
			
			//aleatoriamente calculamos cuantas
			//monedas van a ser jugadas en esta
			//sesion
			int monedasJugadas = (int)(SlotsRandomGenerator.getInstance().nextDouble() * 30d);
			
			//calculamos cuantas monedas tenemos actualmente
			//para no pasarnos en la cantidad de monedas jugadas
			int monedasTotales = (int)(dineroGastable / (double)Sistema.APUESTA_ADMITIDA);
			
			//chequeo de limites por
			//la cantidad de monedas
			//restantes
			if(monedasTotales < monedasJugadas) {
				//igualamos las monedas que
				//vamos a jugar con las que
				//nos quedan
				monedasJugadas = monedasTotales;
			}
			
			//calculamos el dinero
			double dineroActual = monedasJugadas * Sistema.APUESTA_ADMITIDA;

			//le decrementamos al monto
			//total la cantidad de monedas
			//que decidimos jugar durante
			//la sesion actual
			dineroGastable -= dineroActual;

			//ingresamos una moneda al sistema
			ingresarMonedas(monedasJugadas);
			
			//calculamos la cantidad de 
			//combinaciones de monto por
			//linea
			int combinacionesMontoPorLinea = (int)(Sistema.APUESTA_MAXIMA_POR_LINEA / Sistema.INCREMENTO_APUESTA);
			
			//mientras no terminemos de 
			//gastar el dinero de la sesion
			while(maquina.montoJugador > 0) {
				
				//definicion de variables que
				//van a decir como va a ser la
				//apuesta
				short lineasApostadas = 0;
				float montoPorLinea = 0f;
				
				//elegimos aleatoriamente la
				//cantidad de lineas apostadas
				lineasApostadas = (short)(SlotsRandomGenerator.getInstance().nextDouble() * (double)cantidadLineasSistema);
				if(lineasApostadas == 0) {
					lineasApostadas = 1;
				}
				
				//elegimos aleatoriamente el
				//monto apostado por linea
				montoPorLinea = Sistema.INCREMENTO_APUESTA * (int)(SlotsRandomGenerator.getInstance().nextDouble() * (float)combinacionesMontoPorLinea);
				if(montoPorLinea == 0) {
					montoPorLinea = Sistema.INCREMENTO_APUESTA;
				}
				
				//en base a la apuesta aleatoria
				//elegida, calculamos cuanto es 
				//el monto apostado
				double montoApostado = montoPorLinea * (double)lineasApostadas;
				
				//obtenemos el monto actual
				//que tiene la maquina dentro
				double montoJugador = maquina.montoJugador;
				
				//si el monto apostado es mayor que
				//el disponible del jugador, entonces
				//decrementamos la apuesta al minimo
				if(montoApostado > montoJugador) {
					montoPorLinea = Sistema.INCREMENTO_APUESTA;
					lineasApostadas = 1;
				}
				
				/**
				 * Llegado aqui, ya se encuentra configurada
				 * una sesion de usuario con una cantidad 
				 * aleatoria de monedas, como asi tambien una
				 * apuesta aleatoria. Lo siguiente a ejecutar
				 * es la apuesta asi se puede comenzar a recabar
				 * datos
				 */
				maquina.setDineroApostadoPorLinea(montoPorLinea);
				maquina.lineasApostadas = lineasApostadas;
				maquina.logicaMaquina.apostarSlots(false);
				
//				System.out.println(maquina.montoJugador + " \t\t __ \t\t" + maquina.logicaMaquina.sistemaControlPago.valorFuncionControlPago + " \t\t __ \t\t" + + maquina.logicaMaquina.sistemaControlPago.shifterY);
				
				/**
				 * Cagamos la logica en los tiempos
				 * de la onda sinusoidal
				 */
				float sleepTimeBasico = 10f;
				SecureRandom sr = SlotsRandomGenerator.getInstance();
				float varianzaTiempo = sr.nextFloat() * (sleepTimeBasico/2);
				varianzaTiempo = sr.nextBoolean() ? -varianzaTiempo : varianzaTiempo;
				
				try {
					float sleepTime = sleepTimeBasico + varianzaTiempo;
					Thread.sleep((long)sleepTime);
				} 
				catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				/**
				 * Comportamiento cambiable de pago. 
				 * En la configuracion siguiente se indica
				 * que si el jugador ha alcanzado a ganar
				 * el doble del dinero ingresado, se retira
				 * dejando una nueva sesion disponible
				 */
				float aleatorioSacada = SlotsRandomGenerator.getInstance().nextFloat() - 0.5f;
				if(aleatorioSacada < 0) {
					aleatorioSacada = 0f;
				}
				
				if(maquina.montoJugador >= (maquina.logicaMaquina.dineroRealIngresadoSesion * (1f + aleatorioSacada)) && maquina.logicaMaquina.dineroRealIngresadoSesion > 0f) {

					if(dineroActual > Sistema.APUESTA_ADMITIDA) {

						//calculamos la cantidad
						//de monedas a extraer
						int monedasAExtraer = (int)(maquina.montoJugador / (double)Sistema.APUESTA_ADMITIDA);
						
						//extrae las monedas 
						extraerMonedas(monedasAExtraer);
					}
				}
			}
		}
	}
	
	/**
	 * Ingresa virtualmente monedas al sistema
	 * para replicar de la manera mas parecida
	 * posible el funcionamiento real de la 
	 * maquina, como asi tambien para poder
	 * tomar valores estadisticos de la duracion
	 * de juegos, etc.
	 */
	private void ingresarMonedas(int monedas) {
		
		ComandoEvent comandoEvent = new ComandoEvent();
		comandoEvent.setComandoId(Comando.COMANDO_DINERO_INGRESADO);
		comandoEvent.setGenerico((byte)0); //puerta cerrada
		
		//para cada moneda 
		for(int i=0; i<monedas; i++) {
			
			//ejecuta el comando de 
			//ingreso de dinero virtual
			maquina.logicaMaquina.procesarEventoComando(comandoEvent);
			
		}
	}
	
	/**
	 * Pide virtualmente a la maquina que 
	 * realice el pago de las monedas a 
	 * favor del jugador
	 */
	private void extraerMonedas(int monedasAExtraer) {
		
		ComandoEvent comandoEvent = new ComandoEvent();
		comandoEvent.setComandoId(Comando.COMANDO_VERIFICAR_PAGO_MONEDA);
		comandoEvent.setGenerico(ModoExpulsionMoneda.MODO_EXPULSION_MONEDA_PAGO_JUGADOR);
		comandoEvent.setGenericoHelper((byte)0);
		
		for(int i=0; i<monedasAExtraer; i++) {
			
			//ejecuta el comando de
			//egreso de dinero real
			maquina.logicaMaquina.procesarEventoComando(comandoEvent);
		}
	}
	
//	public static void main(String[] args) {
//		
//		try {
//			
//			JugadorVirtual jv1 = new JugadorVirtual();
//			jv1.comenzarCicloApuestas(10000);
//
//			List<GanadorTipoJugada> gtjList = jv1.maquina.estadisticaMaquina.ganadoresTipoJugada;
//			for(GanadorTipoJugada gtj : gtjList) {
//				System.out.println("Tipo jugada: " + gtj.tipoJugada + " \t\t\t Veces Salidas: " + gtj.ganadas + "\t\t Multiplicador: " + gtj.multiplicador);
//			}
//			
//		} 
//		catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}