package org.microuy.javaslot.logica.configuracion;

import org.microuy.javaslot.constante.FiguraTipo;
import org.microuy.javaslot.constante.JugadaTipo;
import org.microuy.javaslot.dominio.Figura;
import org.microuy.javaslot.dominio.JugadaGanadora;
import org.microuy.javaslot.dominio.JugadaGanadoraBonus;
import org.microuy.javaslot.dominio.Rodillo;

public class ConfiguracionGanaSiempre extends Configuracion {

	protected void inicializarFiguras() {

		Figura figura4 = new Figura(4, "figura4", FiguraTipo.NORMAL);
		Figura figura7 = new Figura(7, "figura7", FiguraTipo.NORMAL);
		Figura figura8 = new Figura(8, "figura8", FiguraTipo.NORMAL);
		Figura figura9 = new Figura(9, "figura9", FiguraTipo.NORMAL);
		Figura figuraScatter = new Figura(1000, "scatter", FiguraTipo.SCATTER);		
		Figura figuraBonus = new Figura(99, "bonus", FiguraTipo.BONUS);
		
		figuras.add(figura4);
		figuras.add(figura7);
		figuras.add(figura8);
		figuras.add(figura9);
		figuras.add(figuraScatter);
		figuras.add(figuraBonus);
		
	}

	protected void inicializarJugadasGanadoras() {

		//jugada bonus
		//jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99", figuras, 1));

		//jugada bonus
		//jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99,99", figuras, 2));

		//jugada scatter
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1000,1000", figuras, JugadaTipo.SCATTER, 2));
//		
//		//jugada
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4", figuras, JugadaTipo.NORMAL, 50));
//		
//		//jugada
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4,4", figuras, JugadaTipo.NORMAL, 100));
//
//		//jugada
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4,4,4", figuras, JugadaTipo.NORMAL, 500));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7", figuras, JugadaTipo.NORMAL, 1));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7,7", figuras, JugadaTipo.NORMAL, 10));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7,7,7", figuras, JugadaTipo.NORMAL, 100));

	}

	protected void inicializarRodillos() {

		//rodillo 1
		rodillos.add(Rodillo.configurarRodillo(1, "4,4,4,4,4,7,7,7,7,7,7,1000,99", figuras));

		//rodillo 2
		rodillos.add(Rodillo.configurarRodillo(2, "4,4,4,4,4,7,7,7,7,7,7,1000,1000", figuras));

		//rodillo 3
		rodillos.add(Rodillo.configurarRodillo(3, "4,4,4,4,4,7,7,7,7,7,7,7,99", figuras));

		//rodillo 4
		rodillos.add(Rodillo.configurarRodillo(4, "4,4,4,4,4,7,7,7,7,7,7,1000,99", figuras));

		//rodillo 5
		rodillos.add(Rodillo.configurarRodillo(5, "4,4,4,4,4,7,7,7,7,7,7,99,99", figuras));

	}
}