package org.microuy.javaslot.persistencia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.dominio.ResumenCobranza;

public class ResumenCobranzaDAO extends GenericDAO {

	public void inicializar() {
		TABLA_NOMBRE = "resumen_cobranza";
	}

	public void crearTabla(Connection connection) throws SQLException {
		Statement stmt = connection.createStatement();
		stmt.executeUpdate("CREATE TABLE " + TABLA_NOMBRE + " " +
						   "(id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), " +
						   "fecha_desde TIMESTAMP NOT NULL, " +
						   "fecha_hasta TIMESTAMP, " +
						   "base FLOAT NOT NULL, " +
						   "entrada FLOAT NOT NULL, " +
						   "salida FLOAT NOT NULL, " +
						   "saldo FLOAT NOT NULL, " +
						   "entrada_monedas_pa INTEGER NOT NULL, " +
						   "entrada_monedas_pc INTEGER NOT NULL, " +
						   "salida_monedas_pa INTEGER NOT NULL, " +
						   "salida_monedas_pc INTEGER NOT NULL, " +
						   "PRIMARY KEY (id)) ");
		
		stmt.close();
	}

	public void crearData(Connection connection) throws SQLException {
		
		//se crea el primer resumen
		//de la maquina
		//crearResumen(0,0,0, new Timestamp(System.currentTimeMillis()), null);
		
	}
	
	public void crearResumen(float base, float entrada, float salida, float saldo, int entradaMonedasPA, int entradaMonedasPC, int salidaMonedasPA, int salidaMonedasPC, Timestamp fechaDesde, Timestamp fechaHasta) throws SQLException {
		
		String sql = "INSERT INTO " + TABLA_NOMBRE + "(base, entrada, salida, saldo, entrada_monedas_pa, entrada_monedas_pc, salida_monedas_pa, salida_monedas_pc, fecha_desde, fecha_hasta) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		PreparedStatement ps = obtenerConexion().prepareStatement(sql);
		ps.setFloat(1, base);
		ps.setFloat(2, entrada);
		ps.setFloat(3, salida);
		ps.setFloat(4, saldo);
		ps.setFloat(5, entradaMonedasPA);
		ps.setFloat(6, entradaMonedasPC);
		ps.setFloat(7, salidaMonedasPA);
		ps.setFloat(8, salidaMonedasPC);
		ps.setTimestamp(9, fechaDesde);
		ps.setTimestamp(10, fechaHasta);
		ps.executeUpdate();
		
		ps.close();
	}
	
	/**
	 * Actualiza los valores del resumen
	 * especificado por id
	 * @throws SQLException
	 */
	public void actualizarResumen(int id, float base, float entrada, float salida, float saldo, int entradaMonedasPA, int entradaMonedasPC, int salidaMonedasPA, int salidaMonedasPC) throws SQLException {
		
		Statement stmt = obtenerConexion().createStatement();
		String sql = "UPDATE " + TABLA_NOMBRE + " " + 
		 			 "SET " +
		 			 "base = " + base + ", " +
		 			 "entrada = " + entrada + ", " +
		 			 "salida = " + salida + ", " +
		 			 "saldo = " + saldo + ", " +
		 			 "entrada_monedas_pa = " + entradaMonedasPA + ", " +
		 			 "entrada_monedas_pc = " + entradaMonedasPC + ", " +
		 			 "salida_monedas_pa = " + salidaMonedasPA + ", " +
		 			 "salida_monedas_pc = " + salidaMonedasPC + " " +
		 			 "WHERE id=" + id;
		
		stmt.executeUpdate(sql);
		stmt.close();
	}

	public void actualizarResumenFechaHasta(int id, Timestamp fechaHasta) throws SQLException {
		
		String sql = "UPDATE " + TABLA_NOMBRE + " " + 
		 			 "SET " +
		 			 "fecha_hasta = ? " +
		 			 "WHERE id=" + id;
		
		PreparedStatement ps = obtenerConexion().prepareStatement(sql);
		ps.setTimestamp(1, fechaHasta);
		
		ps.executeUpdate();
		ps.close();
	}

	/**
	 * Devuelve el id del ultimo
	 * resumen insertado en la tabla
	 * @return
	 * @throws SQLException 
	 */
	public ResumenCobranza obtenerUltimoResumen() throws SQLException {
		
		Statement stmt = obtenerConexion().createStatement();
		String sql = "SELECT * FROM " + TABLA_NOMBRE + " ORDER BY id desc FETCH FIRST ROW ONLY";
		ResultSet rs = stmt.executeQuery(sql);
		
		//vamos al primer registro
		boolean existeRegistro = rs.next();
		
		//el objeto a devolver
		ResumenCobranza resumenCobranza = null;
		
		if(existeRegistro) {
			
			//obtenemos el id
			int id = rs.getInt("id");
			float base = rs.getFloat("base");
			float entrada = rs.getFloat("entrada");
			float salida = rs.getFloat("salida");
			float saldo = rs.getFloat("saldo");
			int entradaMonedasPA = rs.getInt("entrada_monedas_pa");
			int entradaMonedasPC = rs.getInt("entrada_monedas_pc");
			int salidaMonedasPA = rs.getInt("salida_monedas_pa");
			int salidaMonedasPC = rs.getInt("salida_monedas_pc");
			Timestamp fechaDesde = rs.getTimestamp("fecha_desde");
			Timestamp fechaHasta = rs.getTimestamp("fecha_hasta");
			
			//resumen de cobranza
			resumenCobranza = new ResumenCobranza(id, base, entrada, salida, saldo, entradaMonedasPA, entradaMonedasPC, salidaMonedasPA, salidaMonedasPC, fechaDesde, fechaHasta);
			
		}
		
		//cerramos recursos
		rs.close();
		stmt.close();
		
		return resumenCobranza;
	}
	
	public List<ResumenCobranza> obtenerUltimosResumenes(int cantidad) throws SQLException {
		
		List<ResumenCobranza> lista = new ArrayList<ResumenCobranza>();
		
		//creamos el sql con un limite
		//de registros a obtener
		String sql = "SELECT * FROM " + TABLA_NOMBRE + " ORDER BY id asc FETCH FIRST " + cantidad + " ROWS ONLY";
		
		Statement stmt = obtenerConexion().createStatement();
		ResultSet rs = stmt.executeQuery(sql);
	
		//recorremos la lista de 
		//resumenes obtenidos
		while(rs.next()) {

			//obtenemos el id
			int id = rs.getInt("id");
			float base = rs.getFloat("base");
			float entrada = rs.getFloat("entrada");
			float salida = rs.getFloat("salida");
			float saldo = rs.getFloat("saldo");
			int entradaMonedasPA = rs.getInt("entrada_monedas_pa");
			int entradaMonedasPC = rs.getInt("entrada_monedas_pc");
			int salidaMonedasPA = rs.getInt("salida_monedas_pa");
			int salidaMonedasPC = rs.getInt("salida_monedas_pc");
			Timestamp fechaDesde = rs.getTimestamp("fecha_desde");
			Timestamp fechaHasta = rs.getTimestamp("fecha_hasta");

			ResumenCobranza resumenCobranza = new ResumenCobranza(id, base, entrada, salida, saldo, entradaMonedasPA, entradaMonedasPC, salidaMonedasPA, salidaMonedasPC, fechaDesde, fechaHasta);
			lista.add(resumenCobranza);

		}
		
		//cerramos recursos
		rs.close();
		stmt.close();		
		
		return lista;
	}
	
	public void incrementarEntradaMoneda(int id, boolean puertaAbierta) throws SQLException {
		
		String nombreCampo = puertaAbierta ? "entrada_monedas_pa" : "entrada_monedas_pc";
		
		String sql = "UPDATE " + TABLA_NOMBRE + " " + 
		 			 "SET " +
		 			 nombreCampo + " = " + nombreCampo + " + 1 " +
		 			 "WHERE id=" + id;

		Connection conexion = obtenerConexion();
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
		ps.close();
		conexion.commit();

	}

	public void incrementarSalidaMoneda(int id, boolean puertaAbierta) throws SQLException {
		
		String nombreCampo = puertaAbierta ? "salida_monedas_pa" : "salida_monedas_pc";
		
		String sql = "UPDATE " + TABLA_NOMBRE + " " + 
		 			 "SET " +
		 			 nombreCampo + " = " + nombreCampo + " + 1 " +
		 			 "WHERE id=" + id;

		Connection conexion = obtenerConexion();
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
		ps.close();
		conexion.commit();

	}
}