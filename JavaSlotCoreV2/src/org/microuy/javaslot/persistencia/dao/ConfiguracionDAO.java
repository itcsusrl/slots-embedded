package org.microuy.javaslot.persistencia.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.constante.SistemaFrontend;

public class ConfiguracionDAO extends GenericDAO {

	/**
	 * DEFINICION DE LAS CONSTANTES DE CONFIGURACION
	 * PARA SER USADAS LUEGO DURANTE LA ACTUALIZACION
	 * O LECTURA DE CAMPOS INDEPENDIENTES
	 */
	public static final String APUESTA_ADMITIDA = "APUESTA_ADMITIDA";
	public static final String INCREMENTO_APUESTA = "INCREMENTO_APUESTA";
	public static final String APUESTA_MAXIMA_POR_LINEA = "APUESTA_MAXIMA_POR_LINEA";
	public static final String FACTOR_CONVERSION_MONEDA = "FACTOR_CONVERSION_MONEDA";
	public static final String MONTO_APUESTA_GRATUITA = "MONTO_APUESTA_GRATUITA";
	public static final String CANTIDAD_LINEAS_APUESTA_GRATUITA = "CANTIDAD_LINEAS_APUESTA_GRATUITA";

	public static final String APUESTA_WILD = "APUESTA_WILD";

	public static final String PORCENTAJE_JUGADOR = "PORCENTAJE_JUGADOR";
	public static final String AUDIO = "AUDIO";
	
	public static final String JACKPOT_MONTO_MINIMO = "JACKPOT_MONTO_MINIMO";
	public static final String JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA = "JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA";
	public static final String JACKPOT_PROBABILIDADES_JUGADOR = "JACKPOT_PROBABILIDADES_JUGADOR";
	public static final String JACKPOT_MONTO_ACTUAL = "JACKPOT_MONTO_ACTUAL";
	public static final String JACKPOT_APERTURA_POZO = "JACKPOT_APERTURA_POZO";
	
	public static final String MENOR_MAYOR_PORCENTAJE_EXTRAIDO_APUESTA = "MENOR_MAYOR_PORCENTAJE_EXTRAIDO_APUESTA";
	public static final String MENOR_MAYOR_MONTO_ACTUAL = "MENOR_MAYOR_MONTO_ACTUAL";
	
	public static final String MONTO_PREMIOS_SESION_JUGADOR = "MONTO_PREMIOS_SESION_JUGADOR";
	public static final String MONTO_APOSTADO_SESION_JUGADOR = "MONTO_APOSTADO_SESION_JUGADOR";
	
	/**
	 * El monto del jugador se respalda 
	 * ante cualquier inconveniente, de
	 * esta manera cuando se reinicia la
	 * maquina mantenemos un registro del
	 * dinero que habia en la misma antes
	 * de resetearses
	 */
	public static final String MONTO_JUGADOR = "MONTO_JUGADOR";
	
	/**
	 * Monto de pago excedido. Este
	 * valor se actualiza cada vez
	 * que la maquina se ve excecida
	 * en su capacidad de pago debido
	 * a un premio de gran volumen, o
	 * porque la misma se estaba 
	 * quedando sin monedas
	 */
	public static final String CAPACIDAD_PAGO_EXCEDIDA = "CAPACIDAD_PAGO_EXCEDIDA";
	
	public ConfiguracionDAO() {
		
	}
	
	public void inicializar() {
		TABLA_NOMBRE = "configuracion";
	}

	public void crearTabla(Connection connection) throws SQLException {

		Statement stmt = connection.createStatement();
		stmt.executeUpdate("CREATE TABLE " + TABLA_NOMBRE + " " +
						   "(codigo VARCHAR(100), valor VARCHAR(100), nombre VARCHAR(100), descripcion VARCHAR(1000)," +
						   " PRIMARY KEY (codigo))");
		
		stmt.close();
	}

	/**
	 * Dado un codigo de configuracion, actualiza su
	 * valor por uno nuevo recibido tambien por 
	 * parametro
	 * @param codigo
	 * @throws SQLException 
	 */
	public void actualizarValor(String codigo, String nuevoValor) throws SQLException {
	
		String sql = "UPDATE " + TABLA_NOMBRE + " " + 
		 			 "SET " +
		 			 "valor = '" + nuevoValor + "' " +
		 			 "WHERE codigo='" + codigo + "'";
		
		Statement stmt = obtenerConexion().createStatement();
		stmt.executeUpdate(sql);
		stmt.close();
	}
	
	public String obtenerValor(String codigo) throws SQLException {
		
		String valor = null;
		
		String sql = "SELECT valor FROM " + TABLA_NOMBRE + " WHERE codigo='" + codigo + "'";

		Statement stmt = obtenerConexion().createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next()) {
			valor = rs.getString("valor");
		}
		
		rs.close();
		stmt.close();
		
		
		return valor;
	}
	
	public String obtenerDescripcion(String codigo) throws SQLException {
		
		String descripcion = null;
		
		String sql = "SELECT descripcion FROM " + TABLA_NOMBRE + " WHERE codigo='" + codigo + "'";

		Statement stmt = obtenerConexion().createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next()) {
			descripcion = rs.getString("descripcion");
		}
		
		rs.close();
		stmt.close();
		
		return descripcion;
	}
	
	public Map<String, String> obtenerValores() throws SQLException {
		
		Map<String, String> valores = new HashMap<String, String>();
		
		String sql = "SELECT codigo, valor FROM " + TABLA_NOMBRE;

		Statement stmt = obtenerConexion().createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next()) {
			
			String codigo = rs.getString("codigo");
			String valor = rs.getString("valor");
			
			valores.put(codigo, valor);
		}
		
		rs.close();
		stmt.close();
		
		return valores;
	}
	
	public void crearData(Connection connection) throws SQLException {

		String sql = "INSERT INTO " + TABLA_NOMBRE + "(codigo, valor, nombre, descripcion) " +
					 "VALUES ";
		
		//CONFIGURACION GENERAL
		sql += "('" + APUESTA_ADMITIDA + "', '" + Sistema.APUESTA_ADMITIDA + "', 'Apuesta Admitida', 'Monto real de incremento tras el ingreso de dinero a la maquina.'),";
		sql += "('" + INCREMENTO_APUESTA + "', '" + Sistema.INCREMENTO_APUESTA + "', 'Incremento Apuesta', 'Cada vez que el monto de la apuesta es incrementado, el valor de incremento es obtenido de aqui'),";
		sql += "('" + APUESTA_MAXIMA_POR_LINEA + "', '" + Sistema.APUESTA_MAXIMA_POR_LINEA + "', 'Apuesta maxima por linea', 'El monto maximo apostable por linea'),";
		sql += "('" + FACTOR_CONVERSION_MONEDA + "', '" + Sistema.FACTOR_CONVERSION_MONEDA + "', 'Factor conversion moneda', 'Factor multiplicador o divisor de moneda. Usado para modificar el tipo de moneda en juego y usar valores mas altos o mas bajos dependiendo de lo deseado'),";
		sql += "('" + APUESTA_WILD + "', '" + Sistema.APUESTA_WILD + "', 'Apuesta Wild', 'Si se encuentra habilitado indica que se evaluaran jugadas conformadas totalmente por figuras wild. El valor del premio para esta jugada es el mismo que el de la jugada de mayor pago del juego.'),";
		sql += "('" + MONTO_APUESTA_GRATUITA + "', '" + Sistema.MONTO_APUESTA_GRATUITA + "', 'Monto Apuesta Gratuita', 'El monto apostado durante la ejecucion de una tirada gratuita generada por el bonus'),";
		sql += "('" + CANTIDAD_LINEAS_APUESTA_GRATUITA + "', '" + Sistema.CANTIDAD_LINEAS_APUESTA_GRATUITA + "', 'Cantidad Lineas Apuesta Gratuita', 'Las lineas apostadas durante la ejecucion de una tirada gratuita generada por el bonus'),";
		sql += "('" + PORCENTAJE_JUGADOR + "', '" + Sistema.PORCENTAJE_JUGADOR + "', 'Porcentaje Jugador', 'Define el porcentaje de pago de la maquina a el jugador. Ej.: Si fuera 30%, en una apuesta de 100 pesos, 30 son devueltos al jugador, y la casa se queda con los 70 restantes'),";
		sql += "('" + AUDIO + "', '" + SistemaFrontend.AUDIO_FX_ENABLED + "', 'Audio', 'Estado del sistema de audio de la maquina (Activado o desactivado)'),";

		//JUGADOR
		sql += "('" + MONTO_JUGADOR + "', '0', 'Monto del Jugador', 'Monto del jugador. Usado en caso de que se reinicie la maquina por alguna razon inesperada'),";
		sql += "('" + CAPACIDAD_PAGO_EXCEDIDA + "', '0', 'Capacidad de pago de la maquina excedida', 'Cuando se exige que la maquina pague y la misma no tiene mas credito, se guarda en este registro la informacion del monto adeudado hacia el jugador.'),";
		
		//CONFIGURACION JACKPOT
		sql += "('" + JACKPOT_MONTO_MINIMO + "', '" + Sistema.JACKPOT_MONTO_MINIMO + "', 'Monto Minimo', 'El monto al cual se debe llegar antes de que se entregue el pozo'),";
		sql += "('" + JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA + "', '" + Sistema.JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA + "', '% Extraido de Apuesta para el Jackpot', 'De cada apuesta maxima ejecutada se extrae un porcentaje que es agregado al pozo Jackpot para irlo incrementando.'),";
		sql += "('" + JACKPOT_PROBABILIDADES_JUGADOR + "', '" + Sistema.JACKPOT_PROBABILIDADES_JUGADOR + "', 'Probabilidades Jugador', 'Una vez que el pozo llega al monto minimo, el apostador no lo obtiene directamente, sino que debe de alcanzar las probabilidades de ganarlo definidas en este campo.'),";
		sql += "('" + JACKPOT_APERTURA_POZO + "', '" + Sistema.JACKPOT_APERTURA_POZO + "', 'Apertura de Pozo', 'Define el monto minimo necesario en la apuesta para participar por el pozo'),";
		sql += "('" + JACKPOT_MONTO_ACTUAL + "', '" + 0 + "', 'Monto Actual Jackpot','El monto actual del pozo acumulado'),";

		//CONFIGURACION MINIJUEGO
		sql += "('" + MENOR_MAYOR_MONTO_ACTUAL + "', '" + 0 + "', 'Monto Actual Minijuego Menor o Mayor','El monto actual del pozo acumulado del minijuego menor o mayor'),";
		sql += "('" + MENOR_MAYOR_PORCENTAJE_EXTRAIDO_APUESTA + "', '" + Sistema.MENOR_MAYOR_PORCENTAJE_EXTRAIDO_APUESTA + "', '% Extraido de Apuesta para el pozo -menor o mayor-', 'De cada apuesta maxima ejecutada se extrae un porcentaje que es agregado al pozo -menor o mayor- para irlo incrementando.'),";
		
		//SESION JUGADOR
		sql += "('" + MONTO_PREMIOS_SESION_JUGADOR + "', '" + 0 + "', '', ''),";
		sql += "('" + MONTO_APOSTADO_SESION_JUGADOR + "', '" + 0 + "', '', '')";
		
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(sql);
		
		stmt.close();
	}
}