package org.microuy.javaslot.persistencia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.dominio.PagoExcedido;
import org.microuy.javaslot.dominio.ResumenCobranza;

public class PagoExcedidoDAO extends GenericDAO {

	public void inicializar() {
		TABLA_NOMBRE = "pago_excedido";
	}

	public void crearTabla(Connection connection) throws SQLException {
		Statement stmt = connection.createStatement();
		stmt.executeUpdate("CREATE TABLE " + TABLA_NOMBRE + " " +
						   "(id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), " +
						   "fecha TIMESTAMP NOT NULL, " +
						   "monto FLOAT NOT NULL, " +
						   "pagado BOOLEAN NOT NULL, " +
						   "PRIMARY KEY (id)) ");
		stmt.close();
	}

	public void crearData(Connection connection) throws SQLException {
	
	}
	
	public List<PagoExcedido> obtenerUltimosPagosExcedidos() throws SQLException {
		
		List<PagoExcedido> lista = new ArrayList<PagoExcedido>();
		
		//creamos el sql con un limite
		//de registros a obtener
		String sql = "SELECT * FROM " + TABLA_NOMBRE + " ORDER BY fecha desc FETCH FIRST 10 ROWS ONLY";
		
		Statement stmt = obtenerConexion().createStatement();
		ResultSet rs = stmt.executeQuery(sql);
	
		//recorremos la lista de 
		//resumenes obtenidos
		while(rs.next()) {

			//obtenemos el id
			int id = rs.getInt("id");
			float monto = rs.getFloat("monto");
			Timestamp fecha = rs.getTimestamp("fecha");
			boolean pagado = rs.getBoolean("pagado");

			PagoExcedido pagoExcedido = new PagoExcedido(id, fecha, monto, pagado);
			lista.add(pagoExcedido);

		}
		
		//cerramos recursos
		rs.close();
		stmt.close();		
		
		return lista;
	}
	
	/**
	 * Devuelve el id del ultimo
	 * pago excedido insertado en 
	 * la tabla
	 * @return
	 * @throws SQLException 
	 */
	public PagoExcedido obtenerUltimoResumen() throws SQLException {
		
		Statement stmt = obtenerConexion().createStatement();
		String sql = "SELECT * FROM " + TABLA_NOMBRE + " ORDER BY id desc FETCH FIRST ROW ONLY";
		ResultSet rs = stmt.executeQuery(sql);
		
		//vamos al primer registro
		boolean existeRegistro = rs.next();
		
		//el objeto a devolver
		PagoExcedido pagoExcedido = null;
		
		if(existeRegistro) {
			
			//obtenemos el id
			int id = rs.getInt("id");
			float monto = rs.getFloat("monto");
			Timestamp fecha = rs.getTimestamp("fecha");
			boolean pagado = rs.getBoolean("pagado");

			pagoExcedido = new PagoExcedido(id, fecha, monto, pagado);
			
		}
		
		//cerramos recursos
		rs.close();
		stmt.close();
		
		return pagoExcedido;
	}
	
	public void crearPagoExcedido(float monto, Timestamp fecha, boolean pagado) throws SQLException {
		
		String sql = "INSERT INTO " + TABLA_NOMBRE + "(monto, fecha, pagado) VALUES (?, ?, ?)";
		
		PreparedStatement ps = obtenerConexion().prepareStatement(sql);
		ps.setFloat(1, monto);
		ps.setTimestamp(2, fecha);
		ps.setBoolean(3, pagado);
		ps.executeUpdate();
		
		ps.close();
	}
	
	public void cambiarEstadoPagoExcedido(int id, boolean estado) throws SQLException {
		
		String sql = "UPDATE " + TABLA_NOMBRE + " " + 
		 			 "SET pagado = " + estado + " " +
		 			 "WHERE id=" + id;

		Connection conexion = obtenerConexion();
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
		ps.close();
		conexion.commit();

	}

}