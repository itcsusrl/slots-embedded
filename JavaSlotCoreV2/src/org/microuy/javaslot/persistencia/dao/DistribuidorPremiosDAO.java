package org.microuy.javaslot.persistencia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.dominio.DistribucionPremio;

public class DistribuidorPremiosDAO extends GenericDAO {

	public void inicializar() {
		TABLA_NOMBRE = "distribuidor_premios";
	}

	public void crearTabla(Connection connection) throws SQLException {
		Statement stmt = connection.createStatement();
		stmt.executeUpdate("CREATE TABLE " + TABLA_NOMBRE + " " +
						   "(id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), " +
						   "forma_jugada VARCHAR(100) NOT NULL, " +
						   "acumulado FLOAT NOT NULL, " +
						   "PRIMARY KEY (id)) ");
		
		stmt.close();
	}

	public void crearData(Connection connection) throws SQLException {
		
	}
	
	/**
	 * Crea un nuevo registro de distribucion 
	 * de jugadas en la persistencia
	 * 
	 * @param formaJugada
	 * @throws SQLException
	 */
	public void crearDistribucion(String formaJugada) throws SQLException {
		
		String sql = "INSERT INTO " + TABLA_NOMBRE + "(forma_jugada, acumulado) VALUES (?, ?)";
		
		PreparedStatement ps = obtenerConexion().prepareStatement(sql);
		ps.setString(1, formaJugada);
		ps.setFloat(2, 0f);
		ps.executeUpdate();
		
		ps.close();
	}
	
	/**
	 * Actualiza el monto acumulable de 
	 * la jugada ganadora recibida por
	 * parametro
	 * @param formaJugada
	 * @throws SQLException 
	 */
	public void actualizarAcumulable(String formaJugada, float acumulado) throws SQLException {
		
		Statement stmt = obtenerConexion().createStatement();
		String sql = "UPDATE " + TABLA_NOMBRE + " " + 
		 			 "SET " +
		 			 "acumulado = " + acumulado + " " +
		 			 "WHERE forma_jugada='" + formaJugada + "'";
		
		stmt.executeUpdate(sql);
		stmt.close();
	}
	
	/**
	 * Obtiene todos los registros de la tabla 
	 * 
	 * @return
	 * @throws SQLException
	 */
	public List<DistribucionPremio> obtenerDistribuciones() throws SQLException {
		
		List<DistribucionPremio> lista = new ArrayList<DistribucionPremio>();
		
		//creamos el sql 
		String sql = "SELECT * FROM " + TABLA_NOMBRE;
		
		Statement stmt = obtenerConexion().createStatement();
		ResultSet rs = stmt.executeQuery(sql);
	
		//recorremos la lista de 
		//distribuciones obtenidas
		while(rs.next()) {

			//obtenemos el id
			int id = rs.getInt("id");
			String formaJugada = rs.getString("forma_jugada");
			float acumulado = rs.getFloat("acumulado");

			DistribucionPremio distribucionPremio = new DistribucionPremio(id, formaJugada, acumulado);
			lista.add(distribucionPremio);
			
		}
		
		//cerramos recursos
		rs.close();
		stmt.close();		
		
		return lista;
	}
	
	/**
	 * Borra todos los registros de la tabla
	 * @throws SQLException 
	 */
	public void borrarTodasDistribuciones() throws SQLException {
		
		//creamos el sql 
		String sql = "DELETE FROM " + TABLA_NOMBRE;
		
		//creamos el statement
		Statement stmt = obtenerConexion().createStatement();

		//seteamos sql y lo ejecutamos
		stmt.executeUpdate(sql);
		
	}
}