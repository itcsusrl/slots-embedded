package org.microuy.javaslot.persistencia.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.microuy.javaslot.persistencia.util.DBConnection;


public abstract class GenericDAO {

	protected String TABLA_NOMBRE;
	
	public abstract void inicializar();
	public abstract void crearTabla(Connection connection) throws SQLException;
	public abstract void crearData(Connection connection) throws SQLException;

	public GenericDAO() {
		inicializar();
	}
	
	/**
	 * Abre y devuelve una conexion a la base de datos
	 * @return
	 * @throws SQLException
	 */
	public Connection obtenerConexion() throws SQLException {
		return DBConnection.abrirConexion();
	}
}