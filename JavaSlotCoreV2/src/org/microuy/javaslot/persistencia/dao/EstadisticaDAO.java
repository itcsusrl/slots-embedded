package org.microuy.javaslot.persistencia.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class EstadisticaDAO extends GenericDAO {

	/**
	 * DEFINICION DE LAS CONSTANTES DE CONFIGURACION
	 * PARA SER USADAS LUEGO DURANTE LA ACTUALIZACION
	 * O LECTURA DE CAMPOS INDEPENDIENTES
	 */
	public static final String DINERO_APOSTADO = "DINERO_APOSTADO";
	public static final String DINERO_GANADO = "DINERO_GANADO";
	public static final String DINERO_GANADO_NORMAL = "DINERO_GANADO_NORMAL";
	public static final String DINERO_GANADO_SCATTER = "DINERO_GANADO_SCATTER";
	public static final String DINERO_GANADO_BONUS = "DINERO_GANADO_BONUS";
	public static final String DINERO_GANADO_JACKPOT = "DINERO_GANADO_JACKPOT";
	public static final String CANTIDAD_APUESTAS = "CANTIDAD_APUESTAS";
	public static final String CANTIDAD_APUESTAS_GANADAS = "CANTIDAD_APUESTAS_GANADAS";
	public static final String CANTIDAD_APUESTAS_GANADAS_BONUS = "CANTIDAD_APUESTAS_GANADAS_BONUS";
	
	public EstadisticaDAO() {
		
	}
	
	public void inicializar() {
		TABLA_NOMBRE = "estadistica";
	}

	public void crearTabla(Connection connection) throws SQLException {

		Statement stmt = connection.createStatement();
		stmt.executeUpdate("CREATE TABLE " + TABLA_NOMBRE + " " +
						   "(DINERO_APOSTADO FLOAT NOT NULL, " +
						    "DINERO_GANADO FLOAT NOT NULL, " +
						    "DINERO_GANADO_NORMAL FLOAT NOT NULL, " +
						    "DINERO_GANADO_SCATTER FLOAT NOT NULL, " +
						    "DINERO_GANADO_BONUS FLOAT NOT NULL, " +
						    "DINERO_GANADO_JACKPOT FLOAT NOT NULL, " +
						    "CANTIDAD_APUESTAS FLOAT NOT NULL, " +
						    "CANTIDAD_APUESTAS_GANADAS FLOAT NOT NULL, " +
						    "CANTIDAD_APUESTAS_GANADAS_BONUS FLOAT NOT NULL)");
		
		stmt.close();
	}

	/**
	 * Dado un codigo de configuracion, actualiza su
	 * valor por uno nuevo recibido tambien por 
	 * parametro
	 * @param codigo
	 * @throws SQLException 
	 */
	public void actualizarValor(String nombre, float nuevoValor) throws SQLException {
	
		String sql = "UPDATE " + TABLA_NOMBRE + " " + 
		 			 "SET " +
		 			 nombre + " = " + nuevoValor;
		
		Statement stmt = obtenerConexion().createStatement();
		stmt.executeUpdate(sql);
		stmt.close();
	}
	
	public void actualizarValoresEstadisticos(float dineroApostado, float dineroGanado, float dineroGanadoNormal, float dineroGanadoScatter, float dineroGanadoBonus, float dineroGanadoJackpot, long cantidadApuestas, long cantidadApuestasGanadas, long cantidadApuestasBonus) throws SQLException {
		
		String sql = "UPDATE " + TABLA_NOMBRE + " " + 
		 "SET " +
		 "DINERO_APOSTADO = " + dineroApostado + ", " +
		 "DINERO_GANADO = " + dineroGanado + ", " +
		 "DINERO_GANADO_NORMAL = " + dineroGanadoNormal + ", " +
		 "DINERO_GANADO_SCATTER = " + dineroGanadoScatter + ", " +
		 "DINERO_GANADO_BONUS = " + dineroGanadoBonus + ", " +
		 "DINERO_GANADO_JACKPOT = " + dineroGanadoJackpot + ", " +
		 "CANTIDAD_APUESTAS = " + cantidadApuestas + ", " +
		 "CANTIDAD_APUESTAS_GANADAS = " + cantidadApuestasGanadas + ", " +
		 "CANTIDAD_APUESTAS_GANADAS_BONUS = " + cantidadApuestasBonus;

		Statement stmt = obtenerConexion().createStatement();
		stmt.executeUpdate(sql);
		stmt.close();
		
	}
	
	
	public float obtenerValor(String nombre) throws SQLException {
		
		float valor = 0f;
		
		String sql = "SELECT " + nombre + " FROM " + TABLA_NOMBRE;

		Statement stmt = obtenerConexion().createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		//nos movemos al primer 
		//y unico registro
		rs.next();
		valor = rs.getFloat(nombre);
		
		rs.close();
		stmt.close();
		
		return valor;
	}
	
	public Map<String, Float> obtenerValores() throws SQLException {
		
		Map<String, Float> valores = new HashMap<String, Float>();
		
		String sql = "SELECT * FROM " + TABLA_NOMBRE;

		Statement stmt = obtenerConexion().createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		//nos movemos al primer 
		//y unico registro
		rs.next();

		//cargamos valores
		valores.put(DINERO_APOSTADO, rs.getFloat("DINERO_APOSTADO"));
		valores.put(DINERO_GANADO, rs.getFloat("DINERO_GANADO"));
		valores.put(DINERO_GANADO_NORMAL, rs.getFloat("DINERO_GANADO_NORMAL"));
		valores.put(DINERO_GANADO_SCATTER, rs.getFloat("DINERO_GANADO_SCATTER"));
		valores.put(DINERO_GANADO_BONUS, rs.getFloat("DINERO_GANADO_BONUS"));
		valores.put(DINERO_GANADO_JACKPOT, rs.getFloat("DINERO_GANADO_JACKPOT"));
		valores.put(CANTIDAD_APUESTAS, rs.getFloat("CANTIDAD_APUESTAS"));
		valores.put(CANTIDAD_APUESTAS_GANADAS, rs.getFloat("CANTIDAD_APUESTAS_GANADAS"));
		valores.put(CANTIDAD_APUESTAS_GANADAS_BONUS, rs.getFloat("CANTIDAD_APUESTAS_GANADAS_BONUS"));
		
		rs.close();
		stmt.close();
		
		return valores;
	}
	
	public void crearData(Connection connection) throws SQLException {

		String sql = "INSERT INTO " + TABLA_NOMBRE + 
					 "(DINERO_APOSTADO, DINERO_GANADO, DINERO_GANADO_NORMAL, DINERO_GANADO_SCATTER, DINERO_GANADO_BONUS, DINERO_GANADO_JACKPOT, CANTIDAD_APUESTAS, CANTIDAD_APUESTAS_GANADAS, CANTIDAD_APUESTAS_GANADAS_BONUS) " +
					 "VALUES (0,0,0,0,0,0,0,0,0)";
		
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(sql);
		
		stmt.close();
	}
}