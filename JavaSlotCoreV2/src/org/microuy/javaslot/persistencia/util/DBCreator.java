package org.microuy.javaslot.persistencia.util;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.expepcion.SystemException;
import org.microuy.javaslot.persistencia.dao.ConfiguracionDAO;
import org.microuy.javaslot.persistencia.dao.DistribuidorPremiosDAO;
import org.microuy.javaslot.persistencia.dao.EstadisticaDAO;
import org.microuy.javaslot.persistencia.dao.GenericDAO;
import org.microuy.javaslot.persistencia.dao.PagoExcedidoDAO;
import org.microuy.javaslot.persistencia.dao.ResumenCobranzaDAO;

public class DBCreator {

    /**
     * Lista con todas las clases de 
     * tipo DAO (Data Access Object)
     */
    private static List<GenericDAO> daos = new ArrayList<GenericDAO>();
    
    /**
     * Verifica si la base de datos del sistema
     * existe actualmente en el sistema de archivos
     * y la crea si es necesario. Tambien realiza
     * los chequeos de data mas granular, como tablas
     * y datos dentro de estas.
     */
	public static void verificarYCrearDB() {
		
		try {

			//cargamos el driver
	        Class.forName(Sistema.dbDriver); 

	        //creamos una conexion a la base, y en caso
	        //de que la misma todavia no exista en el 
	        //sistema de archivos, la creamos
	        Connection connection = DriverManager.getConnection(Sistema.dbProtocol + Sistema.dbFolder + Sistema.dbName + ";create=true");

	        //inicializamos los DAO's
	        daos.add(new ConfiguracionDAO());
	        daos.add(new ResumenCobranzaDAO());
	        daos.add(new PagoExcedidoDAO());
	        daos.add(new DistribuidorPremiosDAO());
	        daos.add(new EstadisticaDAO());
	        
			//verificamos si es necesario crear la
			//estructura de la base de datos (tablas)
	        verificarYCrearTablas(connection);
			
			//finaliza la instancia de Derby
			//finalizar();

		} 
		catch (SQLException e) {
			e.printStackTrace();
			throw new SystemException(e.getMessage());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new SystemException(e.getMessage());
		}
	}
	
	/**
	 * Chequea la lista de tablas creadas actualmente
	 * en la base de datos, y en caso de que no esten
	 * creadas, ejecuta los sql necesarios para crearlas
	 * @throws SQLException 
	 */
	private static void verificarYCrearTablas(Connection connection) throws SQLException {
		
		//obtenemos la lista de tablas que
		//actualmente existen en la base
        List<String> tablas = obtenerTablas(connection);

        //si no existen tablas, las creamos
        if(tablas.size() == 0) {
        	
        	//recorremos la lista de DAO's
        	//para crear las tablas y los
        	//datos iniciales
        	for(GenericDAO genericDao : daos) {
        		genericDao.crearTabla(connection);
        		genericDao.crearData(connection);
        	}
        }
	}
	
	/**
	 * Finaliza la instancia del sistema de 
	 * base de datos Derby.
	 */
	public static void finalizar() {
		
        try
        {
            // the shutdown=true attribute shuts down Derby
            DriverManager.getConnection("jdbc:derby:;shutdown=true");

            // To shut down a specific database only, but keep the
            // engine running (for example for connecting to other
            // databases), specify a database in the connection URL:
            //DriverManager.getConnection("jdbc:derby:" + dbName + ";shutdown=true");
        }
        catch (SQLException se)
        {
            if (( (se.getErrorCode() == 50000) && ("XJ015".equals(se.getSQLState()) ))) {
                // we got the expected exception
                System.out.println("DB shut down normally");
                // Note that for single database shutdown, the expected
                // SQL state is "08006", and the error code is 45000
            } 
            else {
                // if the error code or SQLState is different, we have
                // an unexpected exception (shutdown failed)
                System.err.println("DB did not shut down normally");
            }
        }
	}
	
	private static List<String> obtenerTablas(Connection targetDBConn) throws SQLException {
		
		List<String> lista = new ArrayList<String>();
		DatabaseMetaData dbmeta = targetDBConn.getMetaData();
		leerTabla(lista, dbmeta, "TABLE", null);
		
		return lista;
	}

	private static void leerTabla(List<String> lista, DatabaseMetaData dbmeta, String searchCriteria, String schema) throws SQLException {
		
		ResultSet rs = dbmeta.getTables(null, schema, null, new String[] { searchCriteria });
		while (rs.next()) {
			lista.add(rs.getString("TABLE_NAME").toLowerCase());
		}
	}
}