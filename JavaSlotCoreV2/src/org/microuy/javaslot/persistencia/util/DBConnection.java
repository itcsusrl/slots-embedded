package org.microuy.javaslot.persistencia.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.microuy.javaslot.constante.Sistema;

public class DBConnection {

	//singleton de conexion
	private static Connection connection;
	
	public static Connection abrirConexion() throws SQLException {
		
		if(connection == null) {

			//creamos la conexion a la base
			connection = DriverManager.getConnection(Sistema.dbProtocol + Sistema.dbFolder + Sistema.dbName);
		}
		
		return connection;
	}
}