package org.microuy.javaslot.comm;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import org.microuy.javaslot.constante.Sistema;

public class PanelBotonesJFrame extends JFrame implements ActionListener, Runnable {
	
	private JButton boton1;
	private JButton boton2;
	private JButton boton3;
	private JButton boton4;
	private JButton boton5;
	private JButton boton6;

	private Color botonEncendido = new Color(255,0,0);
	private Color botonApagado = new Color(255,99,71);
	private CommVirtualPort commVirtualPort;
	
	private int idJuegoLuces = -1;
	private int contadorTiempo;
	private long lastLoopTime;
	
	private JButton[] botones;
	private boolean[] estadoLuces;
	
	public PanelBotonesJFrame(CommVirtualPort commVirtualPort) {
		
		super("");
		this.commVirtualPort = commVirtualPort;
		setBounds(1100, 60, 200, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(6,1));
	
		boton1 = new JButton("");
		boton2 = new JButton("");
		boton3 = new JButton("");
		boton4 = new JButton("");
		boton5 = new JButton("");
		boton6 = new JButton("");

		botones = new JButton[]{boton1,boton2,boton3,boton4,boton5,boton6};
		estadoLuces = new boolean[]{false,false,false,false,false,false};
		
		boton1.setBackground(botonApagado);
		boton1.setFocusPainted(false);
		
		boton2.setBackground(botonApagado);
		boton2.setFocusPainted(false);

		boton1.addActionListener(this);
		boton2.addActionListener(this);
		boton3.addActionListener(this);
		boton4.addActionListener(this);
		boton5.addActionListener(this);
		boton6.addActionListener(this);
		
		add(boton1);
		add(boton2);
		add(boton3);
		add(boton4);
		add(boton5);
		add(boton6);

		Thread t = new Thread(this);
		t.start();

		if(Sistema.MOSTRAR_PANEL_BOTONES_VIRTUALES) {
			setVisible(true);	
		}
	}
	
	public void actionPerformed(ActionEvent e) {
	
		if(e.getSource() == boton1) {
			commVirtualPort.presionarBoton((byte)49);
		}
		else if(e.getSource() == boton2) {
			commVirtualPort.presionarBoton((byte)50);
		}
		else if(e.getSource() == boton3) {
			commVirtualPort.presionarBoton((byte)51);
		}
		else if(e.getSource() == boton4) {
			commVirtualPort.presionarBoton((byte)52);	
		}
		else if(e.getSource() == boton5) {
			commVirtualPort.presionarBoton((byte)53);
		}
		else if(e.getSource() == boton6) {
			commVirtualPort.presionarBoton((byte)54);	
		}
	}
	
	public void comenzarJuegoLuces(int idJuegoLuces) {
		this.idJuegoLuces = idJuegoLuces;
		
		//si el id de juego de luces
		//es 1 prendemos todas al 
		//comienzo
		if(idJuegoLuces == 1) {
			for(int i=0; i<estadoLuces.length; i++) {
				estadoLuces[i] = true;
			}
		}
	}
	
	public void detenerJuegoLuces() {
		idJuegoLuces = -1;
	}
	
	public void configurarEstadoLuces(boolean[] estadoLuces) {
		this.estadoLuces = estadoLuces;
		idJuegoLuces = -1;
	}
	
	public void run() {
		
		while(true) {
			
			long inicioLoopTime = System.currentTimeMillis();
	
			if(idJuegoLuces != -1) {
				
				if(idJuegoLuces == 0) {
					
					//incremento el contador de tiempo
					contadorTiempo += lastLoopTime;
					
					if(contadorTiempo > 100) {

						int luzActual = -1;
						for(int i=0; i<estadoLuces.length; i++) {
							if(estadoLuces[i]) {
								luzActual = i;
								break;
							}
						}
						
						luzActual++;
						contadorTiempo = 0;
						if(luzActual > botones.length-1) {
							luzActual = 0;
						}
						
						for(int i=0; i<botones.length; i++) {
							if(i == luzActual) {
								estadoLuces[i] = true;
							}
							else {
								estadoLuces[i] = false;
							}
						}

					}
				}
				else if(idJuegoLuces == 1) {
					
					//incremento el contador de tiempo
					contadorTiempo += lastLoopTime;
					
					if(contadorTiempo > 1000) {
						for(int i=0; i<botones.length; i++) {
							estadoLuces[i] = !estadoLuces[i];
						}
						contadorTiempo = 0;
					}
				}
			}
			
			try {
				Thread.sleep(10);
				long finLoopTime = System.currentTimeMillis();
				lastLoopTime = finLoopTime - inicioLoopTime;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			for(int i=0; i<botones.length; i++) {
				if(estadoLuces[i] ) {
					botones[i].setBackground(botonEncendido);
				}
				else {
					botones[i].setBackground(botonApagado);
				}
			}
		}
	}
}