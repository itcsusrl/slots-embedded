package org.microuy.javaslot.comm.test;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.microuy.javaslot.comm.CommLayer;
import org.microuy.javaslot.comm.EjecutorComando;
import org.microuy.javaslot.constante.CodigoError;
import org.microuy.javaslot.constante.Comando;
import org.microuy.javaslot.constante.ModoExpulsionMoneda;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.expepcion.SystemException;

/**
 * Implementacion de protocolo de comunicacion
 * a traves de puerto serial.

 * @author Pablo Caviglia
 *
 */
public class TestSerialPort extends CommLayer implements SerialPortEventListener {

	private static String PORT_NAME = "COM11";
//	p1rivate static String PORT_NAME = "/dev/ttyS0";
	
	private final int BAUD_RATE = 9600;
	private final int DATA_BITS = SerialPort.DATABITS_8;
	private final int STOP_BITS = SerialPort.STOPBITS_1;
	private final int PARITY = SerialPort.PARITY_NONE;

	private SerialPort serialPort;
	private InputStream in;
	private OutputStream out;
	private CommPort commPort;
	private byte[] buffer = new byte[1024];

	public TestSerialPort() {
		super();
	}

	public void inicializar() {

		try {

			//chequeamos si el usuario
			//configuro algun puerto en
			//particular
			String puertoUsario = System.getProperty("slot_port_name");

			if(puertoUsario != null && !puertoUsario.trim().equalsIgnoreCase("")) {
				PORT_NAME = puertoUsario;
			}
			
			CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(PORT_NAME);
			if (portIdentifier.isCurrentlyOwned()) {
				System.out.println("Error: Port is currently in use");
			} 
			else {

				commPort = portIdentifier.open(this.getClass().getName(), 2000);

				if (commPort instanceof SerialPort) {
					
					serialPort = (SerialPort) commPort;
					serialPort.setSerialPortParams(BAUD_RATE, DATA_BITS,STOP_BITS, PARITY);
					serialPort.setOutputBufferSize(0);
					serialPort.addEventListener(this);
					serialPort.notifyOnDataAvailable(true);

					//streams
					in = serialPort.getInputStream();
					out = serialPort.getOutputStream();

				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException("error");
		}
	}
	
	long tiempoAnterior;
	long tiempoActual;

	public void serialEvent(SerialPortEvent arg0) {

		try {

			int data;
			int len = 0;
			
			while ((data = in.read()) > -1) {

				// si el valor 10 es recibido
				// interpretamos que el fin de
				// la data del comando fue recibida
				if (data == 10) {
					break;
				}
				buffer[len++] = (byte) data;
			}
			
			if(tiempoActual != 0) {
				tiempoAnterior = tiempoActual;
			}
			
			tiempoActual = System.currentTimeMillis();
			
			System.out.println("Tiempo desde comando anterior: " + (tiempoActual - tiempoAnterior));

			byte[] receivedData = new byte[len];
			System.arraycopy(buffer, 0, receivedData, 0, len);
			
			// texto recibido
			String recibido = new String(receivedData);

			// chequeo que no sea vacio lo recibido
			if (!recibido.equals("") && !recibido.startsWith("DEBUG")) {
				
				if(Sistema.DEBUG_PUERTO_SERIAL) {
					System.out.println("RECIBIDO: " + recibido);
				}

				//procesarComandoEntrada(recibido);
			} 
			else if (recibido.startsWith("DEBUG")) {
				System.out.println(recibido);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public synchronized void enviar(byte[] data, long sleepTime) {
		try {
			
			if(Sistema.DEBUG_PUERTO_SERIAL) {

				System.out.print("ENVIADO: ");
				for(byte b: data) {
					System.out.print(b+",");	
				}
				System.out.println();
			}
			
			for (int i = 0; i < data.length; i++) {
				out.write(data[i]);
				out.flush();
				try {
					if (sleepTime > 0) {
						/**
						 * ALGUNOS COMANDOS IMPORTANTES RELACIONADOS AL PAGO O
						 * VALIDACION USAN UN TIMING ENTRE EL ENVIO DE CADA BYTE
						 * DEL COMANDO PARA ASEGURARSE QUE LLEGA TO_DO BIEN AL
						 * PIC
						 */
						Thread.sleep(sleepTime);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new SystemException(CodigoError.ENVIANDO_INFORMACION_SERIAL);
		}

	}

	public synchronized void enviar(byte[] data) {

		enviar(data, 0);
	}

	public synchronized void procesarComandoEntrada(String data) {
		EjecutorComando.interpretarYEjecutar(data);
	}
	
	public void finalizar() {
		
		try {
			in.close();
			out.close();
			commPort.close();
		} catch (IOException e) {
			throw new SystemException(e.getMessage());
		}
	}
	
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		
//		try {
//			
//			TestSerialPort c = new TestSerialPort();
//			
//			//COMANDO_VALIDAR_LICENCIA_PIC
//			c.enviar(new byte[]{Comando.COMANDO_VALIDAR_LICENCIA_PIC,'E','4','3','5','K','U','E','Y','7','8','1','2'});
//			Thread.sleep(500);
//			
//			//COMANDO_CONFIGURAR_JUEGO_LUCES
//			//c.enviar(new byte[]{Comando.COMANDO_CONFIGURAR_JUEGO_LUCES,1});
//			//Thread.sleep(500);
//			
//			//COMANDO_EFECTUAR_PAGO
//			//c.enviar(new byte[]{Comando.COMANDO_EFECTUAR_PAGO,3,8});
//			//Thread.sleep(500);
//			
//			//c.enviar(new byte[]{Comando.COMANDO_CAMBIAR_MODO_EXPULSION_MONEDA, ModoExpulsionMoneda.MODO_EXPULSION_MONEDA_ADMINISTRADOR});
//			//Thread.sleep(500);
//			
//			//c.enviar(new byte[]{Comando.COMANDO_OBTENER_ESTADO_MOTOR});
//			//Thread.sleep(500);
//			
//			//c.enviar(new byte[]{Comando.COMANDO_CONFIGURAR_ESTADO_BOTONES,1,1,1,1,0,1});
//			
//			
//			
//			
//			Thread.sleep(5000);
//			//cerramos la conexion
//			c.finalizar();
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	public void inicializarKeepAlive() {
		
	}
}