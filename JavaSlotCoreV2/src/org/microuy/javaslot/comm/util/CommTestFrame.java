package org.microuy.javaslot.comm.util;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.microuy.javaslot.comm.CommLayer;
import org.microuy.javaslot.comm.EjecutorComando;
import org.microuy.javaslot.comm.listener.ComandoEvent;
import org.microuy.javaslot.comm.listener.ComandoListener;
import org.microuy.javaslot.constante.Comando;


public class CommTestFrame extends JFrame implements ActionListener, ComandoListener {
	
	private JLabel entregarMonedasJLabel;
	private JLabel monedasEntregadasJLabel;
	private JTextField monedasEntregadasJTextField;
	private JTextField entregarMonedasJTextField;
	private JButton entregarMonedasJButton;
	
	public CommTestFrame() {

		super("Slots Debug UI");
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	    // Get the size of the screen
	    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	    
	    // Determine the new location of the window
	    int w = 400;
	    int h = 150;
	    int x = (dim.width-w)/2;
	    int y = (dim.height-h)/2;
	    
	    setBounds(x, y, w, h);
	    setLayout(null);

	    entregarMonedasJLabel = new JLabel("Monedas a entregar");
	    entregarMonedasJTextField = new JTextField("10");
	    entregarMonedasJButton = new JButton("Entregar monedas");
	    monedasEntregadasJLabel = new JLabel("Monedas entregadas:");
	    monedasEntregadasJTextField = new JTextField();
	    
	    entregarMonedasJLabel.setBounds(70,10,170,30);
	    entregarMonedasJTextField.setBounds(210,15,100,20);
	    entregarMonedasJButton.setBounds(120,75,150,30);
	    monedasEntregadasJLabel.setBounds(70,35,170,30);
	    monedasEntregadasJTextField.setBounds(210,40,100,20);
	    monedasEntregadasJTextField.setEditable(false);
	    
	    //agregamos los listeners
	    entregarMonedasJButton.addActionListener(this);
	    EjecutorComando.agregarComandoListener(this);
	    
	    add(entregarMonedasJLabel);
	    add(entregarMonedasJTextField);
	    add(monedasEntregadasJLabel);
	    add(monedasEntregadasJTextField);
	    add(entregarMonedasJButton);
		
	    
	    setVisible(true);
	}


	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource()==entregarMonedasJButton) {
			
			String monedasStr=entregarMonedasJTextField.getText();
            EjecutorComando.efectuarPago(Integer.parseInt(monedasStr));
            entregarMonedasJButton.setEnabled(false);
            monedasEntregadasJTextField.setText("");
            entregarMonedasJTextField.setEditable(false);
        }
	}
	
	public void setMonedasEntregadas(String monedasEntregadasStr) {
		
		int monedasTotal = Integer.parseInt(entregarMonedasJTextField.getText());
		int tmp = monedasTotal - Integer.parseInt(monedasEntregadasStr);
		monedasEntregadasJTextField.setText("" + tmp);
		
		int monedasEntregadas = Integer.parseInt(monedasEntregadasStr);
		
		if(monedasEntregadas == 0) {
			entregarMonedasJButton.setEnabled(true);
			entregarMonedasJTextField.setEditable(true);
		}
	}
	

	public void procesarEventoComando(ComandoEvent comandoEvent) {
		
		if(comandoEvent.getComandoId() == Comando.COMANDO_VERIFICAR_PAGO_MONEDA) {
			
			setMonedasEntregadas(comandoEvent.toString());
		}
	}
}