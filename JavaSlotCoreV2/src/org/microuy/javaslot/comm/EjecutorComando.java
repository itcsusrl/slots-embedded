package org.microuy.javaslot.comm;

import java.util.ArrayList;
import java.util.List;

import org.microuy.javaslot.comm.listener.BotonListener;
import org.microuy.javaslot.comm.listener.ComandoEvent;
import org.microuy.javaslot.comm.listener.ComandoListener;
import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.constante.Comando;
import org.microuy.javaslot.expepcion.SystemException;

public class EjecutorComando {

    public static List<ComandoListener> comandoListeners = new ArrayList<ComandoListener>();
    public static List<BotonListener> botonListeners = new ArrayList<BotonListener>();
    private static CommLayer commLayer = CommLayer.getDefaultInstance();

    public static void agregarComandoListener(ComandoListener comandoListener) {
    	
    	comandoListeners.add(comandoListener);
    }

    public static void agregarBotonListener(BotonListener botonListener) {
    	
    	botonListeners.add(botonListener);
    }

    public static void ejecutarComandoEvento(ComandoEvent comandoEvent) {
    	
    	if(comandoEvent.getComandoId() == Comando.COMANDO_BOTON_PRESIONADO) {
    		for(int i=0; i<botonListeners.size(); i++) {
        		
    			BotonListener botonListener = (BotonListener)botonListeners.get(i);
        		botonListener.procesarBotonPresionado((Byte)comandoEvent.getGenerico());
        	}
    	}
    	else { //el resto de los comandos
    		
    		for(int i=0; i<comandoListeners.size(); i++) {
        		
        		ComandoListener comandoListener = (ComandoListener)comandoListeners.get(i);
        		comandoListener.procesarEventoComando(comandoEvent);
        	}
    	}
    }
    
    public synchronized static void realizarHandShake(char[] codigoPic) {
    	
    	byte[] data = new byte[1+codigoPic.length];
    	data[0] = Comando.COMANDO_VALIDAR_LICENCIA_PIC;
    	for(int i=0; i<codigoPic.length; i++) {
    		data[i+1] = (byte)codigoPic[i];
    	}
    	
    	//enviamos la data con un delay 
    	//de 50ms entre byte y byte
  		commLayer.enviar(data,50);
  		
    }
    
	public synchronized static void efectuarPago(int monedas) throws SystemException {

		int repeticiones = (int)(monedas / 128);
		int resto = (int)(monedas % 128);

		commLayer.enviar(new byte[]{Comando.COMANDO_EFECTUAR_PAGO, ((byte)repeticiones), ((byte)resto)}, 100);
	}
	
	public synchronized static void obtenerEstadoMotor() throws SystemException {
		commLayer.enviarComando(new byte[]{Comando.COMANDO_OBTENER_ESTADO_MOTOR});
	}
	
	public synchronized static void cambiarEstadoMotor(boolean prendido) throws SystemException {
		commLayer.enviarComando(new byte[]{Comando.COMANDO_CAMBIAR_ESTADO_MOTOR, ((byte)(prendido ? 1 : 0))});
	}
	
	public synchronized static void cambiarModoExpulsionMoneda(byte modo) throws SystemException {
		commLayer.enviarComando(new byte[]{Comando.COMANDO_CAMBIAR_MODO_EXPULSION_MONEDA, modo});
	}
	
	public synchronized static void configurarJuegoLuces(byte juegoLucesId) {
		commLayer.enviarComando(new byte[]{Comando.COMANDO_CONFIGURAR_JUEGO_LUCES, juegoLucesId});
	}
		
	public synchronized static void configurarEstadoBotones(byte[] estadoBotones) {
		
		byte[] data = new byte[1+Boton.CANTIDAD_BOTONES];
		data[0] = Comando.COMANDO_CONFIGURAR_ESTADO_BOTONES;
		for(int i=0;i<estadoBotones.length;i++) {
			data[i+1] = estadoBotones[i];
		}
		commLayer.enviarComando(data);
	}
	
	public synchronized static byte obtenerBotonPresionado(byte botonId) {
		
		byte botonPresionado = -1;
		
		switch((char)botonId) {
		case '1':
			botonPresionado = Boton.BOTON1;
			break;
		case '2':
			botonPresionado = Boton.BOTON2;
			break;
		case '3':
			botonPresionado = Boton.BOTON3;
			break;
		case '4':
			botonPresionado = Boton.BOTON4;
			break;
		case '5':
			botonPresionado = Boton.BOTON5;
			break;
		case '0':
			botonPresionado = Boton.BOTON6;
			break;
		}
		
		return botonPresionado;
	}
	
	public synchronized static void interpretarYEjecutar(String dataString) {
		
		//transformamos el string recibido
		//en un array de strings numericos
		String[] dataArrayStr = dataString.split(" ");
		
		//transformamos los string numericos
		//en valores numericos reales
		int[] data = new int[dataArrayStr.length];
		for(int i=0; i<dataArrayStr.length; i++) {
			
			try {
				data[i] = Integer.parseInt(dataArrayStr[i]);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		if(data[0] == Comando.COMANDO_ESTADO_LICENCIAMIENTO_PIC) {
			
			ComandoEvent comandoEvent = new ComandoEvent();
			comandoEvent.setComandoId(Comando.COMANDO_ESTADO_LICENCIAMIENTO_PIC);
			comandoEvent.setGenerico(new Byte((byte)data[1]));
			ejecutarComandoEvento(comandoEvent);

		}
		else if(data[0] == Comando.COMANDO_VERIFICAR_PAGO_MONEDA) {
			
			ComandoEvent comandoEvent = new ComandoEvent();
			comandoEvent.setComandoId(Comando.COMANDO_VERIFICAR_PAGO_MONEDA);
			comandoEvent.setGenerico(new Byte((byte)data[1]));
			comandoEvent.setGenericoHelper(new Byte((byte)data[2]));
			ejecutarComandoEvento(comandoEvent);

		}
		else if(data[0] == Comando.COMANDO_BOTON_PRESIONADO) {

			//una vez dio un arrayindexoutofbounds... REVISAR TODO
			if(data.length == 2) {
				ComandoEvent comandoEvent = new ComandoEvent();
				comandoEvent.setComandoId(Comando.COMANDO_BOTON_PRESIONADO);
				comandoEvent.setGenerico(new Byte((byte)data[1]));
				ejecutarComandoEvento(comandoEvent);
			}
		}
		else if(data[0] == Comando.COMANDO_DINERO_INGRESADO) {
			
			ComandoEvent comandoEvent = new ComandoEvent();
			comandoEvent.setComandoId(Comando.COMANDO_DINERO_INGRESADO);
			comandoEvent.setGenerico(new Byte((byte)data[1]));
			ejecutarComandoEvento(comandoEvent);

		}
		else if(data[0] == Comando.COMANDO_CAMBIO_SWITCH_CONFIGURACION) {
			
			ComandoEvent comandoEvent = new ComandoEvent();
			comandoEvent.setComandoId(Comando.COMANDO_CAMBIO_SWITCH_CONFIGURACION);
			comandoEvent.setGenerico(new Byte((byte)data[1]));
			ejecutarComandoEvento(comandoEvent);

		}
		else if(data[0] == Comando.COMANDO_CAMBIO_SWITCH_LLAVE_ADMINISTRADOR) {
			
			ComandoEvent comandoEvent = new ComandoEvent();
			comandoEvent.setComandoId(Comando.COMANDO_CAMBIO_SWITCH_LLAVE_ADMINISTRADOR);
			comandoEvent.setGenerico(new Byte((byte)data[1]));
			ejecutarComandoEvento(comandoEvent);

		}
		else if(data[0] == Comando.COMANDO_CAPACIDAD_PAGO_EXCEDIDA) {
			
			ComandoEvent comandoEvent = new ComandoEvent();
			comandoEvent.setComandoId(Comando.COMANDO_CAPACIDAD_PAGO_EXCEDIDA);
			
			//calculamos cuantas monedas
			//restantes quedar por pagar
			byte repeticiones = new Byte((byte)data[1]);
			byte resto = new Byte((byte)data[2]);
			int totalMonedas = (repeticiones * 128) + resto;
			
			comandoEvent.setGenerico(totalMonedas);
			ejecutarComandoEvento(comandoEvent);

		}
		else if(data[0] == Comando.COMANDO_CODIGO_ESTADO_MOTOR) {
			
			ComandoEvent comandoEvent = new ComandoEvent();
			comandoEvent.setComandoId(Comando.COMANDO_CODIGO_ESTADO_MOTOR);
			
			//obtenemos el estado
			//del motor
			byte estadoMotor = new Byte((byte)data[1]);
			comandoEvent.setGenerico(estadoMotor);

			ejecutarComandoEvento(comandoEvent);
			
		}
	}
}