package org.microuy.javaslot.comm.listener;

public interface ComandoListener {

	public void procesarEventoComando(ComandoEvent comandoEvent);
	
}
