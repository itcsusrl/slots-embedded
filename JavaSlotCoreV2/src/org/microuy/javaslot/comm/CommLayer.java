package org.microuy.javaslot.comm;

import org.microuy.javaslot.constante.Comando;
import org.microuy.javaslot.constante.Sistema;
import org.microuy.javaslot.license.LicenseValidator;

public abstract class CommLayer {
	
	private static CommLayer commLayer;
	
	public CommLayer() {
		inicializar();
	}
	
	public static CommLayer getDefaultInstance() {
		
		if(commLayer == null) {
			if(Sistema.PUERTO_COMUNICACION_VIRTUAL) {
				commLayer = new CommVirtualPort();
			}
			else {
				commLayer = new CommSerialPort();
			}
		}
		
		return commLayer;
	}

	public void enviarComando(byte[] data) {
		
		if(LicenseValidator.retornoValidacionLicenciaPic || data[0] == Comando.COMANDO_VALIDAR_LICENCIA_PIC) {
			
			//enviamos el comando por
			//el protocolo de comunicacion
			enviar(data);
		}
	}

	public abstract void inicializar();
	public abstract void procesarComandoEntrada(String data);
	public abstract void enviar(byte[] data);
	public abstract void finalizar();
	public abstract void enviar(byte[] data, long sleepTime);
}
