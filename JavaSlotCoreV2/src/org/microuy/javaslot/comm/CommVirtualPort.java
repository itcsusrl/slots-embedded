package org.microuy.javaslot.comm;

import org.microuy.javaslot.constante.Boton;
import org.microuy.javaslot.constante.Comando;
import org.microuy.javaslot.constante.ModoExpulsionMoneda;

public class CommVirtualPort extends CommLayer implements Runnable {

	private Thread t;
	public byte[] dataEnviada;
	public byte[] dataRecibida;

	// data protocolo
	private int verificacionMonedasTotal;
	private boolean devolverValidacionPIC = false;
	private boolean devolverEstadoMotorPIC = false;
	private boolean entreVerificacionPagoMoneda = false;
	private boolean entreBotonPresionado = false;
	private boolean motorPrendido = false;
	private int monedasDentroHopper;
	public byte botonPresionado = -1;
	public boolean monedaIntroduciendose;
	public boolean faltaMonedaPago;
	
	//estado inicial del switch 
	//de configuracion (puerta)
	public boolean estadoSwitchConfiguracion;
	//flag que indica si el estado
	//del switch ha cambiado
	public boolean estadoSwitchConfiguracionModificado;
	
	//estado inicial del switch 
	//de configuracion (llave)
	public boolean estadoSwitchLlave;
	//flag que indica si el estado
	//del switch de la llave ha cambiado
	public boolean estadoSwitchLlaveModificado;

	
	private PanelBotonesJFrame panelBotonesJFrame;
	
	public CommVirtualPort() {
		super();
	}

	public void inicializar() {
		
		//creo el frame que muestra 
		//el panel con botones
		panelBotonesJFrame = new PanelBotonesJFrame(this);
		
		t = new Thread(this);
		t.start();
	}

	public synchronized void procesarComandoEntrada(String data) {
		EjecutorComando.interpretarYEjecutar(data);
	}

	public synchronized void enviar(byte[] data) {
		enviar(data, 0);
	}
	
	public synchronized void enviar(byte[] data, long sleepTime) {
		this.dataEnviada = data;
		procesarEnviosVirtual();
	}
	
	public void run() {

		while (true) {

			// interpreta la data recibida
			procesarRecepcionVirtual();

			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void procesarEnviosVirtual() {

		// obtengo el id de comando
		int comandoId = dataEnviada[0];

		if (comandoId == Comando.COMANDO_EFECTUAR_PAGO) {
			int repeticiones = (int) dataEnviada[1];
			int resto = (int) dataEnviada[2];
			verificacionMonedasTotal = (repeticiones * 128) + resto;
		}
		else if(comandoId == Comando.COMANDO_CONFIGURAR_ESTADO_BOTONES) {
			
			boolean[] estadoLuces = new boolean[Boton.CANTIDAD_BOTONES];
			
			//recorremos cada uno de los 
			//byte que identifica da boton
			//y nos saltamos el primero que
			//es el id de comando
			for(int i=1; i<dataEnviada.length; i++) {
				
				if(dataEnviada[i] != 0) {
					estadoLuces[i-1] = true;
				}
				else {
					estadoLuces[i-1] = false;
				}
			}
			
			panelBotonesJFrame.configurarEstadoLuces(estadoLuces);
		}
		else if(comandoId == Comando.COMANDO_CONFIGURAR_JUEGO_LUCES) {
			
			if(dataEnviada[1] != -1) {
				panelBotonesJFrame.comenzarJuegoLuces(dataEnviada[1]);	
			}
			else {
				panelBotonesJFrame.detenerJuegoLuces();
			}
		}
		else if(comandoId == Comando.COMANDO_VALIDAR_LICENCIA_PIC) {
			devolverValidacionPIC = true;
		}
		else if(comandoId == Comando.COMANDO_OBTENER_ESTADO_MOTOR) {
			devolverEstadoMotorPIC = true;
		}
		else if(comandoId == Comando.COMANDO_CAMBIAR_ESTADO_MOTOR) {

			//obtenemos el valor configurado
			motorPrendido = dataEnviada[1] == 1;
			
			//este valor lo configuramos para que
			//cuando se prenda el motor suceda algo
			monedasDentroHopper = 10;
		}
	}
	
	public void finalizar() {
		
	}

	public synchronized void procesarRecepcionVirtual() {

		//COMANDO_VERIFICAR_PAGO_MONEDA
		if (verificacionMonedasTotal > 0 && !entreVerificacionPagoMoneda) {
			dataRecibida = new byte[] { Comando.COMANDO_VERIFICAR_PAGO_MONEDA };
			entreVerificacionPagoMoneda = true;
			return;
		}
		
		//COMANDO_BOTON_PRESIONADO
		if(botonPresionado != -1 && !entreBotonPresionado) {
			dataRecibida = new byte[] { Comando.COMANDO_BOTON_PRESIONADO };
			entreBotonPresionado = true;
			return;
		}
		
		//COMANDO_DINERO_INGRESADO
		if(monedaIntroduciendose) {
			monedaIntroduciendose = false;
			procesarComandoEntrada(String.valueOf(Comando.COMANDO_DINERO_INGRESADO));
		}
		
		//COMANDO_CAPACIDAD_PAGO_EXCEDIDA
		if(faltaMonedaPago) {
			faltaMonedaPago = false;
			procesarComandoEntrada(Comando.COMANDO_CAPACIDAD_PAGO_EXCEDIDA + " 2 3");
		}

		/**
		 * COMANDO_VERIFICAR_PAGO_MONEDA
		 */
		if (entreVerificacionPagoMoneda) {

			verificacionMonedasTotal--;
			try {
				Thread.sleep(350);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			procesarComandoEntrada(String.valueOf(Comando.COMANDO_VERIFICAR_PAGO_MONEDA + " " + ModoExpulsionMoneda.MODO_EXPULSION_MONEDA_PAGO_JUGADOR + " 1"));
			
			if (verificacionMonedasTotal <= 0) {
				dataRecibida = null;
				entreVerificacionPagoMoneda = false;
			}
		}
		
		/**
		 * COMANDO_CAMBIAR_ESTADO_MOTOR
		 * 
		 * Simulacion del motor prendido
		 * y expulsando monedas
		 */
		if(monedasDentroHopper > 0) {
			
			if(motorPrendido) {
				
				monedasDentroHopper--;
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				procesarComandoEntrada(String.valueOf(Comando.COMANDO_VERIFICAR_PAGO_MONEDA + " " + ModoExpulsionMoneda.MODO_EXPULSION_MONEDA_ADMINISTRADOR + " 1"));
			}
		}
		
		/**
		 * COMANDO_BOTON_PRESIONADO
		 */
		if(entreBotonPresionado) {
			
			entreBotonPresionado = false;

			if(botonPresionado == 49) { //Boton0
				dataRecibida = new byte[]{Comando.COMANDO_BOTON_PRESIONADO,Boton.BOTON1};	
			}
			else if(botonPresionado == 50) { //Boton1
				dataRecibida = new byte[]{Comando.COMANDO_BOTON_PRESIONADO,Boton.BOTON2};	
			}
			else if(botonPresionado == 51) { //Boton2
				dataRecibida = new byte[]{Comando.COMANDO_BOTON_PRESIONADO,Boton.BOTON3};	
			}
			else if(botonPresionado == 52) { //Boton3
				dataRecibida = new byte[]{Comando.COMANDO_BOTON_PRESIONADO,Boton.BOTON4};	
			}
			else if(botonPresionado == 53) { //Boton4
				dataRecibida = new byte[]{Comando.COMANDO_BOTON_PRESIONADO,Boton.BOTON5};	
			}
			else if(botonPresionado == 54) { //Boton5
				dataRecibida = new byte[]{Comando.COMANDO_BOTON_PRESIONADO,Boton.BOTON6};	
			}
			
			String tmp = "";
			for(int i=0; i<dataRecibida.length; i++) {
				tmp += dataRecibida[i] + " ";
			}
			tmp = tmp.trim();
			
			procesarComandoEntrada(tmp);
			botonPresionado = -1;
		}
		
		/**
		 * VALIDACION LICENCIA PIC
		 */
		if(devolverValidacionPIC) {
			devolverValidacionPIC = false;
			dataRecibida = new byte[]{Comando.COMANDO_ESTADO_LICENCIAMIENTO_PIC, 1};
			
			String tmp = "";
			for(int i=0; i<dataRecibida.length; i++) {
				tmp += dataRecibida[i] + " ";
			}
			tmp = tmp.trim();
			
			procesarComandoEntrada(tmp);
		}
		
		/**
		 * Devolvemos el estado del motor
		 */
		if(devolverEstadoMotorPIC) {
			
			devolverEstadoMotorPIC = false;
			procesarComandoEntrada(Comando.COMANDO_CODIGO_ESTADO_MOTOR + " " + ((byte)(motorPrendido ? 1 : 0)));
		}
		
		//cambio de estado en el switch 
		//de configuracion, enviamos
		//comando
		if(estadoSwitchConfiguracionModificado) {
			estadoSwitchConfiguracionModificado = false;
			dataRecibida = new byte[]{Comando.COMANDO_CAMBIO_SWITCH_CONFIGURACION, ((byte)(estadoSwitchConfiguracion ? 1 : 0))};
			
			String tmp = "";
			for(int i=0; i<dataRecibida.length; i++) {
				tmp += dataRecibida[i] + " ";
			}
			tmp = tmp.trim();
			procesarComandoEntrada(tmp);
		}
		
		
		//cambio de estado en el switch 
		//de la llave, enviamos comando
		if(estadoSwitchLlaveModificado) {
			estadoSwitchLlaveModificado = false;
			dataRecibida = new byte[]{Comando.COMANDO_CAMBIO_SWITCH_LLAVE_ADMINISTRADOR, ((byte)(estadoSwitchLlave ? 1 : 0))};
			
			String tmp = "";
			for(int i=0; i<dataRecibida.length; i++) {
				tmp += dataRecibida[i] + " ";
			}
			tmp = tmp.trim();
			procesarComandoEntrada(tmp);
		}
	}
	
	public void presionarBoton(byte b) { 
		dataRecibida = null;
		botonPresionado = b;
	}
	
	public void introducirMoneda() {
		monedaIntroduciendose = true;
	}
	
	/**
	 * Envia un comando de falta 
	 *de monedas para el pago
	 */
	public void faltaMonedasPago() {
		faltaMonedaPago = true;
	}
	
	/**
	 * Envia un comando para 
	 * simular el cambio de estado
	 * del switch de configuracion
	 * (apertura de puerta)
	 */
	public void cambioEstadoSwitchConfiguracion() {
		
		estadoSwitchConfiguracion = !estadoSwitchConfiguracion;
		estadoSwitchConfiguracionModificado = true;
		
	}
	
	/**
	 * Envia un comando para 
	 * simular el cambio de estado
	 * del switch de la llave
	 * (capacidad de pago excedida)
	 */
	public void cambioEstadoSwitchLlave() {
		
		estadoSwitchLlave = !estadoSwitchLlave;
		estadoSwitchLlaveModificado = true;
	}
}