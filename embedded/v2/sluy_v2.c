#include <18F4550.h>
#include <stdlib.h>
#fuses HSPLL,NOWDT,NOBROWNOUT,NOPUT,NOLVP,NODEBUG,USBDIV,PLL5,CPUDIV1,VREGEN,CPD,PROTECT,EBTR,EBTRB,CPB
#use delay(clock=48000000)
#include "usb_cdc_sluy.h"
#include "usb_desc_cdc_sluy.h"

/**
 * Esta instruccion escribe en la memoria 
 * EEPROM del PIC (que empieza en la direccion
 * de memoria (0xf00000) el valor que indica
 * que la licencia del programa es valida.
 * Este valor solo puede ser cambiado cuando
 * se recibe a traves del comando COMANDO_VALIDAR_LICENCIA_PIC
 * una licencia invalida
 */ 
#ROM int8 0xf00000 = {114,10,54,41,67,99,64,100}

#DEFINE COMANDO_NULO                                  -1
#DEFINE COMANDO_EFECTUAR_PAGO                         100
#DEFINE COMANDO_VERIFICAR_PAGO_MONEDA                 101
#DEFINE COMANDO_BOTON_PRESIONADO                      102
#DEFINE COMANDO_DINERO_INGRESADO                      103
#DEFINE COMANDO_CONFIGURAR_ESTADO_BOTONES             104
#DEFINE COMANDO_CONFIGURAR_JUEGO_LUCES                105
#DEFINE COMANDO_VALIDAR_LICENCIA_PIC                  106
#DEFINE COMANDO_ESTADO_LICENCIAMIENTO_PIC             107
#DEFINE COMANDO_CAPACIDAD_PAGO_EXCEDIDA               108
#DEFINE COMANDO_CAMBIO_SWITCH_CONFIGURACION           109
#DEFINE COMANDO_CAMBIO_SWITCH_LLAVE_ADMINISTRADOR     110
#DEFINE COMANDO_CAMBIAR_ESTADO_MOTOR                  111
#DEFINE COMANDO_CAMBIAR_MODO_EXPULSION_MONEDA         112
#DEFINE COMANDO_OBTENER_ESTADO_MOTOR                  113
#DEFINE COMANDO_CODIGO_ESTADO_MOTOR                   114

#byte RCSTA   = 0xFAB
#byte SPPCON  = 0xF65
#byte SSPCON1 = 0xFC6
#byte CCP1CON = 0xFBD

#byte PORTA = 0xF80
#byte PORTB = 0xF81
#byte PORTC = 0xF82
#byte PORTD = 0xF83
#byte PORTE = 0xF84

#DEFINE SECUENCIA_LUCES_1     0
#DEFINE SECUENCIA_LUCES_2     1
#DEFINE SECUENCIA_LUCES_3     2
#DEFINE SECUENCIA_LUCES_4     3

#DEFINE MODO_EXPULSION_MONEDA_PAGO_JUGADOR            0
#DEFINE MODO_EXPULSION_MONEDA_ADMINISTRADOR           1

int16 DEBOUNCE_BOTONES_MS = 200;
int16 contadorDebounceBotones;
boolean flagDebounceBotones;

int16 DEBOUNCE_LLAVE_ADMINISTRADOR_MS = 100;
int16 contadorDebounceLlaveAdministrador;
boolean flagDebounceLlaveAdministrador;

int16 DEBOUNCE_COIN_SELECTOR_MS = 500;
int16 contadorDebounceCoinSelector;
boolean flagDebounceCoinSelector;

int16 DEBOUNCE_HOPPER_MS = 185;
int16 contadorDebounceHopper;
boolean flagDebounceHopper;


//flag que indica que se
//ha finalizado la recepcion
//de informacion correspondiente
//a la validacion
boolean validacionFinalizada = false;

//luego de recibidos todos los bytes
//necesarios para realizar la validacion
//este flag que setea con el valor 
//correspondiente
boolean validacionCorrecta = false;

//Token individual de cada PIC
char TOKEN[] = {"31E1A1BO3233"};

//usado para que no se repitan
//varias pasadas de una moneda
//en nuestra logica debido a la
//velocidad de ejecucion del 
//programa
boolean flagHopper = true;

//direccion actual del primer secuenciador
boolean haciaDerechaSecuenciador = false;

//estado de las luces usado 
//por el segundo secuenciador
boolean estadoLucesSegundoSecuenciador = false;

//habla por si solo
byte botonPresionado;
byte ultimoBotonPresionado;

//id del secuenciado 
//de luces actual
byte secuenciaActualId = 0;

//indica el modo en el cual la
//logica del sistema debe operar
//al momento de expulsarse una 
//moneda a traves del hopper
byte modoExpulsionMoneda = MODO_EXPULSION_MONEDA_PAGO_JUGADOR;

//indica cual es el boton
//prendido actualmente
byte secuenciaBotonActual = 0;

//contador de monedas a entregar
int16 monedasAEntregar = 0;

//indica si el motor debe
//de estar prendido o no
boolean motorPrendido = false;

//flags usados por el 
//coin selector
boolean flagCoinSelector = false;
boolean coinEventSentFlag = true;

//usado por el timer rtcc
//para contar hasta el 
//valor definido en la 
//constante TIMER_MAXIMO
int16 contador;
int16 contadorSecuenciador;
int16 contadorSecuenciadorSegundo;
boolean flagSecuenciador = false;
boolean flagSecuenciadorSegundo = false;

//tiempo que el motor se encuentra 
//prendido sin que pase una moneda
int16 noPasajeMoneda = 0;

//TODO Todavia no entiendo como se calculan
//tiempos en base al clock del procesador.
//La manera actual de calcularlo es cambiando
//el numero de la constante OVERFLOW_MS y enviar
//un mensaje por usb cada vez que sucede. Del 
//otro lado del pic se calcula el intervalo entre
//cada mensaje
int16 OVERFLOW_MS = 300; // 22 ms 


/**
 * El estado de cada una de 
 * las luces de los botones
 */
boolean estadoLuzBoton1 = false;
boolean estadoLuzBoton2 = false;
boolean estadoLuzBoton3 = false;
boolean estadoLuzBoton4 = false;
boolean estadoLuzBoton5 = false;
boolean estadoLuzBoton6 = false;

/**
 * Flag usado para chequear si
 * el estado de las luces de los
 * botones lo configura el 
 * secuenciador o las luces
 * independientes
 */
boolean manejaLuzSecuenciador = true;

//estado del switch de configuracion
boolean estadoSwitchConfiguracion = true;

//estado del switch de la llave de administrador
boolean estadoSwitchLlaveAdministrador = false;

/*
 * TODO CUIDADO CON ESTO!!!!!!
 *
 * Esto solo es con fines de desarrollo
 */
boolean validacionHabilitada = false;

#int_rtcc
void rtcc_timer() {  
   
   int repeticiones = 1000 / OVERFLOW_MS;
   if(contador < repeticiones) {
       contador++;
   }
   else {

      /**
       * AQUI SE ENTRA CADA 22ms
       */
      contador = 0;
      //char string[]  = {"SEGUNDO?\n"};
      //usb_cdc_puts(string);

      //si hay monedas a entregar
      if(monedasAEntregar > 0) {
         //incrementamos esta variable asi la
         //logica del motor realiza sus calculos
         //adecuadamente
         noPasajeMoneda = noPasajeMoneda + 22;
      }
 
      /** DEBOUNCE PARA BOTONES **/
      if(flagDebounceBotones) {
         if(contadorDebounceBotones > DEBOUNCE_BOTONES_MS) {
            flagDebounceBotones = false;
            contadorDebounceBotones = 0;
         }
         else {
            contadorDebounceBotones = contadorDebounceBotones + 22;
         }
      }

      /** DEBOUNCE PARA LLAVE ADMINISTRADOR **/
      if(flagDebounceLlaveAdministrador) {
         if(contadorDebounceLlaveAdministrador > DEBOUNCE_BOTONES_MS) {
            flagDebounceLlaveAdministrador = false;
            contadorDebounceLlaveAdministrador = 0;
         }
         else {
            contadorDebounceLlaveAdministrador = contadorDebounceLlaveAdministrador + 22;
         }
      }

      /** DEBOUNCE PARA COIN SELECTOR **/
      if(flagDebounceCoinSelector) {
         if(contadorDebounceCoinSelector > DEBOUNCE_COIN_SELECTOR_MS) {
            flagDebounceCoinSelector = false;
            contadorDebounceCoinSelector = 0;
         }
         else {
            contadorDebounceCoinSelector = contadorDebounceCoinSelector + 22;
         }
      }

      /** DEBOUNCE PARA HOPPER **/
      if(flagDebounceHopper) {
         if(contadorDebounceHopper > DEBOUNCE_HOPPER_MS) {
            flagDebounceHopper = false;
            contadorDebounceHopper = 0;
         }
         else {
            contadorDebounceHopper = contadorDebounceHopper + 22;
         }
      }

      /**
       * TIMER SECUENCIADOR --EMPIEZA--
       */
      //110ms / 22 = 5 repeticiones
      if(contadorSecuenciador < 5) {
         contadorSecuenciador++;
      }
      else {
         contadorSecuenciador = 0;
         flagSecuenciador = true;
      }
      /**
       * TIMER SECUENCIADOR --TERMINA--
       */
       
   }
}

/*
* Apaga todas las luces
* de los botones
*/
void logicaEstadoLuces(boolean b1, boolean b2, boolean b3, boolean b4, boolean b5, boolean b6) {
   
   //Luz 1
   b1 ? output_high(PIN_D2) : output_low(PIN_D2);
   //Luz 2
   b2 ? output_high(PIN_D3) : output_low(PIN_D3);
   //Luz 3
   b3 ? output_high(PIN_D4) : output_low(PIN_D4);
   //Luz 4
   b4 ? output_high(PIN_D5) : output_low(PIN_D5);
   //Luz 5
   b5 ? output_high(PIN_D6) : output_low(PIN_D6);
   //Luz 6
   b6 ? output_high(PIN_D7) : output_low(PIN_D7);
   
}

/**
* Procesa el estado de
* los botones y envia
* comando si hubo cambio
* en uno de ellos
*/
void procesarEstadoBotones() {

   if(!flagDebounceBotones) {
   
      botonPresionado = 0;
      
      //lectura de puertos
      if(input(PIN_E1)) {
         botonPresionado = 6;
      }
      else if(input(PIN_E0)) {
         botonPresionado = 5;
      }
      else if(input(PIN_A5)) {
         botonPresionado = 4;
      }
      else if(input(PIN_A4)) {
         botonPresionado = 3;
      }
      else if(input(PIN_A3)) {
         botonPresionado = 2;
      }
      else if(input(PIN_A2)) {
         botonPresionado = 1;
      }
      else {
         ultimoBotonPresionado = 0;
      }
   }
}

/**
* Encargado de procesar la 
* logica relacionada al
* secuenciamiento de luces
*/
void procesarSecuenciador() {

   //apagamos todas las luces
   output_low(PIN_D5);
   output_low(PIN_D4);
   output_low(PIN_D6);
   output_low(PIN_D7);
   output_low(PIN_D3);
   output_low(PIN_D2);

   //barrido de un lado al otro
   if(secuenciaActualId == SECUENCIA_LUCES_1) {
      
      //Usamos el contador de secuenciacion.
      //En el codigo de interrupcion de timer0
      //se define el tiempo de la secuenciacion
      if(flagSecuenciador) {

         //cambiamos el flag para no
         //entrar dos veces en un mismo
         //ciclo
         flagSecuenciador = false;

         //direccion de este secuenciamiento
         //en particular
         haciaDerechaSecuenciador ? secuenciaBotonActual++ : secuenciaBotonActual--;
         
         //no pasarnos de los botones
         if(secuenciaBotonActual > 6) {
            haciaDerechaSecuenciador = false;
            secuenciaBotonActual = 5;
         }
         else if(secuenciaBotonActual < 1) {
            haciaDerechaSecuenciador = true;
            secuenciaBotonActual = 2;
         }
      }
   }
   else if(secuenciaActualId == SECUENCIA_LUCES_2) {
      
      //Usamos el contador de secuenciacion.
      //En el codigo de interrupcion de timer0
      //se define el tiempo de la secuenciacion
      if(flagSecuenciadorSegundo) {

         //cambiamos el flag para no
         //entrar dos veces en un mismo
         //ciclo
         flagSecuenciadorSegundo = false;
         
         //cambiamos el estado actual de las
         //luces al opuesto del estado actual
         estadoLucesSegundoSecuenciador = !estadoLucesSegundoSecuenciador;
      }
   }
   
   
   if(secuenciaActualId == SECUENCIA_LUCES_1) {
      //manejamos el estado 
      //de las luces
      switch(secuenciaBotonActual) {
         case 1:
            logicaEstadoLuces(TRUE,FALSE,FALSE,FALSE,FALSE,FALSE);
            break;
         case 2:
            logicaEstadoLuces(FALSE,TRUE,FALSE,FALSE,FALSE,FALSE);
            break;
         case 3:
            logicaEstadoLuces(FALSE,FALSE,TRUE,FALSE,FALSE,FALSE);
            break;
         case 4:
            logicaEstadoLuces(FALSE,FALSE,FALSE,TRUE,FALSE,FALSE);
            break;
         case 5:
            logicaEstadoLuces(FALSE,FALSE,FALSE,FALSE,TRUE,FALSE);
            break;
         case 6:
            logicaEstadoLuces(FALSE,FALSE,FALSE,FALSE,FALSE,TRUE);
            break;
      }
   }
   else if(secuenciaActualId == SECUENCIA_LUCES_2) {
      
      if(estadoLucesSegundoSecuenciador) {
         logicaEstadoLuces(TRUE,TRUE,TRUE,TRUE,TRUE,TRUE);
      }
      else {
         logicaEstadoLuces(FALSE,FALSE,FALSE,FALSE,FALSE,FALSE);
      }
   }
}

/**
* Logica relacionada al 
* funcionamiento del motor
*/
void procesarMotor() {

   if(motorPrendido) {
      output_high(PIN_B1);
   }
   else {
      output_low(PIN_B1);
   }
}

/**
 * Logica relacionada al switch 
 * de configuracion
 */
void procesarSwitchConfiguracion() {
   
   //pin de lectura
   boolean estadoPin = input(PIN_A0);
   
   if(estadoPin != estadoSwitchConfiguracion) {
      
      if(estadoPin) {
         printf(usb_cdc_putc, "%u %u\n", COMANDO_CAMBIO_SWITCH_CONFIGURACION, 1);
      }
      else {
         printf(usb_cdc_putc, "%u %u\n", COMANDO_CAMBIO_SWITCH_CONFIGURACION, 0);
      }
   }
   
   //cambiamos el estado del pin
   estadoSwitchConfiguracion = estadoPin;
   
}

/**
 * Obtiene el estado del switch
 * de configuracion
 */
byte obtenerEstadoSwitchConfiguracion() {
   
   //pin de lectura
   byte puertaAbierta = (input(PIN_A0) == true ? 1 : 0);
   return puertaAbierta;
}


/**
 * Logica relacionada al switch 
 * de la llave de administrador
 */
void procesarSwitchLlaveAdministrador() {

   //pin de lectura
   boolean estadoPin = input(PIN_A1);
   
   if(estadoPin != estadoSwitchLlaveAdministrador) {
      
      if(!flagDebounceLlaveAdministrador) {
      
         if(estadoPin) {
            printf(usb_cdc_putc, "%u %u\n", COMANDO_CAMBIO_SWITCH_LLAVE_ADMINISTRADOR, 1);
         }
         else {
            printf(usb_cdc_putc, "%u %u\n", COMANDO_CAMBIO_SWITCH_LLAVE_ADMINISTRADOR, 0);
         }
      
         ultimoBotonPresionado = botonPresionado;
         flagDebounceLlaveAdministrador = true;
         contadorDebounceLlaveAdministrador = 0;
      }
   }

   //cambiamos el estado del pin
   estadoSwitchLlaveAdministrador = estadoPin;
   
}

void logicaMonedas() {

   if(monedasAEntregar > 0) {
      
      //seteamos el flag
      motorPrendido = true;
      
      //realizamos un chequeo para
      //ver hace cuantos segundos el
      //motor esta prendido sin que
      //haya pasaje de una moneda
      if(noPasajeMoneda > 5000) {
        
        //contabilizamos en multiplos 
        //de 128 la cantidad de monedas
        //que quedan por entregar todavia
        byte repeticiones = monedasAEntregar / 128;
        byte resto = monedasAEntregar % 128;
        
        printf(usb_cdc_putc, "%u %d %d\n", COMANDO_CAPACIDAD_PAGO_EXCEDIDA, repeticiones, resto);
        
        //apagamos el motor
        noPasajeMoneda = 0;
        monedasAEntregar = 0;
     }
   }
   else {
      motorPrendido = false;
   }
}

void procesarLogicaPagoJugador() {

   //solo se procesa la logica 
   //de pago del jugador si el 
   //modo de expulsion actual
   //es el correcto
   if(modoExpulsionMoneda == MODO_EXPULSION_MONEDA_PAGO_JUGADOR) {
  
      if(input(PIN_B2)) {
      
         //pasa moneda por hopper
         if(!flagHopper) {

            if(!flagDebounceHopper) {

               flagHopper = true;
               
               if(monedasAEntregar > 0) {
   
                  //decrementamos
                  --monedasAEntregar;
   
                  //seteamos este valor en cero 
                  //debido a que paso una moneda 
                  noPasajeMoneda = 0;
                  
                  //obtenemos el estado de 
                  //la puerta al momento de
                  //salir la moneda desde el
                  //hopper
                  byte estado = obtenerEstadoSwitchConfiguracion();
                  
                  //enviamos el comando serialmente
                  printf(usb_cdc_putc,"%u %d %d\n",COMANDO_VERIFICAR_PAGO_MONEDA,MODO_EXPULSION_MONEDA_PAGO_JUGADOR,estado);
    
               }
            
               flagDebounceHopper = true;
               contadorDebounceHopper = 0;
            }
         }
      }
      else {
         flagHopper = false;
      }
      
      logicaMonedas();      
   }
   else if(modoExpulsionMoneda == MODO_EXPULSION_MONEDA_ADMINISTRADOR) {
      
      if(input(PIN_B2)) {
      
         if(!flagDebounceHopper) {

            //pasa moneda por hopper
            if(!flagHopper) {
               
               flagHopper = true;
               
               //obtenemos el estado de 
               //la puerta al momento de
               //salir la moneda desde el
               //hopper
               byte estado = obtenerEstadoSwitchConfiguracion();
               
               //enviamos el comando serialmente
               printf(usb_cdc_putc,"%u %d %d\n",COMANDO_VERIFICAR_PAGO_MONEDA,MODO_EXPULSION_MONEDA_ADMINISTRADOR,estado);
   
            }
            
            flagDebounceHopper = true;
            contadorDebounceHopper = 0;
         }
      }
      else {
         flagHopper = false;
      }
   }
}

void procesarCoinSelector() {

   if(!input(PIN_B0) && !flagCoinSelector) {
      flagCoinSelector = true;
   }
   else if(input(PIN_B0)) {
      flagCoinSelector = false;
      coinEventSentFlag = false;
   }
}

void procesarEnvioComando() {

   if(flagCoinSelector && !coinEventSentFlag) {
   
      if(!flagDebounceCoinSelector) {
      
         //obtenemos el estado de la puerta al momento
         //de salir la moneda desde el hopper
         byte estado = obtenerEstadoSwitchConfiguracion();
         coinEventSentFlag = true;
         
         printf(usb_cdc_putc, "%u %u\n",COMANDO_DINERO_INGRESADO, estado);
         
         flagDebounceCoinSelector = true;
         contadorDebounceCoinSelector = 0;
         
      }
   }
   else if(botonPresionado != 0 && ultimoBotonPresionado != botonPresionado) {
      
      if(!flagDebounceBotones) {
      
         //si el boton se dejo apretado ese boton ya no surte efecto
         //hasta que se suelte y se presione ese o cualquier otro
         //boton nuevamente
         printf(usb_cdc_putc, "%u %u\n", COMANDO_BOTON_PRESIONADO, botonPresionado);

         ultimoBotonPresionado = botonPresionado;
         flagDebounceBotones = true;
         contadorDebounceBotones = 0;
         
      }
   }
}

/**
 * Inicializamos la configuracion
 * basica del procesador, timers,
 * io ports, comparadores, etc
 */
void init_config() {

   //por las dudas para que
   //no se prenda el motor
   //de primera
   output_low(PIN_B1);
   setup_adc_ports(NO_ANALOGS|VSS_VDD);
   setup_adc(ADC_OFF);
   setup_psp(PSP_DISABLED);
   setup_spi(FALSE);
   setup_wdt(WDT_OFF);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);

   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   
   //configuracion de timers
   setup_timer_0(RTCC_INTERNAL|RTCC_8_BIT|RTCC_DIV_256);
   
   //deshabilitamos el modulo 
   //paralelo del controlador
   SPPCON = 0b00000000;
   CCP1CON = 0b0000000;

   //configuracion IO puertos
   set_tris_a(0b11111111);
   set_tris_b(0b00000101);
   set_tris_c(0b00000000);
   set_tris_d(0b00000000);
   set_tris_e(0b11111011);

   disable_interrupts(int_ext);
   disable_interrupts(int_ext1);
   disable_interrupts(int_ext2);
   disable_interrupts(int_rda);
   enable_interrupts(int_rtcc);
   enable_interrupts(global);
   
}

/**
 * Envia el comando que valida o
 * invalida el licenciamiento del
 * sistema
 */
void enviarComandoLicenciamiento(boolean licenciaValida) {

   if(licenciaValida) {
      printf(usb_cdc_putc, "%u %u\n", COMANDO_ESTADO_LICENCIAMIENTO_PIC, 1);
   }
   else {
      printf(usb_cdc_putc, "%u %u\n", COMANDO_ESTADO_LICENCIAMIENTO_PIC, 0);
   }
}


void vaciarEEPROM() {

   write_eeprom(0x00000000, 0xFF); 
   write_eeprom(0x00000001, 0xFF); 
   write_eeprom(0x00000002, 0xFF); 
   write_eeprom(0x00000003, 0xFF); 
   write_eeprom(0x00000004, 0xFF); 
   write_eeprom(0x00000005, 0xFF); 
   write_eeprom(0x00000006, 0xFF); 
   write_eeprom(0x00000007, 0xFF); 

}

void logicaRecepcionToken(byte byteRecibido) {
   
   if(!validacionHabilitada) {
      
      validacionCorrecta = true;
      validacionFinalizada = true;
      
      //configuramos el inicio de la
      //secuencia de luces como muestra
      //de que el licenciamiento fue 
      //correcto
      secuenciaActualId = SECUENCIA_LUCES_1;

      //enviamos un uno en el licenciamiento,
      //lo que indica que el mismo fue valido
      enviarComandoLicenciamiento(true);
     
   }
   else {
   
      //si el primer byte recibido no es
      //la validacion de la licencia, damos
      //un segundo intento de validacion
      if(byteRecibido != COMANDO_VALIDAR_LICENCIA_PIC) {
         
         //debemos de continuar la validacion
         validacionFinalizada = false;
         validacionCorrecta = false;
         
         vaciarEEPROM();
         enviarComandoLicenciamiento(false);
         
      }
      else {

         //si el comando era el correcto
         //comenzamos a recibir cada caracter
         //para luego realizasr la comparacion
         char validacion1 = usb_cdc_getc();
         char validacion2 = usb_cdc_getc();
         char validacion3 = usb_cdc_getc();
         char validacion4 = usb_cdc_getc();
         char validacion5 = usb_cdc_getc();
         char validacion6 = usb_cdc_getc();
         char validacion7 = usb_cdc_getc();
         char validacion8 = usb_cdc_getc();
         char validacion9 = usb_cdc_getc();
         char validacion10 = usb_cdc_getc();
         char validacion11 = usb_cdc_getc();
         char validacion12 = usb_cdc_getc();
         
         if(validacion1 == TOKEN[0] &&
            validacion2 == TOKEN[1] &&
            validacion3 == TOKEN[2] &&
            validacion4 == TOKEN[3] &&
            validacion5 == TOKEN[4] &&
            validacion6 == TOKEN[5] &&
            validacion7 == TOKEN[6] &&
            validacion8 == TOKEN[7] &&
            validacion9 == TOKEN[8] &&
            validacion10 == TOKEN[9] &&
            validacion11 == TOKEN[10] &&
            validacion12 == TOKEN[11]) 
         {
            
            validacionFinalizada = true;
            validacionCorrecta = true;
            
            //configuramos el inicio de la
            //secuencia de luces como muestra
            //de que el licenciamiento fue 
            //correcto
            secuenciaActualId = SECUENCIA_LUCES_1;
      
            //enviamos un uno en el licenciamiento,
            //lo que indica que el mismo fue valido
            enviarComandoLicenciamiento(true);
         }
         else {
      
            //si el resultado del handshake
            //no es satisfactorio debemos de
            //borrar las direcciones de memoria
            //de la EEPROM que identifican
            //a este chip como valido
            vaciarEEPROM();
       
            //enviamos un cero en el licenciamiento, 
            //lo que indica que el mismo fue invalido
            enviarComandoLicenciamiento(false);
       
            validacionFinalizada = true;
            validacionCorrecta = false;

         }
      }
   }
}

/**
 * LA LICENCIA INICIAL ES LA GUARDADA
 * EN LA MEMORIA EEPROM, Y ES UTIL PARA
 * HACER UNA PRIMER REVISION Y VER SI 
 * ESTA TODO BIEN ANTES DE ARRANCAR LA
 * EJECUCION DEL PROGRAMA, INCLUSIVE EL
 * HANDSHAKE.
 */
boolean validarLicenciaInicial() {

   /**
    * Leemos el valor de los bytes guardados
    * en la EEPROM que identifican el PIC
    * con una licencia valida o no
    */
   byte licenciaValidaByte1 = read_eeprom(0x00000000);
   byte licenciaValidaByte2 = read_eeprom(0x00000001);
   byte licenciaValidaByte3 = read_eeprom(0x00000002);
   byte licenciaValidaByte4 = read_eeprom(0x00000003);
   byte licenciaValidaByte5 = read_eeprom(0x00000004);
   byte licenciaValidaByte6 = read_eeprom(0x00000005);
   byte licenciaValidaByte7 = read_eeprom(0x00000006);
   byte licenciaValidaByte8 = read_eeprom(0x00000007);

   //flag que seteamos a continuacion
   //que indica si la licencia es
   //valida o no
   boolean licenciaValida = licenciaValidaByte1 == 114 && licenciaValidaByte2 == 10 && 
                            licenciaValidaByte3 == 54  && licenciaValidaByte4 == 41 && 
                            licenciaValidaByte5 == 67  && licenciaValidaByte6 == 99 && 
                            licenciaValidaByte7 == 64  && licenciaValidaByte8 == 100;
                            
   return licenciaValida;
}

/**
 * Maneja la entrada de datos
 * a traves del puerto USB
 */
void entradaUSBHandler() {

   if (usb_cdc_kbhit()) 
   { 
      //el byte que genero la interrupcion
      byte byteRecibido = usb_cdc_getc();
      
      /**
       * Mientras no se haya terminado la 
       * validacion ejecutamos solo codigo
       * relacionado a eso, nada de logica
       * de programa u otra cosa
       */
      if(!validacionFinalizada) {
         logicaRecepcionToken(byteRecibido);
      }
      else {

         /**
          * SOLO ATENDEMOS INTERRUPCIONES SI 
          * LA VALIDACION DEL PIC FUE CORRECTA.
          * EN OTRO CASO NO HACEMOS NADA
          */
         if(validacionCorrecta) {
         
            if(byteRecibido == COMANDO_VALIDAR_LICENCIA_PIC) {
            
               //si entramos a esto significa que el pic no se
               //reseteo pero el programa java si, lo que hacemos
               //es devolver que la licencia es correcta ya que
               //se valido previamente
               enviarComandoLicenciamiento(true);
               
            }
            else if(byteRecibido == COMANDO_CONFIGURAR_JUEGO_LUCES) {
 
               secuenciaActualId = usb_cdc_getc();
               manejaLuzSecuenciador = true;
            }
            else if(byteRecibido == COMANDO_EFECTUAR_PAGO) {
               
               //obtengo las repeticiones y 
               //el resto para calcular cuantas
               //monedas desean entregarse
               byte repeticiones = usb_cdc_getc();
               byte resto = usb_cdc_getc();
               
               //calculamos las monedas 
               //a entregar
               monedasAEntregar = (repeticiones * 128) + resto; 

            }
            else if(byteRecibido == COMANDO_CAMBIAR_MODO_EXPULSION_MONEDA) {
            
               modoExpulsionMoneda = usb_cdc_getc();
            }
            else if(byteRecibido == COMANDO_OBTENER_ESTADO_MOTOR) {
               
               byte estadoActualMotor = (motorPrendido ? 1 : 0);
               printf(usb_cdc_putc, "%u %u\n", COMANDO_CODIGO_ESTADO_MOTOR, estadoActualMotor);
               
            }
            else if(byteRecibido == COMANDO_CONFIGURAR_ESTADO_BOTONES) {
               
               manejaLuzSecuenciador = false;
               
               estadoLuzBoton1 = usb_cdc_getc() != 0;
               estadoLuzBoton2 = usb_cdc_getc() != 0;
               estadoLuzBoton3 = usb_cdc_getc() != 0;
               estadoLuzBoton4 = usb_cdc_getc() != 0;
               estadoLuzBoton5 = usb_cdc_getc() != 0;
               estadoLuzBoton6 = usb_cdc_getc() != 0;
            }
            else if(byteRecibido == COMANDO_CAMBIAR_ESTADO_MOTOR) {
               
               //cambiamos el estado del motor
               //en base al primer byte recibido
               motorPrendido = usb_cdc_getc() == 1;
            }
         }
      }
   }
}

void manejarPinsLibres() {
   
   output_high(PIN_B3);
   output_high(PIN_B4);
   output_high(PIN_B5);
   
   output_high(PIN_C0);
   output_high(PIN_C1);
   output_high(PIN_C2);
   output_high(PIN_C6);
   output_high(PIN_C7);
   
   output_high(PIN_D0);
   
   output_high(PIN_E2);
   
}

void main() {

   //inicializamos el USB
   usb_cdc_init();
   usb_init();
   
   while (!usb_cdc_connected()) {}

   /**
    * Hacer el primer chequeo de
    * licenciamiento contra la
    * memoria del PIC
    */
   boolean licenciaValida = validarLicenciaInicial();
                            
   if(licenciaValida) {
      
      //inicializamos la
      //configuracion del pic
      init_config();

      while (true)
      {
         
         //necesario para que no muera
         //la conexion USB
         usb_task();
         
         //maneja los pines que
         //quedaron libres para
         //que permanentemente
         //esten como salida alta
         manejarPinsLibres();
         
         if(usb_enumerated()) 
         {
            //maneja la entrada de datos
            //a traves del puerto USB
            entradaUSBHandler();
            
            /**
             * SI LA VALIDACION FINALIZO Y FUE
             * CORRECTA CORREMOS EL FLUJO NORMAL
             * DE LA APLICACION
             */
            if(validacionCorrecta && validacionFinalizada) {
            
               //se procesa el envio
               //de todos los comandos
               //necesarios
               procesarEnvioComando();
               
               //procesamos la logica de pago
               procesarLogicaPagoJugador();
               
               //logica de prendido y
               //apagado de motor
               procesarMotor();
               
               //procesamos la logica del
               //coin selector
               procesarCoinSelector();
               
               //procesamos la logica del
               //switch de la llave de admin
               procesarSwitchLlaveAdministrador();
               
               //procesamos la logica
               //del switch de configuracion
               procesarSwitchConfiguracion();
               
               //se obtiene el estado de los botones, y se envia
               //serialmente el comando indicado si se presiono
               procesarEstadoBotones();
               
               if(manejaLuzSecuenciador) {
               
                  //procesamos la logica necesaria
                  //para el modulo de secuenciamiento
                  procesarSecuenciador();
               }
               else {
                  
                  //las luces son manejadas
                  //independientemente
                  logicaEstadoLuces(estadoLuzBoton1, estadoLuzBoton2, estadoLuzBoton3, estadoLuzBoton4, estadoLuzBoton5, estadoLuzBoton6);
               }
            }
            else {
               logicaEstadoLuces(true,true,true,true,true,true);
            }
         }
      }    
   }
   else {
   
      //***********************************************
      //Si no se valida la licencia correctamente
      //no dejamos caer la conexion USB manteniendo
      //siempre la comunicacion con "usb_task()"
      //***********************************************
      //NOTA: Una vez que sepamos que todo lo 
      //relacionado al USB funciona correctamente
      //podemos quitar el siguiente while. De esta
      //manera vamos a hacer que la conexion usb muera
      //y maree un poco mas al posible intruso
      while (true) {
         //necesario para que no muera
         //la conexion USB
         usb_task();
      }
   }
}
