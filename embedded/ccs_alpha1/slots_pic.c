#include <18F4550.h>

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOPUT                    //No Power Up Timer
#FUSES NOCPD                    //No EE protection
#FUSES STVREN                   //Stack full/underflow will cause reset
#FUSES NODEBUG                  //No Debug mode for ICD
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O

#use delay(clock=4000000)
#use standard_io(c)
#use rs232(baud=9600,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8)

/**
 * Esta instruccion escribe en la memoria 
 * EEPROM del PIC (que empieza en la direccion
 * de memoria (0xf00000) el valor que indica
 * que la licencia del programa es valida.
 * Este valor solo puede ser cambiado cuando
 * se recibe a traves del comando COMANDO_VALIDAR_LICENCIA_PIC
 * una licencia invalida
 */ 
#ROM int8 0xf00000 = {114,10,54,41,67,99,64,100}

#byte OSCCON  = 0xFD3
#byte UCON    = 0xF6D
#byte UCFG    = 0xF6F
#byte RCSTA   = 0xFAB
#byte SPPCON  = 0xF65
#byte SSPCON1 = 0xFC6
#byte CCP1CON = 0xFBD

#byte PORTA = 0xF80
#byte PORTB = 0xF81
#byte PORTC = 0xF82
#byte PORTD = 0xF83
#byte PORTE = 0xF84

#DEFINE COMANDO_NULO                                  -1
#DEFINE COMANDO_EFECTUAR_PAGO                         100
#DEFINE COMANDO_VERIFICAR_PAGO_MONEDA                 101
#DEFINE COMANDO_BOTON_PRESIONADO                      102
#DEFINE COMANDO_DINERO_INGRESADO                      103
#DEFINE COMANDO_CONFIGURAR_ESTADO_BOTONES             104
#DEFINE COMANDO_CONFIGURAR_JUEGO_LUCES                105
#DEFINE COMANDO_VALIDAR_LICENCIA_PIC                  106
#DEFINE COMANDO_ESTADO_LICENCIAMIENTO_PIC             107
#DEFINE COMANDO_CAPACIDAD_PAGO_EXCEDIDA               108
#DEFINE COMANDO_CAMBIO_SWITCH_CONFIGURACION           109
#DEFINE COMANDO_CAMBIO_SWITCH_LLAVE_ADMINISTRADOR     110
#DEFINE COMANDO_CAMBIAR_ESTADO_MOTOR                  111
#DEFINE COMANDO_CAMBIAR_MODO_EXPULSION_MONEDA         112
#DEFINE COMANDO_OBTENER_ESTADO_MOTOR                  113
#DEFINE COMANDO_CODIGO_ESTADO_MOTOR                   114


#DEFINE SECUENCIA_LUCES_1     0
#DEFINE SECUENCIA_LUCES_2     1
#DEFINE SECUENCIA_LUCES_3     2
#DEFINE SECUENCIA_LUCES_4     3

#DEFINE MODO_EXPULSION_MONEDA_PAGO_JUGADOR            0
#DEFINE MODO_EXPULSION_MONEDA_ADMINISTRADOR           1


//flag que indica que se 
//ha finalizado la recepcion
//de informacion correspondiente
//a la validacion
boolean validacionFinalizada = false;

//luego de recibidos todos los bytes
//necesarios para realizar la validacion
//este flag que setea con el valor 
//correspondiente
boolean validacionCorrecta = false;

//que posicion de byte se
//ha recibido y validado
//hasta el momento
byte byteValidacionActual = 0;

byte TOKEN[] = {0xF, 0xE, 0x11, 0x35, 0x45, 0x45, 0x1F, 0xD, 0xD, 0xF, 0x29, 0x29};

//usado para que no se repitan
//varias pasadas de una moneda
//en nuestra logica debido a la
//velocidad de ejecucion del 
//programa
boolean flagHopper = true;

//direccion actual del primer secuenciador
boolean haciaDerechaSecuenciador = false;

//estado de las luces usado 
//por el segundo secuenciador
boolean estadoLucesSegundoSecuenciador = false;

//habla por si solo
byte botonPresionado;
byte ultimoBotonPresionado;

//id del secuenciado 
//de luces actual
byte secuenciaActualId = 0;

//indica el modo en el cual la
//logica del sistema debe operar
//al momento de expulsarse una 
//moneda a traves del hopper
byte modoExpulsionMoneda = MODO_EXPULSION_MONEDA_PAGO_JUGADOR;

//indica cual es el boton
//prendido actualmente
byte secuenciaBotonActual = 0;

//contador de monedas a entregar
int16 monedasAEntregar = 0;

//indica si el motor debe
//de estar prendido o no
boolean motorPrendido = false;

//flags usados por el 
//coin selector
boolean flagCoinSelector = false;
boolean coinEventSentFlag = true;

//usado por el timer rtcc
//para contar hasta el 
//valor definido en la 
//constante TIMER_MAXIMO
int16 contador;
int16 contadorSecuenciador;
int16 contadorSecuenciadorSegundo;
boolean flagSecuenciador = false;
boolean flagSecuenciadorSegundo = false;

//tiempo que el motor se encuentra 
//prendido sin que pase una moneda
int16 noPasajeMoneda = 0;

//definido por la siguiente cuenta:
// MHz / (�4? * RTCC_8_BIT * RTCC_DIV_256)
// 4,000,000 / (4 * 256 * 64) = 61.03
int16 CONTADOR_MAXIMO = 61;

/**
 * El estado de cada una de 
 * las luces de los botones
 */
boolean estadoLuzBoton1 = false;
boolean estadoLuzBoton2 = false;
boolean estadoLuzBoton3 = false;
boolean estadoLuzBoton4 = false;
boolean estadoLuzBoton5 = false;
boolean estadoLuzBoton6 = false;

/**
 * Flag usado para chequear si
 * el estado de las luces de los
 * botones lo configura el 
 * secuenciador o las luces
 * independientes
 */
boolean manejaLuzSecuenciador = true;

//estado del switch de configuracion
boolean estadoSwitchConfiguracion = true;

//estado del switch de la llave de administrador
boolean estadoSwitchLlaveAdministrador = false;

/*
 * TODO CUIDADO CON ESTO!!!!!!
 *
 * Esto solo es con fines de desarrollo
 */
boolean validacionHabilitada = true;


void logicaRecepcionToken(byte byteRecibido) {
   
   if(!validacionHabilitada) {
      validacionCorrecta = true;
      validacionFinalizada = true;
      
      //enviamos un uno en el licenciamiento,
      //lo que indica que el mismo fue valido
      printf("%u %u\n", COMANDO_ESTADO_LICENCIAMIENTO_PIC, 1);
      
      return;
   }
   
   
   //primer byte debe de ser
   //el id de comando
   if(byteValidacionActual == 0) {
   
      //debe de ser el id del
      //comando de verificacion
      if(byteRecibido != COMANDO_VALIDAR_LICENCIA_PIC) {
         
         //terminamos la validacion porque
         //era especificamente lo que 
         //estabamos esperando que viniera
         validacionFinalizada = true;
         validacionCorrecta = false;
      }
   }
   else {
      if(byteRecibido == TOKEN[byteValidacionActual-1]) {
         
         byte TAMANO_TOKEN = sizeof(TOKEN);
         
         if(byteValidacionActual == TAMANO_TOKEN) {
         
            validacionFinalizada = true;
            validacionCorrecta = true;
            
            //configuramos el inicio de la
            //secuencia de luces como muestra
            //de que el licenciamiento fue 
            //correcto
            secuenciaActualId = SECUENCIA_LUCES_1;

            //enviamos un uno en el licenciamiento,
            //lo que indica que el mismo fue valido
            printf("%u %u\n", COMANDO_ESTADO_LICENCIAMIENTO_PIC, 1);
            
         }
      }
      else {
      
         //si el resultado del handshake
         //no es satisfactorio debemos de
         //borrar las direcciones de memoria
         //de la EEPROM que identifican
         //a este chip como valido
         write_eeprom(0x00000000, 0xFF); 
         write_eeprom(0x00000001, 0xFF); 
         write_eeprom(0x00000002, 0xFF); 
         write_eeprom(0x00000003, 0xFF); 
         write_eeprom(0x00000004, 0xFF); 
         write_eeprom(0x00000005, 0xFF); 
         write_eeprom(0x00000006, 0xFF); 
         write_eeprom(0x00000007, 0xFF); 
    
         //enviamos un cero en el licenciamiento, 
         //lo que indica que el mismo fue invalido
         printf("%u %u\n", COMANDO_ESTADO_LICENCIAMIENTO_PIC, 0);
    
         validacionFinalizada = true;
         validacionCorrecta = false;
         return;
      }
   }
   
   byteValidacionActual++;   
}

#int_rda
void serial_isr() {

  if(kbhit()){

      //el byte que genero la interrupcion
      byte byteRecibido = getc();
      
      /**
       * Mientrsa no se haya terminado la 
       * validacion ejecutamos solo codigo
       * relacionado a eso, nada de logica
       * de programa u otra cosa
       */
      if(!validacionFinalizada) {
         logicaRecepcionToken(byteRecibido);
      }
      else {
      
         /**
          * SOLO ATENDEMOS INTERRUPCIONES SI 
          * LA VALIDACION DEL PIC FUE CORRECTA.
          * EN OTRO CASO NO HACEMOS NADA
          */
         if(validacionCorrecta) {
         
            if(byteRecibido == COMANDO_VALIDAR_LICENCIA_PIC) {
            
               //si entramos a esto significa que el pic no se
               //reseteo pero el programa java si, lo que hacemos
               //es devolver que la licencia es correcta ya que
               //se valido previamente
               printf("%u %u\n", COMANDO_ESTADO_LICENCIAMIENTO_PIC, 1);
               
            }
            else if(byteRecibido == COMANDO_CONFIGURAR_JUEGO_LUCES) {
               secuenciaActualId = getc();
               manejaLuzSecuenciador = true;
            }
            else if(byteRecibido == COMANDO_EFECTUAR_PAGO) {
               
               //obtengo las repeticiones y 
               //el resto para calcular cuantas
               //monedas desean entregarse
               byte repeticiones = getc();
               byte resto = getc();
               
               //calculamos las monedas 
               //a entregar
               monedasAEntregar = (repeticiones * 128) + resto; 
      
            }
            else if(byteRecibido == COMANDO_CAMBIAR_MODO_EXPULSION_MONEDA) {
               modoExpulsionMoneda = getc();
            }
            else if(byteRecibido == COMANDO_OBTENER_ESTADO_MOTOR) {
               printf("%u %u\n", COMANDO_CODIGO_ESTADO_MOTOR, (motorPrendido ? 1 : 0));
            }
            else if(byteRecibido == COMANDO_CONFIGURAR_ESTADO_BOTONES) {
               
               manejaLuzSecuenciador = false;
               
               estadoLuzBoton1 = getc() != 0;
               estadoLuzBoton2 = getc() != 0;
               estadoLuzBoton3 = getc() != 0;
               estadoLuzBoton4 = getc() != 0;
               estadoLuzBoton5 = getc() != 0;
               estadoLuzBoton6 = getc() != 0;
               
            }
            else if(byteRecibido == COMANDO_CAMBIAR_ESTADO_MOTOR) {
               
               //cambiamos el estado del motor
               //en base al primer byte recibido
               motorPrendido = getc() == 1;
               
            }
         }
      }
   }
}

#int_rtcc
void rtcc_timer() {

   //cuando llega a TIMER_MAXIMO
   //es que se completo un segundo
   //entero
   if(contador < CONTADOR_MAXIMO) {
      contador++;
   }
   else {
      contador = 0;
   }
   
   //0.016 * 8 = 0.128s
   if(contadorSecuenciador < 8) {
      contadorSecuenciador++;
   }
   else {
      contadorSecuenciador = 0;
      flagSecuenciador = true;

      //si hay monedas a entregar
      if(monedasAEntregar > 0) {
         //incrementamos esta variable asi la
         //logica del motor realiza sus calculos
         //adecuadamente
         noPasajeMoneda = noPasajeMoneda + 128;
      }
   }
   
   //0.016 * 31 = 500 ms.
   if(contadorSecuenciadorSegundo < 31) {
      contadorSecuenciadorSegundo++;
   }
   else {
      contadorSecuenciadorSegundo = 0;
      flagSecuenciadorSegundo = true;
   }
}

/*
* Apaga todas las luces
* de los botones
*/
void logicaEstadoLuces(boolean b1, boolean b2, boolean b3, boolean b4, boolean b5, boolean b6) {
   
   //Luz 1
   b1 ? output_high(PIN_D2) : output_low(PIN_D2);
   //Luz 2
   b2 ? output_high(PIN_D3) : output_low(PIN_D3);
   //Luz 3
   b3 ? output_high(PIN_D4) : output_low(PIN_D4);
   //Luz 4
   b4 ? output_high(PIN_D5) : output_low(PIN_D5);
   //Luz 5
   b5 ? output_high(PIN_D6) : output_low(PIN_D6);
   //Luz 6
   b6 ? output_high(PIN_D7) : output_low(PIN_D7);
   
}

/**
* Procesa el estado de
* los botones y envia
* comando si hubo cambio
* en uno de ellos
*/
void procesarEstadoBotones() {

   botonPresionado = 0;
   
   //lectura de puertos
   if(input(PIN_E1)) {
      botonPresionado = 6;
   }
   else if(input(PIN_E0)) {
      botonPresionado = 5;
   }
   else if(input(PIN_A5)) {
      botonPresionado = 4;
   }
   else if(input(PIN_A4)) {
      botonPresionado = 3;
   }
   else if(input(PIN_A3)) {
      botonPresionado = 2;
   }
   else if(input(PIN_A2)) {
      botonPresionado = 1;
   }
   else {
      ultimoBotonPresionado = 0;
   }
}



/**
* Encargado de procesar la 
* logica relacionada al
* secuenciamiento de luces
*/
void procesarSecuenciador() {

   //apagamos todas las luces
   output_low(PIN_D5);
   output_low(PIN_D4);
   output_low(PIN_D6);
   output_low(PIN_D7);
   output_low(PIN_D3);
   output_low(PIN_D2);

   //barrido de un lado al otro
   if(secuenciaActualId == SECUENCIA_LUCES_1) {
      
      //Usamos el contador de secuenciacion.
      //En el codigo de interrupcion de timer0
      //se define el tiempo de la secuenciacion
      if(flagSecuenciador) {

         //cambiamos el flag para no
         //entrar dos veces en un mismo
         //ciclo
         flagSecuenciador = false;

         //direccion de este secuenciamiento
         //en particular
         haciaDerechaSecuenciador ? secuenciaBotonActual++ : secuenciaBotonActual--;
         
         //no pasarnos de los botones
         if(secuenciaBotonActual > 6) {
            haciaDerechaSecuenciador = false;
            secuenciaBotonActual = 5;
         }
         else if(secuenciaBotonActual < 1) {
            haciaDerechaSecuenciador = true;
            secuenciaBotonActual = 2;
         }
      }
   }
   else if(secuenciaActualId == SECUENCIA_LUCES_2) {
      
      //Usamos el contador de secuenciacion.
      //En el codigo de interrupcion de timer0
      //se define el tiempo de la secuenciacion
      if(flagSecuenciadorSegundo) {

         //cambiamos el flag para no
         //entrar dos veces en un mismo
         //ciclo
         flagSecuenciadorSegundo = false;
         
         //cambiamos el estado actual de las
         //luces al opuesto del estado actual
         estadoLucesSegundoSecuenciador = !estadoLucesSegundoSecuenciador;
      }
   }
   
   
   if(secuenciaActualId == SECUENCIA_LUCES_1) {
      //manejamos el estado 
      //de las luces
      switch(secuenciaBotonActual) {
         case 1:
            logicaEstadoLuces(TRUE,FALSE,FALSE,FALSE,FALSE,FALSE);
            break;
         case 2:
            logicaEstadoLuces(FALSE,TRUE,FALSE,FALSE,FALSE,FALSE);
            break;
         case 3:
            logicaEstadoLuces(FALSE,FALSE,TRUE,FALSE,FALSE,FALSE);
            break;
         case 4:
            logicaEstadoLuces(FALSE,FALSE,FALSE,TRUE,FALSE,FALSE);
            break;
         case 5:
            logicaEstadoLuces(FALSE,FALSE,FALSE,FALSE,TRUE,FALSE);
            break;
         case 6:
            logicaEstadoLuces(FALSE,FALSE,FALSE,FALSE,FALSE,TRUE);
            break;
      }
   }
   else if(secuenciaActualId == SECUENCIA_LUCES_2) {
      
      if(estadoLucesSegundoSecuenciador) {
         logicaEstadoLuces(TRUE,TRUE,TRUE,TRUE,TRUE,TRUE);
      }
      else {
         logicaEstadoLuces(FALSE,FALSE,FALSE,FALSE,FALSE,FALSE);
      }
   }
}

/**
* Logica relacionada al 
* funcionamiento del motor
*/
void procesarMotor() {

   if(motorPrendido) {
      output_high(PIN_B1);
   }
   else {
      output_low(PIN_B1);
   }
}

/**
 * Logica relacionada al switch 
 * de configuracion
 */
void procesarSwitchConfiguracion() {
   
   //pin de lectura
   boolean estadoPin = input(PIN_A0);
   
   if(estadoPin != estadoSwitchConfiguracion) {
      
      if(estadoPin) {
         printf("%u %u\n", COMANDO_CAMBIO_SWITCH_CONFIGURACION, 0);
      }
      else {
         printf("%u %u\n", COMANDO_CAMBIO_SWITCH_CONFIGURACION, 1);
      }
   }
   
   //cambiamos el estado del pin
   estadoSwitchConfiguracion = estadoPin;
   
}

/**
 * Obtiene el estado del switch
 * de configuracion
 */
byte obtenerEstadoSwitchConfiguracion() {
   
   //pin de lectura
   byte puertaAbierta = (input(PIN_A0) == true ? 0 : 1);
   return puertaAbierta;
}


/**
 * Logica relacionada al switch 
 * de la llave de administrador
 */
void procesarSwitchLlaveAdministrador() {
   
   //pin de lectura
   boolean estadoPin = input(PIN_A1);
   
   if(estadoPin != estadoSwitchLlaveAdministrador) {
      
      if(estadoPin) {
         printf("%u %u\n", COMANDO_CAMBIO_SWITCH_LLAVE_ADMINISTRADOR, 1);
      }
      else {
         printf("%u %u\n", COMANDO_CAMBIO_SWITCH_LLAVE_ADMINISTRADOR, 0);
      }
   }
   
   //cambiamos el estado del pin
   estadoSwitchLlaveAdministrador = estadoPin;
   
}

void logicaMonedas() {

   if(monedasAEntregar > 0) {
      
      //seteamos el flag
      motorPrendido = true;
      
      //realizamos un chequeo para
      //ver hace cuantos segundos el
      //motor esta prendido sin que
      //haya pasaje de una moneda
      if(noPasajeMoneda > 5000) {
        
        //contabilizamos en multiplos 
        //de 128 la cantidad de monedas
        //que quedan por entregar todavia
        byte repeticiones = monedasAEntregar / 128;
        byte resto = monedasAEntregar % 128;
        
        //si el tiempo excede los 
        //diez segundos, apagamos el
        //motor y enviamos un comando
        //indicando que no hay mas 
        //monedas disponibles durante
        //un pago
        printf("%u %u %u\n", COMANDO_CAPACIDAD_PAGO_EXCEDIDA, repeticiones, resto);
        
        //apagamos el motor
        noPasajeMoneda = 0;
        monedasAEntregar = 0;
     }
   }
   else {
      motorPrendido = false;
   }
}

void procesarLogicaPagoJugador() {

   //solo se procesa la logica 
   //de pago del jugador si el 
   //modo de expulsion actual
   //es el correcto
   if(modoExpulsionMoneda == MODO_EXPULSION_MONEDA_PAGO_JUGADOR) {
  
      if(input(PIN_B2)) {
      
         //pasa moneda por hopper
         if(!flagHopper) {
            
            flagHopper = true;
            
            if(monedasAEntregar > 0) {

               //decrementamos
               --monedasAEntregar;

               //seteamos este valor en cero 
               //debido a que paso una moneda 
               noPasajeMoneda = 0;
               
               //obtenemos el estado de 
               //la puerta al momento de
               //salir la moneda desde el
               //hopper
               byte estado = obtenerEstadoSwitchConfiguracion();
               
               //enviamos el comando serialmente
               printf("%u %u %u\n",COMANDO_VERIFICAR_PAGO_MONEDA,MODO_EXPULSION_MONEDA_PAGO_JUGADOR,estado);
   
               }
         }
      }
      else {
         flagHopper = false;
      }
      
      logicaMonedas();      
   }
   else if(modoExpulsionMoneda == MODO_EXPULSION_MONEDA_ADMINISTRADOR) {
      
      if(input(PIN_B2)) {
      
         //pasa moneda por hopper
         if(!flagHopper) {
            
            flagHopper = true;
            
            //obtenemos el estado de 
            //la puerta al momento de
            //salir la moneda desde el
            //hopper
            byte estado = obtenerEstadoSwitchConfiguracion();
            
            //enviamos el comando serialmente
            printf("%u %u %u\n",COMANDO_VERIFICAR_PAGO_MONEDA,MODO_EXPULSION_MONEDA_ADMINISTRADOR,estado);
            
         }
      }
      else {
         flagHopper = false;
      }
   }
}


void procesarCoinSelector() {

   if(!input(PIN_B0) && !flagCoinSelector) {
      flagCoinSelector = true;
   }
   else if(input(PIN_B0)) {
      flagCoinSelector = false;
      coinEventSentFlag = false;
   }
}

void procesarEnvioComando() {

   if(flagCoinSelector && !coinEventSentFlag) {
   
      //obtenemos el estado de 
      //la puerta al momento de
      //salir la moneda desde el
      //hopper
      byte estado = obtenerEstadoSwitchConfiguracion();
      
      coinEventSentFlag = true;
      printf("%u %u\n",COMANDO_DINERO_INGRESADO, estado);
   
   }
   else if(botonPresionado != 0 && ultimoBotonPresionado != botonPresionado) { 
      
      //si el boton se dejo apretado ese boton ya no surte efecto
      //hasta que se suelte y se presione ese o cualquier otro
      //boton nuevamente      
      printf("%u %u\n", COMANDO_BOTON_PRESIONADO, botonPresionado);
      ultimoBotonPresionado = botonPresionado;
   }
}

/**
 * Inicializamos la configuracion
 * basica del procesador, timers,
 * io ports, comparadores, etc
 */
void init_config() {

   //por las dudas para que
   //no se prenda el motor
   //de primera
   output_low(PIN_B1);
   setup_adc_ports(NO_ANALOGS|VSS_VDD);
   setup_adc(ADC_OFF);
   setup_psp(PSP_DISABLED);
   setup_spi(FALSE);
   setup_wdt(WDT_OFF);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);

   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   
   //configuracion de timers
   setup_timer_0(RTCC_INTERNAL|RTCC_8_BIT|RTCC_DIV_64);
   
   //TODO chequear si se puede usar osc > 4mhz
   //configuramos el oscilador
   //interno a 4mhz asi el
   //serial anda a 9600
   OSCCON = 0b11100110;
   
   //deshabilitamos el modulo
   //usb asi nos quedan libres
   //los puertos de entrada 
   //RC4 y RC5
   UCON = 0b00010010;
   UCFG = 0b00001000;
   
   //deshabilitamos el modulo 
   //paralelo del controlador
   SPPCON = 0b00000000;
   CCP1CON = 0b0000000;

   //configuracion IO puertos
   set_tris_a(0b11111111);
   set_tris_b(0b00000101);
   set_tris_c(0b00000000);
   set_tris_d(0b00000000);
   set_tris_e(0b11111111);

   disable_interrupts(int_ext);
   disable_interrupts(int_ext1);
   disable_interrupts(int_ext2);
   enable_interrupts(int_rtcc);
   enable_interrupts(int_rda);
   enable_interrupts(global);
   
}

/**
 * LA LICENCIA INICIAL ES LA GUARDADA
 * EN LA MEMORIA EEPROM, Y ES UTIL PARA
 * HACER UNA PRIMER REVISION Y VER SI 
 * ESTA TODO BIEN ANTES DE ARRANCAR LA
 * EJECUCION DEL PROGRAMA, INCLUSIVE EL
 * HANDSHAKE.
 */
boolean validarLicenciaInicial() {

   /**
    * Leemos el valor de los bytes guardados
    * en la EEPROM que identifican el PIC
    * con una licencia valida o no
    */
   byte licenciaValidaByte1 = read_eeprom(0x00000000);
   byte licenciaValidaByte2 = read_eeprom(0x00000001);
   byte licenciaValidaByte3 = read_eeprom(0x00000002);
   byte licenciaValidaByte4 = read_eeprom(0x00000003);
   byte licenciaValidaByte5 = read_eeprom(0x00000004);
   byte licenciaValidaByte6 = read_eeprom(0x00000005);
   byte licenciaValidaByte7 = read_eeprom(0x00000006);
   byte licenciaValidaByte8 = read_eeprom(0x00000007);

   //flag que seteamos a continuacion
   //que indica si la licencia es
   //valida o no
   boolean licenciaValida = licenciaValidaByte1 == 114 && licenciaValidaByte2 == 10 && 
                            licenciaValidaByte3 == 54  && licenciaValidaByte4 == 41 && 
                            licenciaValidaByte5 == 67  && licenciaValidaByte6 == 99 && 
                            licenciaValidaByte7 == 64  && licenciaValidaByte8 == 100;
                            
   return licenciaValida;
}

void main() {

   /**
    * Hacer el primer chequeo de
    * licenciamiento contra la
    * memoria del PIC
    */
   boolean licenciaValida = validarLicenciaInicial();
                            
   if(licenciaValida) {

      //inicializamos la
      //configuracion del pic
      init_config();
 
      //si el handshake fue
      //satisfactorio entonces
      //corremos el programa
      //normalmente
      while(1) {
         
         /**
          * SI LA VALIDACION FINALIZO Y FUE
          * CORRECTA CORREMOS EL FLUJO NORMAL
          * DE LA APLICACION
          */
         if(validacionCorrecta && validacionFinalizada) {
         
            //se procesa el envio
            //de todos los comandos
            //necesarios
            procesarEnvioComando();
            
            //procesamos la logica de pago
            procesarLogicaPagoJugador();
            
            //logica de prendido y
            //apagado de motor
            procesarMotor();
            
            //procesamos la logica del
            //coin selector
            procesarCoinSelector();
            
            //procesamos la logica del
            //switch de la llave de admin
            procesarSwitchLlaveAdministrador();
            
            //procesamos la logica
            //del switch de configuracion
            procesarSwitchConfiguracion();
            
            //se obtiene el estado de los botones, y se envia
            //serialmente el comando indicado si se presiono
            procesarEstadoBotones();
            
            if(manejaLuzSecuenciador) {
            
               //procesamos la logica necesaria
               //para el modulo de secuenciamiento
               procesarSecuenciador();
            }
            else {
               
               //las luces son manejadas
               //independientemente
               logicaEstadoLuces(estadoLuzBoton1, estadoLuzBoton2, estadoLuzBoton3, estadoLuzBoton4, estadoLuzBoton5, estadoLuzBoton6);
            }
         }
         else {
            
            logicaEstadoLuces(true,true,true,true,true,true);
         }
      }
   }
}
