#include <18F4550.h>

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOPUT                    //No Power Up Timer
#FUSES NOCPD                    //No EE protection
#FUSES STVREN                   //Stack full/underflow will cause reset
#FUSES NODEBUG                  //No Debug mode for ICD
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O

#use delay(clock=4000000)
#use rs232(baud=9600,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8)
#use rtos(timer=0, minor_cycle=500us) //temporizador Timer0, tiempo m�nimo de ejecuci�n de cada tarea 1ms

//Definici�n de las prototipos de funci�n de las tareas
#task (rate=100ms, max=500us) //Ejecutar cada 150ms y consumir como m�ximo 1ms
void procesarSecuenciador();
#task (rate=100ms, max=500us) //Ejecutar cada 10ms y consumir como m�ximo 1ms
void enviarData();
//#task (rate=500us, max=500us) //Ejecutar cada 10ms y consumir como m�ximo 1ms
//void recibirData();
#task (rate=500us, max=500us) //Ejecutar cada 10ms y consumir como m�ximo 1ms
void update();

#byte OSCCON  = 0xFD3
#byte UCON    = 0xF6D
#byte UCFG    = 0xF6F
#byte RCSTA   = 0xFAB
#byte SPPCON  = 0xF65
#byte SSPCON1 = 0xFC6
#byte CCP1CON = 0xFBD

#byte PORTA = 0xF80
#byte PORTB = 0xF81
#byte PORTC = 0xF82
#byte PORTD = 0xF83
#byte PORTE = 0xF84

#DEFINE COMANDO_NULO                        -1
#DEFINE COMANDO_EFECTUAR_PAGO              100
#DEFINE COMANDO_VERIFICAR_PAGO_MONEDA      101
#DEFINE COMANDO_BOTON_PRESIONADO           102
#DEFINE COMANDO_DINERO_INGRESADO           103
#DEFINE COMANDO_CONFIGURAR_ESTADO_BOTONES  104
#DEFINE COMANDO_CONFIGURAR_JUEGO_LUCES     105

#DEFINE SECUENCIA_LUCES_1     100
#DEFINE SECUENCIA_LUCES_2     101
#DEFINE SECUENCIA_LUCES_3     102
#DEFINE SECUENCIA_LUCES_4     103


//direccion actual del secuenciador
boolean haciaDerechaSecuenciador = false;

//habla por si solo
int16 botonPresionado;
byte ultimoBotonPresionado;

//id del secuenciado 
//de luces actual
byte secuenciaActualId = 0;

//indica cual es el boton
//prendido actualmente
byte secuenciaBotonActual = 0;

//flags usados durante
//el polling de puertos
byte cambioEstadoCoinSelector;
byte cambioEstadoHopperCounter;

//contador de monedas a entregar
int16 monedasAEntregar = 0;

//indica si el motor debe
//de estar prendido o no
boolean motorPrendido;

/**
 * Inicializamos la configuracion
 * basica del procesador, timers,
 * io ports, comparadores, etc
 */
void init_config() {

   //configuramos el inicio de 
   //la secuenciacion
   secuenciaActualId = SECUENCIA_LUCES_1;

   //por las dudas para que
   //no se prenda el motor
   //de primera
   motorPrendido = false;
   
   setup_adc_ports(NO_ANALOGS|VSS_VDD);
   setup_adc(ADC_OFF);
   setup_psp(PSP_DISABLED);
   setup_spi(FALSE);
   setup_wdt(WDT_OFF);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);
   disable_interrupts(int_ext);
   disable_interrupts(int_ext1);
   disable_interrupts(int_ext2);   
   disable_interrupts(global);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   
   //configuracion de timers
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);

   //TODO chequear si se puede usar osc > 4mhz
   //configuramos el oscilador
   //interno a 4mhz asi el
   //serial anda a 9600
   OSCCON = 0b11100110;
   
   //deshabilitamos el modulo
   //usb asi nos quedan libres
   //los puertos de entrada 
   //RC4 y RC5
   UCON = 0b00010010;
   UCFG = 0b00001000;
   
   //deshabilitamos el modulo 
   //paralelo del controlador
   SPPCON = 0b00000000;
   CCP1CON = 0b0000000;
   
   //configuracion IO puertos
   set_tris_a(0b11111111);
   set_tris_b(0b00000101);
   set_tris_c(0b00000000);
   set_tris_d(0b00000000);
   set_tris_e(0b11111111);
   
   //habilitamos la tarea
   rtos_enable(enviarData);

   //habilitamos la tarea
   //rtos_enable(recibirData);

   //habilitamos la tarea
   rtos_enable(update);

   //habilitamos la tarea
   rtos_enable(procesarSecuenciador);

   //A partir de aqu� comenzar� la ejecuci�n de las tareas
   rtos_run(); 

}

/*
* Apaga todas las luces
* de los botones
*/
void configurarEstadoLuces(boolean b1, boolean b2, boolean b3, boolean b4, boolean b5, boolean b6) {
   b1 ? output_high(PIN_D5) : output_low(PIN_D5);
   b2 ? output_high(PIN_D4) : output_low(PIN_D4);
   b3 ? output_high(PIN_C7) : output_low(PIN_C7);
   b4 ? output_high(PIN_C6) : output_low(PIN_C6);
   b5 ? output_high(PIN_D3) : output_low(PIN_D3);
   b6 ? output_high(PIN_D2) : output_low(PIN_D2);
}

/**
* Procesa el estado de
* los botones y envia
* comando si hubo cambio
* en uno de ellos
*/
void procesarEstadoBotones() {

   botonPresionado = 0;
   
   //lectura de puertos
   if(input(PIN_E1)) {
      botonPresionado = 1;
   }
   else if(input(PIN_E0)) {
      botonPresionado = 2;
   }
   else if(input(PIN_A5)) {
      botonPresionado = 3;
   }
   else if(input(PIN_A4)) {
      botonPresionado = 4;
   }
   else if(input(PIN_A3)) {
      botonPresionado = 5;
   }
   else if(input(PIN_A2)) {
      botonPresionado = 6;
   }
   else {
      ultimoBotonPresionado = 0;
   }
}

/**
* Encargado de procesar la 
* logica relacionada al
* secuenciamiento de luces
*/
void procesarSecuenciador() {

   //apagamos todas las luces
   output_low(PIN_D5);
   output_low(PIN_D4);
   output_low(PIN_C7);
   output_low(PIN_C6);
   output_low(PIN_D3);
   output_low(PIN_D2);

   //barrido de un lado al otro
   if(secuenciaActualId == SECUENCIA_LUCES_1) {
      
      haciaDerechaSecuenciador ? secuenciaBotonActual++ : secuenciaBotonActual--;
      
      //no pasarnos de los botones
      if(secuenciaBotonActual > 6) {
         haciaDerechaSecuenciador = false;
         secuenciaBotonActual = 5;
      }
      else if(secuenciaBotonActual < 1) {
         haciaDerechaSecuenciador = true;
         secuenciaBotonActual = 2;
      }
   }
   
   //manejamos el estado 
   //de las luces
   switch(secuenciaBotonActual) {
      case 1:
         configurarEstadoLuces(TRUE,FALSE,FALSE,FALSE,FALSE,FALSE);
         break;
      case 2:
         configurarEstadoLuces(FALSE,TRUE,FALSE,FALSE,FALSE,FALSE);
         break;
      case 3:
         configurarEstadoLuces(FALSE,FALSE,TRUE,FALSE,FALSE,FALSE);
         break;
      case 4:
         configurarEstadoLuces(FALSE,FALSE,FALSE,TRUE,FALSE,FALSE);
         break;
      case 5:
         configurarEstadoLuces(FALSE,FALSE,FALSE,FALSE,TRUE,FALSE);
         break;
      case 6:
         configurarEstadoLuces(FALSE,FALSE,FALSE,FALSE,FALSE,TRUE);
         break;
   }
}

/**
* Logica relacionada al 
* funcionamiento del motor
*/
void procesarMotor() {
   motorPrendido ? output_high(PIN_B1) : output_low(PIN_B1);
}

/**
* Ejecuta tareas para poder
* realizar el pago 
*/
void procesarLogicaPago() {
   
   if(monedasAEntregar > 0) {
      motorPrendido = true;
   }
   else {
      motorPrendido = false;
   }
   
   if(!input_state(PIN_B2)) {
   
      if(cambioEstadoHopperCounter) {
         cambioEstadoHopperCounter = 0;
         monedasAEntregar--;
         printf("%d\n",COMANDO_VERIFICAR_PAGO_MONEDA);
      }
   }
   else {
      cambioEstadoHopperCounter = 1;
   }
   
}

/**
* Procesa la entrada de monedas
* a traves del coin selector y
* ejecuta la logica correspondiente
*/
void procesarCoinSelector() {

   if(!input_state(PIN_B0)) {
   
      if(cambioEstadoCoinSelector) {
         cambioEstadoCoinSelector = 0;
         printf("%d\n",COMANDO_DINERO_INGRESADO);
      }
   }
   else {
      cambioEstadoCoinSelector = 1;
   }
}

/**
* Procesa comandos de entrada
* desde el puerto serial
*/
/*
void recibirData() {
   
   //obtenemos el primer byte
   //que es el indicador de
   //comando
   if(kbhit()) {
      byte comandoId = getc();
      printf("VALOR %u\r\n", comandoId);
   }
}
*/

void enviarData() {
   
   /*
    * SECCION BOTONES
    */
   //si el boton se dejo apretado
   //ese boton ya no surte efecto
   //hasta que se suelte y se 
   //presione ese o cualquier otro
   //boton nuevamente
   if(botonPresionado != 0 && ultimoBotonPresionado != botonPresionado) { 

      printf("%u %lu\n", COMANDO_BOTON_PRESIONADO, botonPresionado);
      ultimoBotonPresionado = botonPresionado;
   }
}

void update() {
 
   //procesamos la logica 
   //de recepcion de dinero
   procesarCoinSelector();
   
   //procesa la logica 
   //relacionada al pago
   procesarLogicaPago();
   
   //logica de prendido y 
   //apagado de motor
   procesarMotor();
   
   //se obtiene el estado de los botones, y se envia
   //serialmente el comando indicado si se presiono
   procesarEstadoBotones();
   
   if(kbhit()) {
      char b = getc();
      monedasAEntregar = 1;
      printf("PEPE %u\r\n", b);
   }
   
}

void main() {
   
   //inicializamos la
   //configuracion del pic
   init_config();

}
