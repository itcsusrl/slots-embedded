#include <16F628A.h>

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES INTRC_IO                 //Internal RC Osc, no CLKOUT
#FUSES NOPUT                    //No Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOMCLR                   //Master Clear pin used for I/O
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES RESERVED                 //Used to set the reserved FUSE bits

#use delay(clock=4000000)

//constante que indica la cantidad
//de monedas que deben de pasar por
//el coin selector para habilitar el
//relay un tiempo determinado
const unsigned int32 MONEDAS_NECESARIAS = 3;

//constante que define la cantidad
//de segundos que va a estar prendido
//el relay luego del ingreso de dinero
const unsigned int32 SEGUNDOS_RELAY_PRENDIDO = 2100;

//usada para contabilizar cuantos 
//segundos quedan para apagar el relay
unsigned int32 segundosRestantes = 0;

//indica si el debounce del
//coin selector se encuentra
//activado debido al pasaje de
//una moneda
boolean flagDebounceCoinSelector = false;

//contador de segundos
unsigned int32 segundos = 0;

//contador de monedas
unsigned int16 monedas = 0;

//contador de interrupciones
unsigned int16 interrupciones = 0;

//contador de interrupciones 
//usado por el coin selector
unsigned int16 interrupcionesCoinSelector = 0;

#INT_RTCC
void rtcc()
{
   //solo incrementamos 
   //la variable que cuenta
   //la cantidad de interrupciones
   //que fueron ejecutadas
   interrupciones++;
   
   //si el debounce del
   //coin selector esta
   //habilitado debido a
   //que paso una moneda
   if(flagDebounceCoinSelector) {
      //incrementamos la interrupcion
      interrupcionesCoinSelector++;
   }
}

//rutina encargada de realizar
//los chequeos necesarios de
//lectura del coin selector
void procesarCoinSelector() {

   //la pata B5 que esta conectada
   //al coin selector esta en alta
   //y no existe un debounce de una
   //moneda anterior, entonces seteamos
   //el flag como que 
   //EFECTIVAMENTE PASO UNA MONEDA
   //*************** IMPORTANTE ***************
   //EL COIN SELECTOR DEBE DE ESTAR CONFIGURADO
   //COMO N.O. (NORMAL OPEN)
   if(input(PIN_B5) && !flagDebounceCoinSelector) {
   
      //seteamos el flag que indica que
      //entramos en el periodo de tiempo
      //del debounce, de esta manera no
      //entramos nuevamente a este if a
      //pesar de que la pata todavia se
      //encuentre en alta
      flagDebounceCoinSelector = true;
      
      //reseteamos la variable que guarda
      //la cantidad de interrupciones que
      //se van ejecutando a causa del 
      //modulo RTCC
      interrupcionesCoinSelector = 0;

      //incrementamos en uno
      //la cantidad de monedas
      monedas++;

   }
   
   //1/4 segundo aprox. de debounce 
   //despues de que pasa una moneda
   if(flagDebounceCoinSelector && interrupcionesCoinSelector >= 1000) {
      flagDebounceCoinSelector = false;
      interrupcionesCoinSelector = 0;
   }
}

//en base al tiempo restante
//que le queda disponible al
//usuario maneja el estado
//del relay
void procesarEstadoRelay() {

   //si todavia tiene tiempo
   //mantenemos prendida la
   //luz testigo y el relay
   if(segundosRestantes > 0) {
      output_high(PIN_A1);
      output_high(PIN_B0);
   }
   else {
      output_low(PIN_A1);
      output_low(PIN_B0);
   }
}

//maneja los creditos del usuario
//asignando el tiempo configurado
//al usuario una vez que llega a
//los creditos necesarios
void manejarCreditos() {
   
   if(monedas == MONEDAS_NECESARIAS) {

      //reseteamos la cantidad
      //de monedas
      monedas = 0;

      //asigno los segundos configurados
      segundosRestantes += SEGUNDOS_RELAY_PRENDIDO;
      
   }
}

//cualquier cosa que se precise
//contar fraccionando de a un
//segundo puede usar este metodo
//para hacerlo
void contadorSegundos() {

   //si se entro a la interrupcion
   //la cantidad de veces indicada
   //a continuacion es porque ya ha
   //pasado un segundo
   if(interrupciones >= 3960) {
   
      //incrementamos un segundo
      segundos++;
      
      //logica que cuenta hacia atras
      //los segundos restantes que el
      //relay debe de mantenerse activado
      if(segundosRestantes > 0) {
        segundosRestantes--;
      }
      
      //reajustamos por las dudas que
      //haya entrado al if por el > y
      //no por el =
      interrupciones = interrupciones - 3960;
   }
}

void main()
{
   //inicializamos todas 
   //las varibles importantes
   monedas = 0;
   interrupciones = 0;
   interrupcionesCoinSelector = 0;
   segundosRestantes = 0;
   flagDebounceCoinSelector = false;
   
   //configuracion
   set_tris_a(0b00000000);
   set_tris_b(0b00100000);
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   enable_interrupts(INT_TIMER0);
   enable_interrupts(GLOBAL);
   delay_ms(500);
   
   while(true) {

      //se encarga de ejecutar la logica
      //para poder contabilizar el tiempo
      //fraccionandolo de a un segundo
      contadorSegundos();
      
      //evaluamos entrada de moneda
      procesarCoinSelector();
      
      //maneja la entrada 
      //de creditos y asigna
      //el tiempo al usuario
      manejarCreditos();
      
      //maneja el estado del 
      //relay en base al tiempo
      //disponible del usuario
      procesarEstadoRelay();
      
   }
}
