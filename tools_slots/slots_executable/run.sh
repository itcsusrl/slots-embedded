SCRIPT_PATH=$(cd ${0%/*} && echo $PWD/${0##*/})
SLOT_PATH=`dirname "$SCRIPT_PATH"`"/"
# to get the path only - not the script name - add
NATIVE_PATH=`dirname "$SCRIPT_PATH"`"/native/"

cd $SLOT_PATH

java -Djava.library.path=native/ -jar slots.jar